#version 120

uniform vec4 u_ambientColor;
uniform float u_ambientIntensity;

varying vec4 v_color;
varying vec3 v_normals;

void main()
{
	gl_FragColor = v_color * u_ambientColor * u_ambientIntensity;
}