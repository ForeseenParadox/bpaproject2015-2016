#version 120

uniform vec2 u_position;
uniform vec3 u_color;
uniform vec3 u_attenuation;
uniform float u_intensity;

varying vec2 v_position;
varying vec2 v_texCoord;

void main()
{
	
	float distanceFromLight = length(u_position - gl_FragCoord.xy);
	float attenuation = 1.0 / (u_attenuation.x * distanceFromLight * distanceFromLight + u_attenuation.y * distanceFromLight + u_attenuation.z);
	
	gl_FragColor = vec4(u_color * attenuation * u_intensity, 1.0);
}