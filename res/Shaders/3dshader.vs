#version 120

uniform mat4 u_model;
uniform mat4 u_view;
uniform mat4 u_projection;

attribute vec3 a_position;
attribute vec4 a_color;
attribute vec3 a_normal;

varying vec3 v_position;
varying vec4 v_color;
varying vec3 v_normal;

void main()
{
	gl_Position = u_projection * u_view * (u_model * vec4(a_position, 1.0));
	v_position = (u_view * (u_model * vec4(a_position, 1.0))).xyz;
	v_color = a_color;
	v_normal = (u_model * vec4(a_normal, 0.0)).xyz;
}