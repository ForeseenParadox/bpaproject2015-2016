#version 120
uniform mat4 u_view;
uniform mat4 u_projection;
				
attribute vec3 a_position;
attribute vec4 a_color;
attribute vec2 a_texCoord;

varying vec4 v_color;
varying vec2 v_texCoord;

void main()
{
	gl_Position = u_projection*u_view*vec4(a_position, 1.0);
	v_color = a_color;
	v_texCoord = a_texCoord;
}