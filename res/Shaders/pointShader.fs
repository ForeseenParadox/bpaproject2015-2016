#version 110

uniform vec3 u_lightPosition;
uniform vec4 u_lightColor;
uniform float u_lightIntensity;
uniform vec3 u_eyePosition;

varying vec3 v_position;
varying vec4 v_color;
varying vec3 v_normal;

void main()
{
	
	vec4 result = vec4(0.0, 0.0, 0.0, 0.0);
	
	vec3 l = normalize(u_lightPosition - v_position);
	vec3 n = normalize(v_normal);
	float diffuse = max(dot(n, l), 0.0);	
	
	if(diffuse > 0)
	{
		float distance = distance(v_position, u_lightPosition);
		float attenuation = 1.0 / (distance * distance);
		result += attenuation * u_lightColor * u_lightIntensity * diffuse;
			
		vec3 vertexToEye = normalize(u_eyePosition - v_position);
		vec3 lightReflection = -normalize(reflect(l, n));
		float specular = dot(vertexToEye, lightReflection );
		if(specular > 0)
		{
			float specular = pow(specular, 32);
			result += 0.2 * specular * u_lightColor;
		}
	}
	
	
	gl_FragColor = result;
}