package engine.animation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import engine.graphics.texture.TextureRegion;

/**
 * The Class TextureAnimation.
 */
public class TextureAnimation
{

	/** The loaded animations. */
	private static Map<String, TextureAnimation> loadedAnimations;

	/** The frames. */
	private List<TextureRegion> frames;
	
	/** The frame delay. */
	private long frameDelay;
	
	/** The last update. */
	private long lastUpdate;
	
	/** The index. */
	private int index;
	
	/** The lock frame index. */
	private int lockFrameIndex;

	static
	{
		loadedAnimations = new HashMap<String, TextureAnimation>();
	}

	/**
	 * Instantiates a new texture animation.
	 *
	 * @param frameDelay the frame delay
	 */
	public TextureAnimation(long frameDelay)
	{
		this(null, frameDelay);
	}

	/**
	 * Instantiates a new texture animation.
	 *
	 * @param id the id
	 * @param frameDelay the frame delay
	 */
	public TextureAnimation(String id, long frameDelay)
	{
		frames = new ArrayList<TextureRegion>();
		this.frameDelay = frameDelay;

		if (id != null)
			loadedAnimations.put(id, this);

		lockFrameIndex = -1;
	}

	/**
	 * Instantiates a new texture animation.
	 *
	 * @param texture the texture
	 */
	public TextureAnimation(TextureRegion texture)
	{
		frames = new ArrayList<TextureRegion>();
		this.frameDelay = 0;

		frames.add(texture);

		lockFrameIndex = -1;
	}

	/**
	 * Gets the texture animation.
	 *
	 * @param name the name
	 * @return the texture animation
	 */
	public static TextureAnimation getTextureAnimation(String name)
	{
		return loadedAnimations.get(name);
	}

	/**
	 * Checks if is texture animation loaded.
	 *
	 * @param name the name
	 * @return true, if is texture animation loaded
	 */
	public static boolean isTextureAnimationLoaded(String name)
	{
		return loadedAnimations.containsKey(name);
	}

	/**
	 * Gets the frames.
	 *
	 * @return the frames
	 */
	public List<TextureRegion> getFrames()
	{
		return frames;
	}

	/**
	 * Gets the frame delay.
	 *
	 * @return the frame delay
	 */
	public long getFrameDelay()
	{
		return frameDelay;
	}

	/**
	 * Gets the last update.
	 *
	 * @return the last update
	 */
	public long getLastUpdate()
	{
		return lastUpdate;
	}

	/**
	 * Gets the frame index.
	 *
	 * @return the frame index
	 */
	public int getFrameIndex()
	{
		return index;
	}

	/**
	 * Gets the current frame.
	 *
	 * @return the current frame
	 */
	public TextureRegion getCurrentFrame()
	{
		return frames.get(index);
	}

	/**
	 * Gets the lock frame index.
	 *
	 * @return the lock frame index
	 */
	public int getLockFrameIndex()
	{
		return lockFrameIndex;
	}

	/**
	 * Sets the frame delay.
	 *
	 * @param frameDelay the new frame delay
	 */
	public void setFrameDelay(long frameDelay)
	{
		this.frameDelay = frameDelay;
	}

	/**
	 * Sets the lock frame index.
	 *
	 * @param lockFrameIndex the new lock frame index
	 */
	public void setLockFrameIndex(int lockFrameIndex)
	{
		this.lockFrameIndex = lockFrameIndex;
	}

	/**
	 * Adds the frame.
	 *
	 * @param region the region
	 */
	public void addFrame(TextureRegion region)
	{
		frames.add(region);
	}

	/**
	 * Removes the frame.
	 *
	 * @param region the region
	 */
	public void removeFrame(TextureRegion region)
	{
		frames.remove(region);
	}

	/**
	 * Update.
	 */
	public void update()
	{
		if (frameDelay > 0)
		{
			if (lockFrameIndex > -1)
			{
				index = lockFrameIndex;
			} else
			{
				if (System.currentTimeMillis() - lastUpdate >= frameDelay)
				{
					index = (index + 1) % frames.size();
					lastUpdate = System.currentTimeMillis();
				}
			}
		}
	}

}