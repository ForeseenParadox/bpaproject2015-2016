package engine.graphics;

import engine.util.math.Matrix4f;
import engine.util.math.Vector2f;

/**
 * The Class OrthographicCamera.
 */
public class OrthographicCamera
{

	/** The translation. */
	private Matrix4f translation;
	
	/** The rotation. */
	private Matrix4f rotation;
	
	/** The scale. */
	private Matrix4f scale;
	
	/** The projection. */
	private Matrix4f projection;

	/**
	 * Instantiates a new orthographic camera.
	 *
	 * @param viewWidth the view width
	 * @param viewHeight the view height
	 */
	public OrthographicCamera(float viewWidth, float viewHeight)
	{

		translation = new Matrix4f();
		translation.setIdentity();

		rotation = new Matrix4f();
		rotation.setIdentity();

		scale = new Matrix4f();
		scale.setIdentity();

		projection = new Matrix4f();
		projection.setOrthogonalProjection(0, viewWidth, 0, viewHeight, -1, 1);
	}

	/**
	 * Gets the translation.
	 *
	 * @return the translation
	 */
	public Matrix4f getTranslation()
	{
		return translation;
	}

	/**
	 * Sets the translation.
	 *
	 * @param t the new translation
	 */
	public void setTranslation(Vector2f t)
	{
		translation.setToTranslation(t.getX(), t.getY(), 0);
	}

	/**
	 * Sets the translation.
	 *
	 * @param x the x
	 * @param y the y
	 */
	public void setTranslation(float x, float y)
	{
		translation.setToTranslation(x, y, 0);
	}

	/**
	 * Sets the rotation.
	 *
	 * @param theta the new rotation
	 */
	public void setRotation(float theta)
	{
		rotation.setToRotationZ(theta);
	}

	/**
	 * Sets the scale.
	 *
	 * @param s the new scale
	 */
	public void setScale(Vector2f s)
	{
		scale.setToScale(s.getX(), s.getY(), 1);
	}

	/**
	 * Sets the scale.
	 *
	 * @param sx the sx
	 * @param sy the sy
	 */
	public void setScale(float sx, float sy)
	{
		scale.setToScale(sx, sy, 1);
	}

	/**
	 * Gets the rotation.
	 *
	 * @return the rotation
	 */
	public Matrix4f getRotation()
	{
		return rotation;
	}

	/**
	 * Gets the scale.
	 *
	 * @return the scale
	 */
	public Matrix4f getScale()
	{
		return scale;
	}

	/**
	 * Gets the view.
	 *
	 * @return the view
	 */
	public Matrix4f getView()
	{
		return translation.multiply(rotation).multiply(scale);
	}

	/**
	 * Gets the view projection.
	 *
	 * @return the view projection
	 */
	public Matrix4f getViewProjection()
	{
		return getView().multiply(projection);
	}

	/**
	 * Gets the projection.
	 *
	 * @return the projection
	 */
	public Matrix4f getProjection()
	{
		return projection;
	}

	/**
	 * Sets the translation.
	 *
	 * @param translation the new translation
	 */
	public void setTranslation(Matrix4f translation)
	{
		this.translation = translation;
	}

	/**
	 * Sets the rotation.
	 *
	 * @param rotation the new rotation
	 */
	public void setRotation(Matrix4f rotation)
	{
		this.rotation = rotation;
	}

	/**
	 * Sets the scale.
	 *
	 * @param scale the new scale
	 */
	public void setScale(Matrix4f scale)
	{
		this.scale = scale;
	}

	/**
	 * Sets the projection.
	 *
	 * @param projection the new projection
	 */
	public void setProjection(Matrix4f projection)
	{
		this.projection = projection;
	}

}