package engine.graphics.light;

import engine.graphics.Color;
import engine.physics.QuadraticAttenuation;
import engine.util.math.Vector2f;

/**
 * The Class SpotLight.
 */
public class SpotLight extends PointLight
{

	/** The angle. */
	public float angle;

	/**
	 * Instantiates a new spot light.
	 *
	 * @param col the col
	 * @param inten the inten
	 * @param att the att
	 * @param pos the pos
	 * @param radius the radius
	 */
	public SpotLight(Color col, float inten, QuadraticAttenuation att, Vector2f pos, float radius)
	{
		super(col, inten, att, pos, radius);
	}

	/**
	 * Gets the angle.
	 *
	 * @return the angle
	 */
	public float getAngle()
	{
		return angle;
	}

	/**
	 * Sets the angle.
	 *
	 * @param ang the new angle
	 */
	public void setAngle(float ang)
	{
		angle = ang;
	}

}
