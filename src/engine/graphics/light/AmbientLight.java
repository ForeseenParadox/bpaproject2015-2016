package engine.graphics.light;

import engine.graphics.Color;

/**
 * The Class AmbientLight.
 */
public class AmbientLight extends Light
{

	/**
	 * Instantiates a new ambient light.
	 *
	 * @param col the col
	 * @param inten the inten
	 */
	public AmbientLight(Color col, float inten)
	{
		super(col, inten);
	}

}
