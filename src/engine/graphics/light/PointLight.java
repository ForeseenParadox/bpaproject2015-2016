package engine.graphics.light;

import engine.graphics.Color;
import engine.physics.QuadraticAttenuation;
import engine.util.math.Vector2f;

/**
 * The Class PointLight.
 */
public class PointLight extends Light
{

	/** The atten. */
	private QuadraticAttenuation atten;
	
	/** The position. */
	private Vector2f position;
	
	/** The radius. */
	private float radius;

	/**
	 * Instantiates a new point light.
	 *
	 * @param col the col
	 * @param inten the inten
	 * @param att the att
	 * @param pos the pos
	 * @param radius the radius
	 */
	public PointLight(Color col, float inten, QuadraticAttenuation att, Vector2f pos, float radius)
	{
		super(col, inten);
		atten = att;
		position = pos;
		this.radius = radius;
	}

	/**
	 * Gets the attenuation.
	 *
	 * @return the attenuation
	 */
	public QuadraticAttenuation getAttenuation()
	{
		return atten;
	}

	/**
	 * Gets the position.
	 *
	 * @return the position
	 */
	public Vector2f getPosition()
	{
		return position;
	}

	/**
	 * Gets the radius.
	 *
	 * @return the radius
	 */
	public float getRadius()
	{
		return radius;
	}

	/**
	 * Sets the attenuation.
	 *
	 * @param att the new attenuation
	 */
	public void setAttenuation(QuadraticAttenuation att)
	{
		atten = att;
	}

	/**
	 * Sets the attenuation.
	 *
	 * @param a the a
	 * @param b the b
	 * @param c the c
	 */
	public void setAttenuation(float a, float b, float c)
	{
		this.atten = new QuadraticAttenuation(a, b, c);
	}

	/**
	 * Sets the position.
	 *
	 * @param pos the new position
	 */
	public void setPosition(Vector2f pos)
	{
		position = pos;
	}

	/**
	 * Sets the position.
	 *
	 * @param x the x
	 * @param y the y
	 */
	public void setPosition(float x, float y)
	{
		this.position = new Vector2f(x, y);
	}

	/**
	 * Sets the radius.
	 *
	 * @param radius the new radius
	 */
	public void setRadius(float radius)
	{
		this.radius = radius;
	}

}
