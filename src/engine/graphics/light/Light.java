package engine.graphics.light;

import engine.graphics.Color;

/**
 * The Class Light.
 */
public abstract class Light
{

	/** The color. */
	private Color color;
	
	/** The intensity. */
	private float intensity;

	/**
	 * Instantiates a new light.
	 *
	 * @param col the col
	 * @param inten the inten
	 */
	public Light(Color col, float inten)
	{
		color = col;
		intensity = inten;
	}

	/**
	 * Gets the color.
	 *
	 * @return the color
	 */
	public Color getColor()
	{
		return color;
	}

	/**
	 * Gets the intensity.
	 *
	 * @return the intensity
	 */
	public float getIntensity()
	{
		return intensity;
	}

	/**
	 * Sets the color.
	 *
	 * @param c the new color
	 */
	public void setColor(Color c)
	{
		color = c;
	}

	/**
	 * Sets the intensity.
	 *
	 * @param i the new intensity
	 */
	public void setIntensity(float i)
	{
		intensity = i;
	}
}
