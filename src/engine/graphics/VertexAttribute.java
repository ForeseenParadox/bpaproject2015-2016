package engine.graphics;

/**
 * The Class VertexAttribute.
 */
public class VertexAttribute
{

	/** The Constant ATTRIB_POSITION. */
	public static final VertexAttribute ATTRIB_POSITION = new VertexAttribute(0, "a_position");
	
	/** The Constant ATTRIB_COLOR. */
	public static final VertexAttribute ATTRIB_COLOR = new VertexAttribute(1, "a_color");
	
	/** The Constant ATTRIB_TEX_COORD. */
	public static final VertexAttribute ATTRIB_TEX_COORD = new VertexAttribute(2, "a_texCoord");
	
	/** The Constant ATTRIB_NORMAL. */
	public static final VertexAttribute ATTRIB_NORMAL = new VertexAttribute(3, "a_normal");

	/** The index. */
	private int index;
	
	/** The name. */
	private String name;

	/**
	 * Instantiates a new vertex attribute.
	 */
	public VertexAttribute()
	{
		this(0, "");
	}

	/**
	 * Instantiates a new vertex attribute.
	 *
	 * @param index the index
	 * @param name the name
	 */
	public VertexAttribute(int index, String name)
	{
		this.index = index;
		this.name = name;
	}

	/**
	 * Gets the index.
	 *
	 * @return the index
	 */
	public int getIndex()
	{
		return index;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * Sets the index.
	 *
	 * @param index the new index
	 */
	public void setIndex(int index)
	{
		this.index = index;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name)
	{
		this.name = name;
	}

}
