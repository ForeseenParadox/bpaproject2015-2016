package engine.graphics.batch;

import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL15.GL_DYNAMIC_DRAW;

import engine.core.Engine;
import engine.graphics.Color;
import engine.graphics.VertexArray;
import engine.graphics.VertexAttribute;
import engine.graphics.VertexBuffer;
import engine.graphics.light.AmbientLight;
import engine.graphics.shader.ShaderProgram;
import engine.graphics.texture.Texture;
import engine.graphics.texture.TextureRegion;
import engine.util.StringUtils;
import engine.util.math.Matrix4f;
import engine.util.math.Vector2f;
import engine.util.math.Vector4f;

/**
 * The Class SpriteBatch.
 */
public class SpriteBatch
{

	/** The Constant DEFAULT_MAX_DRAW_COUNT. */
	public static final int DEFAULT_MAX_DRAW_COUNT = 50000;

	/** The vertex array. */
	private VertexArray vertexArray;
	
	/** The vertex buffer. */
	private VertexBuffer vertexBuffer;
	
	/** The texture. */
	private Texture texture;
	
	/** The drawing. */
	private boolean drawing;
	
	/** The max draw count. */
	private int maxDrawCount;
	
	/** The draw count. */
	private int drawCount;
	
	/** The tint color. */
	private Color tintColor;

	/** The mesh point buffer. */
	private Vector4f[] meshPointBuffer;
	
	/** The tex coord buffer. */
	private Vector2f[] texCoordBuffer;
	
	/** The mesh transform buffer. */
	private Matrix4f meshTransformBuffer;

	/** The default shader. */
	private ShaderProgram defaultShader;
	
	/** The shader. */
	private ShaderProgram shader;

	/** The projection matrix. */
	private Matrix4f projectionMatrix;
	
	/** The view matrix. */
	private Matrix4f viewMatrix;

	/** The ambient light. */
	private AmbientLight ambientLight;

	/**
	 * Instantiates a new sprite batch.
	 */
	public SpriteBatch()
	{
		this(DEFAULT_MAX_DRAW_COUNT);
	}

	/**
	 * Instantiates a new sprite batch.
	 *
	 * @param maxDrawCount the max draw count
	 */
	public SpriteBatch(int maxDrawCount)
	{
		this(maxDrawCount, null);
	}

	/**
	 * Instantiates a new sprite batch.
	 *
	 * @param maxDrawCount the max draw count
	 * @param customShader the custom shader
	 */
	public SpriteBatch(int maxDrawCount, ShaderProgram customShader)
	{
		this.maxDrawCount = maxDrawCount;
		vertexArray = new VertexArray();
		vertexBuffer = new VertexBuffer(maxDrawCount * 9 * 4);

		vertexArray.bind();
		vertexBuffer.bind();

		vertexArray.configureVertexAttribute(0, 3, 4 * (3 + 4 + 2), 0);
		vertexArray.configureVertexAttribute(1, 4, 4 * (3 + 4 + 2), 4 * 3);
		vertexArray.configureVertexAttribute(2, 2, 4 * (3 + 4 + 2), 4 * (3 + 4));

		texture = null;

		drawing = false;
		tintColor = Color.WHITE;

		// init buffers
		meshPointBuffer = new Vector4f[4];
		for (int i = 0; i < meshPointBuffer.length; i++)
			meshPointBuffer[i] = new Vector4f();

		texCoordBuffer = new Vector2f[4];
		for (int i = 0; i < texCoordBuffer.length; i++)
			texCoordBuffer[i] = new Vector2f();

		meshTransformBuffer = new Matrix4f();

		// load default shader
		String[] vertexShader = new String[]
		{
				"#version 120",
				"uniform mat4 u_view;",
				"uniform mat4 u_projection;",
				"attribute vec3 a_position;",
				"attribute vec4 a_color;",
				"attribute vec2 a_texCoord;",
				"varying vec4 v_color;",
				"varying vec2 v_texCoord;",
				"void main()",
				"{",
				"	gl_Position = u_projection*u_view*vec4(a_position, 1.0);",
				"	v_color = a_color;",
				"	v_texCoord = a_texCoord;",
				"}" };
		String[] fragmentShader = new String[]
		{
				"#version 120",
				"uniform sampler2D u_sampler;",
				"uniform float u_ambientIntensity;",
				"uniform vec4 u_ambientColor;",
				"varying vec4 v_color;",
				"varying vec2 v_texCoord;",
				"void main()",
				"{",
				"	gl_FragColor = texture2D(u_sampler, v_texCoord) * vec4(v_color.rgb * u_ambientIntensity * u_ambientColor.rgb, v_color.a);",
				"}" };

		defaultShader = new ShaderProgram(StringUtils.concatenate(vertexShader), StringUtils.concatenate(fragmentShader), new VertexAttribute[]
		{
				VertexAttribute.ATTRIB_POSITION,
				VertexAttribute.ATTRIB_COLOR,
				VertexAttribute.ATTRIB_TEX_COORD });

		projectionMatrix = new Matrix4f();
		viewMatrix = new Matrix4f();
		projectionMatrix.setOrthogonalProjection(0, Engine.getInstance().getWindow().getWidth(), 0, Engine.getInstance().getWindow().getHeight(), 1, -1);
		viewMatrix.setIdentity();

		if (customShader != null)
			shader = customShader;
		else
			shader = defaultShader;

		ambientLight = new AmbientLight(Color.WHITE, 1);

	}

	/**
	 * Gets the vertex array.
	 *
	 * @return the vertex array
	 */
	public VertexArray getVertexArray()
	{
		return vertexArray;
	}

	/**
	 * Gets the vertex buffer.
	 *
	 * @return the vertex buffer
	 */
	public VertexBuffer getVertexBuffer()
	{
		return vertexBuffer;
	}

	/**
	 * Gets the texture.
	 *
	 * @return the texture
	 */
	public Texture getTexture()
	{
		return texture;
	}

	/**
	 * Checks if is drawing.
	 *
	 * @return true, if is drawing
	 */
	public boolean isDrawing()
	{
		return drawing;
	}

	/**
	 * Gets the max draw count.
	 *
	 * @return the max draw count
	 */
	public int getMaxDrawCount()
	{
		return maxDrawCount;
	}

	/**
	 * Gets the draw count.
	 *
	 * @return the draw count
	 */
	public int getDrawCount()
	{
		return drawCount;
	}

	/**
	 * Gets the tint color.
	 *
	 * @return the tint color
	 */
	public Color getTintColor()
	{
		return tintColor;
	}

	/**
	 * Sets the max draw count.
	 *
	 * @param maxDrawCount the new max draw count
	 */
	public void setMaxDrawCount(int maxDrawCount)
	{
		this.maxDrawCount = maxDrawCount;
	}

	/**
	 * Sets the tint color.
	 *
	 * @param tintColor the new tint color
	 */
	public void setTintColor(Color tintColor)
	{
		this.tintColor = tintColor;
	}

	/**
	 * Gets the default shader.
	 *
	 * @return the default shader
	 */
	public ShaderProgram getDefaultShader()
	{
		return defaultShader;
	}

	/**
	 * Gets the shader.
	 *
	 * @return the shader
	 */
	public ShaderProgram getShader()
	{
		return shader;
	}

	/**
	 * Gets the transform.
	 *
	 * @return the transform
	 */
	public Matrix4f getTransform()
	{
		return viewMatrix;
	}

	/**
	 * Gets the view matrix.
	 *
	 * @return the view matrix
	 */
	public Matrix4f getViewMatrix()
	{
		return viewMatrix;
	}

	/**
	 * Gets the projection matrix.
	 *
	 * @return the projection matrix
	 */
	public Matrix4f getProjectionMatrix()
	{
		return projectionMatrix;
	}

	/**
	 * Gets the ambient light.
	 *
	 * @return the ambient light
	 */
	public AmbientLight getAmbientLight()
	{
		return ambientLight;
	}

	/**
	 * Use custom shader.
	 *
	 * @param shader the shader
	 */
	public void useCustomShader(ShaderProgram shader)
	{
		this.shader = shader;
	}

	/**
	 * Use default shader.
	 */
	public void useDefaultShader()
	{
		shader = defaultShader;
	}

	/**
	 * Sets the view matrix.
	 *
	 * @param transform the new view matrix
	 */
	public void setViewMatrix(Matrix4f transform)
	{
		this.viewMatrix = transform;
	}

	/**
	 * Sets the projection matrix.
	 *
	 * @param projectionMatrix the new projection matrix
	 */
	public void setProjectionMatrix(Matrix4f projectionMatrix)
	{
		this.projectionMatrix = projectionMatrix;
	}

	/**
	 * Sets the ambient light.
	 *
	 * @param ambientLight the new ambient light
	 */
	public void setAmbientLight(AmbientLight ambientLight)
	{
		this.ambientLight = ambientLight;
	}

	/**
	 * Render texture.
	 *
	 * @param region the region
	 * @param x the x
	 * @param y the y
	 * @param w the w
	 * @param h the h
	 * @param ox the ox
	 * @param oy the oy
	 * @param rot the rot
	 * @param sx the sx
	 * @param sy the sy
	 */
	public void renderTexture(TextureRegion region, float x, float y, float w, float h, float ox, float oy, float rot, float sx, float sy)
	{
		renderTexture(region.getTexture(), x, y, w, h, region.getNormalizedX(), region.getNormalizedY(), region.getNormalizedWidth(), region.getNormalizedHeight(), ox, oy, rot, sx, sy);
	}

	/**
	 * Render texture.
	 *
	 * @param texture the texture
	 * @param x the x
	 * @param y the y
	 * @param tcx the tcx
	 * @param tcy the tcy
	 * @param tcw the tcw
	 * @param tch the tch
	 * @param ox the ox
	 * @param oy the oy
	 * @param rot the rot
	 * @param sx the sx
	 * @param sy the sy
	 */
	public void renderTexture(Texture texture, float x, float y, float tcx, float tcy, float tcw, float tch, float ox, float oy, float rot, float sx, float sy)
	{
		renderTexture(texture, x, y, tcw * texture.getWidth(), tch * texture.getHeight(), tcx, tcy, tcw, tch, ox, oy, rot, sx, sy);
	}

	/**
	 * Render texture.
	 *
	 * @param texture the texture
	 * @param x the x
	 * @param y the y
	 * @param w the w
	 * @param h the h
	 * @param tcx the tcx
	 * @param tcy the tcy
	 * @param tcw the tcw
	 * @param tch the tch
	 * @param ox the ox
	 * @param oy the oy
	 * @param rot the rot
	 * @param sx the sx
	 * @param sy the sy
	 */
	public void renderTexture(Texture texture, float x, float y, float w, float h, float tcx, float tcy, float tcw, float tch, float ox, float oy, float rot, float sx, float sy)
	{
		attemptFlush(texture);

		float hw = w / 2, hh = h / 2;

		meshPointBuffer[0].set(-hw + ox, -hh + oy, 0, 1);
		meshPointBuffer[1].set(-hw + ox, -hh + h + oy, 0, 1);
		meshPointBuffer[2].set(-hw + w + ox, -hh + h + oy, 0, 1);
		meshPointBuffer[3].set(-hw + w + ox, -hh + oy, 0, 1);

		texCoordBuffer[0].set(tcx, tcy);
		texCoordBuffer[1].set(tcx, tcy + tch);
		texCoordBuffer[2].set(tcx + tcw, tcy + tch);
		texCoordBuffer[3].set(tcx + tcw, tcy);

		if (rot != 0)
			meshTransformBuffer.setToRotationZ(rot);

		for (int i = 0; i < 4; i++)
		{
			Vector4f point = meshPointBuffer[i];
			Vector2f texCoord = texCoordBuffer[i];
			point.setX(point.getX() * sx);
			point.setY(point.getY() * sy);
			point.setZ(0);
			if (rot != 0)
				point.mutateMultiply(meshTransformBuffer);

			point.translate(x + hw - ox, y + hh - oy, 0, 0);

			vertexBuffer.put(point.getX()).put(point.getY()).put(point.getZ());
			vertexBuffer.put(tintColor.getRed()).put(tintColor.getGreen()).put(tintColor.getBlue()).put(tintColor.getAlpha());
			vertexBuffer.put(texCoord.getX()).put(texCoord.getY());
		}

		drawCount++;
	}

	/**
	 * Attempt flush.
	 *
	 * @param requestedTexture the requested texture
	 */
	private void attemptFlush(Texture requestedTexture)
	{
		if (requestedTexture != texture || drawCount + 1 >= maxDrawCount)
			flush();
		texture = requestedTexture;
	}

	/**
	 * Begin.
	 */
	public void begin()
	{
		if (drawing)
			throw new IllegalStateException("SpriteBatch is already in drawing mode.");

		// prepare for rendering mode
		drawing = true;
		vertexBuffer.clearBuffer();
	}

	/**
	 * Flush.
	 */
	public void flush()
	{
		if (!drawing)
			throw new IllegalStateException("SpriteBatch must be in drawing mode before flushing data.");

		if (drawCount > 0)
		{

			shader.bindProgram();
			shader.setMatrix4f("u_view", viewMatrix);
			shader.setMatrix4f("u_projection", projectionMatrix);
			shader.setFloat("u_ambientIntensity", ambientLight.getIntensity());
			shader.setColor("u_ambientColor", ambientLight.getColor());

			texture.bind();

			vertexArray.bind();
			vertexBuffer.bind();

			vertexBuffer.pushToGpu(GL_DYNAMIC_DRAW);

			vertexArray.render(GL_QUADS, drawCount * 4);
			drawCount = 0;
			vertexBuffer.clearBuffer();
		}
	}

	/**
	 * End.
	 */
	public void end()
	{
		if (!drawing)
			throw new IllegalStateException("SpriteBatch must be in drawing mode before ending a drawing cycle.");

		flush();
		drawing = false;
	}

}
