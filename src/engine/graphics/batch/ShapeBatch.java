package engine.graphics.batch;

import static org.lwjgl.opengl.GL11.GL_LINES;
import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.GL_TRIANGLES;
import static org.lwjgl.opengl.GL15.GL_DYNAMIC_DRAW;

import engine.core.Engine;
import engine.graphics.Color;
import engine.graphics.VertexArray;
import engine.graphics.VertexAttribute;
import engine.graphics.VertexBuffer;
import engine.graphics.shader.ShaderProgram;
import engine.util.StringUtils;
import engine.util.math.Matrix4f;
import engine.util.math.Vector2f;
import engine.util.math.Vector4f;

/**
 * The Class ShapeBatch.
 */
public class ShapeBatch
{

	/** The vertex array. */
	private VertexArray vertexArray;
	
	/** The vertex buffer. */
	private VertexBuffer vertexBuffer;
	
	/** The primitive. */
	private int primitive;
	
	/** The drawing. */
	private boolean drawing;
	
	/** The max vertex count. */
	private int maxVertexCount;
	
	/** The vertex count. */
	private int vertexCount;
	
	/** The draw color. */
	private Color drawColor;

	/** The mesh point buffer. */
	private Vector4f[] meshPointBuffer;
	
	/** The mesh transform buffer. */
	private Matrix4f meshTransformBuffer;

	/** The default shader. */
	private ShaderProgram defaultShader;
	
	/** The shader. */
	private ShaderProgram shader;

	/** The projection matrix. */
	private Matrix4f projectionMatrix;
	
	/** The view matrix. */
	private Matrix4f viewMatrix;

	/**
	 * Instantiates a new shape batch.
	 */
	public ShapeBatch()
	{
		this(100000, null);
	}

	/**
	 * Instantiates a new shape batch.
	 *
	 * @param maxVertexCount the max vertex count
	 * @param customShader the custom shader
	 */
	public ShapeBatch(int maxVertexCount, ShaderProgram customShader)
	{
		this.maxVertexCount = maxVertexCount;
		vertexArray = new VertexArray();
		vertexBuffer = new VertexBuffer(maxVertexCount * 6);

		vertexArray.bind();
		vertexBuffer.bind();

		vertexArray.configureVertexAttribute(0, 2, 4 * (2 + 4), 0);
		vertexArray.configureVertexAttribute(1, 4, 4 * (2 + 4), 4 * 2);

		// default primitive
		primitive = GL_LINES;

		drawing = false;
		drawColor = Color.WHITE;

		// init buffers
		meshPointBuffer = new Vector4f[4];
		for (int i = 0; i < meshPointBuffer.length; i++)
			meshPointBuffer[i] = new Vector4f();

		meshTransformBuffer = new Matrix4f();

		// load default shader
		String[] vertexShader = new String[]
		{
				"#version 120",
				"uniform mat4 u_view;",
				"uniform mat4 u_projection;",
				"attribute vec3 a_position;",
				"attribute vec4 a_color;",
				"varying vec4 v_color;",
				"void main()",
				"{",
				"	gl_Position = u_projection*u_view*vec4(a_position, 1.0);",
				"	v_color = a_color;",
				"}" };
		String[] fragmentShader = new String[]
		{
				"#version 120",
				"varying vec4 v_color;",
				"void main()",
				"{",
				"	gl_FragColor = v_color;",
				"}" };

		defaultShader = new ShaderProgram(StringUtils.concatenate(vertexShader), StringUtils.concatenate(fragmentShader), new VertexAttribute[]
		{
				VertexAttribute.ATTRIB_POSITION,
				VertexAttribute.ATTRIB_COLOR });

		projectionMatrix = new Matrix4f();
		viewMatrix = new Matrix4f();
		projectionMatrix.setOrthogonalProjection(0, Engine.getInstance().getWindow().getWidth(), 0, Engine.getInstance().getWindow().getHeight(), 1, -1);
		viewMatrix.setIdentity();

		if (customShader != null)
			shader = customShader;
		else
			shader = defaultShader;
	}

	/**
	 * Gets the vertex array.
	 *
	 * @return the vertex array
	 */
	public VertexArray getVertexArray()
	{
		return vertexArray;
	}

	/**
	 * Gets the vertex buffer.
	 *
	 * @return the vertex buffer
	 */
	public VertexBuffer getVertexBuffer()
	{
		return vertexBuffer;
	}

	/**
	 * Gets the primitive.
	 *
	 * @return the primitive
	 */
	public int getPrimitive()
	{
		return primitive;
	}

	/**
	 * Checks if is drawing.
	 *
	 * @return true, if is drawing
	 */
	public boolean isDrawing()
	{
		return drawing;
	}

	/**
	 * Gets the max vertex count.
	 *
	 * @return the max vertex count
	 */
	public int getMaxVertexCount()
	{
		return maxVertexCount;
	}

	/**
	 * Gets the vertex count.
	 *
	 * @return the vertex count
	 */
	public int getVertexCount()
	{
		return vertexCount;
	}

	/**
	 * Gets the draw color.
	 *
	 * @return the draw color
	 */
	public Color getDrawColor()
	{
		return drawColor;
	}

	/**
	 * Gets the default shader.
	 *
	 * @return the default shader
	 */
	public ShaderProgram getDefaultShader()
	{
		return defaultShader;
	}

	/**
	 * Gets the shader.
	 *
	 * @return the shader
	 */
	public ShaderProgram getShader()
	{
		return shader;
	}

	/**
	 * Gets the projection matrix.
	 *
	 * @return the projection matrix
	 */
	public Matrix4f getProjectionMatrix()
	{
		return projectionMatrix;
	}

	/**
	 * Gets the view matrix.
	 *
	 * @return the view matrix
	 */
	public Matrix4f getViewMatrix()
	{
		return viewMatrix;
	}

	/**
	 * Use default shader.
	 */
	public void useDefaultShader()
	{
		shader = defaultShader;
	}

	/**
	 * Use custom shader.
	 *
	 * @param customShader the custom shader
	 */
	public void useCustomShader(ShaderProgram customShader)
	{
		shader = customShader;
	}

	/**
	 * Sets the max vertex count.
	 *
	 * @param maxVertexCount the new max vertex count
	 */
	public void setMaxVertexCount(int maxVertexCount)
	{
		this.maxVertexCount = maxVertexCount;
	}

	/**
	 * Sets the draw color.
	 *
	 * @param drawColor the new draw color
	 */
	public void setDrawColor(Color drawColor)
	{
		this.drawColor = drawColor;
	}

	/**
	 * Sets the projection matrix.
	 *
	 * @param projectionMatrix the new projection matrix
	 */
	public void setProjectionMatrix(Matrix4f projectionMatrix)
	{
		this.projectionMatrix = projectionMatrix;
	}

	/**
	 * Sets the view matrix.
	 *
	 * @param viewMatrix the new view matrix
	 */
	public void setViewMatrix(Matrix4f viewMatrix)
	{
		this.viewMatrix = viewMatrix;
	}

	/**
	 * Render line.
	 *
	 * @param x1 the x1
	 * @param y1 the y1
	 * @param x2 the x2
	 * @param y2 the y2
	 */
	public void renderLine(float x1, float y1, float x2, float y2)
	{
		attemptFlush(GL_LINES, 2);

		vertexBuffer.put(x1).put(y1);
		vertexBuffer.put(drawColor.getRed()).put(drawColor.getGreen()).put(drawColor.getBlue()).put(drawColor.getAlpha());
		vertexBuffer.put(x2).put(y2);
		vertexBuffer.put(drawColor.getRed()).put(drawColor.getGreen()).put(drawColor.getBlue()).put(drawColor.getAlpha());

		vertexCount += 2;
	}

	/**
	 * Render line.
	 *
	 * @param v1 the v1
	 * @param v2 the v2
	 */
	public void renderLine(Vector2f v1, Vector2f v2)
	{
		renderLine(v1.getX(), v1.getY(), v2.getX(), v2.getY());
	}

	/**
	 * Render triangle.
	 *
	 * @param x1 the x1
	 * @param y1 the y1
	 * @param x2 the x2
	 * @param y2 the y2
	 * @param x3 the x3
	 * @param y3 the y3
	 */
	public void renderTriangle(float x1, float y1, float x2, float y2, float x3, float y3)
	{
		attemptFlush(GL_TRIANGLES, 3);

		vertexBuffer.put(x1).put(y1);
		vertexBuffer.put(drawColor.getRed()).put(drawColor.getGreen()).put(drawColor.getBlue()).put(drawColor.getAlpha());
		vertexBuffer.put(x2).put(y2);
		vertexBuffer.put(drawColor.getRed()).put(drawColor.getGreen()).put(drawColor.getBlue()).put(drawColor.getAlpha());
		vertexBuffer.put(x3).put(y3);
		vertexBuffer.put(drawColor.getRed()).put(drawColor.getGreen()).put(drawColor.getBlue()).put(drawColor.getAlpha());

		vertexCount += 3;
	}

	/**
	 * Render triangle.
	 *
	 * @param v1 the v1
	 * @param v2 the v2
	 * @param v3 the v3
	 */
	public void renderTriangle(Vector2f v1, Vector2f v2, Vector2f v3)
	{
		renderTriangle(v1.getX(), v1.getY(), v2.getX(), v2.getY(), v3.getX(), v3.getY());
	}

	/**
	 * Render quad.
	 *
	 * @param x1 the x1
	 * @param y1 the y1
	 * @param x2 the x2
	 * @param y2 the y2
	 * @param x3 the x3
	 * @param y3 the y3
	 * @param x4 the x4
	 * @param y4 the y4
	 */
	public void renderQuad(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4)
	{
		attemptFlush(GL_QUADS, 4);

		vertexBuffer.put(x1).put(y1);
		vertexBuffer.put(drawColor.getRed()).put(drawColor.getGreen()).put(drawColor.getBlue()).put(drawColor.getAlpha());
		vertexBuffer.put(x2).put(y2);
		vertexBuffer.put(drawColor.getRed()).put(drawColor.getGreen()).put(drawColor.getBlue()).put(drawColor.getAlpha());
		vertexBuffer.put(x3).put(y3);
		vertexBuffer.put(drawColor.getRed()).put(drawColor.getGreen()).put(drawColor.getBlue()).put(drawColor.getAlpha());
		vertexBuffer.put(x4).put(y4);
		vertexBuffer.put(drawColor.getRed()).put(drawColor.getGreen()).put(drawColor.getBlue()).put(drawColor.getAlpha());

		vertexCount += 4;
	}

	/**
	 * Render quad.
	 *
	 * @param v1 the v1
	 * @param v2 the v2
	 * @param v3 the v3
	 * @param v4 the v4
	 */
	public void renderQuad(Vector2f v1, Vector2f v2, Vector2f v3, Vector2f v4)
	{
		renderQuad(v1.getX(), v1.getY(), v2.getX(), v2.getY(), v3.getX(), v3.getY(), v4.getX(), v4.getY());
	}

	/**
	 * Render rect.
	 *
	 * @param x the x
	 * @param y the y
	 * @param w the w
	 * @param h the h
	 * @param ox the ox
	 * @param oy the oy
	 * @param rot the rot
	 * @param sx the sx
	 * @param sy the sy
	 */
	public void renderRect(float x, float y, float w, float h, float ox, float oy, float rot, float sx, float sy)
	{
		renderLine(x, y, x, y + h);
		renderLine(x, y + h, x + w, y + h);
		renderLine(x + w, y + h, x + w, y);
		renderLine(x + w, y, x, y);
	}

	/**
	 * Render filled rect.
	 *
	 * @param x the x
	 * @param y the y
	 * @param w the w
	 * @param h the h
	 * @param ox the ox
	 * @param oy the oy
	 * @param rot the rot
	 * @param sx the sx
	 * @param sy the sy
	 */
	public void renderFilledRect(float x, float y, float w, float h, float ox, float oy, float rot, float sx, float sy)
	{
		attemptFlush(GL_QUADS, 4);

		float hw = w / 2, hh = h / 2;

		meshPointBuffer[0].set(-hw + ox, -hh + oy, 0, 1);
		meshPointBuffer[1].set(-hw + ox, -hh + h + oy, 0, 1);
		meshPointBuffer[2].set(-hw + w + ox, -hh + h + oy, 0, 1);
		meshPointBuffer[3].set(-hw + w + ox, -hh + oy, 0, 1);

		if (rot != 0)
			meshTransformBuffer.setToRotationZ(rot);

		for (Vector4f vec : meshPointBuffer)
		{
			vec.setX(vec.getX() * sx);
			vec.setY(vec.getY() * sy);
			if (rot != 0)
				vec.mutateMultiply(meshTransformBuffer);

			vec.translate(x + hw - ox, y + hh - oy, 0, 0);

			vertexBuffer.put(vec.getX()).put(vec.getY());
			vertexBuffer.put(drawColor.getRed()).put(drawColor.getGreen()).put(drawColor.getBlue()).put(drawColor.getAlpha());
		}

		vertexCount += 4;
	}

	/**
	 * Render polygon.
	 *
	 * @param xPoints the x points
	 * @param yPoints the y points
	 * @param vertCount the vert count
	 * @param translationX the translation x
	 * @param translationY the translation y
	 * @param rotation the rotation
	 */
	public void renderPolygon(float[] xPoints, float[] yPoints, int vertCount, float translationX, float translationY, float rotation)
	{
		attemptFlush(GL_LINES, vertCount);

		// rotate vertices
		if (rotation != 0)
		{
			for (int i = 0; i < vertCount; i++)
			{
				meshTransformBuffer.setToRotationZ(rotation);
				meshPointBuffer[0].set(xPoints[i], yPoints[i], 0, 1);
				meshPointBuffer[0].mutateMultiply(meshTransformBuffer);
				xPoints[i] = meshPointBuffer[0].getX();
				yPoints[i] = meshPointBuffer[0].getY();
			}
		}

		for (int i = 0; i < vertCount; i++)
		{
			xPoints[i] += translationX;
			yPoints[i] += translationY;
		}

		for (int i = 0; i < vertCount; i++)
		{
			vertexBuffer.put(xPoints[i]).put(yPoints[i]);
			vertexBuffer.put(drawColor.getRed()).put(drawColor.getGreen()).put(drawColor.getBlue()).put(drawColor.getAlpha());
			vertexBuffer.put(xPoints[(i + 1) % vertCount]).put(yPoints[(i + 1) % vertCount]);
			vertexBuffer.put(drawColor.getRed()).put(drawColor.getGreen()).put(drawColor.getBlue()).put(drawColor.getAlpha());
		}

		vertexCount += vertCount * 2;
	}

	/**
	 * Attempt flush.
	 *
	 * @param requestedPrimitive the requested primitive
	 * @param requestedVertices the requested vertices
	 */
	private void attemptFlush(int requestedPrimitive, int requestedVertices)
	{
		if (requestedPrimitive != primitive || vertexCount + requestedVertices >= maxVertexCount)
			flush();
		primitive = requestedPrimitive;
	}

	/**
	 * Begin.
	 */
	public void begin()
	{
		if (drawing)
			throw new IllegalStateException("ShapeBatch is already in drawing mode.");

		// prepare for rendering mode
		drawing = true;
		vertexBuffer.clearBuffer();
	}

	/**
	 * Flush.
	 */
	public void flush()
	{
		if (!drawing)
			throw new IllegalStateException("ShapeBatch must be in drawing mode before flushing data.");

		if (vertexCount > 0)
		{
			shader.bindProgram();
			shader.setMatrix4f("u_view", viewMatrix);
			shader.setMatrix4f("u_projection", projectionMatrix);

			vertexArray.bind();
			vertexBuffer.bind();

			vertexBuffer.pushToGpu(GL_DYNAMIC_DRAW);

			vertexArray.render(primitive, vertexCount);

			vertexCount = 0;
			vertexBuffer.clearBuffer();
		}
	}

	/**
	 * End.
	 */
	public void end()
	{
		if (!drawing)
			throw new IllegalStateException("ShapeBatch must be in drawing mode before ending a drawing cycle.");

		flush();
		drawing = false;
	}

}
