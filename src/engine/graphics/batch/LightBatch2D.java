package engine.graphics.batch;

import static org.lwjgl.opengl.GL11.GL_ALWAYS;
import static org.lwjgl.opengl.GL11.GL_BLEND;
import static org.lwjgl.opengl.GL11.GL_BLEND_DST;
import static org.lwjgl.opengl.GL11.GL_BLEND_SRC;
import static org.lwjgl.opengl.GL11.GL_KEEP;
import static org.lwjgl.opengl.GL11.GL_NOTEQUAL;
import static org.lwjgl.opengl.GL11.GL_ONE;
import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.GL_REPLACE;
import static org.lwjgl.opengl.GL11.GL_STENCIL_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_STENCIL_TEST;
import static org.lwjgl.opengl.GL11.glBlendFunc;
import static org.lwjgl.opengl.GL11.glClear;
import static org.lwjgl.opengl.GL11.glClearStencil;
import static org.lwjgl.opengl.GL11.glColorMask;
import static org.lwjgl.opengl.GL11.glDepthMask;
import static org.lwjgl.opengl.GL11.glDisable;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL11.glGetBoolean;
import static org.lwjgl.opengl.GL11.glGetInteger;
import static org.lwjgl.opengl.GL11.glStencilFunc;
import static org.lwjgl.opengl.GL11.glStencilOp;
import static org.lwjgl.opengl.GL15.GL_STATIC_DRAW;

import java.util.LinkedList;
import java.util.Stack;

import engine.graphics.VertexArray;
import engine.graphics.VertexAttribute;
import engine.graphics.VertexBuffer;
import engine.graphics.light.Light;
import engine.graphics.light.PointLight;
import engine.graphics.shader.ShaderProgram;
import engine.physics.BoundingBox;
import engine.util.math.Vector2f;

/**
 * The Class LightBatch2D.
 */
public class LightBatch2D
{

	/** The Constant SHADOW_SCALE_FACTOR. */
	public static final int SHADOW_SCALE_FACTOR = 1000;

	/** The default point light shader. */
	private ShaderProgram defaultPointLightShader;
	
	/** The point light shader. */
	private ShaderProgram pointLightShader;

	/** The light vao. */
	private VertexArray lightVao;
	
	/** The light vbo. */
	private VertexBuffer lightVbo;

	/** The shadows enabled. */
	private boolean shadowsEnabled;
	
	/** The box occluders. */
	private LinkedList<BoundingBox> boxOccluders;

	/** The lights to render. */
	private Stack<Light> lightsToRender;

	/**
	 * Instantiates a new light batch2 d.
	 */
	public LightBatch2D()
	{
		String defaultPointLightVertex = "#version 110\n" + "attribute vec2 a_position;\n" + "varying vec2 v_position;\n" + "void main(){\n" + "gl_Position = vec4(a_position, 0.0, 1.0);\n" + "v_position = a_position;\n" + "}\n";
		String defaultPointLightFragment = "#version 120\n" + "uniform vec2 u_position;\n" + "uniform vec4 u_color;\n" + "uniform vec3 u_attenuation;\n" + "uniform vec2 u_translation;\n" + "uniform float u_intensity;\n" + "varying vec2 v_position;\n" + "varying vec2 v_texCoord;" + "void main(){\n" + "float distanceFromLight = length(u_position - (gl_FragCoord.xy-u_translation));\n"
				+ "float attenuation = 1.0 / (u_attenuation.x * distanceFromLight * distanceFromLight + u_attenuation.y * distanceFromLight + u_attenuation.z);\n" + "gl_FragColor = u_color * attenuation * u_intensity; }\n";

		defaultPointLightShader = new ShaderProgram(defaultPointLightVertex, defaultPointLightFragment, new VertexAttribute[]
		{
				new VertexAttribute(0, "a_position") });

		pointLightShader = defaultPointLightShader;

		lightVao = new VertexArray();
		lightVbo = new VertexBuffer(4 * 2);

		lightVao.bind();
		lightVbo.bind();

		lightVao.configureVertexAttribute(0, 2, 0, 0);

		lightVbo.put(-1).put(-1);
		lightVbo.put(-1).put(1);
		lightVbo.put(1).put(1);
		lightVbo.put(1).put(-1);

		lightVbo.pushToGpu(GL_STATIC_DRAW);

		boxOccluders = new LinkedList<BoundingBox>();

		shadowsEnabled = true;

		lightsToRender = new Stack<Light>();
	}

	/**
	 * Gets the default point light shader.
	 *
	 * @return the default point light shader
	 */
	public ShaderProgram getDefaultPointLightShader()
	{
		return defaultPointLightShader;
	}

	/**
	 * Gets the point light shader.
	 *
	 * @return the point light shader
	 */
	public ShaderProgram getPointLightShader()
	{
		return pointLightShader;
	}

	/**
	 * Gets the light vao.
	 *
	 * @return the light vao
	 */
	public VertexArray getLightVao()
	{
		return lightVao;
	}

	/**
	 * Gets the light vbo.
	 *
	 * @return the light vbo
	 */
	public VertexBuffer getLightVbo()
	{
		return lightVbo;
	}

	/**
	 * Checks if is shadows enabled.
	 *
	 * @return true, if is shadows enabled
	 */
	public boolean isShadowsEnabled()
	{
		return shadowsEnabled;
	}

	/**
	 * Gets the box occluders.
	 *
	 * @return the box occluders
	 */
	public LinkedList<BoundingBox> getBoxOccluders()
	{
		return boxOccluders;
	}

	/**
	 * Use default point light shader.
	 */
	public void useDefaultPointLightShader()
	{
		pointLightShader = defaultPointLightShader;
	}

	/**
	 * Use custom point light shader.
	 *
	 * @param pointLightShader the point light shader
	 */
	public void useCustomPointLightShader(ShaderProgram pointLightShader)
	{
		this.pointLightShader = pointLightShader;
	}

	/**
	 * Sets the shadows enabled.
	 *
	 * @param shadowsEnabled the new shadows enabled
	 */
	public void setShadowsEnabled(boolean shadowsEnabled)
	{
		this.shadowsEnabled = shadowsEnabled;
	}

	/**
	 * Render point light.
	 *
	 * @param pointLight the point light
	 * @param occluderRenderer the occluder renderer
	 * @param translation the translation
	 */
	public void renderPointLight(PointLight pointLight, ShapeBatch occluderRenderer, Vector2f translation)
	{

		if (translation == null)
			translation = Vector2f.ZERO;

		if (shadowsEnabled)
		{
			// set up stenciling
			glEnable(GL_STENCIL_TEST);
			glClearStencil(0);
			glClear(GL_STENCIL_BUFFER_BIT);
			glStencilFunc(GL_ALWAYS, 1, 1);
			glStencilOp(GL_REPLACE, GL_REPLACE, GL_REPLACE);
			glColorMask(false, false, false, false);
			glDepthMask(true);

			boolean drawingBefore = occluderRenderer.isDrawing();
			if (!drawingBefore)
				occluderRenderer.begin();

			for (BoundingBox box : boxOccluders)
			{
				float bx = box.getPosition().getX() + box.getWidth() / 2, by = box.getPosition().getY() + box.getHeight() / 2;

				if (new Vector2f(bx, by).subtract(pointLight.getPosition()).length() > pointLight.getRadius())
					continue;

				Vector2f[] vertices = new Vector2f[4];
				vertices[0] = new Vector2f(box.getPosition().getX(), box.getPosition().getY());
				vertices[1] = new Vector2f(box.getPosition().getX(), box.getPosition().getY() + box.getHeight());
				vertices[2] = new Vector2f(box.getPosition().getX() + box.getWidth(), box.getPosition().getY() + box.getHeight());
				vertices[3] = new Vector2f(box.getPosition().getX() + box.getWidth(), box.getPosition().getY());

				Vector2f lightPos = pointLight.getPosition();

				for (int i = 0; i < vertices.length; i++)
				{
					Vector2f vert = vertices[i];
					Vector2f next = vertices[(i + 1) % vertices.length];
					Vector2f edge = next.subtract(vert);
					Vector2f normal = new Vector2f(-edge.getY(), edge.getX());
					Vector2f lightToVertex = vert.subtract(lightPos).normalized();
					if (normal.dotProduct(lightToVertex) < 0)
					{
						Vector2f point1 = vert.add(vert.subtract(lightPos).multiply(pointLight.getRadius()));
						Vector2f point2 = next.add(next.subtract(lightPos).multiply(pointLight.getRadius()));

						occluderRenderer.renderQuad(next, point2, point1, vert);
					}
				}

			}

			if (!drawingBefore)
				occluderRenderer.end();
			else
				occluderRenderer.flush();

			glColorMask(true, true, true, true);
			glDepthMask(false);
			glStencilFunc(GL_NOTEQUAL, 1, 1);
			glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
		}

		boolean blendEnabled = glGetBoolean(GL_BLEND);
		int oldBlendSource = glGetInteger(GL_BLEND_SRC), oldBlendDst = glGetInteger(GL_BLEND_DST);

		glEnable(GL_BLEND);

		glBlendFunc(GL_ONE, GL_ONE);

		pointLightShader.bindProgram();
		pointLightShader.setVector2f("u_position", pointLight.getPosition());
		pointLightShader.setColor("u_color", pointLight.getColor());
		pointLightShader.setVector3f("u_attenuation", pointLight.getAttenuation().getQuadratic(), pointLight.getAttenuation().getLinear(), pointLight.getAttenuation().getConstant());
		pointLightShader.setFloat("u_intensity", pointLight.getIntensity());
		pointLightShader.setVector2f("u_translation", translation);

		lightVao.render(GL_QUADS, 4);

		if (!blendEnabled)
			glDisable(GL_BLEND);

		glBlendFunc(oldBlendSource, oldBlendDst);
		glDisable(GL_STENCIL_TEST);
	}

	/**
	 * Push light.
	 *
	 * @param l the l
	 */
	public void pushLight(Light l)
	{
		lightsToRender.push(l);
	}

	/**
	 * Render all lights.
	 *
	 * @param occluderRenderer the occluder renderer
	 * @param translation the translation
	 */
	public void renderAllLights(ShapeBatch occluderRenderer, Vector2f translation)
	{
		while (!lightsToRender.isEmpty())
		{
			Light l = lightsToRender.pop();
			if (l instanceof PointLight)
				renderPointLight((PointLight) l, occluderRenderer, translation);
		}
	}
}