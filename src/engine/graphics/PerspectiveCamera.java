package engine.graphics;

import engine.util.math.Matrix4f;

/**
 * The Class PerspectiveCamera.
 */
public class PerspectiveCamera
{

	/** The view. */
	private Transform view;
	
	/** The projection. */
	private Matrix4f projection;

	/**
	 * Instantiates a new perspective camera.
	 *
	 * @param aspect the aspect
	 * @param fov the fov
	 * @param zNear the z near
	 * @param zFar the z far
	 */
	public PerspectiveCamera(float aspect, float fov, float zNear, float zFar)
	{
		projection = new Matrix4f();
		projection.setPerspectiveProjection(aspect, fov, zNear, zFar);

		view = new Transform();
	}

	/**
	 * Gets the view.
	 *
	 * @return the view
	 */
	public Transform getView()
	{
		return view;
	}

	/**
	 * Gets the projection.
	 *
	 * @return the projection
	 */
	public Matrix4f getProjection()
	{
		return projection;
	}

	/**
	 * Sets the view.
	 *
	 * @param view the new view
	 */
	public void setView(Transform view)
	{
		this.view = view;
	}

	/**
	 * Sets the projection.
	 *
	 * @param projection the new projection
	 */
	public void setProjection(Matrix4f projection)
	{
		this.projection = projection;
	}

	/**
	 * Gets the view projection.
	 *
	 * @return the view projection
	 */
	public Matrix4f getViewProjection()
	{
		return getProjection().multiply(view.getTransformationMatrix());
	}

}
