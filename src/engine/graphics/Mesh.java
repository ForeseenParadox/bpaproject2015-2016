package engine.graphics;

import engine.resource.obj.OBJResource;
import engine.util.math.Vector3f;

import static org.lwjgl.opengl.GL15.*;

/**
 * The Class Mesh.
 */
public class Mesh
{

	/** The vertex array. */
	private VertexArray vertexArray;
	
	/** The vertex buffer. */
	private VertexBuffer vertexBuffer;
	
	/** The vertex count. */
	private int vertexCount;

	/**
	 * Instantiates a new mesh.
	 */
	public Mesh()
	{
		vertexArray = new VertexArray();
		vertexBuffer = new VertexBuffer(50000000);

		vertexArray.bind();
		vertexBuffer.bind();

		vertexArray.configureVertexAttribute(0, 3, 0, 0);

		vertexCount = 0;
	}

	/**
	 * Instantiates a new mesh.
	 *
	 * @param obj the obj
	 */
	public Mesh(OBJResource obj)
	{
		this();
		vertexCount = obj.getIndices().size();

		for (int i = 0; i < obj.getIndices().size(); i++)
		{
			Vector3f vert = obj.getVertices().get(obj.getIndices().get(i).getPositionIndex());
			Vector3f normal = obj.getNormals().get(obj.getIndices().get(i).getNormalIndex());

			vertexBuffer.put(vert.getX()).put(vert.getY()).put(vert.getZ());
			vertexBuffer.put(1).put(1).put(1).put(1);
			vertexBuffer.put(normal.getX()).put(normal.getY()).put(normal.getZ());
		}
		vertexBuffer.pushToGpu(GL_STATIC_DRAW);
	}

	/**
	 * Gets the vertex array.
	 *
	 * @return the vertex array
	 */
	public VertexArray getVertexArray()
	{
		return vertexArray;
	}

	/**
	 * Gets the vertex buffer.
	 *
	 * @return the vertex buffer
	 */
	public VertexBuffer getVertexBuffer()
	{
		return vertexBuffer;
	}

	/**
	 * Gets the vertex count.
	 *
	 * @return the vertex count
	 */
	public int getVertexCount()
	{
		return vertexCount;
	}

	/**
	 * Sets the vertex array.
	 *
	 * @param vertexArray the new vertex array
	 */
	public void setVertexArray(VertexArray vertexArray)
	{
		this.vertexArray = vertexArray;
	}

	/**
	 * Sets the vertex buffer.
	 *
	 * @param vertexBuffer the new vertex buffer
	 */
	public void setVertexBuffer(VertexBuffer vertexBuffer)
	{
		this.vertexBuffer = vertexBuffer;
	}

	/**
	 * Sets the vertex count.
	 *
	 * @param vertexCount the new vertex count
	 */
	public void setVertexCount(int vertexCount)
	{
		this.vertexCount = vertexCount;
	}

	/**
	 * Render.
	 *
	 * @param renderMode the render mode
	 */
	public void render(int renderMode)
	{
		vertexArray.render(renderMode, vertexCount);
	}

}