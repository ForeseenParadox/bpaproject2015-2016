package engine.graphics;

/**
 * The Class Color.
 */
public class Color
{

	/** The Constant RED. */
	public static final Color RED = new Color(255, 0, 0);
	
	/** The Constant GREEN. */
	public static final Color GREEN = new Color(0, 255, 0);
	
	/** The Constant BLUE. */
	public static final Color BLUE = new Color(0, 0, 255);
	
	/** The Constant BLACK. */
	public static final Color BLACK = new Color(0, 0, 0);
	
	/** The Constant WHITE. */
	public static final Color WHITE = new Color(255, 255, 255);
	
	/** The Constant YELLOW. */
	public static final Color YELLOW = new Color(255, 255, 0);
	
	/** The Constant ORANGE. */
	public static final Color ORANGE = new Color(255, 127, 0);
	
	/** The Constant PURPLE. */
	public static final Color PURPLE = new Color(128, 0, 128);
	
	/** The Constant PINK. */
	public static final Color PINK = new Color(255, 203, 219);
	
	/** The Constant BEIGE. */
	public static final Color BEIGE = new Color(245, 245, 220);
	
	/** The Constant SILVER. */
	public static final Color SILVER = new Color(192, 192, 192);
	
	/** The Constant GRAY. */
	public static final Color GRAY = new Color(128, 128, 128);
	
	/** The Constant CYAN. */
	public static final Color CYAN = new Color(0, 255, 255);
	
	/** The Constant FUSHSIA. */
	public static final Color FUSHSIA = new Color(255, 119, 255);
	
	/** The Constant MAROON. */
	public static final Color MAROON = new Color(128, 0, 0);
	
	/** The Constant LIME. */
	public static final Color LIME = new Color(191, 255, 0);
	
	/** The Constant CRIMSON. */
	public static final Color CRIMSON = new Color(220, 20, 60);
	
	/** The Constant NAVY. */
	public static final Color NAVY = new Color(0, 0, 128);
	
	/** The Constant LIGHTGREEN. */
	public static final Color LIGHTGREEN = new Color(144, 238, 144);
	
	/** The Constant POWDERBLUE. */
	public static final Color POWDERBLUE = new Color(176, 224, 230);
	
	/** The Constant TEAL. */
	public static final Color TEAL = new Color(0, 128, 128);
	
	/** The Constant INDIGO. */
	public static final Color INDIGO = new Color(0xFF4B0082);
	
	/** The Constant GOLD. */
	public static final Color GOLD = new Color(0xFFFFD700);
	
	/** The Constant CORNFLOWER_BLUE. */
	public static final Color CORNFLOWER_BLUE = new Color(0xFF6495ED);

	/** The red. */
	private float red;
	
	/** The green. */
	private float green;
	
	/** The blue. */
	private float blue;
	
	/** The alpha. */
	private float alpha;

	/**
	 * Instantiates a new color.
	 */
	public Color()
	{
		this(0, 0, 0, 1);
	}

	/**
	 * Instantiates a new color.
	 *
	 * @param argb the argb
	 */
	public Color(int argb)
	{
		set(argb);
	}

	/**
	 * Instantiates a new color.
	 *
	 * @param red the red
	 * @param green the green
	 * @param blue the blue
	 */
	public Color(int red, int green, int blue)
	{
		this(red / 255f, green / 255f, blue / 255f, 1.0f);
	}

	/**
	 * Instantiates a new color.
	 *
	 * @param red the red
	 * @param green the green
	 * @param blue the blue
	 * @param alpha the alpha
	 */
	public Color(float red, float green, float blue, float alpha)
	{
		this.red = red;
		this.green = green;
		this.blue = blue;
		this.alpha = alpha;
	}

	/**
	 * Instantiates a new color.
	 *
	 * @param color the color
	 */
	public Color(Color color)
	{
		this.red = color.red;
		this.green = color.green;
		this.blue = color.blue;
		this.alpha = color.alpha;
	}

	/**
	 * Gets the red.
	 *
	 * @return the red
	 */
	public float getRed()
	{
		return red;
	}

	/**
	 * Gets the green.
	 *
	 * @return the green
	 */
	public float getGreen()
	{
		return green;
	}

	/**
	 * Gets the blue.
	 *
	 * @return the blue
	 */
	public float getBlue()
	{
		return blue;
	}

	/**
	 * Gets the alpha.
	 *
	 * @return the alpha
	 */
	public float getAlpha()
	{
		return alpha;
	}

	/**
	 * Sets the.
	 *
	 * @param argb the argb
	 */
	public void set(int argb)
	{
		int a = (argb >> 24) & 0xFF;
		int r = (argb >> 16) & 0xFF;
		int g = (argb >> 8) & 0xFF;
		int b = (argb >> 0) & 0xFF;
		red = r / 255f;
		green = g / 255f;
		blue = b / 255f;
		alpha = a / 255f;
	}

	/**
	 * Sets the red.
	 *
	 * @param red the new red
	 */
	public void setRed(float red)
	{
		this.red = red;
	}

	/**
	 * Sets the green.
	 *
	 * @param green the new green
	 */
	public void setGreen(float green)
	{
		this.green = green;
	}

	/**
	 * Sets the blue.
	 *
	 * @param blue the new blue
	 */
	public void setBlue(float blue)
	{
		this.blue = blue;
	}

	/**
	 * Sets the alpha.
	 *
	 * @param alpha the new alpha
	 */
	public void setAlpha(float alpha)
	{
		this.alpha = alpha;
	}

	/**
	 * Scale.
	 *
	 * @param value the value
	 * @return the color
	 */
	public Color scale(float value)
	{
		return new Color(red * value, green * value, blue * value, 1.0f);
	}

	@Override
	public String toString()
	{
		return "Color[r=" + red + ", g=" + green + ", b=" + blue + ", a=" + alpha + "]";
	}

}
