package engine.graphics.font;

/**
 * The Class Glyph.
 */
public class Glyph
{

	/** The x. */
	private int x;
	
	/** The y. */
	private int y;
	
	/** The width. */
	private int width;

	/**
	 * Instantiates a new glyph.
	 *
	 * @param x the x
	 * @param y the y
	 * @param width the width
	 */
	public Glyph(int x, int y, int width)
	{
		this.x = x;
		this.y = y;
		this.width = width;
	}

	/**
	 * Gets the x.
	 *
	 * @return the x
	 */
	public int getX()
	{
		return x;
	}

	/**
	 * Gets the y.
	 *
	 * @return the y
	 */
	public int getY()
	{
		return y;
	}

	/**
	 * Gets the width.
	 *
	 * @return the width
	 */
	public int getWidth()
	{
		return width;
	}

	/**
	 * Sets the x.
	 *
	 * @param x the new x
	 */
	public void setX(int x)
	{
		this.x = x;
	}

	/**
	 * Sets the y.
	 *
	 * @param y the new y
	 */
	public void setY(int y)
	{
		this.y = y;
	}

	/**
	 * Sets the width.
	 *
	 * @param width the new width
	 */
	public void setWidth(int width)
	{
		this.width = width;
	}

}
