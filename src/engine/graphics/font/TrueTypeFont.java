package engine.graphics.font;

import static org.lwjgl.opengl.GL11.GL_BLEND;
import static org.lwjgl.opengl.GL11.GL_BLEND_DST;
import static org.lwjgl.opengl.GL11.GL_BLEND_SRC;
import static org.lwjgl.opengl.GL11.GL_ONE_MINUS_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.glBlendFunc;
import static org.lwjgl.opengl.GL11.glDisable;
import static org.lwjgl.opengl.GL11.*;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Map;

import engine.graphics.Color;
import engine.graphics.batch.SpriteBatch;
import engine.graphics.texture.Texture;
import engine.graphics.texture.TextureData;

/**
 * The Class TrueTypeFont.
 */
public class TrueTypeFont
{

	/** The loaded fonts. */
	private static Map<String, TrueTypeFont> loadedFonts;

	/** The sheet width. */
	private final int SHEET_WIDTH;
	
	/** The sheet height. */
	private final int SHEET_HEIGHT;
	
	/** The font texture. */
	private Texture fontTexture;
	
	/** The character height. */
	private int characterHeight;
	
	/** The characters tall. */
	private int charactersTall;
	
	/** The glyphs. */
	private Glyph[] glyphs;

	static
	{
		loadedFonts = new HashMap<String, TrueTypeFont>();
	}

	/**
	 * Instantiates a new true type font.
	 *
	 * @param fontName the font name
	 * @param size the size
	 * @param antialias the antialias
	 */
	public TrueTypeFont(String fontName, int size, boolean antialias)
	{
		Font awtFont = new Font(fontName, Font.PLAIN, size);
		BufferedImage result = new BufferedImage(1, 1, BufferedImage.TYPE_INT_RGB);
		Graphics g = result.getGraphics();
		FontMetrics metrics = g.getFontMetrics(awtFont);

		int[] widths = metrics.getWidths();
		SHEET_WIDTH = 512;
		glyphs = new Glyph[256];
		characterHeight = metrics.getMaxDescent() + metrics.getMaxAscent();
		for (int i = 0, x = 0; i < 256; i++)
		{
			if (x + widths[i] >= SHEET_WIDTH)
			{
				charactersTall++;
				x = 0;
			}
			glyphs[i] = new Glyph(x, charactersTall * characterHeight + characterHeight, widths[i]);
			x += glyphs[i].getWidth();
		}
		charactersTall++;

		SHEET_HEIGHT = charactersTall * characterHeight;
		result = new BufferedImage(SHEET_WIDTH, SHEET_HEIGHT, BufferedImage.TYPE_INT_RGB);

		g = result.getGraphics();
		g.setFont(awtFont);

		if (antialias)
			((Graphics2D) g).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		for (int i = 0; i < glyphs.length; i++)
		{
			Glyph c = glyphs[i];
			if (i < 256)
			{
				g.drawString(Character.toString((char) i), c.getX(), c.getY() - metrics.getMaxDescent());
			}
		}

		fontTexture = new Texture(new TextureData(result, 0x00000000));

		loadedFonts.put(fontName + "-" + size + "-" + antialias, this);
	}

	/**
	 * Gets the true type font.
	 *
	 * @param fontName the font name
	 * @param size the size
	 * @param antialias the antialias
	 * @return the true type font
	 */
	public static TrueTypeFont getTrueTypeFont(String fontName, int size, boolean antialias)
	{
		return loadedFonts.get(fontName + "-" + size + "-" + antialias);
	}

	/**
	 * Checks if is true type font loaded.
	 *
	 * @param fontName the font name
	 * @param size the size
	 * @param antialias the antialias
	 * @return true, if is true type font loaded
	 */
	public static boolean isTrueTypeFontLoaded(String fontName, int size, boolean antialias)
	{
		return loadedFonts.containsKey(fontName + "-" + size + "-" + antialias);
	}

	/**
	 * Gets the font texture.
	 *
	 * @return the font texture
	 */
	public Texture getFontTexture()
	{
		return fontTexture;
	}

	/**
	 * Gets the character height.
	 *
	 * @return the character height
	 */
	public int getCharacterHeight()
	{
		return characterHeight;
	}

	/**
	 * Gets the characters tall.
	 *
	 * @return the characters tall
	 */
	public int getCharactersTall()
	{
		return charactersTall;
	}

	/**
	 * Gets the character normalized x.
	 *
	 * @param c the c
	 * @return the character normalized x
	 */
	public float getCharacterNormalizedX(char c)
	{
		return glyphs[c].getX() / (float) SHEET_WIDTH;
	}

	/**
	 * Gets the character normalized y.
	 *
	 * @param c the c
	 * @return the character normalized y
	 */
	public float getCharacterNormalizedY(char c)
	{
		return 1 - (glyphs[c].getY()) / (float) SHEET_HEIGHT;
	}

	/**
	 * Gets the string width.
	 *
	 * @param s the s
	 * @return the string width
	 */
	public int getStringWidth(String s)
	{
		int width = 0;
		char[] chars = s.toCharArray();
		for (char c : chars)
			width += glyphs[c].getWidth();
		return width;
	}

	/**
	 * Gets the character width.
	 *
	 * @param c the c
	 * @return the character width
	 */
	public int getCharacterWidth(char c)
	{
		return glyphs[c].getWidth();
	}

	/**
	 * Render.
	 *
	 * @param batch the batch
	 * @param text the text
	 * @param color the color
	 * @param x the x
	 * @param y the y
	 */
	public void render(SpriteBatch batch, String text, Color color, float x, float y)
	{

		int blendSrc = glGetInteger(GL_BLEND_SRC);
		int blendDst = glGetInteger(GL_BLEND_DST);

		boolean blendEnabled = glGetBoolean(GL_BLEND);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		char[] chars = text.toCharArray();

		float cx = x;

		float h = (float) characterHeight / SHEET_HEIGHT;
		Color old = batch.getTintColor();
		batch.setTintColor(color);
		for (char c : chars)
		{
			Glyph g = glyphs[c];
			float w = (float) g.getWidth() / SHEET_WIDTH;
			batch.renderTexture(fontTexture, cx, y, getCharacterNormalizedX(c), getCharacterNormalizedY(c), w, h, 0, 0, 0, 1, 1);
			cx += g.getWidth();
		}
		batch.setTintColor(old);
		batch.flush();

		if (!blendEnabled)
			glDisable(GL_BLEND);
		
		glBlendFunc(blendSrc, blendDst);
	}

}
