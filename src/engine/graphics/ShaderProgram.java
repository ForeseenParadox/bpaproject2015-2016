package engine.graphics;

import static org.lwjgl.opengl.GL11.GL_TRUE;
import static org.lwjgl.opengl.GL20.GL_COMPILE_STATUS;
import static org.lwjgl.opengl.GL20.GL_FRAGMENT_SHADER;
import static org.lwjgl.opengl.GL20.GL_LINK_STATUS;
import static org.lwjgl.opengl.GL20.GL_VERTEX_SHADER;
import static org.lwjgl.opengl.GL20.glAttachShader;
import static org.lwjgl.opengl.GL20.glBindAttribLocation;
import static org.lwjgl.opengl.GL20.glCompileShader;
import static org.lwjgl.opengl.GL20.glCreateProgram;
import static org.lwjgl.opengl.GL20.glCreateShader;
import static org.lwjgl.opengl.GL20.glGetProgramInfoLog;
import static org.lwjgl.opengl.GL20.glGetProgrami;
import static org.lwjgl.opengl.GL20.glGetShaderInfoLog;
import static org.lwjgl.opengl.GL20.glGetShaderi;
import static org.lwjgl.opengl.GL20.glGetUniformLocation;
import static org.lwjgl.opengl.GL20.glLinkProgram;
import static org.lwjgl.opengl.GL20.glShaderSource;
import static org.lwjgl.opengl.GL20.glUniform1f;
import static org.lwjgl.opengl.GL20.glUniform2f;
import static org.lwjgl.opengl.GL20.glUniform3f;
import static org.lwjgl.opengl.GL20.glUniform4f;
import static org.lwjgl.opengl.GL20.glUniformMatrix4fv;
import static org.lwjgl.opengl.GL20.glUseProgram;

import engine.resource.ShaderResource;
import engine.util.BufferUtil;
import engine.util.math.Matrix4f;
import engine.util.math.Vector2f;
import engine.util.math.Vector3f;
import engine.util.math.Vector4f;

/**
 * The Class ShaderProgram.
 */
public class ShaderProgram
{

	/** The program handle. */
	private int programHandle;
	
	/** The vertex handle. */
	private int vertexHandle;
	
	/** The fragment handle. */
	private int fragmentHandle;

	/**
	 * Instantiates a new shader program.
	 *
	 * @param resource the resource
	 * @param attribs the attribs
	 */
	public ShaderProgram(ShaderResource resource, VertexAttribute[] attribs)
	{
		this(resource.getVertexComponent().getLoadedText(), resource.getFragmentComponent().getLoadedText(), attribs);
	}

	/**
	 * Instantiates a new shader program.
	 *
	 * @param vertexSource the vertex source
	 * @param fragmentSource the fragment source
	 * @param attribs the attribs
	 */
	public ShaderProgram(String vertexSource, String fragmentSource, VertexAttribute[] attribs)
	{
		programHandle = glCreateProgram();
		createVertexShader(vertexSource);
		createFragmentShader(fragmentSource);
		for (VertexAttribute attrib : attribs)
			glBindAttribLocation(programHandle, attrib.getIndex(), attrib.getName());
		linkProgram();
	}

	/**
	 * Gets the program handle.
	 *
	 * @return the program handle
	 */
	public int getProgramHandle()
	{
		return programHandle;
	}

	/**
	 * Gets the vertex handle.
	 *
	 * @return the vertex handle
	 */
	public int getVertexHandle()
	{
		return vertexHandle;
	}

	/**
	 * Gets the fragment handle.
	 *
	 * @return the fragment handle
	 */
	public int getFragmentHandle()
	{
		return fragmentHandle;
	}

	/**
	 * Creates the vertex shader.
	 *
	 * @param source the source
	 */
	public void createVertexShader(String source)
	{
		vertexHandle = createShader(GL_VERTEX_SHADER, source);
	}

	/**
	 * Creates the fragment shader.
	 *
	 * @param source the source
	 */
	public void createFragmentShader(String source)
	{
		fragmentHandle = createShader(GL_FRAGMENT_SHADER, source);
	}

	/**
	 * Creates the shader.
	 *
	 * @param type the type
	 * @param source the source
	 * @return the int
	 */
	private int createShader(int type, String source)
	{
		int handle = glCreateShader(type);

		glShaderSource(handle, source);
		glCompileShader(handle);

		int status = glGetShaderi(handle, GL_COMPILE_STATUS);
		if (status != GL_TRUE)
		{
			System.err.println("Could not compile OpenGL shader with status code: " + status);
			System.err.println(glGetShaderInfoLog(handle));
			return 1;
		} else
		{
			return handle;
		}
	}

	/**
	 * Link program.
	 */
	public void linkProgram()
	{
		glAttachShader(programHandle, vertexHandle);
		glAttachShader(programHandle, fragmentHandle);

		glLinkProgram(programHandle);

		int status = glGetProgrami(programHandle, GL_LINK_STATUS);

		if (status != GL_TRUE)
		{
			System.err.println("Failed to link OpenGL shader with status code: " + status);
			System.err.println(glGetProgramInfoLog(programHandle));
		}
	}

	/**
	 * Bind program.
	 */
	public void bindProgram()
	{
		glUseProgram(programHandle);
	}

	/**
	 * Gets the uniform location.
	 *
	 * @param uniformName the uniform name
	 * @return the uniform location
	 */
	// uniform manipulation
	public int getUniformLocation(String uniformName)
	{
		return glGetUniformLocation(programHandle, uniformName);
	}

	/**
	 * Sets the matrix4f.
	 *
	 * @param uniformName the uniform name
	 * @param value the value
	 */
	public void setMatrix4f(String uniformName, Matrix4f value)
	{
		glUniformMatrix4fv(getUniformLocation(uniformName), true, BufferUtil.matrix4fToFloatBuffer(value));
	}

	/**
	 * Sets the vector2f.
	 *
	 * @param uniformName the uniform name
	 * @param value the value
	 */
	public void setVector2f(String uniformName, Vector2f value)
	{
		glUniform2f(getUniformLocation(uniformName), value.getX(), value.getY());
	}

	/**
	 * Sets the vector2f.
	 *
	 * @param uniformName the uniform name
	 * @param x the x
	 * @param y the y
	 */
	public void setVector2f(String uniformName, float x, float y)
	{
		glUniform2f(getUniformLocation(uniformName), x, y);
	}

	/**
	 * Sets the vector3f.
	 *
	 * @param uniformName the uniform name
	 * @param value the value
	 */
	public void setVector3f(String uniformName, Vector3f value)
	{
		glUniform3f(getUniformLocation(uniformName), value.getX(), value.getY(), value.getZ());
	}

	/**
	 * Sets the vector3f.
	 *
	 * @param uniformName the uniform name
	 * @param x the x
	 * @param y the y
	 * @param z the z
	 */
	public void setVector3f(String uniformName, float x, float y, float z)
	{
		glUniform3f(getUniformLocation(uniformName), x, y, z);
	}

	/**
	 * Sets the vector4f.
	 *
	 * @param uniformName the uniform name
	 * @param value the value
	 */
	public void setVector4f(String uniformName, Vector4f value)
	{
		glUniform4f(getUniformLocation(uniformName), value.getX(), value.getY(), value.getZ(), value.getW());
	}

	/**
	 * Sets the vector4f.
	 *
	 * @param uniformName the uniform name
	 * @param x the x
	 * @param y the y
	 * @param z the z
	 * @param w the w
	 */
	public void setVector4f(String uniformName, float x, float y, float z, float w)
	{
		glUniform4f(getUniformLocation(uniformName), x, y, z, w);
	}

	/**
	 * Sets the color.
	 *
	 * @param uniformName the uniform name
	 * @param value the value
	 */
	public void setColor(String uniformName, Color value)
	{
		glUniform4f(getUniformLocation(uniformName), value.getRed(), value.getGreen(), value.getBlue(), value.getAlpha());
	}

	/**
	 * Sets the float.
	 *
	 * @param uniformName the uniform name
	 * @param value the value
	 */
	public void setFloat(String uniformName, float value)
	{
		glUniform1f(getUniformLocation(uniformName), value);
	}

}