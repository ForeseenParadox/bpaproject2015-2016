package engine.graphics;

import static org.lwjgl.opengl.EXTFramebufferObject.GL_COLOR_ATTACHMENT0_EXT;
import static org.lwjgl.opengl.EXTFramebufferObject.GL_FRAMEBUFFER_COMPLETE_EXT;
import static org.lwjgl.opengl.EXTFramebufferObject.GL_FRAMEBUFFER_EXT;
import static org.lwjgl.opengl.EXTFramebufferObject.glBindFramebufferEXT;
import static org.lwjgl.opengl.EXTFramebufferObject.glCheckFramebufferStatusEXT;
import static org.lwjgl.opengl.EXTFramebufferObject.glFramebufferTexture2DEXT;
import static org.lwjgl.opengl.EXTFramebufferObject.glGenFramebuffersEXT;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;

import engine.graphics.texture.Texture;

/**
 * The Class FrameBuffer.
 */
public class FrameBuffer
{

	/** The handle. */
	private int handle;
	
	/** The color buffer. */
	private Texture colorBuffer;
	
	/** The width. */
	private int width;
	
	/** The height. */
	private int height;

	/**
	 * Instantiates a new frame buffer.
	 *
	 * @param w the w
	 * @param h the h
	 */
	public FrameBuffer(int w, int h)
	{
		width = w;
		height = h;
		handle = glGenFramebuffersEXT();
		colorBuffer = new Texture().loadEmptyTexture(w, h);
		attachTextureToColorBuffer(colorBuffer);
	}

	/**
	 * Gets the handle.
	 *
	 * @return the handle
	 */
	public int getHandle()
	{
		return handle;
	}

	/**
	 * Gets the color buffer.
	 *
	 * @return the color buffer
	 */
	public Texture getColorBuffer()
	{
		return colorBuffer;
	}

	/**
	 * Gets the width.
	 *
	 * @return the width
	 */
	public int getWidth()
	{
		return width;
	}

	/**
	 * Gets the height.
	 *
	 * @return the height
	 */
	public int getHeight()
	{
		return height;
	}

	/**
	 * Attach texture to color buffer.
	 *
	 * @param t the t
	 */
	public void attachTextureToColorBuffer(Texture t)
	{
		bind();

		t.bind();
		glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, t.getHandle(), 0);

		int status = glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT);
		if (status != GL_FRAMEBUFFER_COMPLETE_EXT)
		{
			System.err.println("Error binding texture to frame buffer, FBO returned status " + status);
		}

		unbind();
		colorBuffer = t;
	}

	/**
	 * Bind.
	 */
	public void bind()
	{
		glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, handle);
	}

	/**
	 * Unbind.
	 */
	public void unbind()
	{
		glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
	}
}
