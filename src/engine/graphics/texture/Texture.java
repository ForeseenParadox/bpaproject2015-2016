package engine.graphics.texture;

import static org.lwjgl.opengl.GL11.GL_LINEAR;
import static org.lwjgl.opengl.GL11.GL_NEAREST;
import static org.lwjgl.opengl.GL11.GL_REPEAT;
import static org.lwjgl.opengl.GL11.GL_RGBA;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_MAG_FILTER;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_MIN_FILTER;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_WRAP_S;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_WRAP_T;
import static org.lwjgl.opengl.GL11.GL_UNSIGNED_BYTE;
import static org.lwjgl.opengl.GL11.GL_UNSIGNED_INT;
import static org.lwjgl.opengl.GL11.glBindTexture;
import static org.lwjgl.opengl.GL11.glGenTextures;
import static org.lwjgl.opengl.GL11.glTexImage2D;
import static org.lwjgl.opengl.GL11.glTexParameteri;

import java.util.HashMap;
import java.util.Map;

import engine.resource.TextureResource;

/**
 * The Class Texture.
 */
public class Texture
{

	/** The Constant NEAREST_FILTER. */
	// filtering constants
	public static final int NEAREST_FILTER = GL_NEAREST;
	
	/** The Constant LINEAR_FILTER. */
	public static final int LINEAR_FILTER = GL_LINEAR;

	/** The Constant WRAP_REPEAT. */
	// wrap constants
	public static final int WRAP_REPEAT = GL_REPEAT;

	/** The loaded textures. */
	// texture map
	private static Map<String, Texture> loadedTextures;

	/** The width. */
	private int width;
	
	/** The height. */
	private int height;
	
	/** The data. */
	private TextureData data;
	
	/** The handle. */
	private int handle;

	static
	{
		loadedTextures = new HashMap<String, Texture>();
	}

	/**
	 * Instantiates a new texture.
	 */
	public Texture()
	{
		this((String) null);
	}

	/**
	 * Instantiates a new texture.
	 *
	 * @param textureName the texture name
	 */
	public Texture(String textureName)
	{
		handle = glGenTextures();

		if (textureName != null)
			loadedTextures.put(textureName, this);
	}

	/**
	 * Instantiates a new texture.
	 *
	 * @param resource the resource
	 */
	public Texture(TextureResource resource)
	{
		this(null, resource);
	}

	/**
	 * Instantiates a new texture.
	 *
	 * @param textureName the texture name
	 * @param resource the resource
	 */
	public Texture(String textureName, TextureResource resource)
	{
		this(textureName, new TextureData(resource));
	}

	/**
	 * Instantiates a new texture.
	 *
	 * @param data the data
	 */
	public Texture(TextureData data)
	{
		this(null, data);
	}

	/**
	 * Instantiates a new texture.
	 *
	 * @param textureName the texture name
	 * @param data the data
	 */
	public Texture(String textureName, TextureData data)
	{
		handle = glGenTextures();
		load(data);

		if (textureName != null)
			loadedTextures.put(textureName, this);
	}

	/**
	 * Gets the texture.
	 *
	 * @param name the name
	 * @return the texture
	 */
	public static Texture getTexture(String name)
	{
		return loadedTextures.get(name);
	}

	/**
	 * Checks if is texture loaded.
	 *
	 * @param name the name
	 * @return true, if is texture loaded
	 */
	public static boolean isTextureLoaded(String name)
	{
		return loadedTextures.containsKey(name);
	}

	/**
	 * Gets the width.
	 *
	 * @return the width
	 */
	public int getWidth()
	{
		return width;
	}

	/**
	 * Gets the height.
	 *
	 * @return the height
	 */
	public int getHeight()
	{
		return height;
	}

	/**
	 * Gets the data.
	 *
	 * @return the data
	 */
	public TextureData getData()
	{
		return data;
	}

	/**
	 * Gets the handle.
	 *
	 * @return the handle
	 */
	public int getHandle()
	{
		return handle;
	}

	/**
	 * Bind.
	 */
	public void bind()
	{
		glBindTexture(GL_TEXTURE_2D, handle);
	}

	/**
	 * Load empty texture.
	 *
	 * @param w the w
	 * @param h the h
	 * @return the texture
	 */
	public Texture loadEmptyTexture(int w, int h)
	{

		bind();

		width = w;
		height = h;

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_INT, (java.nio.ByteBuffer) null);
		return this;
	}

	/**
	 * Sets the filter.
	 *
	 * @param mag the mag
	 * @param min the min
	 */
	public void setFilter(int mag, int min)
	{
		bind();

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, min);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, mag);
	}

	/**
	 * Sets the wrap.
	 *
	 * @param s the s
	 * @param t the t
	 */
	public void setWrap(int s, int t)
	{
		bind();

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, s);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, t);
	}

	/**
	 * Load.
	 *
	 * @param resource the resource
	 */
	public void load(TextureResource resource)
	{
		load(new TextureData(resource));
	}

	/**
	 * Load.
	 *
	 * @param data the data
	 */
	public void load(TextureData data)
	{
		width = data.getWidth();
		height = data.getHeight();

		this.data = data;

		bind();

		setFilter(NEAREST_FILTER, NEAREST_FILTER);
		setWrap(WRAP_REPEAT, WRAP_REPEAT);

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data.getDataAsByteBuffer());
	}

}
