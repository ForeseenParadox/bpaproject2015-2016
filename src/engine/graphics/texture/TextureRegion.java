package engine.graphics.texture;

import java.util.HashMap;
import java.util.Map;

/**
 * The Class TextureRegion.
 */
public class TextureRegion
{

	/** The loaded regions. */
	private static Map<String, TextureRegion> loadedRegions;

	/** The texture. */
	private Texture texture;
	
	/** The x. */
	private float x;
	
	/** The y. */
	private float y;
	
	/** The w. */
	private float w;
	
	/** The h. */
	private float h;

	static
	{
		loadedRegions = new HashMap<String, TextureRegion>();
	}

	/**
	 * Instantiates a new texture region.
	 *
	 * @param texture the texture
	 */
	public TextureRegion(Texture texture)
	{
		this(null, texture);
	}

	/**
	 * Instantiates a new texture region.
	 *
	 * @param id the id
	 * @param texture the texture
	 */
	public TextureRegion(String id, Texture texture)
	{
		this.texture = texture;
		if (id != null)
			loadedRegions.put(id, this);
	}

	/**
	 * Instantiates a new texture region.
	 *
	 * @param texture the texture
	 * @param x the x
	 * @param y the y
	 * @param w the w
	 * @param h the h
	 */
	public TextureRegion(Texture texture, float x, float y, float w, float h)
	{
		this(null, texture, x, y, w, h);
	}

	/**
	 * Instantiates a new texture region.
	 *
	 * @param id the id
	 * @param texture the texture
	 * @param x the x
	 * @param y the y
	 * @param w the w
	 * @param h the h
	 */
	public TextureRegion(String id, Texture texture, float x, float y, float w, float h)
	{
		this.texture = texture;
		setX(x);
		setY(y);
		setWidth(w);
		setHeight(h);
		
		if (id != null)
			loadedRegions.put(id, this);
	}

	/**
	 * Gets the texture region.
	 *
	 * @param id the id
	 * @return the texture region
	 */
	public static TextureRegion getTextureRegion(String id)
	{
		return loadedRegions.get(id);
	}

	/**
	 * Checks if is texture region loaded.
	 *
	 * @param name the name
	 * @return true, if is texture region loaded
	 */
	public static boolean isTextureRegionLoaded(String name)
	{
		return loadedRegions.containsKey(name);
	}

	/**
	 * Gets the texture.
	 *
	 * @return the texture
	 */
	public Texture getTexture()
	{
		return texture;
	}

	/**
	 * Gets the x.
	 *
	 * @return the x
	 */
	public float getX()
	{
		return x * texture.getWidth();
	}

	/**
	 * Gets the normalized x.
	 *
	 * @return the normalized x
	 */
	public float getNormalizedX()
	{
		return x;
	}

	/**
	 * Gets the y.
	 *
	 * @return the y
	 */
	public float getY()
	{
		return y * texture.getHeight();
	}

	/**
	 * Gets the normalized y.
	 *
	 * @return the normalized y
	 */
	public float getNormalizedY()
	{
		return y;
	}

	/**
	 * Gets the width.
	 *
	 * @return the width
	 */
	public float getWidth()
	{
		return w * texture.getWidth();
	}

	/**
	 * Gets the normalized width.
	 *
	 * @return the normalized width
	 */
	public float getNormalizedWidth()
	{
		return w;
	}

	/**
	 * Gets the height.
	 *
	 * @return the height
	 */
	public float getHeight()
	{
		return h * texture.getHeight();
	}

	/**
	 * Gets the normalized height.
	 *
	 * @return the normalized height
	 */
	public float getNormalizedHeight()
	{
		return h;
	}

	/**
	 * Sets the texture.
	 *
	 * @param texture the new texture
	 */
	public void setTexture(Texture texture)
	{
		this.texture = texture;
	}

	/**
	 * Sets the x.
	 *
	 * @param x the new x
	 */
	public void setX(float x)
	{
		this.x = x / texture.getWidth();
	}

	/**
	 * Sets the normalized x.
	 *
	 * @param x the new normalized x
	 */
	public void setNormalizedX(float x)
	{
		this.x = x;
	}

	/**
	 * Sets the y.
	 *
	 * @param y the new y
	 */
	public void setY(float y)
	{
		this.y = y / texture.getHeight();
	}

	/**
	 * Sets the normalized y.
	 *
	 * @param y the new normalized y
	 */
	public void setNormalizedY(float y)
	{
		this.y = y;
	}

	/**
	 * Sets the width.
	 *
	 * @param w the new width
	 */
	public void setWidth(float w)
	{
		this.w = w / texture.getWidth();
	}

	/**
	 * Sets the normalized width.
	 *
	 * @param w the new normalized width
	 */
	public void setNormalizedWidth(float w)
	{
		this.w = w;
	}

	/**
	 * Sets the height.
	 *
	 * @param h the new height
	 */
	public void setHeight(float h)
	{
		this.h = h / texture.getHeight();
	}

	/**
	 * Sets the normalized height.
	 *
	 * @param h the new normalized height
	 */
	public void setNormalizedHeight(float h)
	{
		this.h = h;
	}

}
