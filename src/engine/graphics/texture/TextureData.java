package engine.graphics.texture;

import java.awt.image.BufferedImage;
import java.nio.ByteBuffer;

import org.lwjgl.BufferUtils;

import engine.resource.TextureResource;

/**
 * The Class TextureData.
 */
public class TextureData
{

	/** The width. */
	private int width;
	
	/** The height. */
	private int height;
	
	/** The data. */
	private int[] data;

	/**
	 * Instantiates a new texture data.
	 *
	 * @param resource the resource
	 */
	public TextureData(TextureResource resource)
	{
		this(resource, -1);
	}

	/**
	 * Instantiates a new texture data.
	 *
	 * @param resource the resource
	 * @param transparency the transparency
	 */
	public TextureData(TextureResource resource, int transparency)
	{
		this(resource.getWidth(), resource.getHeight(), resource.getData(), transparency);
	}

	/**
	 * Instantiates a new texture data.
	 *
	 * @param width the width
	 * @param height the height
	 * @param data the data
	 */
	public TextureData(int width, int height, int[] data)
	{
		this(width, height, data, -1);
	}

	/**
	 * Instantiates a new texture data.
	 *
	 * @param width the width
	 * @param height the height
	 * @param data the data
	 * @param transparency the transparency
	 */
	public TextureData(int width, int height, int[] data, int transparency)
	{
		this.width = width;
		this.height = height;
		this.data = data;

		replaceColorWithTransparency(transparency);
	}

	/**
	 * Instantiates a new texture data.
	 *
	 * @param image the image
	 */
	public TextureData(BufferedImage image)
	{
		this(image, -1);
	}

	/**
	 * Instantiates a new texture data.
	 *
	 * @param image the image
	 * @param transparency the transparency
	 */
	public TextureData(BufferedImage image, int transparency)
	{
		this.width = image.getWidth();
		this.height = image.getHeight();
		data = new int[width * height];
		image.getRGB(0, 0, width, height, data, 0, width);
		replaceColorWithTransparency(transparency);
	}

	/**
	 * Replace color with transparency.
	 *
	 * @param transparency the transparency
	 */
	public void replaceColorWithTransparency(int transparency)
	{
		if (transparency >= 0)
			for (int i = 0; i < data.length; i++)
				if ((data[i] & 0x00FFFFFF) == transparency)
					data[i] = 0x00000000;
	}

	/**
	 * Gets the width.
	 *
	 * @return the width
	 */
	public int getWidth()
	{
		return width;
	}

	/**
	 * Gets the height.
	 *
	 * @return the height
	 */
	public int getHeight()
	{
		return height;
	}

	/**
	 * Gets the data.
	 *
	 * @return the data
	 */
	public int[] getData()
	{
		return data;
	}

	/**
	 * Gets the data as byte buffer.
	 *
	 * @return the data as byte buffer
	 */
	public ByteBuffer getDataAsByteBuffer()
	{
		ByteBuffer res = BufferUtils.createByteBuffer(data.length * 4);
		for (int y = 0; y < height; y++)
		{
			for (int x = 0; x < width; x++)
			{
				int i = data[x + (height - y - 1) * width];
				res.put((byte) ((i >> 16) & 0xFF));
				res.put((byte) ((i >> 8) & 0xFF));
				res.put((byte) ((i >> 0) & 0xFF));
				res.put((byte) ((i >> 24) & 0xFF));
			}
		}
		res.flip();
		return res;
	}

	/**
	 * Reallocate.
	 *
	 * @param w the w
	 * @param h the h
	 */
	public void reallocate(int w, int h)
	{
		reallocate(w, h, false);
	}

	/**
	 * Reallocate.
	 *
	 * @param w the w
	 * @param h the h
	 * @param move the move
	 */
	public void reallocate(int w, int h, boolean move)
	{
		int[] newAlloc = new int[w * h];
		for (int i = 0; i < w && i < width; i++)
			for (int j = 0; j < h && j < height; j++)
				newAlloc[j + i * h] = data[j + i * height];
		width = w;
		height = h;
		data = newAlloc;
	}

}
