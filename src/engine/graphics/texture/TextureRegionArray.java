package engine.graphics.texture;

import java.util.HashMap;
import java.util.Map;

/**
 * The Class TextureRegionArray.
 */
public class TextureRegionArray
{

	/** The loaded region arrays. */
	private static Map<String, TextureRegionArray> loadedRegionArrays;

	/** The region array. */
	private TextureRegion[] regionArray;

	static
	{
		loadedRegionArrays = new HashMap<String, TextureRegionArray>();
	}

	/**
	 * Instantiates a new texture region array.
	 *
	 * @param count the count
	 */
	public TextureRegionArray(int count)
	{
		this(null, count);
	}

	/**
	 * Instantiates a new texture region array.
	 *
	 * @param id the id
	 * @param count the count
	 */
	public TextureRegionArray(String id, int count)
	{
		regionArray = new TextureRegion[count];

		if (id != null)
			loadedRegionArrays.put(id, this);
	}

	/**
	 * Gets the texture region array.
	 *
	 * @param name the name
	 * @return the texture region array
	 */
	public static TextureRegionArray getTextureRegionArray(String name)
	{
		return loadedRegionArrays.get(name);
	}

	/**
	 * Checks if is texture region array loaded.
	 *
	 * @param name the name
	 * @return true, if is texture region array loaded
	 */
	public static boolean isTextureRegionArrayLoaded(String name)
	{
		return loadedRegionArrays.containsKey(name);
	}

	/**
	 * Gets the region array.
	 *
	 * @return the region array
	 */
	public TextureRegion[] getRegionArray()
	{
		return regionArray;
	}

	/**
	 * Gets the region.
	 *
	 * @param index the index
	 * @return the region
	 */
	public TextureRegion getRegion(int index)
	{
		return regionArray[index];
	}

	/**
	 * Length.
	 *
	 * @return the int
	 */
	public int length()
	{
		return regionArray.length;
	}

	/**
	 * Sets the region.
	 *
	 * @param index the index
	 * @param region the region
	 */
	public void setRegion(int index, TextureRegion region)
	{
		regionArray[index] = region;
	}

}
