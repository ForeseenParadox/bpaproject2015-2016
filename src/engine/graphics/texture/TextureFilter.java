package engine.graphics.texture;

/**
 * The Class TextureFilter.
 */
public class TextureFilter
{

	/** The min filter. */
	private int minFilter;
	
	/** The mag filter. */
	private int magFilter;

	/**
	 * Instantiates a new texture filter.
	 */
	public TextureFilter()
	{
		this(0, 0);
	}

	/**
	 * Instantiates a new texture filter.
	 *
	 * @param minFilter the min filter
	 * @param magFilter the mag filter
	 */
	public TextureFilter(int minFilter, int magFilter)
	{
		this.minFilter = minFilter;
		this.magFilter = magFilter;
	}

	/**
	 * Gets the min filter.
	 *
	 * @return the min filter
	 */
	public int getMinFilter()
	{
		return minFilter;
	}

	/**
	 * Gets the mag filter.
	 *
	 * @return the mag filter
	 */
	public int getMagFilter()
	{
		return magFilter;
	}

	/**
	 * Sets the min filter.
	 *
	 * @param minFilter the new min filter
	 */
	public void setMinFilter(int minFilter)
	{
		this.minFilter = minFilter;
	}

	/**
	 * Sets the mag filter.
	 *
	 * @param magFilter the new mag filter
	 */
	public void setMagFilter(int magFilter)
	{
		this.magFilter = magFilter;
	}

}
