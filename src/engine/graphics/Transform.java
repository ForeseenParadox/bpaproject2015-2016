package engine.graphics;

import engine.util.math.Matrix4f;
import engine.util.math.Vector3f;

/**
 * The Class Transform.
 */
public class Transform
{

	/** The translation. */
	private Vector3f translation;
	
	/** The rotation. */
	private Vector3f rotation;
	
	/** The scale. */
	private Vector3f scale;

	/** The translation matrix. */
	private Matrix4f translationMatrix;
	
	/** The rotation x matrix. */
	private Matrix4f rotationXMatrix;
	
	/** The rotation y matrix. */
	private Matrix4f rotationYMatrix;
	
	/** The rotation z matrix. */
	private Matrix4f rotationZMatrix;
	
	/** The scale matrix. */
	private Matrix4f scaleMatrix;

	/**
	 * Instantiates a new transform.
	 */
	public Transform()
	{

		translation = new Vector3f();
		rotation = new Vector3f();
		scale = new Vector3f(1, 1, 1);

		translationMatrix = new Matrix4f();
		rotationXMatrix = new Matrix4f();
		rotationYMatrix = new Matrix4f();
		rotationZMatrix = new Matrix4f();
		scaleMatrix = new Matrix4f();
	}

	/**
	 * Gets the translation.
	 *
	 * @return the translation
	 */
	public Vector3f getTranslation()
	{
		return translation;
	}

	/**
	 * Gets the rotation.
	 *
	 * @return the rotation
	 */
	public Vector3f getRotation()
	{
		return rotation;
	}

	/**
	 * Gets the scale.
	 *
	 * @return the scale
	 */
	public Vector3f getScale()
	{
		return scale;
	}

	/**
	 * Gets the translation matrix.
	 *
	 * @return the translation matrix
	 */
	public Matrix4f getTranslationMatrix()
	{
		translationMatrix.setToTranslation(translation);
		return translationMatrix;
	}

	/**
	 * Gets the rotation x matrix.
	 *
	 * @return the rotation x matrix
	 */
	public Matrix4f getRotationXMatrix()
	{
		rotationXMatrix.setToRotationX(rotation.getX());
		return rotationXMatrix;
	}

	/**
	 * Gets the rotation y matrix.
	 *
	 * @return the rotation y matrix
	 */
	public Matrix4f getRotationYMatrix()
	{
		rotationYMatrix.setToRotationY(rotation.getY());
		return rotationYMatrix;
	}

	/**
	 * Gets the rotation z matrix.
	 *
	 * @return the rotation z matrix
	 */
	public Matrix4f getRotationZMatrix()
	{
		rotationXMatrix.setToRotationZ(rotation.getZ());
		return rotationZMatrix;
	}

	/**
	 * Gets the rotation matrix.
	 *
	 * @return the rotation matrix
	 */
	public Matrix4f getRotationMatrix()
	{
		return rotationXMatrix.multiply(rotationYMatrix).multiply(rotationZMatrix);
	}

	/**
	 * Gets the scale matrix.
	 *
	 * @return the scale matrix
	 */
	public Matrix4f getScaleMatrix()
	{
		scaleMatrix.setToScale(scale);
		return scaleMatrix;
	}

	/**
	 * Sets the translation.
	 *
	 * @param translation the new translation
	 */
	public void setTranslation(Vector3f translation)
	{
		this.translation = translation;
	}

	/**
	 * Sets the rotation.
	 *
	 * @param rotation the new rotation
	 */
	public void setRotation(Vector3f rotation)
	{
		this.rotation = rotation;
	}

	/**
	 * Sets the scale.
	 *
	 * @param scale the new scale
	 */
	public void setScale(Vector3f scale)
	{
		this.scale = scale;
	}

	/**
	 * Gets the transformation matrix.
	 *
	 * @return the transformation matrix
	 */
	public Matrix4f getTransformationMatrix()
	{
		return getTranslationMatrix().multiply(getRotationMatrix()).multiply(getScaleMatrix());
	}

}
