package engine.graphics;

import static org.lwjgl.opengl.GL15.*;

import java.nio.FloatBuffer;

import org.lwjgl.BufferUtils;

/**
 * The Class VertexBuffer.
 */
public class VertexBuffer
{

	/** The handle. */
	private int handle;
	
	/** The max store. */
	private int maxStore;
	
	/** The buffer. */
	private FloatBuffer buffer;

	/**
	 * Instantiates a new vertex buffer.
	 *
	 * @param maxStore the max store
	 */
	public VertexBuffer(int maxStore)
	{
		handle = glGenBuffers();
		this.maxStore = maxStore;
		buffer = BufferUtils.createFloatBuffer(maxStore);
	}

	/**
	 * Gets the handle.
	 *
	 * @return the handle
	 */
	public int getHandle()
	{
		return handle;
	}

	/**
	 * Gets the max store.
	 *
	 * @return the max store
	 */
	public int getMaxStore()
	{
		return maxStore;
	}

	/**
	 * Realloc.
	 *
	 * @param maxStore the max store
	 */
	public void realloc(int maxStore)
	{
		this.maxStore = maxStore;
		buffer.clear();
		buffer = BufferUtils.createFloatBuffer(maxStore);
	}

	/**
	 * Clear buffer.
	 */
	public void clearBuffer()
	{
		buffer.clear();
	}

	/**
	 * Put.
	 *
	 * @param data the data
	 * @return the vertex buffer
	 */
	public VertexBuffer put(float data)
	{
		buffer.put(data);
		return this;
	}

	/**
	 * Bind.
	 */
	public void bind()
	{
		glBindBuffer(GL_ARRAY_BUFFER, handle);
	}

	/**
	 * Push to gpu.
	 *
	 * @param usage the usage
	 */
	public void pushToGpu(int usage)
	{
		bind();
		buffer.flip();
		glBufferData(GL_ARRAY_BUFFER, buffer, usage);
	}

	/**
	 * Unbind buffer.
	 */
	public static void unbindBuffer()
	{
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
}
