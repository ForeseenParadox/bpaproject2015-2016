package engine.graphics.shader;

import engine.graphics.VertexAttribute;

/**
 * The Class ForwardShader.
 */
public class ForwardShader extends ShaderProgram
{

	/** The Constant VERTEX_SHADER. */
	public static final String VERTEX_SHADER = "#version 120\n uniform mat4 u_model; uniform mat4 u_view; uniform mat4 u_projection; attribute vec3 a_position; varying vec3 v_position; varying vec4 v_color; void main() { gl_Position = u_projection * u_view * (u_model * vec4(a_position, 1.0)); v_position = (u_view * (u_model * vec4(a_position, 1.0))).xyz;}";;
	
	/** The Constant FRAGMENT_SHADER. */
	public static final String FRAGMENT_SHADER = "#version 120\n varying vec4 v_color; void main() { gl_FragColor = vec4(1.0,1.0,1.0,1.0); }";

	/**
	 * Instantiates a new forward shader.
	 */
	public ForwardShader()
	{
		super(VERTEX_SHADER, FRAGMENT_SHADER, new VertexAttribute[]
		{
				new VertexAttribute(0, "a_position"),
				new VertexAttribute(0, "a_color") });
	}

}
