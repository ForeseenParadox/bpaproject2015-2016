package engine.graphics.gui;

import java.util.ArrayList;
import java.util.List;

import engine.audio.Audio;
import engine.core.Engine;
import engine.event.listener.ComponentListener;
import engine.input.Mouse;

/**
 * The Class Button.
 */
public abstract class Button extends SizedComponent
{

	/** The Constant DEFAULT_BLIP. */
	public static final String DEFAULT_BLIP = "Blip";

	/** The blip. */
	private Audio blip;
	
	/** The buttons pressed. */
	private List<Integer> buttonsPressed;
	
	/** The buttons just pressed. */
	private List<Integer> buttonsJustPressed;

	/**
	 * Instantiates a new button.
	 *
	 * @param p the p
	 * @param rx the rx
	 * @param ry the ry
	 * @param w the w
	 * @param h the h
	 */
	public Button(Component p, int rx, int ry, int w, int h)
	{
		super(p, rx, ry, w, h);

		// if (AudioSource.getAudioSource(DEFAULT_BLIP) != null)
		// blip = AudioSource.getAudioSource(DEFAULT_BLIP);

		buttonsPressed = new ArrayList<Integer>();
		buttonsJustPressed = new ArrayList<Integer>();

		setVerticalPadding(5);
		setHorizontalPadding(5);
	}

	/**
	 * Gets the blip.
	 *
	 * @return the blip
	 */
	public Audio getBlip()
	{
		return blip;
	}

	/**
	 * Sets the blip.
	 *
	 * @param b the new blip
	 */
	public void setBlip(Audio b)
	{
		blip = b;
	}

	@Override
	public void update(float delta)
	{
		super.update(delta);
		if (wasJustHovered() && blip != null)
		{
			blip.start();
		}

		for (int i = 0; i < Mouse.MAX_BUTTONS; i++)
		{
			Mouse m = Engine.getInstance().getInput().getMouse();
			if (buttonsJustPressed.contains(i))
				buttonsJustPressed.remove(i);
			if (m.isButtonPressed(i))
			{
				if (m.isMouseInRect(getScreenX(), getScreenY(), getWidth(), getHeight()))
				{
					if (m.isButtonJustPressed(i))
					{
						buttonsPressed.add(i);
						buttonsJustPressed.add(i);
						for (ComponentListener listener : getComponentListeners())
							listener.componentClicked(this, i);
					} else
					{
						for (ComponentListener listener : getComponentListeners())
							listener.componentPressed(this, i);
					}
				}
			} else if (buttonsPressed.contains(i))
			{
				buttonsPressed.remove(i);
				for (ComponentListener listener : getComponentListeners())
					listener.componentReleased(this, i);
			}
		}
	}

}
