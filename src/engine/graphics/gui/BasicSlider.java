package engine.graphics.gui;

import engine.core.Engine;
import engine.graphics.Color;
import engine.graphics.batch.ShapeBatch;
import engine.graphics.batch.SpriteBatch;

/**
 * The Class BasicSlider.
 */
public class BasicSlider extends SizedComponent
{

	/** The Constant SLIDER_PIECE_WIDTH. */
	public static final int SLIDER_PIECE_WIDTH = 10;
	
	/** The Constant SLIDER_PIECE_HEIGHT. */
	public static final int SLIDER_PIECE_HEIGHT = 20;

	/** The piece x. */
	private int pieceX;
	
	/** The line color. */
	private Color lineColor;
	
	/** The slider piece color. */
	private Color sliderPieceColor;
	
	/** The sliding. */
	private boolean sliding;

	/**
	 * Instantiates a new basic slider.
	 *
	 * @param width the width
	 * @param sliderX the slider x
	 * @param sliderY the slider y
	 * @param lineColor the line color
	 * @param sliderPieceColor the slider piece color
	 */
	public BasicSlider(int width, int sliderX, int sliderY, Color lineColor, Color sliderPieceColor)
	{
		super(null, sliderX, sliderY, width, SLIDER_PIECE_HEIGHT);
		this.lineColor = lineColor;
		this.sliderPieceColor = sliderPieceColor;
		pieceX = sliderX;
	}

	/**
	 * Gets the piece x.
	 *
	 * @return the piece x
	 */
	public int getPieceX()
	{
		return pieceX;
	}

	/**
	 * Sets the piece x.
	 *
	 * @param pieceX the new piece x
	 */
	public void setPieceX(int pieceX)
	{
		this.pieceX = pieceX;
	}

	/**
	 * Gets the line color.
	 *
	 * @return the line color
	 */
	public Color getLineColor()
	{
		return lineColor;
	}

	/**
	 * Sets the line color.
	 *
	 * @param lineColor the new line color
	 */
	public void setLineColor(Color lineColor)
	{
		this.lineColor = lineColor;
	}

	/**
	 * Gets the slider piece color.
	 *
	 * @return the slider piece color
	 */
	public Color getSliderPieceColor()
	{
		return sliderPieceColor;
	}

	/**
	 * Sets the slider piece color.
	 *
	 * @param sliderPieceColor the new slider piece color
	 */
	public void setSliderPieceColor(Color sliderPieceColor)
	{
		this.sliderPieceColor = sliderPieceColor;
	}

	@Override
	public void update(float delta)
	{
		// boolean before = mouseDown;
		//
		// if (!mouseDown && )
		// {
		// mouseDown = true;
		// } else if (mouseDown &&
		// Engine.getInstance().getInput().getMouse().isButtonPressed(0))
		// {
		// mouseDown = false;
		// }

		int mx = Engine.getInstance().getInput().getMouse().getX();
		// int my = Engine.getInstance().getInput().getMouse().getY();
		boolean mouseDown = Engine.getInstance().getInput().getMouse().isButtonPressed(0);

		if (mouseDown && Engine.getInstance().getInput().getMouse().isMouseInRect(getScreenX(), getScreenY(), getWidth(), getHeight()))
		{
			sliding = true;
		}

		if (sliding && !mouseDown)
		{
			sliding = false;
		}

		if (sliding)
		{
			pieceX = Engine.getInstance().getInput().getMouse().getX();
		}

		if (pieceX < getScreenX())
			pieceX = getScreenX();
		if (pieceX > getScreenX() + getWidth())
			pieceX = getScreenX() + getWidth();

		// if (!before && mouseDown)
		// {
		// // TODO: reset mouse dx and dy
		// }
		//
		// if (mouseDown)
		// {
		// // in bounds
		// pieceX += Engine.getInstance().getInput().getMouse().getDeltaX();

	}

	/**
	 * Sets the normalized slider position.
	 *
	 * @param pos the new normalized slider position
	 */
	public void setNormalizedSliderPosition(float pos)
	{
		if (pos < 0)
			pieceX = 0;
		else if (pos > 1)
			pieceX = getScreenX() + getWidth();
		else
		{
			pieceX = getScreenX() + (int) (getWidth() * pos);
		}
	}

	/**
	 * Gets the normalized slider position.
	 *
	 * @return the normalized slider position
	 */
	public float getNormalizedSliderPosition()
	{
		return (float) (pieceX - getScreenX()) / getWidth();
	}

	@Override
	public void render(SpriteBatch spriteBatch, ShapeBatch shapeBatch)
	{
		shapeBatch.setDrawColor(lineColor);
		shapeBatch.renderLine(getScreenX(), getScreenY() + SLIDER_PIECE_HEIGHT / 2, getScreenX() + getWidth(), getScreenY() + SLIDER_PIECE_HEIGHT / 2);

		shapeBatch.setDrawColor(sliderPieceColor);
		shapeBatch.renderFilledRect(pieceX - SLIDER_PIECE_WIDTH / 2, getScreenY(), SLIDER_PIECE_WIDTH, SLIDER_PIECE_HEIGHT, 0, 0, 0, 1, 1);
	}

}
