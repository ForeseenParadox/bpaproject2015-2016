package engine.graphics.gui;

import engine.graphics.Color;
import engine.graphics.batch.ShapeBatch;
import engine.graphics.batch.SpriteBatch;
import engine.graphics.font.TrueTypeFont;
import engine.graphics.texture.TextureRegion;

/**
 * The Class ImageTextButton.
 */
public class ImageTextButton extends ImageButton
{

	/** The font. */
	private TrueTypeFont font;
	
	/** The text. */
	private String text;
	
	/** The horizontal centering. */
	private HorizontalCentering horizontalCentering;
	
	/** The vertical centering. */
	private VerticalCentering verticalCentering;

	/**
	 * Instantiates a new image text button.
	 *
	 * @param p the p
	 * @param rx the rx
	 * @param ry the ry
	 * @param texture the texture
	 * @param text the text
	 * @param font the font
	 */
	public ImageTextButton(Component p, int rx, int ry, TextureRegion texture, String text, TrueTypeFont font)
	{
		this(p, rx, ry, texture, text, font, HorizontalCentering.LEFT, VerticalCentering.MIDDLE);
	}

	/**
	 * Instantiates a new image text button.
	 *
	 * @param p the p
	 * @param rx the rx
	 * @param ry the ry
	 * @param texture the texture
	 * @param text the text
	 * @param font the font
	 * @param horizontalCentering the horizontal centering
	 * @param verticalCentering the vertical centering
	 */
	public ImageTextButton(Component p, int rx, int ry, TextureRegion texture, String text, TrueTypeFont font, HorizontalCentering horizontalCentering, VerticalCentering verticalCentering)
	{
		super(p, rx, ry, texture);
		this.text = text;
		this.font = font;

		this.horizontalCentering = horizontalCentering;
		this.verticalCentering = verticalCentering;
	}

	/**
	 * Gets the font.
	 *
	 * @return the font
	 */
	public TrueTypeFont getFont()
	{
		return font;
	}

	/**
	 * Gets the text.
	 *
	 * @return the text
	 */
	public String getText()
	{
		return text;
	}

	/**
	 * Gets the horizontal centering.
	 *
	 * @return the horizontal centering
	 */
	public HorizontalCentering getHorizontalCentering()
	{
		return horizontalCentering;
	}

	/**
	 * Gets the vertical centering.
	 *
	 * @return the vertical centering
	 */
	public VerticalCentering getVerticalCentering()
	{
		return verticalCentering;
	}

	/**
	 * Sets the font.
	 *
	 * @param font the new font
	 */
	public void setFont(TrueTypeFont font)
	{
		this.font = font;
	}

	/**
	 * Sets the text.
	 *
	 * @param text the new text
	 */
	public void setText(String text)
	{
		this.text = text;
	}

	/**
	 * Sets the horizontal centering.
	 *
	 * @param horizontalCentering the new horizontal centering
	 */
	public void setHorizontalCentering(HorizontalCentering horizontalCentering)
	{
		this.horizontalCentering = horizontalCentering;
	}

	/**
	 * Sets the vertical centering.
	 *
	 * @param verticalCentering the new vertical centering
	 */
	public void setVerticalCentering(VerticalCentering verticalCentering)
	{
		this.verticalCentering = verticalCentering;
	}

	@Override
	public void render(SpriteBatch spriteBatch, ShapeBatch shapeBatch)
	{
		super.render(spriteBatch, shapeBatch);

		if (font != null)
		{
			int startX = getScreenX() + getHorizontalPadding(), startY = getScreenY() + getVerticalPadding();
			int width = getWidth() - 2 * getHorizontalPadding(), height = getHeight() - 2 * getVerticalPadding();

			int x = startX, y = startY;
			if (verticalCentering == VerticalCentering.MIDDLE)
				y += (height - font.getCharacterHeight()) / 2;
			else if (verticalCentering == VerticalCentering.HIGH)
				y += height - font.getCharacterHeight();

			if (horizontalCentering == HorizontalCentering.MIDDLE)
				x += (width - font.getStringWidth(text)) / 2;
			else if (horizontalCentering == HorizontalCentering.RIGHT)
				x += width - font.getStringWidth(text);

			font.render(spriteBatch, text, Color.WHITE, x, y);
		}
	}

}
