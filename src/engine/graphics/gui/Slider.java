package engine.graphics.gui;

import engine.graphics.batch.ShapeBatch;
import engine.graphics.batch.SpriteBatch;

/**
 * The Class Slider.
 */
public class Slider extends SizedComponent
{

	/**
	 * Instantiates a new slider.
	 *
	 * @param p the p
	 * @param rx the rx
	 * @param ry the ry
	 */
	public Slider(Component p, int rx, int ry)
	{
		super(p, rx, ry);
	}

	@Override
	public void render(SpriteBatch spriteBatch, ShapeBatch shapeBatch)
	{
		
	}

}
