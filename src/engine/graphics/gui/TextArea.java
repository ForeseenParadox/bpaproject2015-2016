package engine.graphics.gui;

import engine.graphics.Color;
import engine.graphics.batch.ShapeBatch;
import engine.graphics.batch.SpriteBatch;
import engine.graphics.font.TrueTypeFont;

/**
 * The Class TextArea.
 */
public class TextArea extends SizedComponent
{

	/** The text. */
	private String text;
	
	/** The font. */
	private TrueTypeFont font;

	/**
	 * Instantiates a new text area.
	 *
	 * @param p the p
	 * @param rx the rx
	 * @param ry the ry
	 * @param font the font
	 */
	public TextArea(Component p, int rx, int ry, TrueTypeFont font)
	{
		super(p, rx, ry);
		this.text = "";
		this.font = font;
	}

	/**
	 * Instantiates a new text area.
	 *
	 * @param p the p
	 * @param rx the rx
	 * @param ry the ry
	 * @param w the w
	 * @param h the h
	 * @param font the font
	 */
	public TextArea(Component p, int rx, int ry, int w, int h, TrueTypeFont font)
	{
		super(p, rx, ry, w, h);
		this.text = "";
		this.font = font;
	}

	/**
	 * Gets the text.
	 *
	 * @return the text
	 */
	public String getText()
	{
		return text;
	}

	/**
	 * Gets the font.
	 *
	 * @return the font
	 */
	public TrueTypeFont getFont()
	{
		return font;
	}

	/**
	 * Sets the text.
	 *
	 * @param text the new text
	 */
	public void setText(String text)
	{
		this.text = text;
	}

	/**
	 * Sets the font.
	 *
	 * @param font the new font
	 */
	public void setFont(TrueTypeFont font)
	{
		this.font = font;
	}

	@Override
	public void render(SpriteBatch spriteBatch, ShapeBatch shapeBatch)
	{
		String[] lines = text.split("\\n");

		int maxLines = (getHeight()) / font.getCharacterHeight();
		int y = getScreenY() + getVerticalPadding();
		for (int i = 0; i < maxLines; i++)
		{
			int index = lines.length - i - 1;
			if (index >= 0)
			{
				String line = lines[index];
				font.render(spriteBatch, line, Color.WHITE, getScreenX() + getHorizontalPadding(), y);
				y += font.getCharacterHeight();
			}
		}
	}

}
