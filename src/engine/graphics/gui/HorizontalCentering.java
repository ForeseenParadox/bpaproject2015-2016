package engine.graphics.gui;

/**
 * The Enum HorizontalCentering.
 */
public enum HorizontalCentering
{

	/** The left. */
	LEFT,
	
	/** The middle. */
	MIDDLE,
	
	/** The right. */
	RIGHT;

}