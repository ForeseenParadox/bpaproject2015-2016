package engine.graphics.gui;

import engine.graphics.Color;
import engine.graphics.batch.ShapeBatch;
import engine.graphics.batch.SpriteBatch;
import engine.graphics.font.TrueTypeFont;

/**
 * The Class PasswordField.
 */
public abstract class PasswordField extends SizedComponent
{

	/** The font. */
	private TrueTypeFont font;
	
	/** The input. */
	private TextInputHandler input;
	
	/** The cursor color. */
	private Color cursorColor;

	/**
	 * Instantiates a new password field.
	 *
	 * @param p the p
	 * @param rx the rx
	 * @param ry the ry
	 * @param w the w
	 * @param h the h
	 * @param font the font
	 * @param cCursor the c cursor
	 */
	public PasswordField(Component p, int rx, int ry, int w, int h, TrueTypeFont font, Color cCursor)
	{
		super(p, rx, ry, w, h);
		this.font = font;
		input = new TextInputHandler("");
		cursorColor = cCursor;
	}

	/**
	 * Gets the font.
	 *
	 * @return the font
	 */
	public TrueTypeFont getFont()
	{
		return font;
	}

	/**
	 * Gets the text.
	 *
	 * @return the text
	 */
	public String getText()
	{
		return input.getText();
	}

	/**
	 * Gets the input handler.
	 *
	 * @return the input handler
	 */
	public TextInputHandler getInputHandler()
	{
		return input;
	}

	/**
	 * Gets the cursor color.
	 *
	 * @return the cursor color
	 */
	public Color getCursorColor()
	{
		return cursorColor;
	}

	/**
	 * Sets the font.
	 *
	 * @param font the new font
	 */
	public void setFont(TrueTypeFont font)
	{
		this.font = font;
	}

	/**
	 * Sets the text.
	 *
	 * @param t the new text
	 */
	public void setText(String t)
	{
		input.setText(t);
	}

	/**
	 * Sets the cursor color.
	 *
	 * @param cColor the new cursor color
	 */
	public void setCursorColor(Color cColor)
	{
		cursorColor = cColor;
	}

	@Override
	public void render(SpriteBatch spriteBatch, ShapeBatch shapeBatch)
	{
		// render text
		char[] chars = getText().toCharArray();

		int indexPosition = getScreenX();
		if (chars.length > 0)
		{
			String t = "";
			int x = getScreenX() + getWidth();
			int i = chars.length - 1;
			while (i >= 0 && x - font.getCharacterWidth('*') >= getScreenX())
			{
				t += '*';
				x -= font.getCharacterWidth('*');
				i--;
			}
			indexPosition = 2 * getScreenX() + getWidth() - x;

			// render the text
			font.render(spriteBatch, t, Color.WHITE, getScreenX(), getScreenY());
		}

		shapeBatch.setDrawColor(cursorColor);
		shapeBatch.renderFilledRect(indexPosition, getScreenY(), 1, getHeight(), 0, 0, 0, 1, 1);

	}
}
