package engine.graphics.gui;

import engine.graphics.Color;
import engine.graphics.batch.ShapeBatch;
import engine.graphics.batch.SpriteBatch;
import engine.graphics.font.TrueTypeFont;

/**
 * The Class Label.
 */
public class Label extends Component
{

	/** The font. */
	private TrueTypeFont font;
	
	/** The text. */
	private String text;

	/**
	 * Instantiates a new label.
	 *
	 * @param p the p
	 * @param rx the rx
	 * @param ry the ry
	 * @param t the t
	 * @param font the font
	 */
	public Label(Component p, int rx, int ry, String t, TrueTypeFont font)
	{
		super(p, rx, ry);
		text = t;
		this.font = font;
	}

	/**
	 * Gets the text.
	 *
	 * @return the text
	 */
	public String getText()
	{
		return text;
	}

	/**
	 * Gets the font.
	 *
	 * @return the font
	 */
	public TrueTypeFont getFont()
	{
		return font;
	}

	/**
	 * Sets the text.
	 *
	 * @param t the new text
	 */
	public void setText(String t)
	{
		text = t;
	}

	/**
	 * Sets the font.
	 *
	 * @param font the new font
	 */
	public void setFont(TrueTypeFont font)
	{
		this.font = font;
	}

	@Override
	public void update(float delta)
	{

	}

	@Override
	public void render(SpriteBatch spriteBatch, ShapeBatch shapeBatch)
	{
		font.render(spriteBatch, getText(), Color.WHITE, getScreenX(), getScreenY());
	}
}
