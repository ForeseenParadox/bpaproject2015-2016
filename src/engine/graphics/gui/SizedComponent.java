package engine.graphics.gui;

import engine.core.Engine;
import engine.event.listener.ResizeListener;
import engine.input.Input;
import engine.util.math.Vector2f;

/**
 * The Class SizedComponent.
 */
public abstract class SizedComponent extends Component implements ResizeListener
{

	/** The width. */
	private int width;
	
	/** The height. */
	private int height;

	/** The scale. */
	private Vector2f scale;

	/** The horizontal padding. */
	private int horizontalPadding;
	
	/** The vertical padding. */
	private int verticalPadding;

	/** The hovered. */
	// event attributes
	private boolean hovered;
	
	/** The just hovered. */
	private boolean justHovered;
	
	/** The just exited. */
	private boolean justExited;

	/**
	 * Instantiates a new sized component.
	 *
	 * @param p the p
	 * @param rx the rx
	 * @param ry the ry
	 */
	public SizedComponent(Component p, int rx, int ry)
	{
		super(p, rx, ry);

		scale = new Vector2f(1, 1);
	}

	/**
	 * Instantiates a new sized component.
	 *
	 * @param p the p
	 * @param rx the rx
	 * @param ry the ry
	 * @param w the w
	 * @param h the h
	 */
	public SizedComponent(Component p, int rx, int ry, int w, int h)
	{
		super(p, rx, ry);
		width = w;
		height = h;

		scale = new Vector2f(1, 1);
	}

	/**
	 * Gets the width.
	 *
	 * @return the width
	 */
	public int getWidth()
	{
		return (int) (width * scale.getX());
	}

	/**
	 * Gets the height.
	 *
	 * @return the height
	 */
	public int getHeight()
	{
		return (int) (height * scale.getY());
	}

	/**
	 * Gets the scale.
	 *
	 * @return the scale
	 */
	public Vector2f getScale()
	{
		return scale;
	}

	/**
	 * Gets the horizontal padding.
	 *
	 * @return the horizontal padding
	 */
	public int getHorizontalPadding()
	{
		return (int) (horizontalPadding * scale.getX());
	}

	/**
	 * Gets the vertical padding.
	 *
	 * @return the vertical padding
	 */
	public int getVerticalPadding()
	{
		return (int) (verticalPadding * scale.getY());
	}

	/**
	 * Checks if is hovered.
	 *
	 * @return true, if is hovered
	 */
	public boolean isHovered()
	{
		return hovered;
	}

	/**
	 * Was just hovered.
	 *
	 * @return true, if successful
	 */
	public boolean wasJustHovered()
	{
		return justHovered;
	}

	/**
	 * Was just exited.
	 *
	 * @return true, if successful
	 */
	public boolean wasJustExited()
	{
		return justExited;
	}

	/**
	 * Sets the width.
	 *
	 * @param w the new width
	 */
	public void setWidth(int w)
	{
		width = w;
	}

	/**
	 * Sets the height.
	 *
	 * @param h the new height
	 */
	public void setHeight(int h)
	{
		height = h;
	}

	/**
	 * Sets the scale.
	 *
	 * @param scale the new scale
	 */
	public void setScale(Vector2f scale)
	{
		this.scale = scale;
	}

	/**
	 * Sets the horizontal padding.
	 *
	 * @param horizontalPadding the new horizontal padding
	 */
	public void setHorizontalPadding(int horizontalPadding)
	{
		this.horizontalPadding = horizontalPadding;
	}

	/**
	 * Sets the vertical padding.
	 *
	 * @param verticalPadding the new vertical padding
	 */
	public void setVerticalPadding(int verticalPadding)
	{
		this.verticalPadding = verticalPadding;
	}

	/**
	 * Sets the padding.
	 *
	 * @param padding the new padding
	 */
	public void setPadding(int padding)
	{
		setHorizontalPadding(padding);
		setVerticalPadding(padding);
	}

	@Override
	public void resize(int w, int h)
	{

	}

	@Override
	public void update(float delta)
	{
		Input in = Engine.getInstance().getInput();

		justHovered = false;
		justExited = false;

		if (in.getMouse().isMouseInRect(getScreenX(), getScreenY(), getWidth(), getHeight()))
		{
			if (!hovered)
				justHovered = true;
			hovered = true;
		} else
		{
			justExited = hovered;
			hovered = false;
		}
	}

}
