package engine.graphics.gui;

import engine.core.Engine;
import engine.core.task.Task;
import engine.graphics.Color;
import engine.graphics.batch.ShapeBatch;
import engine.graphics.batch.SpriteBatch;
import engine.graphics.font.TrueTypeFont;

/**
 * The Class FadingBasicButton.
 */
public class FadingBasicButton extends BasicButton
{
	
	// TODO: add disposing to components so we can cancel engine events...

	/** The fade. */
	private float fade;

	/**
	 * Instantiates a new fading basic button.
	 *
	 * @param p the p
	 * @param rx the rx
	 * @param ry the ry
	 * @param w the w
	 * @param h the h
	 * @param bg the bg
	 * @param fg the fg
	 * @param text the text
	 * @param font the font
	 */
	public FadingBasicButton(Component p, int rx, int ry, int w, int h, Color bg, Color fg, String text, TrueTypeFont font)
	{
		super(p, rx, ry, w, h, bg, fg, text, font);
		Task t = new Task(3, new Runnable()
		{
			@Override
			public void run()
			{
				fade -= 0.01f;
			}
		}, true);

		Engine.getInstance().getTaskScheduler().scheduleSyncTask(t);

		fade = 0.5f;
	}

	@Override
	public void update(float delta)
	{
		super.update(delta);

		if (isHovered())
			fade = 1.0f;

		if (fade < 0.5f)
			fade = 0.5f;
	}

	@Override
	public void render(SpriteBatch spriteBatch, ShapeBatch shapeBatch)
	{
		spriteBatch.setTintColor(Color.WHITE.scale(fade));
		shapeBatch.setDrawColor(getBackground().scale(fade));
		super.render(spriteBatch, shapeBatch);
		spriteBatch.setTintColor(Color.WHITE);
		shapeBatch.setDrawColor(Color.WHITE);
	}
	
	
}
