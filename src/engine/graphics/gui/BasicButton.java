package engine.graphics.gui;

import engine.graphics.Color;
import engine.graphics.batch.ShapeBatch;
import engine.graphics.batch.SpriteBatch;
import engine.graphics.font.TrueTypeFont;

/**
 * The Class BasicButton.
 */
public class BasicButton extends Button
{

	/** The Constant DEFAULT_PADDING. */
	public static final int DEFAULT_PADDING = 5;

	/** The background. */
	private Color background;
	
	/** The foreground. */
	private Color foreground;
	
	/** The text. */
	private String text;
	
	/** The font. */
	private TrueTypeFont font;

	/**
	 * Instantiates a new basic button.
	 *
	 * @param p the p
	 * @param rx the rx
	 * @param ry the ry
	 * @param w the w
	 * @param h the h
	 * @param bg the bg
	 * @param fg the fg
	 * @param text the text
	 * @param font the font
	 */
	public BasicButton(Component p, int rx, int ry, int w, int h, Color bg, Color fg, String text, TrueTypeFont font)
	{
		super(p, rx, ry, w, h);
		this.background = bg;
		this.foreground = fg;
		this.text = text;
		this.font = font;
		setPadding(DEFAULT_PADDING);
	}

	/**
	 * Gets the background.
	 *
	 * @return the background
	 */
	public Color getBackground()
	{
		return background;
	}

	/**
	 * Gets the foreground.
	 *
	 * @return the foreground
	 */
	public Color getForeground()
	{
		return foreground;
	}

	/**
	 * Gets the text.
	 *
	 * @return the text
	 */
	public String getText()
	{
		return text;
	}

	/**
	 * Gets the font.
	 *
	 * @return the font
	 */
	public TrueTypeFont getFont()
	{
		return font;
	}

	/**
	 * Sets the text.
	 *
	 * @param text the new text
	 */
	public void setText(String text)
	{
		this.text = text;
	}

	/**
	 * Sets the foreground.
	 *
	 * @param fg the new foreground
	 */
	public void setForeground(Color fg)
	{
		foreground = fg;
	}

	/**
	 * Sets the background.
	 *
	 * @param bg the new background
	 */
	public void setBackground(Color bg)
	{
		background = bg;
	}

	/**
	 * Sets the font.
	 *
	 * @param font the new font
	 */
	public void setFont(TrueTypeFont font)
	{
		this.font = font;
	}

	@Override
	public void render(SpriteBatch spriteBatch, ShapeBatch shapeBatch)
	{
		// shapeBatch.setDrawColor(getBackground());
		shapeBatch.renderFilledRect(getScreenX() - getHorizontalPadding(), getScreenY() - getVerticalPadding(), getWidth() + getHorizontalPadding() * 2, getHeight() + getVerticalPadding() * 2, 0, 0, 0, 1, 1);
		if (isHovered())
		{
			// shapeBatch.setDrawColor(getForeground());
			// shapeBatch.renderRect(getScreenX(), getScreenY(), getWidth(),
			// getHeight(), 0, 0, 0, 1, 1);
		}

		// flush shape batch to text will appear over button
		shapeBatch.flush();

		font.render(spriteBatch, getText(), foreground, getScreenX(), getScreenY());
	}
}
