package engine.graphics.gui;

import java.util.ArrayList;
import java.util.List;

import engine.graphics.Color;
import engine.graphics.batch.ShapeBatch;
import engine.graphics.batch.SpriteBatch;

/**
 * The Class Pane.
 */
public abstract class Pane extends SizedComponent
{

	/** The background color. */
	private Color backgroundColor;
	
	/** The foreground color. */
	private Color foregroundColor;
	
	/** The children. */
	private List<Component> children;

	/**
	 * Instantiates a new pane.
	 *
	 * @param p the p
	 * @param rx the rx
	 * @param ry the ry
	 * @param w the w
	 * @param h the h
	 * @param bg the bg
	 * @param fg the fg
	 */
	public Pane(Component p, int rx, int ry, int w, int h, Color bg, Color fg)
	{
		super(p, rx, ry, w, h);
		backgroundColor = bg;
		foregroundColor = fg;
		children = new ArrayList<Component>();
	}

	/**
	 * Gets the background color.
	 *
	 * @return the background color
	 */
	public Color getBackgroundColor()
	{
		return backgroundColor;
	}

	/**
	 * Gets the foreground color.
	 *
	 * @return the foreground color
	 */
	public Color getForegroundColor()
	{
		return foregroundColor;
	}

	/**
	 * Sets the background color.
	 *
	 * @param bg the new background color
	 */
	public void setBackgroundColor(Color bg)
	{
		backgroundColor = bg;
	}

	/**
	 * Sets the foreground color.
	 *
	 * @param fg the new foreground color
	 */
	public void setForegroundColor(Color fg)
	{
		foregroundColor = fg;
	}

	/**
	 * Adds the.
	 *
	 * @param comp the comp
	 */
	public void add(Component comp)
	{
		children.add(comp);
	}

	/**
	 * Removes the.
	 *
	 * @param comp the comp
	 */
	public void remove(Component comp)
	{
		children.remove(comp);
	}

	@Override
	public void update(float delta)
	{
		super.update(delta);
		for (Component child : children)
			child.update(delta);
	}

	@Override
	public void render(SpriteBatch spriteBatch, ShapeBatch shapeBatch)
	{
		shapeBatch.setDrawColor(backgroundColor);
		shapeBatch.renderFilledRect(getScreenX(), getScreenY(), getWidth(), getHeight(), 0, 0, 0, 1, 1);

		if (isFocused())
		{
			shapeBatch.setDrawColor(foregroundColor);
			shapeBatch.renderFilledRect(getScreenX(), getScreenY(), getWidth(), getHeight(), 0, 0, 0, 1, 1);
		}

		shapeBatch.flush();

		for (Component child : children)
			child.render(spriteBatch, shapeBatch);
	}

}
