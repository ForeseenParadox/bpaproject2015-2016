package engine.graphics.gui;

/**
 * The Enum VerticalCentering.
 */
public enum VerticalCentering
{

	/** The low. */
	LOW,
	
	/** The middle. */
	MIDDLE,
	
	/** The high. */
	HIGH;

}
