package engine.graphics.gui;

import engine.core.Engine;
import engine.event.listener.KeyListener;

/**
 * The Class TextInputHandler.
 */
public class TextInputHandler implements KeyListener
{

	/** The text. */
	private String text;
	
	/** The index position. */
	private int indexPosition;
	
	/** The enabled. */
	private boolean enabled;

	/**
	 * Instantiates a new text input handler.
	 *
	 * @param startText the start text
	 */
	public TextInputHandler(String startText)
	{
		text = startText;
		indexPosition = -1;

		Engine.getInstance().getInput().getKeyboard().addKeyListener(this);

		enabled = true;
	}

	/**
	 * Gets the text.
	 *
	 * @return the text
	 */
	public String getText()
	{
		return text;
	}

	/**
	 * Gets the index position.
	 *
	 * @return the index position
	 */
	public int getIndexPosition()
	{
		return indexPosition;
	}

	/**
	 * Checks if is enabled.
	 *
	 * @return true, if is enabled
	 */
	public boolean isEnabled()
	{
		return enabled;
	}

	/**
	 * Sets the text.
	 *
	 * @param t the new text
	 */
	public void setText(String t)
	{
		text = t;
	}

	/**
	 * Sets the index position.
	 *
	 * @param ip the new index position
	 */
	public void setIndexPosition(int ip)
	{
		indexPosition = ip;
	}

	/**
	 * Sets the enabled.
	 *
	 * @param enabled the new enabled
	 */
	public void setEnabled(boolean enabled)
	{
		this.enabled = enabled;
	}

	@Override
	public void keyPressed(int key)
	{
		if (enabled)
		{
			if (key == 259 && text.length() > 0)
			{
				text = text.substring(0, text.length() - 1);
				indexPosition--;
			}
		}
	}

	@Override
	public void keyReleased(int key)
	{
		// do nothing
	}

	@Override
	public void charPressed(char key)
	{
		if (enabled)
		{
			char c = key;
			text += c;
			indexPosition++;
		}
	}

	@Override
	public void keyRepeated(int key)
	{
		keyPressed(key);
	}
}
