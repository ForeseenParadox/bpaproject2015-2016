package engine.graphics.gui;

import engine.core.Engine;
import engine.graphics.Color;
import engine.graphics.batch.ShapeBatch;
import engine.graphics.batch.SpriteBatch;
import engine.graphics.texture.TextureRegion;

/**
 * The Class ImageSlider.
 */
public class ImageSlider extends SizedComponent
{

	/** The piece x. */
	private int pieceX;
	
	/** The line color. */
	private Color lineColor;
	
	/** The sliding. */
	private boolean sliding;
	
	/** The slider piece texture. */
	private TextureRegion sliderPieceTexture;

	/**
	 * Instantiates a new image slider.
	 *
	 * @param width the width
	 * @param sliderX the slider x
	 * @param sliderY the slider y
	 * @param lineColor the line color
	 * @param sliderPieceTexture the slider piece texture
	 */
	public ImageSlider(int width, int sliderX, int sliderY, Color lineColor, TextureRegion sliderPieceTexture)
	{
		super(null, sliderX, sliderY, width, (int) sliderPieceTexture.getHeight());
		this.lineColor = lineColor;
		pieceX = sliderX;
		this.sliderPieceTexture = sliderPieceTexture;
	}

	/**
	 * Gets the piece x.
	 *
	 * @return the piece x
	 */
	public int getPieceX()
	{
		return pieceX;
	}

	/**
	 * Sets the piece x.
	 *
	 * @param pieceX the new piece x
	 */
	public void setPieceX(int pieceX)
	{
		this.pieceX = pieceX;
	}

	/**
	 * Gets the line color.
	 *
	 * @return the line color
	 */
	public Color getLineColor()
	{
		return lineColor;
	}

	/**
	 * Sets the line color.
	 *
	 * @param lineColor the new line color
	 */
	public void setLineColor(Color lineColor)
	{
		this.lineColor = lineColor;
	}

	/**
	 * Gets the slider piece texture.
	 *
	 * @return the slider piece texture
	 */
	public TextureRegion getSliderPieceTexture()
	{
		return sliderPieceTexture;
	}

	/**
	 * Sets the slider piece texture.
	 *
	 * @param sliderPieceTexture the new slider piece texture
	 */
	public void setSliderPieceTexture(TextureRegion sliderPieceTexture)
	{
		this.sliderPieceTexture = sliderPieceTexture;
	}

	@Override
	public void update(float delta)
	{
		// boolean before = mouseDown;
		//
		// if (!mouseDown && )
		// {
		// mouseDown = true;
		// } else if (mouseDown &&
		// Engine.getInstance().getInput().getMouse().isButtonPressed(0))
		// {
		// mouseDown = false;
		// }

		int mx = Engine.getInstance().getInput().getMouse().getX();
		// int my = Engine.getInstance().getInput().getMouse().getY();
		boolean mouseDown = Engine.getInstance().getInput().getMouse().isButtonPressed(0);

		if (mouseDown && Engine.getInstance().getInput().getMouse().isMouseInRect(getScreenX(), getScreenY(), getWidth(), getHeight()))
		{
			sliding = true;
		}

		if (sliding && !mouseDown)
		{
			sliding = false;
		}

		if (sliding)
		{
			pieceX = Engine.getInstance().getInput().getMouse().getX();
		}

		if (pieceX < getScreenX())
			pieceX = getScreenX();
		if (pieceX > getScreenX() + getWidth())
			pieceX = getScreenX() + getWidth();

		// if (!before && mouseDown)
		// {
		// // TODO: reset mouse dx and dy
		// }
		//
		// if (mouseDown)
		// {
		// // in bounds
		// pieceX += Engine.getInstance().getInput().getMouse().getDeltaX();

	}

	/**
	 * Sets the normalized slider position.
	 *
	 * @param pos the new normalized slider position
	 */
	public void setNormalizedSliderPosition(float pos)
	{
		if (pos < 0)
			pieceX = 0;
		else if (pos > 1)
			pieceX = getScreenX() + getWidth();
		else
		{
			pieceX = getScreenX() + (int) (getWidth() * pos);
		}
	}

	/**
	 * Gets the normalized slider position.
	 *
	 * @return the normalized slider position
	 */
	public float getNormalizedSliderPosition()
	{
		return (float) (pieceX - getScreenX()) / getWidth();
	}

	@Override
	public void render(SpriteBatch spriteBatch, ShapeBatch shapeBatch)
	{
		shapeBatch.setDrawColor(lineColor);
		shapeBatch.renderLine(getScreenX(), getScreenY() + sliderPieceTexture.getHeight() / 2, getScreenX() + getWidth(), getScreenY() + sliderPieceTexture.getHeight() / 2);

		spriteBatch.renderTexture(sliderPieceTexture, pieceX - sliderPieceTexture.getWidth() / 2, getScreenY(), sliderPieceTexture.getWidth(), sliderPieceTexture.getHeight(), 0, 0, 0, 1, 1);
	}
}
