package engine.graphics.gui;

import java.util.ArrayList;
import java.util.List;

import engine.event.listener.ComponentListener;
import engine.event.listener.UpdateListener;
import engine.graphics.batch.ShapeBatch;
import engine.graphics.batch.SpriteBatch;

/**
 * The Class Component.
 */
public abstract class Component implements UpdateListener
{

	/** The parent. */
	private Component parent;
	
	/** The relative x. */
	private int relativeX;
	
	/** The relative y. */
	private int relativeY;
	
	/** The focused. */
	private boolean focused;
	
	/** The hidden. */
	private boolean hidden;

	/** The component listeners. */
	private List<ComponentListener> componentListeners;

	/**
	 * Instantiates a new component.
	 *
	 * @param p the p
	 * @param rx the rx
	 * @param ry the ry
	 */
	public Component(Component p, int rx, int ry)
	{
		parent = p;
		relativeX = rx;
		relativeY = ry;
		focused = false;
		componentListeners = new ArrayList<ComponentListener>();
	}

	/**
	 * Gets the parent.
	 *
	 * @return the parent
	 */
	public Component getParent()
	{
		return parent;
	}

	/**
	 * Gets the relative x.
	 *
	 * @return the relative x
	 */
	public int getRelativeX()
	{
		return relativeX;
	}

	/**
	 * Gets the screen x.
	 *
	 * @return the screen x
	 */
	public int getScreenX()
	{
		if (parent == null)
			return relativeX;
		else
			return parent.getScreenX() + relativeX;
	}

	/**
	 * Gets the relative y.
	 *
	 * @return the relative y
	 */
	public int getRelativeY()
	{
		return relativeY;
	}

	/**
	 * Gets the screen y.
	 *
	 * @return the screen y
	 */
	public int getScreenY()
	{
		if (parent == null)
			return relativeY;
		else
			return parent.getScreenY() + relativeY;
	}

	/**
	 * Checks if is focused.
	 *
	 * @return true, if is focused
	 */
	public boolean isFocused()
	{
		return focused;
	}

	/**
	 * Checks if is hidden.
	 *
	 * @return true, if is hidden
	 */
	public boolean isHidden()
	{
		return hidden;
	}

	/**
	 * Gets the component listeners.
	 *
	 * @return the component listeners
	 */
	public List<ComponentListener> getComponentListeners()
	{
		return componentListeners;
	}

	/**
	 * Sets the parent.
	 *
	 * @param p the new parent
	 */
	public void setParent(Component p)
	{
		parent = p;
	}

	/**
	 * Sets the relative x.
	 *
	 * @param rx the new relative x
	 */
	public void setRelativeX(int rx)
	{
		relativeX = rx;
	}

	/**
	 * Sets the relative y.
	 *
	 * @param ry the new relative y
	 */
	public void setRelativeY(int ry)
	{
		relativeY = ry;
	}

	/**
	 * Sets the focused.
	 *
	 * @param focus the new focused
	 */
	public void setFocused(boolean focus)
	{
		focused = focus;
	}

	/**
	 * Sets the hidden.
	 *
	 * @param hidden the new hidden
	 */
	public void setHidden(boolean hidden)
	{
		this.hidden = hidden;
	}

	/**
	 * Adds the component listener.
	 *
	 * @param listener the listener
	 */
	public void addComponentListener(ComponentListener listener)
	{
		componentListeners.add(listener);
	}

	/**
	 * Removes the component listener.
	 *
	 * @param listener the listener
	 */
	public void removeComponentListener(ComponentListener listener)
	{
		componentListeners.remove(listener);
	}

	/**
	 * Render.
	 *
	 * @param spriteBatch the sprite batch
	 * @param shapeBatch the shape batch
	 */
	public abstract void render(SpriteBatch spriteBatch, ShapeBatch shapeBatch);

}
