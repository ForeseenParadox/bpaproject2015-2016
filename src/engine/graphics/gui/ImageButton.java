package engine.graphics.gui;

import engine.graphics.Color;
import engine.graphics.batch.ShapeBatch;
import engine.graphics.batch.SpriteBatch;
import engine.graphics.texture.TextureRegion;

/**
 * The Class ImageButton.
 */
public class ImageButton extends Button
{
	
	/** The texture. */
	private TextureRegion texture;

	/**
	 * Instantiates a new image button.
	 *
	 * @param p the p
	 * @param rx the rx
	 * @param ry the ry
	 * @param texture the texture
	 */
	public ImageButton(Component p, int rx, int ry, TextureRegion texture)
	{
		super(p, rx, ry, (int) texture.getWidth(), (int) texture.getHeight());
		setTexture(texture);
	}

	/**
	 * Instantiates a new image button.
	 *
	 * @param p the p
	 * @param rx the rx
	 * @param ry the ry
	 * @param w the w
	 * @param h the h
	 * @param texture the texture
	 */
	public ImageButton(Component p, int rx, int ry, int w, int h, TextureRegion texture)
	{
		super(p, rx, ry, w, h);
		setTexture(texture);
	}

	/**
	 * Gets the texture.
	 *
	 * @return the texture
	 */
	public TextureRegion getTexture()
	{
		return texture;
	}

	/**
	 * Sets the texture.
	 *
	 * @param texture the new texture
	 */
	public void setTexture(TextureRegion texture)
	{
		this.texture = texture;
	}

	@Override
	public void render(SpriteBatch spriteBatch, ShapeBatch shapeBatch)
	{
		spriteBatch.renderTexture(getTexture(), getScreenX(), getScreenY(), getWidth(), getHeight(), 0, 0, 0, 1, 1);
		spriteBatch.flush();
		if (isHovered())
		{
			// TODO: make and change to non filled rectangle drawing
			shapeBatch.setDrawColor(Color.WHITE);
			shapeBatch.renderRect(getScreenX(), getScreenY(), getWidth(), getHeight(), 0, 0, 0, 1, 1);
			shapeBatch.flush();
		}
	}
}
