package engine.graphics.gui;

import engine.graphics.Color;
import engine.graphics.batch.ShapeBatch;
import engine.graphics.batch.SpriteBatch;
import engine.graphics.font.TrueTypeFont;

/**
 * The Class BasicPasswordField.
 */
public class BasicPasswordField extends PasswordField
{
	
	/** The background color. */
	private Color backgroundColor;
	
	/** The foreground color. */
	private Color foregroundColor;

	/**
	 * Instantiates a new basic password field.
	 *
	 * @param p the p
	 * @param rx the rx
	 * @param ry the ry
	 * @param w the w
	 * @param h the h
	 * @param font the font
	 * @param cColor the c color
	 * @param bgColor the bg color
	 * @param fgColor the fg color
	 */
	public BasicPasswordField(Component p, int rx, int ry, int w, int h, TrueTypeFont font, Color cColor, Color bgColor, Color fgColor)
	{
		super(p, rx, ry, w, h, font, cColor);
		backgroundColor = bgColor;
		foregroundColor = fgColor;
	}

	/**
	 * Gets the background color.
	 *
	 * @return the background color
	 */
	public Color getBackgroundColor()
	{
		return backgroundColor;
	}

	/**
	 * Gets the foreground color.
	 *
	 * @return the foreground color
	 */
	public Color getForegroundColor()
	{
		return foregroundColor;
	}

	/**
	 * Sets the background color.
	 *
	 * @param bgColor the new background color
	 */
	public void setBackgroundColor(Color bgColor)
	{
		backgroundColor = bgColor;
	}

	/**
	 * Sets the foreground color.
	 *
	 * @param fgColor the new foreground color
	 */
	public void setForegroundColor(Color fgColor)
	{
		foregroundColor = fgColor;
	}

	@Override
	public void render(SpriteBatch spriteBatch, ShapeBatch shapeBatch)
	{
		if (!shapeBatch.isDrawing())
			shapeBatch.begin();

		shapeBatch.setDrawColor(backgroundColor);
		shapeBatch.renderFilledRect(getScreenX(), getScreenY(), getWidth(), getHeight(), 0, 0, 0, 1, 1);

		if (isFocused())
		{
			shapeBatch.setDrawColor(foregroundColor);
			shapeBatch.renderFilledRect(getScreenX(), getScreenY(), getWidth(), getHeight(), 0, 0, 0, 1, 1);
		}

		shapeBatch.flush();

		super.render(spriteBatch, shapeBatch);
	}
}