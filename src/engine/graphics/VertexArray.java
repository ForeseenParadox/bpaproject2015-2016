package engine.graphics;

import static org.lwjgl.opengl.GL11.GL_FLOAT;
import static org.lwjgl.opengl.GL11.glDrawArrays;
import static org.lwjgl.opengl.GL20.glEnableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glVertexAttribPointer;
import static org.lwjgl.opengl.GL30.glBindVertexArray;
import static org.lwjgl.opengl.GL30.glGenVertexArrays;

/**
 * The Class VertexArray.
 */
public class VertexArray
{

	/** The handle. */
	private int handle;

	/**
	 * Instantiates a new vertex array.
	 */
	public VertexArray()
	{
		handle = glGenVertexArrays();
	}

	/**
	 * Bind.
	 */
	public void bind()
	{
		glBindVertexArray(handle);
	}

	/**
	 * Configure vertex attribute.
	 *
	 * @param attrib the attrib
	 * @param size the size
	 * @param stride the stride
	 * @param offset the offset
	 */
	public void configureVertexAttribute(VertexAttribute attrib, int size, int stride, long offset)
	{
		configureVertexAttribute(attrib.getIndex(), size, stride, offset);
	}

	/**
	 * Configure vertex attribute.
	 *
	 * @param index the index
	 * @param size the size
	 * @param stride the stride
	 * @param offset the offset
	 */
	public void configureVertexAttribute(int index, int size, int stride, long offset)
	{
		bind();
		glEnableVertexAttribArray(index);
		glVertexAttribPointer(index, size, GL_FLOAT, false, stride, offset);
	}

	/**
	 * Render.
	 *
	 * @param primitive the primitive
	 * @param elementCount the element count
	 */
	public void render(int primitive, int elementCount)
	{
		bind();
		glDrawArrays(primitive, 0, elementCount);
	}

	/**
	 * Unbind array.
	 */
	public static void unbindArray()
	{
		glBindVertexArray(0);
	}
}
