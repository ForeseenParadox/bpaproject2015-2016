package engine.input;

import static org.lwjgl.glfw.GLFW.GLFW_PRESS;
import static org.lwjgl.glfw.GLFW.GLFW_RELEASE;
import static org.lwjgl.glfw.GLFW.*;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.glfw.GLFWCharCallback;
import org.lwjgl.glfw.GLFWKeyCallback;

import engine.core.Subsystem;
import engine.core.Window;
import engine.event.listener.KeyListener;
import engine.event.listener.UpdateListener;

/**
 * The Class Keyboard.
 */
public class Keyboard implements UpdateListener, Subsystem
{

	/** The Constant MAX_KEYS. */
	public static final int MAX_KEYS = 0xFFFF;
	
	/** The keys down. */
	private byte[] keysDown;
	
	/** The listeners. */
	private List<KeyListener> listeners;
	
	/** The window. */
	private Window window;

	/** The glfw key listener. */
	private GLFWKeyCallback glfwKeyListener = new GLFWKeyCallback()
	{
		@Override
		public void invoke(long window, int key, int scancode, int action, int mods)
		{
			if (action == GLFW_RELEASE)
			{
				keysDown[key] = 4;
				for (KeyListener listener : listeners)
					listener.keyReleased(key);
			} else if (action == GLFW_PRESS)
			{
				keysDown[key] = 1;
				for (KeyListener listener : listeners)
					listener.keyPressed(key);
			} else if (action == GLFW_REPEAT)
			{
				for (KeyListener listener : listeners)
					listener.keyRepeated(key);
			}
		}
	};

	/** The glfw char listener. */
	private GLFWCharCallback glfwCharListener = new GLFWCharCallback()
	{
		@Override
		public void invoke(long window, int codepoint)
		{
			for (KeyListener listener : listeners)
				listener.charPressed((char) codepoint);
		}

	};

	/**
	 * Instantiates a new keyboard.
	 *
	 * @param window the window
	 */
	public Keyboard(Window window)
	{
		keysDown = new byte[MAX_KEYS];
		this.window = window;
	}

	/**
	 * Gets the glfw key listener.
	 *
	 * @return the glfw key listener
	 */
	public GLFWKeyCallback getGlfwKeyListener()
	{
		return glfwKeyListener;
	}

	/**
	 * Checks if is key pressed.
	 *
	 * @param key the key
	 * @return true, if is key pressed
	 */
	public boolean isKeyPressed(int key)
	{
		return keysDown[key] >= 1 && keysDown[key] <= 3;
	}

	/**
	 * Checks if is key just pressed.
	 *
	 * @param key the key
	 * @return true, if is key just pressed
	 */
	public boolean isKeyJustPressed(int key)
	{
		return keysDown[key] == 1 || keysDown[key] == 2;
	}

	/**
	 * Checks if is key just released.
	 *
	 * @param key the key
	 * @return true, if is key just released
	 */
	public boolean isKeyJustReleased(int key)
	{
		return keysDown[key] == 4 || keysDown[key] == 5;
	}

	/**
	 * Adds the key listener.
	 *
	 * @param listener the listener
	 */
	public void addKeyListener(KeyListener listener)
	{
		listeners.add(listener);
	}

	/**
	 * Removes the key listener.
	 *
	 * @param listener the listener
	 */
	public void removeKeyListener(KeyListener listener)
	{
		listeners.remove(listener);
	}

	@Override
	public void update(float delta)
	{
		for (int i = 0; i < keysDown.length; i++)
			if (keysDown[i] == 5)
				keysDown[i] = 0;
		for (int i = 0; i < keysDown.length; i++)
			if (keysDown[i] == 4)
				keysDown[i] = 5;
		for (int i = 0; i < keysDown.length; i++)
			if (keysDown[i] > 0 && keysDown[i] <= 2)
				keysDown[i]++;
	}

	@Override
	public void init()
	{
		listeners = new ArrayList<KeyListener>();
		glfwSetKeyCallback(window.getWindowHandle(), glfwKeyListener);
		glfwSetCharCallback(window.getWindowHandle(), glfwCharListener);
	}

	@Override
	public void dispose()
	{

	}

}
