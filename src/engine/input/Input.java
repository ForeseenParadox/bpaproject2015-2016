package engine.input;

import engine.core.Subsystem;
import engine.core.Window;
import engine.event.listener.UpdateListener;

/**
 * The Class Input.
 */
public class Input implements Subsystem, UpdateListener
{

	/** The keyboard. */
	private Keyboard keyboard;
	
	/** The mouse. */
	private Mouse mouse;
	
	/** The window. */
	private Window window;

	/**
	 * Instantiates a new input.
	 *
	 * @param window the window
	 */
	public Input(Window window)
	{
		this.window = window;
	}

	/**
	 * Gets the keyboard.
	 *
	 * @return the keyboard
	 */
	public Keyboard getKeyboard()
	{
		return keyboard;
	}

	/**
	 * Gets the mouse.
	 *
	 * @return the mouse
	 */
	public Mouse getMouse()
	{
		return mouse;
	}

	@Override
	public void init()
	{
		// initialize key callback system with window module
		keyboard = new Keyboard(window);
		keyboard.init();
		mouse = new Mouse(window);
		mouse.init();
	}

	@Override
	public void dispose()
	{
		keyboard.dispose();
		mouse.dispose();
	}

	@Override
	public void update(float delta)
	{
		keyboard.update(delta);
		mouse.update(delta);
	}

}
