package engine.input;

import static org.lwjgl.glfw.GLFW.GLFW_CURSOR;
import static org.lwjgl.glfw.GLFW.GLFW_CURSOR_HIDDEN;
import static org.lwjgl.glfw.GLFW.GLFW_CURSOR_NORMAL;
import static org.lwjgl.glfw.GLFW.GLFW_PRESS;
import static org.lwjgl.glfw.GLFW.GLFW_RELEASE;
import static org.lwjgl.glfw.GLFW.glfwGetCursorPos;
import static org.lwjgl.glfw.GLFW.glfwSetCursorPos;
import static org.lwjgl.glfw.GLFW.glfwSetInputMode;
import static org.lwjgl.glfw.GLFW.glfwSetMouseButtonCallback;

import java.nio.DoubleBuffer;
import java.util.ArrayList;
import java.util.List;

import org.lwjgl.BufferUtils;
import org.lwjgl.glfw.GLFWMouseButtonCallback;

import engine.core.Subsystem;
import engine.core.Window;
import engine.event.listener.MouseListener;
import engine.event.listener.UpdateListener;;

/**
 * The Class Mouse.
 */
public class Mouse implements UpdateListener, Subsystem
{

	/** The Constant MAX_BUTTONS. */
	public static final int MAX_BUTTONS = 0xFF;
	
	/** The buttons down. */
	private byte[] buttonsDown;
	
	/** The window. */
	private Window window;
	
	/** The position buffer. */
	private DoubleBuffer positionBuffer;
	
	/** The last x. */
	private int lastX;
	
	/** The last y. */
	private int lastY;
	
	/** The y down. */
	private boolean yDown;
	
	/** The listeners. */
	private List<MouseListener> listeners;

	/** The glfw mouse listener. */
	private GLFWMouseButtonCallback glfwMouseListener = new GLFWMouseButtonCallback()
	{
		@Override
		public void invoke(long window, int button, int action, int mods)
		{
			if (action == GLFW_RELEASE)
			{
				buttonsDown[button] = 4;
				for (int i = 0; i < listeners.size(); i++)
					listeners.get(i).buttonReleased(button);
			} else if (action == GLFW_PRESS)
			{
				buttonsDown[button] = 1;
				for (int i = 0; i < listeners.size(); i++)
					listeners.get(i).buttonPressed(button);
			}
		}
	};

	/**
	 * Instantiates a new mouse.
	 *
	 * @param window the window
	 */
	public Mouse(Window window)
	{
		this.window = window;
	}

	/**
	 * Gets the x.
	 *
	 * @return the x
	 */
	public int getX()
	{
		glfwGetCursorPos(window.getWindowHandle(), positionBuffer, null);
		int x = (int) positionBuffer.get();
		positionBuffer.flip();
		return x;
	}

	/**
	 * Gets the y.
	 *
	 * @return the y
	 */
	public int getY()
	{
		glfwGetCursorPos(window.getWindowHandle(), null, positionBuffer);
		int y = (int) positionBuffer.get();
		positionBuffer.flip();
		if (!yDown)
			y = window.getHeight() - y;
		return y;
	}

	/**
	 * Checks if is mouse in rect.
	 *
	 * @param x the x
	 * @param y the y
	 * @param width the width
	 * @param height the height
	 * @return true, if is mouse in rect
	 */
	public boolean isMouseInRect(int x, int y, int width, int height)
	{
		if (getX() >= x && getX() <= x + width && getY() >= y && getY() <= y + height)
			return true;
		else
			return false;
	}

	/**
	 * Gets the delta x.
	 *
	 * @return the delta x
	 */
	public int getDeltaX()
	{
		if (lastX < 0)
			lastX = getX();
		int delta = getX() - lastX;
		lastX = getX();
		return delta;
	}

	/**
	 * Gets the delta y.
	 *
	 * @return the delta y
	 */
	public int getDeltaY()
	{
		if (lastY < 0)
			lastY = getY();
		int delta = getY() - lastY;
		lastY = getY();
		return delta;
	}

	/**
	 * Checks if is y down.
	 *
	 * @return true, if is y down
	 */
	public boolean isYDown()
	{
		return yDown;
	}

	/**
	 * Sets the y down.
	 *
	 * @param yDown the new y down
	 */
	public void setYDown(boolean yDown)
	{
		this.yDown = yDown;
	}

	/**
	 * Center mouse.
	 */
	public void centerMouse()
	{
		glfwSetCursorPos(window.getWindowHandle(), window.getWidth() / 2, window.getHeight() / 2);
		lastX = getX();
		lastY = getY();
	}

	/**
	 * Gets the glfw mouse listener.
	 *
	 * @return the glfw mouse listener
	 */
	public GLFWMouseButtonCallback getGlfwMouseListener()
	{
		return glfwMouseListener;
	}

	/**
	 * Checks if is button pressed.
	 *
	 * @param key the key
	 * @return true, if is button pressed
	 */
	public boolean isButtonPressed(int key)
	{
		return buttonsDown[key] >= 1 && buttonsDown[key] <= 3;
	}

	/**
	 * Checks if is button just pressed.
	 *
	 * @param key the key
	 * @return true, if is button just pressed
	 */
	public boolean isButtonJustPressed(int key)
	{
		return buttonsDown[key] == 1 || buttonsDown[key] == 2;
	}

	/**
	 * Checks if is button just released.
	 *
	 * @param key the key
	 * @return true, if is button just released
	 */
	public boolean isButtonJustReleased(int key)
	{
		return buttonsDown[key] == 4 || buttonsDown[key] == 5;
	}

	/**
	 * Hide mouse.
	 */
	public void hideMouse()
	{
		glfwSetInputMode(window.getWindowHandle(), GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
	}

	/**
	 * Show mouse.
	 */
	public void showMouse()
	{
		glfwSetInputMode(window.getWindowHandle(), GLFW_CURSOR, GLFW_CURSOR_NORMAL);
	}

	/**
	 * Adds the mouse listener.
	 *
	 * @param listener the listener
	 */
	public void addMouseListener(MouseListener listener)
	{
		listeners.add(listener);
	}

	/**
	 * Removes the mouse listener.
	 *
	 * @param listener the listener
	 */
	public void removeMouseListener(MouseListener listener)
	{
		listeners.remove(listener);
	}

	@Override
	public void update(float delta)
	{
		for (int i = 0; i < buttonsDown.length; i++)
			if (buttonsDown[i] == 5)
				buttonsDown[i] = 0;
		for (int i = 0; i < buttonsDown.length; i++)
			if (buttonsDown[i] == 4)
				buttonsDown[i] = 5;
		for (int i = 0; i < buttonsDown.length; i++)
			if (buttonsDown[i] > 0 && buttonsDown[i] <= 2)
				buttonsDown[i]++;
	}

	@Override
	public void init()
	{
		buttonsDown = new byte[MAX_BUTTONS];
		lastX = -1;
		lastY = -1;
		yDown = false;

		listeners = new ArrayList<MouseListener>();

		glfwSetMouseButtonCallback(window.getWindowHandle(), glfwMouseListener);
		positionBuffer = BufferUtils.createDoubleBuffer(1);
	}

	@Override
	public void dispose()
	{

	}

}