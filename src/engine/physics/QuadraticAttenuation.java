package engine.physics;

import engine.util.math.Vector3f;

/**
 * The Class QuadraticAttenuation.
 */
public class QuadraticAttenuation
{
	
	/** The quadratic. */
	private float quadratic;
	
	/** The linear. */
	private float linear;
	
	/** The constant. */
	private float constant;

	/**
	 * Instantiates a new quadratic attenuation.
	 *
	 * @param q the q
	 * @param l the l
	 * @param c the c
	 */
	public QuadraticAttenuation(float q, float l, float c)
	{
		quadratic = q;
		linear = l;
		constant = c;
	}

	/**
	 * Gets the quadratic.
	 *
	 * @return the quadratic
	 */
	public float getQuadratic()
	{
		return quadratic;
	}

	/**
	 * Gets the linear.
	 *
	 * @return the linear
	 */
	public float getLinear()
	{
		return linear;
	}

	/**
	 * Gets the constant.
	 *
	 * @return the constant
	 */
	public float getConstant()
	{
		return constant;
	}

	/**
	 * Sets the quadratic.
	 *
	 * @param q the new quadratic
	 */
	public void setQuadratic(float q)
	{
		quadratic = q;
	}

	/**
	 * Sets the linear.
	 *
	 * @param l the new linear
	 */
	public void setLinear(float l)
	{
		linear = l;
	}

	/**
	 * Sets the constant.
	 *
	 * @param c the new constant
	 */
	public void setConstant(float c)
	{
		constant = c;
	}

	/**
	 * Sample.
	 *
	 * @param d the d
	 * @return the float
	 */
	public float sample(float d)
	{
		return 1f / (quadratic * d * d + linear * d + constant);
	}

	/**
	 * To vector3f.
	 *
	 * @return the vector3f
	 */
	public Vector3f toVector3f()
	{
		return new Vector3f(quadratic, linear, constant);
	}

}
