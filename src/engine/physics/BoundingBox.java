package engine.physics;

import engine.util.math.Vector2f;

/**
 * The Class BoundingBox.
 */
public class BoundingBox extends BoundingVolume
{

	/** The width. */
	private float width;
	
	/** The height. */
	private float height;

	/**
	 * Instantiates a new bounding box.
	 */
	public BoundingBox()
	{

	}

	/**
	 * Instantiates a new bounding box.
	 *
	 * @param position the position
	 * @param width the width
	 * @param height the height
	 */
	public BoundingBox(Vector2f position, float width, float height)
	{
		super(position);
		this.width = width;
		this.height = height;
	}

	/**
	 * Gets the width.
	 *
	 * @return the width
	 */
	public float getWidth()
	{
		return width;
	}

	/**
	 * Gets the height.
	 *
	 * @return the height
	 */
	public float getHeight()
	{
		return height;
	}

	/**
	 * Sets the width.
	 *
	 * @param width the new width
	 */
	public void setWidth(float width)
	{
		this.width = width;
	}

	/**
	 * Sets the height.
	 *
	 * @param height the new height
	 */
	public void setHeight(float height)
	{
		this.height = height;
	}

}
