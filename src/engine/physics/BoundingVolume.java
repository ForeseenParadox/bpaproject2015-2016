package engine.physics;

import engine.util.math.Vector2f;

/**
 * The Class BoundingVolume.
 */
public abstract class BoundingVolume
{

	/** The position. */
	protected Vector2f position;

	/**
	 * Instantiates a new bounding volume.
	 */
	public BoundingVolume()
	{
		position = new Vector2f();
	}

	/**
	 * Instantiates a new bounding volume.
	 *
	 * @param position the position
	 */
	public BoundingVolume(Vector2f position)
	{
		this.position = position;
	}

	/**
	 * Gets the position.
	 *
	 * @return the position
	 */
	public Vector2f getPosition()
	{
		return position;
	}

	/**
	 * Sets the position.
	 *
	 * @param position the new position
	 */
	public void setPosition(Vector2f position)
	{
		this.position = position;
	}

}
