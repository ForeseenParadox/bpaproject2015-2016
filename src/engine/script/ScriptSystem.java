package engine.script;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

import engine.core.Subsystem;

/**
 * The Class ScriptSystem.
 */
public class ScriptSystem implements Subsystem
{

	/** The script engine. */
	private ScriptEngine scriptEngine;

	/**
	 * Instantiates a new script system.
	 */
	public ScriptSystem()
	{
	}

	/**
	 * Gets the java script engine.
	 *
	 * @return the java script engine
	 */
	public ScriptEngine getJavaScriptEngine()
	{
		return scriptEngine;
	}

	@Override
	public void init()
	{
		ScriptEngineManager manager = new ScriptEngineManager();
		scriptEngine = manager.getEngineByName("JavaScript");
	}

	@Override
	public void dispose()
	{

	}

}
