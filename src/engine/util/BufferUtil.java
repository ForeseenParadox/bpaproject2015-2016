package engine.util;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import org.lwjgl.BufferUtils;

import engine.util.math.Matrix4f;

/**
 * The Class BufferUtil.
 */
public class BufferUtil
{

	/**
	 * Matrix4f to float buffer.
	 *
	 * @param matrix the matrix
	 * @return the float buffer
	 */
	public static FloatBuffer matrix4fToFloatBuffer(Matrix4f matrix)
	{
		FloatBuffer result = BufferUtils.createFloatBuffer(16);
		for (float f : matrix.getValues())
			result.put(f);
		result.flip();
		return result;
	}

	/**
	 * Byte array to buffer.
	 *
	 * @param array the array
	 * @return the byte buffer
	 */
	public static ByteBuffer byteArrayToBuffer(byte[] array)
	{
		ByteBuffer result = BufferUtils.createByteBuffer(array.length);
		result.put(array);
		result.flip();
		return result;
	}

	/**
	 * Int array to buffer.
	 *
	 * @param array the array
	 * @return the int buffer
	 */
	public static IntBuffer intArrayToBuffer(int[] array)
	{
		IntBuffer result = BufferUtils.createIntBuffer(array.length);
		result.put(array);
		result.flip();
		return result;
	}

}
