package engine.util.xml;

/**
 * The Class XMLHeader.
 */
public class XMLHeader
{

	/** The xml version. */
	private String xmlVersion;
	
	/** The xml encoding. */
	private String xmlEncoding;

	/**
	 * Instantiates a new XML header.
	 *
	 * @param xmlVersion the xml version
	 * @param xmlEncoding the xml encoding
	 */
	public XMLHeader(String xmlVersion, String xmlEncoding)
	{
		this.xmlVersion = xmlVersion;
		this.xmlEncoding = xmlEncoding;
	}

	/**
	 * Gets the xml version.
	 *
	 * @return the xml version
	 */
	public String getXmlVersion()
	{
		return xmlVersion;
	}

	/**
	 * Gets the xml encoding.
	 *
	 * @return the xml encoding
	 */
	public String getXmlEncoding()
	{
		return xmlEncoding;
	}

	/**
	 * Sets the xml version.
	 *
	 * @param xmlVersion the new xml version
	 */
	public void setXmlVersion(String xmlVersion)
	{
		this.xmlVersion = xmlVersion;
	}

	/**
	 * Sets the xml encoding.
	 *
	 * @param xmlEncoding the new xml encoding
	 */
	public void setXmlEncoding(String xmlEncoding)
	{
		this.xmlEncoding = xmlEncoding;
	}

}
