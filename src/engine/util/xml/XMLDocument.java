package engine.util.xml;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import engine.resource.TextFileResource;

/**
 * The Class XMLDocument.
 */
public class XMLDocument
{

	/** The header. */
	private XMLHeader header;
	
	/** The root element. */
	private XMLElement rootElement;
	
	/**
	 * Instantiates a new XML document.
	 */
	public XMLDocument()
	{
	}

	/**
	 * Instantiates a new XML document.
	 *
	 * @param resource the resource
	 */
	public XMLDocument(TextFileResource resource)
	{
		if (resource == null)
			throw new IllegalArgumentException("TextResource is null.");

		Document doc = createDOMDocument(resource.getLoadedText());

		// initialize root
		Element root = doc.getDocumentElement();
		rootElement = new XMLElement();
		rootElement.setName(root.getNodeName());

		// add atribs
		NamedNodeMap attributes = root.getAttributes();
		List<XMLAttrib> attribs = new ArrayList<XMLAttrib>();
		for (int i = 0; i < attributes.getLength(); i++)
		{
			String key = attributes.item(i).getNodeName();
			String value = attributes.item(i).getNodeValue();
			attribs.add(new XMLAttrib(key, value));
		}
		rootElement.addAllAttributes(attribs);

		// add children
		NodeList children = root.getChildNodes();
		rootElement.addAllNodesAsChildren(children);
	}

	/**
	 * Gets the xml header.
	 *
	 * @return the xml header
	 */
	public XMLHeader getXmlHeader()
	{
		return header;
	}

	/**
	 * Gets the root element.
	 *
	 * @return the root element
	 */
	public XMLElement getRootElement()
	{
		return rootElement;
	}

	/**
	 * Sets the xml header.
	 *
	 * @param header the new xml header
	 */
	public void setXmlHeader(XMLHeader header)
	{
		this.header = header;
	}

	/**
	 * Sets the root element.
	 *
	 * @param rootElement the new root element
	 */
	public void setRootElement(XMLElement rootElement)
	{
		this.rootElement = rootElement;
	}

	/**
	 * Save.
	 *
	 * @param out the out
	 */
	public void save(OutputStream out)
	{
		try
		{
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out));

			// version and encoding
			if (header != null)
				writer.append("<?xml version=\"" + header.getXmlVersion() + "\" encoding=\"" + header.getXmlEncoding() + "\"?>\n");

			if (rootElement != null)
				writer.append(rootElement.toStringWithChildren(0));

			writer.close();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * Creates the dom document.
	 *
	 * @param text the text
	 * @return the document
	 */
	public Document createDOMDocument(String text)
	{
		try
		{
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			// factory.setValidating(true);
			factory.setIgnoringComments(true);
			factory.setIgnoringElementContentWhitespace(true);
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse(new InputSource(new StringReader(text)));
			doc.getDocumentElement().normalize();
			header = new XMLHeader(doc.getXmlVersion(), doc.getXmlEncoding());
			return doc;
		} catch (IOException e)
		{
			e.printStackTrace();
		} catch (SAXException e)
		{
			e.printStackTrace();
		} catch (ParserConfigurationException e)
		{
			e.printStackTrace();
		}

		return null;
	}

}
