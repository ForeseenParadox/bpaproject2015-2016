package engine.util.xml;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * The Class XMLElement.
 */
public class XMLElement
{

	/** The name. */
	private String name;
	
	/** The value. */
	private String value;
	
	/** The children. */
	private List<XMLElement> children;
	
	/** The attribs. */
	private List<XMLAttrib> attribs;

	/**
	 * Instantiates a new XML element.
	 */
	public XMLElement()
	{
		this(null, null);
	}

	/**
	 * Instantiates a new XML element.
	 *
	 * @param name the name
	 * @param value the value
	 */
	public XMLElement(String name, String value)
	{
		this.name = name;
		this.value = value;
		children = new ArrayList<XMLElement>();
		attribs = new ArrayList<XMLAttrib>();
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue()
	{
		return value;
	}

	/**
	 * Gets the children.
	 *
	 * @return the children
	 */
	public List<XMLElement> getChildren()
	{
		return children;
	}

	/**
	 * Gets the descendents.
	 *
	 * @return the descendents
	 */
	public List<XMLElement> getDescendents()
	{
		List<XMLElement> res = new ArrayList<XMLElement>();
		for (XMLElement child : children)
		{
			res.add(child);
			if (child.getChildren().size() > 0)
			{
				res.addAll(child.getDescendents());
			}
		}
		return res;
	}

	/**
	 * Gets the attribs.
	 *
	 * @return the attribs
	 */
	public List<XMLAttrib> getAttribs()
	{
		return attribs;
	}

	/**
	 * Gets the attrib value.
	 *
	 * @param name the name
	 * @return the attrib value
	 */
	public String getAttribValue(String name)
	{
		for (XMLAttrib attrib : getAttribs())
		{
			if (attrib.getKey().equals(name))
			{
				return attrib.getValue();
			}
		}
		return null;
	}

	/**
	 * Gets the attrib.
	 *
	 * @param name the name
	 * @return the attrib
	 */
	public XMLAttrib getAttrib(String name)
	{
		for (XMLAttrib attrib : getAttribs())
		{
			if (attrib.getKey().equals(name))
			{
				return attrib;
			}
		}
		return null;
	}

	/**
	 * Gets the child by name.
	 *
	 * @param name the name
	 * @return the child by name
	 */
	public XMLElement getChildByName(String name)
	{
		List<XMLElement> result = getChildrenByName(name);
		if (result.size() > 0)
		{
			return result.get(0);
		} else
		{
			return null;
		}
	}

	/**
	 * Gets the children by name.
	 *
	 * @param name the name
	 * @return the children by name
	 */
	public List<XMLElement> getChildrenByName(String name)
	{
		List<XMLElement> result = new ArrayList<XMLElement>();
		for (XMLElement element : children)
		{
			if (element.getName().equals(name))
			{
				result.add(element);
			}
		}
		return result;
	}

	/**
	 * Adds the child.
	 *
	 * @param child the child
	 */
	public void addChild(XMLElement child)
	{
		this.children.add(child);
	}

	/**
	 * Adds the all nodes as children.
	 *
	 * @param nodeList the node list
	 */
	public void addAllNodesAsChildren(NodeList nodeList)
	{
		for (int i = 0; i < nodeList.getLength(); i++)
		{
			Node node = nodeList.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE)
			{

				XMLElement element = new XMLElement();
				element.setName(node.getNodeName());

				if (node.hasChildNodes())
				{
					NodeList children = node.getChildNodes();
					boolean child = true;
					for (int j = 0; j < children.getLength(); j++)
					{
						if (children.item(j).getNodeType() == Node.ELEMENT_NODE)
						{
							child = false;
						}
					}

					if (child)
					{
						element.setValue(node.getTextContent());
					}
				}

				if (node.hasAttributes())
				{
					NamedNodeMap attributes = node.getAttributes();
					List<XMLAttrib> attribs = new ArrayList<XMLAttrib>();
					for (int j = 0; j < attributes.getLength(); j++)
					{
						String key = attributes.item(j).getNodeName();
						String value = attributes.item(j).getNodeValue();
						attribs.add(new XMLAttrib(key, value));
					}
					element.addAllAttributes(attribs);
				}
				addChild(element);

				if (node.hasChildNodes())
				{
					element.addAllNodesAsChildren(node.getChildNodes());
				} else
				{
				}
			}

		}
	}

	/**
	 * Removes the child.
	 *
	 * @param child the child
	 */
	public void removeChild(XMLElement child)
	{
		this.children.remove(child);
	}

	/**
	 * Adds the attrib.
	 *
	 * @param attrib the attrib
	 */
	public void addAttrib(XMLAttrib attrib)
	{
		this.attribs.add(attrib);
	}

	/**
	 * Adds the all attributes.
	 *
	 * @param attribs the attribs
	 */
	public void addAllAttributes(List<XMLAttrib> attribs)
	{
		this.attribs.addAll(attribs);
	}

	/**
	 * Removes the attrib.
	 *
	 * @param attrib the attrib
	 */
	public void removeAttrib(XMLAttrib attrib)
	{
		this.attribs.remove(attrib);
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name)
	{
		this.name = name;
	}

	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value)
	{
		this.value = value;
	}

	/**
	 * Tabulate.
	 *
	 * @param builder the builder
	 * @param count the count
	 */
	public void tabulate(StringBuilder builder, int count)
	{
		for (int i = 0; i < count; i++)
		{
			builder.append('\t');
		}
	}

	/**
	 * To string with children.
	 *
	 * @param tabs the tabs
	 * @return the string
	 */
	public String toStringWithChildren(int tabs)
	{
		StringBuilder builder = new StringBuilder();

		// add tabs as needed
		tabulate(builder, tabs);

		// add start tag and attributes
		builder.append("<" + name + "");
		if (attribs.size() > 0)
		{
			for (int i = 0; i < attribs.size(); i++)
			{
				XMLAttrib attrib = attribs.get(i);
				builder.append(" " + attrib.getKey() + "=" + "\"" + attrib.getValue() + "\"");
				/*
				 * if (i < (attribs.size() - 1)) { builder.append(" "); }
				 */
			}
		}

		builder.append(">");

		// test if there are children

		if (getChildren().size() > 0)
		{
			builder.append("\n");
			for (XMLElement child : children)
			{
				builder.append(child.toStringWithChildren(tabs + 1));
			}
			tabulate(builder, tabs);
			builder.append("</" + name + ">\n");
		} else
		{
			if (value != null)
			{
				builder.append(value);
			}
			builder.append("</" + name + ">\n");
		}

		return builder.toString();
	}

	@Override
	public String toString()
	{
		return "";
	}
}
