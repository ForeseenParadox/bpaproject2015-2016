package engine.util;

/**
 * The Class StringUtils.
 */
public class StringUtils
{

	/**
	 * Concatenate.
	 *
	 * @param strings the strings
	 * @return the string
	 */
	public static String concatenate(String[] strings)
	{
		StringBuilder result = new StringBuilder();
		for (String x : strings)
			result.append(x + "\n");
		return result.toString();
	}

}
