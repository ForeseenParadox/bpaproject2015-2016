package engine.util.math;

/**
 * The Class Vector2f.
 */
public class Vector2f
{

	/** The Constant ZERO. */
	public static final Vector2f ZERO = new Vector2f();

	/** The x. */
	private float x;
	
	/** The y. */
	private float y;

	/**
	 * Instantiates a new vector2f.
	 */
	public Vector2f()
	{
		this(0, 0);
	}

	/**
	 * Instantiates a new vector2f.
	 *
	 * @param x the x
	 * @param y the y
	 */
	public Vector2f(float x, float y)
	{
		this.x = x;
		this.y = y;
	}

	/**
	 * Instantiates a new vector2f.
	 *
	 * @param copy the copy
	 */
	public Vector2f(Vector2f copy)
	{
		this.x = copy.getX();
		this.y = copy.getY();
	}

	/**
	 * Gets the x.
	 *
	 * @return the x
	 */
	public float getX()
	{
		return x;
	}

	/**
	 * Gets the y.
	 *
	 * @return the y
	 */
	public float getY()
	{
		return y;
	}

	/**
	 * Sets the x.
	 *
	 * @param x the new x
	 */
	public void setX(float x)
	{
		this.x = x;
	}

	/**
	 * Sets the y.
	 *
	 * @param y the new y
	 */
	public void setY(float y)
	{
		this.y = y;
	}

	/**
	 * Sets the.
	 *
	 * @param x the x
	 * @param y the y
	 */
	public void set(float x, float y)
	{
		this.x = x;
		this.y = y;
	}

	/**
	 * Adds the.
	 *
	 * @param vec the vec
	 * @return the vector2f
	 */
	public Vector2f add(Vector2f vec)
	{
		return new Vector2f(x + vec.x, y + vec.y);
	}

	/**
	 * Mutate add.
	 *
	 * @param vec the vec
	 * @return the vector2f
	 */
	public Vector2f mutateAdd(Vector2f vec)
	{
		this.x += vec.getX();
		this.y += vec.getY();
		return this;
	}

	/**
	 * Subtract.
	 *
	 * @param vec the vec
	 * @return the vector2f
	 */
	public Vector2f subtract(Vector2f vec)
	{
		return new Vector2f(x - vec.x, y - vec.y);
	}

	/**
	 * Multiply.
	 *
	 * @param scaler the scaler
	 * @return the vector2f
	 */
	public Vector2f multiply(float scaler)
	{
		return new Vector2f(x * scaler, y * scaler);
	}

	/**
	 * Length.
	 *
	 * @return the float
	 */
	public float length()
	{
		return (float) Math.sqrt(x * x + y * y);
	}

	/**
	 * Length squared.
	 *
	 * @return the float
	 */
	public float lengthSquared()
	{
		return x * x + y * y;
	}

	/**
	 * Normalized.
	 *
	 * @return the vector2f
	 */
	public Vector2f normalized()
	{
		float length = length();

		return new Vector2f(x / length, y / length);
	}

	/**
	 * Dot product.
	 *
	 * @param vec the vec
	 * @return the float
	 */
	public float dotProduct(Vector2f vec)
	{
		return x * vec.x + y * vec.y;
	}

	@Override
	public String toString()
	{
		return "[" + x + ", " + y + "]";
	}

}