package engine.util.math;

/**
 * The Class Vector3f.
 */
public class Vector3f
{

	/** The x. */
	private float x;
	
	/** The y. */
	private float y;
	
	/** The z. */
	private float z;

	/**
	 * Instantiates a new vector3f.
	 */
	public Vector3f()
	{
		this(0, 0, 0);
	}

	/**
	 * Instantiates a new vector3f.
	 *
	 * @param x the x
	 * @param y the y
	 * @param z the z
	 */
	public Vector3f(float x, float y, float z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
	}

	/**
	 * Gets the x.
	 *
	 * @return the x
	 */
	public float getX()
	{
		return x;
	}

	/**
	 * Gets the y.
	 *
	 * @return the y
	 */
	public float getY()
	{
		return y;
	}

	/**
	 * Gets the z.
	 *
	 * @return the z
	 */
	public float getZ()
	{
		return z;
	}

	/**
	 * Sets the x.
	 *
	 * @param x the new x
	 */
	public void setX(float x)
	{
		this.x = x;
	}

	/**
	 * Sets the y.
	 *
	 * @param y the new y
	 */
	public void setY(float y)
	{
		this.y = y;
	}

	/**
	 * Sets the z.
	 *
	 * @param z the new z
	 */
	public void setZ(float z)
	{
		this.z = z;
	}

	/**
	 * Sets the.
	 *
	 * @param values the values
	 */
	public void set(float[] values)
	{
		x = values[0];
		y = values[1];
		z = values[2];
	}

	@Override
	public String toString()
	{
		return "vec3f[x=" + x + ", y=" + y + ", z=" + z + "]";
	}

}