package engine.util.math;

/**
 * The Class Matrix4f.
 */
public class Matrix4f
{

	/** The values. */
	private float[] values;

	/**
	 * Instantiates a new matrix4f.
	 */
	public Matrix4f()
	{
		values = new float[16];
	}

	/**
	 * Instantiates a new matrix4f.
	 *
	 * @param values the values
	 */
	public Matrix4f(float[] values)
	{
		this.values = values;
	}

	/**
	 * Gets the values.
	 *
	 * @return the values
	 */
	public float[] getValues()
	{
		return values;
	}

	/**
	 * Mutate add.
	 *
	 * @param other the other
	 */
	public void mutateAdd(Matrix4f other)
	{
		for (int i = 0; i < values.length; i++)
			values[i] += other.values[i];
	}

	/**
	 * Mutate subtract.
	 *
	 * @param other the other
	 */
	public void mutateSubtract(Matrix4f other)
	{
		for (int i = 0; i < values.length; i++)
			values[i] -= other.values[i];
	}

	/**
	 * Mutate multiply.
	 *
	 * @param other the other
	 */
	public void mutateMultiply(Matrix4f other)
	{
		float[] newMatrix = new float[16];
		for (int i = 0; i < 4; i++)
			for (int j = 0; j < 4; j++)
				for (int k = 0; k < 4; k++)
					newMatrix[j + i * 4] += values[i * 4 + k] * other.values[k * 4 + j];
		for (int i = 0; i < newMatrix.length; i++)
			values[i] = newMatrix[i];
	}

	/**
	 * Multiply.
	 *
	 * @param other the other
	 * @return the matrix4f
	 */
	public Matrix4f multiply(Matrix4f other)
	{
		Matrix4f result = new Matrix4f();
		for (int i = 0; i < 4; i++)
			for (int j = 0; j < 4; j++)
				for (int k = 0; k < 4; k++)
					result.values[j + i * 4] += values[i * 4 + k] * other.values[k * 4 + j];
		return result;
	}

	/**
	 * Adds the.
	 *
	 * @param other the other
	 * @return the matrix4f
	 */
	public Matrix4f add(Matrix4f other)
	{
		Matrix4f result = new Matrix4f();
		for (int i = 0; i < values.length; i++)
			result.values[i] = values[i] + other.values[i];
		return result;
	}

	/**
	 * Subtract.
	 *
	 * @param other the other
	 * @return the matrix4f
	 */
	public Matrix4f subtract(Matrix4f other)
	{
		Matrix4f result = new Matrix4f();
		for (int i = 0; i < values.length; i++)
			result.values[i] = values[i] - other.values[i];
		return result;
	}

	/**
	 * Sets the identity.
	 */
	public void setIdentity()
	{
		for (int i = 0; i < 4; i++)
			for (int j = 0; j < 4; j++)
				if (i == j)
					values[i * 4 + j] = 1;
				else
					values[i * 4 + j] = 0;
	}

	/**
	 * Copy.
	 *
	 * @param other the other
	 */
	public void copy(Matrix4f other)
	{
		for (int i = 0; i < 16; i++)
			values[i] = other.values[i];
	}

	/**
	 * Sets the zero.
	 */
	public void setZero()
	{
		for (int i = 0; i < values.length; i++)
			values[i] = 0;
	}

	/**
	 * Sets the to translation.
	 *
	 * @param x the x
	 * @param y the y
	 * @param z the z
	 */
	// transformation utils
	public void setToTranslation(float x, float y, float z)
	{
		setIdentity();
		values[(0) * 4 + (3)] = x;
		values[(1) * 4 + (3)] = y;
		values[(2) * 4 + (3)] = z;
	}

	/**
	 * Sets the to translation.
	 *
	 * @param translation the new to translation
	 */
	public void setToTranslation(Vector3f translation)
	{
		setToTranslation(translation.getX(), translation.getY(), translation.getZ());
	}

	/**
	 * Sets the to rotation x.
	 *
	 * @param theta the new to rotation x
	 */
	public void setToRotationX(float theta)
	{
		setIdentity();
		values[(1) * 4 + (1)] = (float) Math.cos(theta);
		values[(1) * 4 + (2)] = (float) -Math.sin(theta);
		values[(2) * 4 + (1)] = (float) Math.sin(theta);
		values[(2) * 4 + (2)] = (float) Math.cos(theta);
	}

	/**
	 * Sets the to rotation y.
	 *
	 * @param theta the new to rotation y
	 */
	public void setToRotationY(float theta)
	{
		setIdentity();
		values[(0) * 4 + (0)] = (float) Math.cos(theta);
		values[(0) * 4 + (2)] = (float) Math.sin(theta);
		values[(2) * 4 + (0)] = (float) -Math.sin(theta);
		values[(2) * 4 + (2)] = (float) Math.cos(theta);
	}

	/**
	 * Sets the to rotation z.
	 *
	 * @param theta the new to rotation z
	 */
	public void setToRotationZ(float theta)
	{
		setIdentity();
		values[(0) * 4 + (0)] = (float) Math.cos(theta);
		values[(0) * 4 + (1)] = (float) -Math.sin(theta);
		values[(1) * 4 + (0)] = (float) Math.sin(theta);
		values[(1) * 4 + (1)] = (float) Math.cos(theta);
	}

	/**
	 * Sets the to scale.
	 *
	 * @param sx the sx
	 * @param sy the sy
	 * @param sz the sz
	 */
	public void setToScale(float sx, float sy, float sz)
	{
		setIdentity();
		values[(0) * 4 + (0)] = sx;
		values[(1) * 4 + (1)] = sy;
		values[(2) * 4 + (2)] = sz;
	}

	/**
	 * Sets the to scale.
	 *
	 * @param scale the new to scale
	 */
	public void setToScale(Vector3f scale)
	{
		setToScale(scale.getX(), scale.getY(), scale.getZ());
	}

	/**
	 * Sets the orthogonal projection.
	 *
	 * @param left the left
	 * @param right the right
	 * @param bottom the bottom
	 * @param top the top
	 * @param zNear the z near
	 * @param zFar the z far
	 */
	public void setOrthogonalProjection(float left, float right, float bottom, float top, float zNear, float zFar)
	{
		setIdentity();
		values[(0) * 4 + (0)] = 2 / (right - left);
		values[(1) * 4 + (1)] = 2 / (top - bottom);
		values[(3) * 4 + (2)] = -2 / (zFar - zNear);
		values[(0) * 4 + (3)] = -(right + left) / (right - left);
		values[(1) * 4 + (3)] = -(right + left) / (right - left);
		values[(2) * 4 + (3)] = -(zFar + zNear) / (zFar - zNear);
	}

	/**
	 * Sets the perspective projection.
	 *
	 * @param aspect the aspect
	 * @param fov the fov
	 * @param zNear the z near
	 * @param zFar the z far
	 */
	public void setPerspectiveProjection(float aspect, float fov, float zNear, float zFar)
	{
		setZero();
		values[(0) * 4 + (0)] = (float) (1 / (aspect * Math.tan(fov / 2)));
		values[(1) * 4 + (1)] = (float) (1 / Math.tan(fov / 2));
		values[(2) * 4 + (2)] = (-zNear - zNear) / (zNear - zFar);
		values[(2) * 4 + (3)] = (2 * zFar * zNear) / (zNear - zFar);
		values[(3) * 4 + (2)] = 1;
	}

	@Override
	public String toString()
	{
		String s = "";
		for (int i = 0; i < 4; i++)
		{
			for (int j = 0; j < 4; j++)
				s += values[j + i * 4] + " ";
			s += "\n";
		}
		return s;
	}

}
