package engine.util.math;

/**
 * The Class Vector4f.
 */
public class Vector4f
{

	/** The x. */
	public float x;
	
	/** The y. */
	public float y;
	
	/** The z. */
	public float z;
	
	/** The w. */
	public float w;

	/**
	 * Instantiates a new vector4f.
	 */
	public Vector4f()
	{
		this(0, 0, 0, 0);
	}

	/**
	 * Instantiates a new vector4f.
	 *
	 * @param x the x
	 * @param y the y
	 * @param z the z
	 * @param w the w
	 */
	public Vector4f(float x, float y, float z, float w)
	{
		this.x = x;
		this.y = y;
		this.z = z;
		this.w = w;
	}

	/**
	 * Gets the x.
	 *
	 * @return the x
	 */
	public float getX()
	{
		return x;
	}

	/**
	 * Gets the y.
	 *
	 * @return the y
	 */
	public float getY()
	{
		return y;
	}

	/**
	 * Gets the z.
	 *
	 * @return the z
	 */
	public float getZ()
	{
		return z;
	}

	/**
	 * Gets the w.
	 *
	 * @return the w
	 */
	public float getW()
	{
		return w;
	}

	/**
	 * Sets the x.
	 *
	 * @param x the new x
	 */
	public void setX(float x)
	{
		this.x = x;
	}

	/**
	 * Sets the y.
	 *
	 * @param y the new y
	 */
	public void setY(float y)
	{
		this.y = y;
	}

	/**
	 * Sets the z.
	 *
	 * @param z the new z
	 */
	public void setZ(float z)
	{
		this.z = z;
	}

	/**
	 * Sets the w.
	 *
	 * @param w the new w
	 */
	public void setW(float w)
	{
		this.w = w;
	}

	/**
	 * Sets the.
	 *
	 * @param x the x
	 * @param y the y
	 * @param z the z
	 * @param w the w
	 */
	public void set(float x, float y, float z, float w)
	{
		this.x = x;
		this.y = y;
		this.z = z;
		this.w = w;
	}

	/**
	 * Mutate multiply.
	 *
	 * @param matrix the matrix
	 */
	public void mutateMultiply(Matrix4f matrix)
	{
		float[] values = matrix.getValues();
		float nx = values[0 * 4 + 0] * x + values[0 * 4 + 1] * y + values[0 * 4 + 2] * z + values[0 * 4 + 3] * w;
		float ny = values[1 * 4 + 0] * x + values[1 * 4 + 1] * y + values[1 * 4 + 2] * z + values[1 * 4 + 3] * w;
		float nz = values[2 * 4 + 0] * x + values[2 * 4 + 1] * y + values[2 * 4 + 2] * z + values[2 * 4 + 3] * w;
		float nw = values[3 * 4 + 0] * x + values[3 * 4 + 1] * y + values[3 * 4 + 2] * z + values[3 * 4 + 3] * w;
		x = nx;
		y = ny;
		z = nz;
		w = nw;
	}

	/**
	 * Translate.
	 *
	 * @param tx the tx
	 * @param ty the ty
	 * @param tz the tz
	 * @param tw the tw
	 */
	public void translate(float tx, float ty, float tz, float tw)
	{
		x += tx;
		y += ty;
		z += tz;
		w += tw;
	}

}
