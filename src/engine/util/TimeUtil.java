package engine.util;

import java.util.Calendar;

/**
 * The Class TimeUtil.
 */
public class TimeUtil
{

	/**
	 * Gets the second.
	 *
	 * @return the second
	 */
	public static int getSecond()
	{
		return Calendar.getInstance().get(Calendar.SECOND);
	}

	/**
	 * Gets the minute.
	 *
	 * @return the minute
	 */
	public static int getMinute()
	{
		return Calendar.getInstance().get(Calendar.MINUTE);
	}

	/**
	 * Gets the hour12.
	 *
	 * @return the hour12
	 */
	public static int getHour12()
	{
		return Calendar.getInstance().get(Calendar.HOUR);
	}

	/**
	 * Gets the hour24.
	 *
	 * @return the hour24
	 */
	public static int getHour24()
	{
		return Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
	}

	/**
	 * Gets the 12 hour time stamp.
	 *
	 * @return the 12 hour time stamp
	 */
	public static String get12HourTimeStamp()
	{
		return String.format("%02d:%02d:%02d %s", getHour12(), getMinute(), getSecond(), getDayPeriod());
	}

	/**
	 * Gets the day period.
	 *
	 * @return the day period
	 */
	public static String getDayPeriod()
	{
		int hours = getHour24();

		if (hours >= 0 && hours < 12)
			return "AM";
		else
			return "PM";
	}

	/**
	 * Gets the 24 hour time stamp.
	 *
	 * @return the 24 hour time stamp
	 */
	public static String get24HourTimeStamp()
	{
		return String.format("%02d:%02d:%02d", getHour24(), getMinute(), getSecond());
	}

}