package engine.util.log;

import java.io.IOException;
import java.io.OutputStream;

import engine.core.Engine;
import engine.util.TimeUtil;

/**
 * The Class Logger.
 */
public class Logger
{

	/** The output streams. */
	private OutputStream[] outputStreams;

	/**
	 * Instantiates a new logger.
	 */
	public Logger()
	{
		this(new OutputStream[]
		{
				System.out });
	}

	/**
	 * Instantiates a new logger.
	 *
	 * @param outputStreams the output streams
	 */
	public Logger(OutputStream[] outputStreams)
	{
		this.outputStreams = outputStreams;
	}

	/**
	 * Gets the output streams.
	 *
	 * @return the output streams
	 */
	public OutputStream[] getOutputStreams()
	{
		return outputStreams;
	}

	/**
	 * Sets the output streams.
	 *
	 * @param outputStreams the new output streams
	 */
	public void setOutputStreams(OutputStream[] outputStreams)
	{
		this.outputStreams = outputStreams;
	}

	/**
	 * Normal.
	 *
	 * @param message the message
	 */
	public void normal(String message)
	{
		log(message, LogLevel.NORMAL);
	}

	/**
	 * Warning.
	 *
	 * @param message the message
	 */
	public void warning(String message)
	{
		log(message, LogLevel.WARNING);
	}

	/**
	 * Severe.
	 *
	 * @param message the message
	 */
	public void severe(String message)
	{
		log(message, LogLevel.SEVERE);
	}

	/**
	 * Log.
	 *
	 * @param message the message
	 * @param level the level
	 */
	public void log(String message, LogLevel level)
	{
		String header = "[" + level + "]" + "[" + TimeUtil.get12HourTimeStamp() + "]: " + message + "\n";
		byte[] bytes = header.getBytes();
		for (OutputStream out : outputStreams)
		{
			try
			{
				out.write(bytes);
			} catch (IOException e)
			{
				Engine.getInstance().crash(e);
			}
		}
	}

}