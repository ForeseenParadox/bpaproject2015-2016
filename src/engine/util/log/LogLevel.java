package engine.util.log;

/**
 * The Enum LogLevel.
 */
public enum LogLevel
{

	/** The normal. */
	NORMAL,
	
	/** The warning. */
	WARNING,
	
	/** The severe. */
	SEVERE;

}