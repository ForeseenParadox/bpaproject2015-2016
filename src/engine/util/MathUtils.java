package engine.util;

/**
 * The Class MathUtils.
 */
public class MathUtils
{

	/**
	 * Random float.
	 *
	 * @param min the min
	 * @param max the max
	 * @return the float
	 */
	public static float randomFloat(float min, float max)
	{
		return min + (float) Math.random() * (max - min);
	}

}
