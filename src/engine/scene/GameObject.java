package engine.scene;

import java.util.ArrayList;
import java.util.List;

import engine.event.listener.RenderListener;
import engine.event.listener.UpdateListener;

/**
 * The Class GameObject.
 */
public abstract class GameObject implements UpdateListener, RenderListener
{

	/** The children. */
	private List<GameObject> children;
	
	/** The components. */
	private List<GameComponent> components;

	/**
	 * Instantiates a new game object.
	 */
	public GameObject()
	{
		children = new ArrayList<GameObject>();
		components = new ArrayList<GameComponent>();
	}

	/**
	 * Adds the child.
	 *
	 * @param child the child
	 */
	public void addChild(GameObject child)
	{
		children.add(child);
	}

	/**
	 * Removes the child.
	 *
	 * @param child the child
	 */
	public void removeChild(GameObject child)
	{
		children.remove(child);
	}

	/**
	 * Gets the children.
	 *
	 * @return the children
	 */
	public List<GameObject> getChildren()
	{
		return children;
	}

	/**
	 * Adds the component.
	 *
	 * @param component the component
	 */
	public void addComponent(GameComponent component)
	{
		components.add(component);
	}

	/**
	 * Removes the component.
	 *
	 * @param component the component
	 */
	public void removeComponent(GameComponent component)
	{
		components.remove(component);
	}

	/**
	 * Gets the components.
	 *
	 * @return the components
	 */
	public List<GameComponent> getComponents()
	{
		return components;
	}

	/**
	 * Update all.
	 *
	 * @param delta the delta
	 */
	public void updateAll(float delta)
	{
		update(delta);
		for (GameObject child : children)
			child.update(delta);
	}

	/**
	 * Render all.
	 */
	public void renderAll()
	{
		render();
		for (GameObject child : children)
			child.render();
	}

	@Override
	public void update(float delta)
	{
		for (GameComponent component : components)
			component.update(delta);
	}

	@Override
	public void render()
	{
		for (GameComponent component : components)
			component.render();
	}
}