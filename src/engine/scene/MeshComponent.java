package engine.scene;

import static org.lwjgl.opengl.GL11.GL_QUADS;

import engine.graphics.Mesh;
import engine.graphics.Transform;
import engine.graphics.shader.ShaderProgram;
import engine.util.math.Matrix4f;

/**
 * The Class MeshComponent.
 */
public class MeshComponent extends GameComponent
{

	/** The shader. */
	private ShaderProgram shader;
	
	/** The mesh. */
	private Mesh mesh;
	
	/** The view. */
	private Matrix4f view;
	
	/** The projection. */
	private Matrix4f projection;
	
	/** The model. */
	private Transform model;

	/**
	 * Instantiates a new mesh component.
	 *
	 * @param shader the shader
	 * @param mesh the mesh
	 * @param view the view
	 * @param projection the projection
	 * @param model the model
	 */
	public MeshComponent(ShaderProgram shader, Mesh mesh, Matrix4f view, Matrix4f projection, Transform model)
	{
		this.shader = shader;
		this.mesh = mesh;
		this.view = view;
		this.projection = projection;
		this.model = model;
	}

	/**
	 * Gets the shader.
	 *
	 * @return the shader
	 */
	public ShaderProgram getShader()
	{
		return shader;
	}

	/**
	 * Gets the mesh.
	 *
	 * @return the mesh
	 */
	public Mesh getMesh()
	{
		return mesh;
	}

	/**
	 * Gets the view.
	 *
	 * @return the view
	 */
	public Matrix4f getView()
	{
		return view;
	}

	/**
	 * Gets the projection.
	 *
	 * @return the projection
	 */
	public Matrix4f getProjection()
	{
		return projection;
	}

	/**
	 * Gets the model.
	 *
	 * @return the model
	 */
	public Transform getModel()
	{
		return model;
	}

	/**
	 * Sets the shader.
	 *
	 * @param shader the new shader
	 */
	public void setShader(ShaderProgram shader)
	{
		this.shader = shader;
	}

	/**
	 * Sets the mesh.
	 *
	 * @param mesh the new mesh
	 */
	public void setMesh(Mesh mesh)
	{
		this.mesh = mesh;
	}

	/**
	 * Sets the view.
	 *
	 * @param view the new view
	 */
	public void setView(Matrix4f view)
	{
		this.view = view;
	}

	/**
	 * Sets the projection.
	 *
	 * @param projection the new projection
	 */
	public void setProjection(Matrix4f projection)
	{
		this.projection = projection;
	}

	/**
	 * Sets the model.
	 *
	 * @param model the new model
	 */
	public void setModel(Transform model)
	{
		this.model = model;
	}

	@Override
	public void render()
	{
		shader.bindProgram();
		
		Matrix4f model = new Matrix4f();
		model.setIdentity();
		
		shader.setMatrix4f("u_model", model);
		
		shader.setMatrix4f("u_view", model);
		
		shader.setMatrix4f("u_projection", projection);

		mesh.render(GL_QUADS);
	}

}
