package engine.scene;

import engine.event.listener.RenderListener;
import engine.event.listener.UpdateListener;

/**
 * The Class GameComponent.
 */
public abstract class GameComponent implements UpdateListener, RenderListener
{
	
	/**
	 * Instantiates a new game component.
	 */
	public GameComponent()
	{
	}
	
	@Override
	public void update(float delta)
	{
		
	}
	
	@Override
	public void render()
	{
		
	}

}
