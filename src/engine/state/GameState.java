package engine.state;

import engine.core.ExitReason;
import engine.event.listener.RenderListener;
import engine.event.listener.ResizeListener;
import engine.event.listener.UpdateListener;

/**
 * The Class GameState.
 */
public abstract class GameState implements UpdateListener, ResizeListener, RenderListener
{

	/** The id. */
	private final String ID;
	
	/** The initialized. */
	private boolean initialized;

	/**
	 * Instantiates a new game state.
	 *
	 * @param id the id
	 */
	public GameState(String id)
	{
		ID = id;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId()
	{
		return ID;
	}

	/**
	 * Checks if is initialized.
	 *
	 * @return true, if is initialized
	 */
	public boolean isInitialized()
	{
		return initialized;
	}

	/**
	 * Sets the initialized.
	 *
	 * @param initialized the new initialized
	 */
	public void setInitialized(boolean initialized)
	{
		this.initialized = initialized;
	}

	/**
	 * Inits the.
	 */
	public abstract void init();

	/**
	 * Dispose.
	 *
	 * @param reason the reason
	 */
	public abstract void dispose(ExitReason reason);

}
