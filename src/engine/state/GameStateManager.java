package engine.state;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

import engine.core.Engine;
import engine.core.ExitReason;

/**
 * The Class GameStateManager.
 */
public class GameStateManager
{

	/** The states. */
	private Map<String, GameState> states;
	
	/** The state stack. */
	private Stack<GameState> stateStack;
	
	/** The default state. */
	private String defaultState;

	/**
	 * Instantiates a new game state manager.
	 *
	 * @param defState the def state
	 */
	public GameStateManager(String defState)
	{
		states = new HashMap<String, GameState>();
		stateStack = new Stack<GameState>();
		defaultState = defState;
	}

	/**
	 * Gets the current.
	 *
	 * @return the current
	 */
	public GameState getCurrent()
	{
		if (stateStack.isEmpty())
			return null;
		return stateStack.peek();
	}

	/**
	 * Gets the game state.
	 *
	 * @param id the id
	 * @return the game state
	 */
	public GameState getGameState(String id)
	{
		return states.get(id);
	}

	/**
	 * Insert game state.
	 *
	 * @param state the state
	 */
	public void insertGameState(GameState state)
	{
		states.put(state.getId(), state);
	}

	/**
	 * Removes the game state.
	 *
	 * @param stateId the state id
	 */
	public void removeGameState(String stateId)
	{
		states.remove(stateId);
	}

	/**
	 * Push game state.
	 *
	 * @param id the id
	 */
	public void pushGameState(String id)
	{
		stateStack.push(states.get(id));
		initCurrent();
	}

	/**
	 * Pop game state.
	 */
	public void popGameState()
	{
		if (getCurrent() != null)
			getCurrent().dispose(ExitReason.STATE_CLOSE);
		stateStack.pop();
		if (stateStack.isEmpty())
			stateStack.push(states.get(defaultState));
		else
		{
			getCurrent().init();
			resizeCurrent(Engine.getInstance().getWindow().getWidth(), Engine.getInstance().getWindow().getHeight());
		}
	}

	/**
	 * Stack size.
	 *
	 * @return the int
	 */
	public int stackSize()
	{
		return stateStack.size();
	}

	/**
	 * Inits the current.
	 */
	public void initCurrent()
	{
		if (getCurrent() != null)
		{
			getCurrent().init();
			getCurrent().setInitialized(true);
		}
	}

	/**
	 * Update current.
	 *
	 * @param delta the delta
	 */
	public void updateCurrent(float delta)
	{
		if (getCurrent() != null)
			getCurrent().update(delta);
	}

	/**
	 * Resize current.
	 *
	 * @param w the w
	 * @param h the h
	 */
	public void resizeCurrent(int w, int h)
	{
		if (getCurrent() != null)
			getCurrent().resize(w, h);
	}

	/**
	 * Render current.
	 */
	public void renderCurrent()
	{
		if (getCurrent() != null)
			getCurrent().render();
	}

	/**
	 * Dispose current.
	 *
	 * @param reason the reason
	 */
	public void disposeCurrent(ExitReason reason)
	{
		if (getCurrent() != null)
			getCurrent().dispose(reason);
	}
}
