package engine.crash;

import java.awt.Desktop;
import java.awt.Desktop.Action;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;

//Created as a Demo for you to base your code on, or to modify. 
/**
 * The Class EmailGenerator.
 */
//Usage - Construct an Email Generator, pass in the message, and call the Open method.
public class EmailGenerator
{
	
	/** The email address. */
	// Constants//
	private final String EMAIL_ADDRESS = "toast_mafia@mail.com"; // email address
																// to send to.
	// End Constants//

	/** The email body. */
																// Variables//
	private String emailBody; // the body of the email.
	// End Variables//

	/**
	 * Instantiates a new email generator.
	 */
	public EmailGenerator()
	{
		this("");
	}

	/**
	 * Instantiates a new email generator.
	 *
	 * @param msg the msg
	 */
	public EmailGenerator(String msg)
	{
		this.emailBody = msg;
	}

	/**
	 * Gets the body.
	 *
	 * @return the string
	 */
	// gets the body of the email
	public String GetBody()
	{
		return this.emailBody;
	}

	/**
	 * Sets the body.
	 *
	 * @param msg the msg
	 */
	// sets the body of the email
	public void SetBody(String msg)
	{
		this.emailBody = msg;
	}

	// this is added in case you want some kind of information at the top of the
	// email.
	/**
	 * Gets the header.
	 *
	 * @return the string
	 */
	// like adding a date or something, so you can pretend you're filtering.
	private String GetHeader()
	{
		String header = "[Support Email]\n[This email is automatically generated - do not modify.]";
		return header;
	}

	// this is added in case you want to dynamically change the subject for each
	// email.
	/**
	 * Gets the subject.
	 *
	 * @return the string
	 */
	// like add date information or something.
	private String GetSubject()
	{
		return "Crash Report";
	}

	/**
	 * Encode uri component.
	 *
	 * @param s the s
	 * @return the string
	 */
	// used to escape the email.
	public static String encodeURIComponent(String s)
	{
		String result;

		try
		{
			// encodes the data to be URI-safe.
			result = URLEncoder.encode(s, "UTF-8").replaceAll("\\+", "%20").replaceAll("\\%21", "!").replaceAll("\\%27", "'").replaceAll("\\%28", "(").replaceAll("\\%29", ")").replaceAll("\\%7E", "~");
		} catch (UnsupportedEncodingException e)
		{
			result = s;
		}

		return result;
	}

	// Tries to create the email.
	/**
	 * Open.
	 *
	 * @return true, if successful
	 */
	// Tests if it can, and if it can't, returns false.
	public boolean Open()
	{
		Desktop desktop = null;
		// checks if the desktop is even supported, which it should be. Desktops
		// are required for games.

		if (Desktop.isDesktopSupported())
		{
			desktop = Desktop.getDesktop();
			// checks if mail is supported
			if (desktop.isSupported(Action.MAIL))
			{

				try
				{
					// mails the email to the address.
					String emailQuery = "mailto:" + EMAIL_ADDRESS + "?subject=" + encodeURIComponent(GetSubject()) + "&body=" + encodeURIComponent(GetHeader() + "\n\n" + GetBody() + "\n\n");
					URI mailto = new URI(emailQuery);
					desktop.mail(mailto);
					return true;
				} catch (URISyntaxException e)
				{
					// this shouldn't ever happen.
				} catch (IOException e)
				{
					// this shouldn't ever happen either, but happens if the
					// string is somehow messed up.
				}

			}
		}

		return false;
	}

}
