package engine.crash;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;

/**
 * The Class CrashWindow.
 */
public class CrashWindow
{

	/** The content pane. */
	private JPanel contentPane;
	
	/** The frame. */
	private JFrame frame;
	
	/** The error area. */
	private JTextArea errorArea;
	
	/** The email button. */
	private JButton emailButton;

	/**
	 * Instantiates a new crash window.
	 *
	 * @param title the title
	 * @param content the content
	 * @param width the width
	 * @param height the height
	 */
	public CrashWindow(String title, final String content, int width, int height)
	{
		frame = new JFrame(title);
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		contentPane = new JPanel();
		contentPane.setPreferredSize(new Dimension(width, height));
		contentPane.setLayout(new BorderLayout());

		emailButton = new JButton("Send Crash Report");
		contentPane.add(emailButton, BorderLayout.SOUTH);

		emailButton.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				new EmailGenerator(content).Open();
			}
		});

		frame.add(contentPane);
		frame.pack();

		errorArea = new JTextArea();
		errorArea.setText(content);
		errorArea.setEditable(false);
		contentPane.add(errorArea, BorderLayout.CENTER);

		frame.setLocationRelativeTo(null);
		frame.setVisible(true);

	}

	/**
	 * Gets the frame.
	 *
	 * @return the frame
	 */
	public JFrame getFrame()
	{
		return frame;
	}

	/**
	 * Gets the content pane.
	 *
	 * @return the content pane
	 */
	public JPanel getContentPane()
	{
		return contentPane;
	}

}