package engine.resource;

import java.util.TreeMap;

/**
 * The Class CompositeResource.
 */
public class CompositeResource extends Resource
{

	/** The components. */
	private TreeMap<String, Resource> components;

	/**
	 * Instantiates a new composite resource.
	 */
	public CompositeResource()
	{
		components = new TreeMap<String, Resource>();
	}

	/**
	 * Gets the resource.
	 *
	 * @param name the name
	 * @return the resource
	 */
	public Resource getResource(String name)
	{
		return components.get(name);
	}

	/**
	 * Insert component.
	 *
	 * @param name the name
	 * @param component the component
	 */
	public void insertComponent(String name, Resource component)
	{
		components.put(name, component);
	}

	/**
	 * Removes the component.
	 *
	 * @param name the name
	 */
	public void removeComponent(String name)
	{
		components.remove(name);
	}

	@Override
	public void load()
	{
		for (String key : components.keySet())
			components.get(key).load();
	}

	@Override
	public void unload()
	{
		for (String key : components.keySet())
			components.get(key).unload();
	}

}
