package engine.resource;

/**
 * The Class Resource.
 */
public abstract class Resource
{
	
	/**
	 * Load.
	 */
	public abstract void load();

	/**
	 * Unload.
	 */
	public abstract void unload();

}
