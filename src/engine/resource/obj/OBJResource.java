package engine.resource.obj;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import engine.file.FileHandle;
import engine.resource.TextFileResource;
import engine.util.math.Vector3f;

/**
 * The Class OBJResource.
 */
public class OBJResource extends TextFileResource
{

	/** The vertices. */
	private List<Vector3f> vertices;
	
	/** The normals. */
	private List<Vector3f> normals;
	
	/** The indices. */
	private List<OBJIndex> indices;

	/**
	 * Instantiates a new OBJ resource.
	 *
	 * @param handle the handle
	 */
	public OBJResource(FileHandle handle)
	{
		super(handle);
		vertices = new ArrayList<Vector3f>();
		indices = new ArrayList<OBJIndex>();
		normals = new ArrayList<Vector3f>();
	}

	/**
	 * Gets the vertices.
	 *
	 * @return the vertices
	 */
	public List<Vector3f> getVertices()
	{
		return vertices;
	}

	/**
	 * Gets the normals.
	 *
	 * @return the normals
	 */
	public List<Vector3f> getNormals()
	{
		return normals;
	}

	/**
	 * Gets the indices.
	 *
	 * @return the indices
	 */
	public List<OBJIndex> getIndices()
	{
		return indices;
	}

	@Override
	public void load()
	{
		try
		{
			BufferedReader reader = new BufferedReader(new InputStreamReader(getHandle().openInputStream()));
			String last = reader.readLine();
			while (last != null)
			{
				String[] tokens = last.split(" ");
				if (tokens[0].equals("v"))
				{
					float x = Float.parseFloat(tokens[1]), y = Float.parseFloat(tokens[2]), z = Float.parseFloat(tokens[3]);
					vertices.add(new Vector3f(x, y, z));
				} else if (tokens[0].equals("vn"))
				{
					float x = Float.parseFloat(tokens[1]), y = Float.parseFloat(tokens[2]), z = Float.parseFloat(tokens[3]);
					normals.add(new Vector3f(x, y, z));
				} else if (last.startsWith("f"))
				{
					for (int i = 1; i <= 3; i++)
					{
						String[] indexTokens = tokens[i].split("/");
						indices.add(new OBJIndex(Integer.parseInt(indexTokens[0]) - 1, Integer.parseInt(indexTokens[2]) - 1));
					}
				}
				last = reader.readLine();
			}
		} catch (IOException e)
		{
			e.printStackTrace();
		}

	}

	@Override
	public void unload()
	{
		super.unload();
	}

}