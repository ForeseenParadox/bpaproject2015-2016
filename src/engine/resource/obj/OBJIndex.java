package engine.resource.obj;

/**
 * The Class OBJIndex.
 */
public class OBJIndex
{

	/** The position index. */
	private int positionIndex;
	
	/** The normal index. */
	private int normalIndex;
	
	/**
	 * Instantiates a new OBJ index.
	 *
	 * @param positionIndex the position index
	 */
	public OBJIndex(int positionIndex)
	{
	}

	/**
	 * Instantiates a new OBJ index.
	 *
	 * @param positionIndex the position index
	 * @param normalIndex the normal index
	 */
	public OBJIndex(int positionIndex, int normalIndex)
	{
		this.positionIndex = positionIndex;
		this.normalIndex = normalIndex;
	}

	/**
	 * Gets the position index.
	 *
	 * @return the position index
	 */
	public int getPositionIndex()
	{
		return positionIndex;
	}

	/**
	 * Gets the normal index.
	 *
	 * @return the normal index
	 */
	public int getNormalIndex()
	{
		return normalIndex;
	}

	/**
	 * Sets the position index.
	 *
	 * @param positionIndex the new position index
	 */
	public void setPositionIndex(int positionIndex)
	{
		this.positionIndex = positionIndex;
	}

	/**
	 * Sets the normal index.
	 *
	 * @param normalIndex the new normal index
	 */
	public void setNormalIndex(int normalIndex)
	{
		this.normalIndex = normalIndex;
	}

}