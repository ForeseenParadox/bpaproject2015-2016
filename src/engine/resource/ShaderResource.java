package engine.resource;

import engine.file.FileHandle;

/**
 * The Class ShaderResource.
 */
public class ShaderResource extends CompositeResource
{

	/** The Constant VERTEX_COMPONENT. */
	public static final String VERTEX_COMPONENT = "VERT_COMP";
	
	/** The Constant FRAGMENT_COMPONENT. */
	public static final String FRAGMENT_COMPONENT = "FRAG_COMP";

	/**
	 * Instantiates a new shader resource.
	 *
	 * @param vertexShaderHandle the vertex shader handle
	 * @param fragmentShaderHandle the fragment shader handle
	 */
	public ShaderResource(FileHandle vertexShaderHandle, FileHandle fragmentShaderHandle)
	{
		insertComponent(VERTEX_COMPONENT, new TextFileResource(vertexShaderHandle));
		insertComponent(FRAGMENT_COMPONENT, new TextFileResource(fragmentShaderHandle));
	}

	/**
	 * Gets the vertex component.
	 *
	 * @return the vertex component
	 */
	public TextFileResource getVertexComponent()
	{
		return (TextFileResource) getResource(ShaderResource.VERTEX_COMPONENT);
	}

	/**
	 * Gets the fragment component.
	 *
	 * @return the fragment component
	 */
	public TextFileResource getFragmentComponent()
	{
		return (TextFileResource) getResource(ShaderResource.FRAGMENT_COMPONENT);
	}
}
