package engine.resource;

import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

import engine.file.FileHandle;

/**
 * The Class TextureResource.
 */
public class TextureResource extends Resource
{

	/** The handle. */
	private FileHandle handle;
	
	/** The data. */
	private int[] data;
	
	/** The width. */
	private int width;
	
	/** The height. */
	private int height;

	/**
	 * Instantiates a new texture resource.
	 *
	 * @param handle the handle
	 */
	public TextureResource(FileHandle handle)
	{
		this.handle = handle;
	}

	/**
	 * Instantiates a new texture resource.
	 *
	 * @param data the data
	 * @param width the width
	 * @param height the height
	 */
	public TextureResource(int[] data, int width, int height)
	{
		this.data = data;
		this.width = width;
		this.height = height;
	}

	/**
	 * Gets the handle.
	 *
	 * @return the handle
	 */
	public FileHandle getHandle()
	{
		return handle;
	}

	/**
	 * Gets the data.
	 *
	 * @return the data
	 */
	public int[] getData()
	{
		return data;
	}

	/**
	 * Gets the width.
	 *
	 * @return the width
	 */
	public int getWidth()
	{
		return width;
	}

	/**
	 * Gets the height.
	 *
	 * @return the height
	 */
	public int getHeight()
	{
		return height;
	}

	/**
	 * Sets the handle.
	 *
	 * @param handle the new handle
	 */
	public void setHandle(FileHandle handle)
	{
		this.handle = handle;
	}

	@Override
	public void load()
	{
		if (handle != null)
		{
			try
			{
				// read texture data
				BufferedImage loadedImage = ImageIO.read(handle.openInputStream());

				width = loadedImage.getWidth();
				height = loadedImage.getHeight();

				data = new int[width * height];
				loadedImage.getRGB(0, 0, width, height, data, 0, width);
			} catch (IOException e)
			{
				System.err.println("Failed to load texture resource at: " + handle.getPath());
				e.printStackTrace();
			}

		}
	}

	@Override
	public void unload()
	{

	}

}
