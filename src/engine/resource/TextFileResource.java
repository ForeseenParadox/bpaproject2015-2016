package engine.resource;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import engine.file.FileHandle;

/**
 * The Class TextFileResource.
 */
public class TextFileResource extends Resource
{

	/** The handle. */
	private FileHandle handle;
	
	/** The loaded text. */
	private String loadedText;

	/**
	 * Instantiates a new text file resource.
	 *
	 * @param handle the handle
	 */
	public TextFileResource(FileHandle handle)
	{
		this.handle = handle;
	}

	/**
	 * Gets the handle.
	 *
	 * @return the handle
	 */
	public FileHandle getHandle()
	{
		return handle;
	}

	/**
	 * Gets the loaded text.
	 *
	 * @return the loaded text
	 */
	public String getLoadedText()
	{
		return loadedText;
	}

	/**
	 * Sets the handle.
	 *
	 * @param handle the new handle
	 */
	public void setHandle(FileHandle handle)
	{
		this.handle = handle;
	}

	@Override
	public void load()
	{
		try
		{
			BufferedReader reader = new BufferedReader(new InputStreamReader(handle.openInputStream()));
			StringBuilder result = new StringBuilder();
			String last = "";
			last = reader.readLine();
			while (last != null)
			{
				result.append(last + "\n");
				last = reader.readLine();
			}

			reader.close();

			loadedText = result.toString();
		} catch (IOException e)
		{
			System.err.println("Failed to load text resource at: " + handle.getPath());
			e.printStackTrace();
		}
	}

	@Override
	public void unload()
	{
		handle = null;
		loadedText = null;
	}

}
