package engine.resource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import engine.animation.TextureAnimation;
import engine.audio.Audio;
import engine.core.Engine;
import engine.core.Subsystem;
import engine.file.ExternalFileHandle;
import engine.file.FileHandle;
import engine.file.InternalFileHandle;
import engine.graphics.texture.Texture;
import engine.graphics.texture.TextureData;
import engine.graphics.texture.TextureRegion;
import engine.graphics.texture.TextureRegionArray;
import engine.util.xml.XMLDocument;
import engine.util.xml.XMLElement;

/**
 * The Class ResourceSystem.
 */
public class ResourceSystem implements Subsystem
{

	/** The resources. */
	// resource handlers
	private Map<String, Resource> resources;

	/**
	 * Instantiates a new resource system.
	 */
	public ResourceSystem()
	{
	}

	/**
	 * Gets the resource.
	 *
	 * @param name the name
	 * @return the resource
	 */
	public Resource getResource(String name)
	{
		return resources.get(name);
	}

	/**
	 * Insert resource.
	 *
	 * @param name the name
	 * @param resource the resource
	 */
	public void insertResource(String name, Resource resource)
	{
		resources.put(name, resource);
	}

	/**
	 * Removes the resource.
	 *
	 * @param name the name
	 * @return the resource
	 */
	public Resource removeResource(String name)
	{
		return resources.remove(name);
	}

	/**
	 * Load resource file.
	 *
	 * @param handle the handle
	 */
	public void loadResourceFile(FileHandle handle)
	{
		// load the text resource file
		TextFileResource resourceText = new TextFileResource(handle);
		resourceText.load();

		Engine.getInstance().getLog().normal("Loading resource file at " + resourceText.getHandle().getPath());

		// load the xml
		XMLDocument resourceXml = new XMLDocument(resourceText);

		for (XMLElement element : resourceXml.getRootElement().getChildren())
		{
			if (element.getName().equals("SpriteSheet"))
				loadSpriteSheetResource(element);
			else if (element.getName().equals("Animation"))
				loadAnimationResource(element);
			else if (element.getName().equals("Audio"))
				loadAudio(element);
			else if (element.getName().equals("TextFile"))
				loadTextFileResource(element);
		}
	}

	/**
	 * Load sprite sheet resource.
	 *
	 * @param root the root
	 */
	private void loadSpriteSheetResource(XMLElement root)
	{
		String id = root.getAttribValue("id");
		String src = root.getAttribValue("src");

		Engine.getInstance().getLog().normal("Loading sprite sheet resource " + id);

		// load texture resource
		TextureResource res = new TextureResource(new InternalFileHandle(src));
		res.load();
		TextureData dat = new TextureData(res);
		Texture result = new Texture(id);

		int spriteWidth = Integer.parseInt(root.getAttribValue("spriteWidth")), spriteHeight = Integer.parseInt(root.getAttribValue("spriteHeight"));

		String[] transparencyTokens = root.getAttribValue("transparency").split(",");

		for (String x : transparencyTokens)
			dat.replaceColorWithTransparency(Integer.parseInt(x, 16));

		result.load(dat);

		loadTextureRegionElements(root, result, spriteWidth, spriteHeight);
		loadTextureRegionArrayElements(root, result, spriteWidth, spriteHeight);

	}

	/**
	 * Load texture region elements.
	 *
	 * @param root the root
	 * @param parent the parent
	 * @param spriteWidth the sprite width
	 * @param spriteHeight the sprite height
	 */
	private void loadTextureRegionElements(XMLElement root, Texture parent, int spriteWidth, int spriteHeight)
	{
		// load texture elements
		List<XMLElement> textureElements = root.getChildrenByName("Texture");
		for (XMLElement textureElement : textureElements)
		{
			String name = textureElement.getAttribValue("id");
			TextureRegion regionResult = new TextureRegion(name, parent);

			Engine.getInstance().getLog().normal("Loading " + name + " texture region");

			String subX = textureElement.getAttribValue("subX"), subY = textureElement.getAttribValue("subY");
			String subWidth = textureElement.getAttribValue("subWidth"), subHeight = textureElement.getAttribValue("subHeight");

			regionResult.setX(parseCoordinate(textureElement.getAttribValue("x"), spriteWidth) + (subX == null ? 0 : Integer.parseInt(subX)));
			regionResult.setY(parseCoordinate(textureElement.getAttribValue("y"), spriteHeight) + (subY == null ? 0 : Integer.parseInt(subY)));

			String width = textureElement.getAttribValue("width"), height = textureElement.getAttribValue("height");

			if (width == null)
				regionResult.setWidth((subWidth == null ? spriteWidth : Integer.parseInt(subWidth)));
			else
				regionResult.setWidth((subWidth == null ? parseCoordinate(width, spriteWidth) : Integer.parseInt(subWidth)));

			if (height == null)
				regionResult.setHeight((subHeight == null ? spriteHeight : Integer.parseInt(subHeight)));
			else
				regionResult.setHeight((subHeight == null ? parseCoordinate(height, spriteHeight) : Integer.parseInt(subHeight)));
		}
	}

	/**
	 * Load texture region array elements.
	 *
	 * @param root the root
	 * @param parent the parent
	 * @param spriteWidth the sprite width
	 * @param spriteHeight the sprite height
	 */
	private void loadTextureRegionArrayElements(XMLElement root, Texture parent, int spriteWidth, int spriteHeight)
	{
		// load texture array elements
		List<XMLElement> regionArrayElements = root.getChildrenByName("TextureArray");
		for (XMLElement regionArrayElement : regionArrayElements)
		{
			String id = regionArrayElement.getAttribValue("id");

			Engine.getInstance().getLog().normal("Loading " + id + " texture region array");
			int startX = 0, startY = 0, endX = 0, endY = 0, width = 0, height = 0;
			startX = Integer.parseInt(regionArrayElement.getAttribValue("startX"));
			startY = Integer.parseInt(regionArrayElement.getAttribValue("startY"));
			endX = Integer.parseInt(regionArrayElement.getAttribValue("endX"));
			endY = Integer.parseInt(regionArrayElement.getAttribValue("endY"));
			width = Integer.parseInt(regionArrayElement.getAttribValue("width"));
			height = Integer.parseInt(regionArrayElement.getAttribValue("height"));

			int spritesWide = ((endX - startX) + 1) * width, spritesTall = ((endY - startY) + 1) * height;
			TextureRegionArray arrayResult = new TextureRegionArray(id, spritesWide * spritesTall);

			String subX = regionArrayElement.getAttribValue("subX"), subY = regionArrayElement.getAttribValue("subY");
			String subWidth = regionArrayElement.getAttribValue("subWidth"), subHeight = regionArrayElement.getAttribValue("subHeight");

			for (int x = startX; x < startX + spritesWide; x++)
				for (int y = startY; y < startY + spritesTall; y++)
				{

					TextureRegion result = new TextureRegion(parent);
					result.setX(x * spriteWidth + (subX == null ? 0 : Integer.parseInt(subX)));
					result.setY(y * spriteHeight + (subY == null ? 0 : Integer.parseInt(subY)));
					result.setWidth((subWidth == null ? width * spriteWidth : Integer.parseInt(subWidth)));
					result.setHeight((subHeight == null ? height * spriteHeight : Integer.parseInt(subHeight)));

					arrayResult.setRegion((y - startY) + (x - startX) * spritesTall, result);
				}
		}
	}

	/**
	 * Load audio.
	 *
	 * @param root the root
	 */
	public void loadAudio(XMLElement root)
	{
		String id = root.getAttribValue("id"), src = root.getAttribValue("src"), type = root.getAttribValue("type");
		Engine.getInstance().getLog().normal("Loading " + id + " audio");
		new Audio(new InternalFileHandle(src).openInputStream(), id, type);
	}

	/**
	 * Load text file resource.
	 *
	 * @param root the root
	 */
	public void loadTextFileResource(XMLElement root)
	{
		String id = root.getAttribValue("id"), src = root.getAttribValue("src");
		Engine.getInstance().getLog().normal("Loading " + id + " text file");
		TextFileResource res = new TextFileResource(new ExternalFileHandle(src));
		res.load();
		resources.put(id, res);
	}

	/**
	 * Parses the coordinate.
	 *
	 * @param coord the coord
	 * @param noUnitMultiplier the no unit multiplier
	 * @return the int
	 */
	private int parseCoordinate(String coord, int noUnitMultiplier)
	{
		if (coord.endsWith("px"))
			return Integer.parseInt(coord.substring(0, coord.length() - 2));
		else
			return Integer.parseInt(coord) * noUnitMultiplier;
	}

	/**
	 * Load animation resource.
	 *
	 * @param root the root
	 */
	private void loadAnimationResource(XMLElement root)
	{
		String id = root.getAttribValue("id");

		Engine.getInstance().getLog().normal("Loading " + id + " animation");
		long delay = Long.parseLong(root.getAttribValue("delay"));
		TextureAnimation result = new TextureAnimation(id, delay);
		List<TextureRegion> frames = new ArrayList<TextureRegion>();
		for (XMLElement frameElement : root.getChildrenByName("FrameSource"))
		{
			String name = frameElement.getAttribValue("src");

			if (TextureRegion.isTextureRegionLoaded(name))
			{
				frames.add(TextureRegion.getTextureRegion(name));
			} else if (TextureRegionArray.isTextureRegionArrayLoaded(name))
			{
				TextureRegionArray array = TextureRegionArray.getTextureRegionArray(name);
				for (TextureRegion region : array.getRegionArray())
					frames.add(region);
			}
		}
		XMLElement framesElement = root.getChildByName("Frames");
		if (framesElement == null)
		{
			for (TextureRegion frame : frames)
				result.addFrame(frame);
		} else
		{
			String[] stringIndices = framesElement.getValue().split(",");
			for (String x : stringIndices)
				result.addFrame(frames.get(Integer.parseInt(x.trim())));
		}
	}

	@Override
	public void init()
	{
		resources = new HashMap<String, Resource>();
	}

	@Override
	public void dispose()
	{
		unloadAllResources();
	}

	/**
	 * Load all resources.
	 */
	public void loadAllResources()
	{
		Engine.getInstance().getLog().normal("Loading all resources...");
		for (String resourceName : resources.keySet())
		{
			Resource resource = resources.get(resourceName);
			if (resource instanceof Resource)
				resource.load();
		}
		Engine.getInstance().getLog().normal("All resources loaded");
	}

	/**
	 * Unload all resources.
	 */
	public void unloadAllResources()
	{
		Engine.getInstance().getLog().normal("Unloading all resources...");
		for (String resourceName : resources.keySet())
		{
			Resource resource = resources.get(resourceName);
			if (resource instanceof Resource)
				resource.unload();
		}
		Engine.getInstance().getLog().normal("All resources unloaded");
	}

}
