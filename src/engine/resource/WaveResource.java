package engine.resource;

import static org.lwjgl.openal.AL10.AL_FORMAT_MONO16;
import static org.lwjgl.openal.AL10.AL_FORMAT_MONO8;
import static org.lwjgl.openal.AL10.AL_FORMAT_STEREO16;
import static org.lwjgl.openal.AL10.AL_FORMAT_STEREO8;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;

import engine.file.FileHandle;
import engine.util.BufferUtil;

/**
 * The Class WaveResource.
 */
public class WaveResource extends Resource
{

	/** The handle. */
	private FileHandle handle;
	
	/** The data. */
	private ByteBuffer data;
	
	/** The format. */
	private int format;
	
	/** The sample rate. */
	private int sampleRate;

	/**
	 * Instantiates a new wave resource.
	 *
	 * @param handle the handle
	 */
	public WaveResource(FileHandle handle)
	{
		this.handle = handle;
	}

	/**
	 * Gets the data.
	 *
	 * @return the data
	 */
	public ByteBuffer getData()
	{
		return data;
	}

	/**
	 * Gets the format.
	 *
	 * @return the format
	 */
	public int getFormat()
	{
		return format;
	}

	/**
	 * Gets the sample rate.
	 *
	 * @return the sample rate
	 */
	public int getSampleRate()
	{
		return sampleRate;
	}

	@Override
	public void load()
	{
		try
		{
			AudioInputStream stream = AudioSystem.getAudioInputStream(new BufferedInputStream(handle.openInputStream()));

			AudioFormat audioFormat = stream.getFormat();

			// load the openal format
			if (audioFormat.getChannels() == 1)
			{
				if (audioFormat.getSampleSizeInBits() == 8)
				{
					format = AL_FORMAT_MONO8;
				} else if (audioFormat.getSampleSizeInBits() == 16)
				{
					format = AL_FORMAT_MONO16;
				}
			} else if (audioFormat.getChannels() == 2)
			{
				if (audioFormat.getSampleSizeInBits() == 8)
				{
					format = AL_FORMAT_STEREO8;
				} else if (audioFormat.getSampleSizeInBits() == 16)
				{
					format = AL_FORMAT_STEREO16;
				}
			}

			sampleRate = (int) audioFormat.getSampleRate();

			// load wave data
			try
			{
				int size = stream.available();
				byte[] rawData = new byte[size];

				stream.read(rawData);

				data = BufferUtil.byteArrayToBuffer(rawData);
			} catch (IOException e)
			{
				e.printStackTrace();
			}
		} catch (UnsupportedAudioFileException e1)
		{
			e1.printStackTrace();
		} catch (IOException e1)
		{
			e1.printStackTrace();
		}

	}

	@Override
	public void unload()
	{
		data.clear();
	}

}
