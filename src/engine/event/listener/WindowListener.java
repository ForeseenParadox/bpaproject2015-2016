package engine.event.listener;

/**
 * The listener interface for receiving window events.
 * The class that is interested in processing a window
 * event implements this interface, and the object created
 * with that class is registered with a component using the
 * component's <code>addWindowListener<code> method. When
 * the window event occurs, that object's appropriate
 * method is invoked.
 *
 * @see WindowEvent
 */
public interface WindowListener
{

	/**
	 * Window resized.
	 *
	 * @param window the window
	 * @param width the width
	 * @param height the height
	 */
	public void windowResized(long window, int width, int height);

}