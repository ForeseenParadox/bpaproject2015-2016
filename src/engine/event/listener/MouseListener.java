package engine.event.listener;

/**
 * The listener interface for receiving mouse events.
 * The class that is interested in processing a mouse
 * event implements this interface, and the object created
 * with that class is registered with a component using the
 * component's <code>addMouseListener<code> method. When
 * the mouse event occurs, that object's appropriate
 * method is invoked.
 *
 * @see MouseEvent
 */
public interface MouseListener
{
	
	/**
	 * Button pressed.
	 *
	 * @param button the button
	 */
	public void buttonPressed(int button);
	
	/**
	 * Button released.
	 *
	 * @param button the button
	 */
	public void buttonReleased(int button);

}
