package engine.event.listener;

/**
 * The listener interface for receiving update events.
 * The class that is interested in processing a update
 * event implements this interface, and the object created
 * with that class is registered with a component using the
 * component's <code>addUpdateListener<code> method. When
 * the update event occurs, that object's appropriate
 * method is invoked.
 *
 * @see UpdateEvent
 */
public interface UpdateListener
{

	/**
	 * Update.
	 *
	 * @param delta the delta
	 */
	public void update(float delta);

}
