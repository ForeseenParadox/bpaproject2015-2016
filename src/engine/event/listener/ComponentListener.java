package engine.event.listener;

import engine.graphics.gui.Component;

/**
 * The listener interface for receiving component events.
 * The class that is interested in processing a component
 * event implements this interface, and the object created
 * with that class is registered with a component using the
 * component's <code>addComponentListener<code> method. When
 * the component event occurs, that object's appropriate
 * method is invoked.
 *
 * @see ComponentEvent
 */
public interface ComponentListener
{

	/**
	 * Component hovered.
	 *
	 * @param source the source
	 */
	public void componentHovered(Component source);

	/**
	 * Component exited.
	 *
	 * @param source the source
	 */
	public void componentExited(Component source);

	/**
	 * Component pressed.
	 *
	 * @param source the source
	 * @param button the button
	 */
	public void componentPressed(Component source, int button);
	
	/**
	 * Component clicked.
	 *
	 * @param source the source
	 * @param button the button
	 */
	public void componentClicked(Component source, int button);
	
	/**
	 * Component released.
	 *
	 * @param source the source
	 * @param button the button
	 */
	public void componentReleased(Component source, int button);

}