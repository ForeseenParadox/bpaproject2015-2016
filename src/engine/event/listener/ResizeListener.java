package engine.event.listener;

/**
 * The listener interface for receiving resize events.
 * The class that is interested in processing a resize
 * event implements this interface, and the object created
 * with that class is registered with a component using the
 * component's <code>addResizeListener<code> method. When
 * the resize event occurs, that object's appropriate
 * method is invoked.
 *
 * @see ResizeEvent
 */
public interface ResizeListener
{
	
	/**
	 * Resize.
	 *
	 * @param w the w
	 * @param h the h
	 */
	public void resize(int w, int h);

}
