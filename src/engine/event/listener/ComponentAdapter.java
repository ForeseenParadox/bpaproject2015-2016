package engine.event.listener;

import engine.graphics.gui.Component;

/**
 * The Class ComponentAdapter.
 */
public class ComponentAdapter implements ComponentListener
{

	@Override
	public void componentHovered(Component source)
	{
		
	}

	@Override
	public void componentExited(Component source)
	{
		
	}

	@Override
	public void componentPressed(Component source, int button)
	{
		
	}

	@Override
	public void componentClicked(Component source, int button)
	{
		
	}

	@Override
	public void componentReleased(Component source, int button)
	{
		
	}

}
