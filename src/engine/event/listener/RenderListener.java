package engine.event.listener;

/**
 * The listener interface for receiving render events.
 * The class that is interested in processing a render
 * event implements this interface, and the object created
 * with that class is registered with a component using the
 * component's <code>addRenderListener<code> method. When
 * the render event occurs, that object's appropriate
 * method is invoked.
 *
 * @see RenderEvent
 */
public interface RenderListener
{

	/**
	 * Render.
	 */
	public void render();

}
