package engine.audio;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.FloatControl;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

/**
 * The Class Audio.
 */
public class Audio
{

	/** The audio tracks. */
	private static Map<String, Audio> audioTracks;
	
	/** The audio track types. */
	private static Map<String, List<String>> audioTrackTypes;

	/** The clip. */
	private Clip clip;
	
	/** The master control. */
	private FloatControl masterControl;

	static
	{
		audioTracks = new HashMap<String, Audio>();
		audioTrackTypes = new HashMap<String, List<String>>();
	}

	/**
	 * Instantiates a new audio.
	 *
	 * @param stream the stream
	 * @param name the name
	 * @param type the type
	 */
	public Audio(InputStream stream, String name, String type)
	{
		try
		{
			clip = AudioSystem.getClip();
			clip.open(AudioSystem.getAudioInputStream(new BufferedInputStream(stream)));

			masterControl = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);

			audioTracks.put(name, this);

			List<String> audioList = audioTrackTypes.get(type.toLowerCase());
			if (audioList == null)
				audioList = new ArrayList<String>();
			audioList.add(name);
			audioTrackTypes.put(type.toLowerCase(), audioList);

		} catch (LineUnavailableException e)
		{
			e.printStackTrace();
		} catch (IOException e)
		{
			e.printStackTrace();
		} catch (UnsupportedAudioFileException e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * Gets the audio by name.
	 *
	 * @param name the name
	 * @return the audio by name
	 */
	public static Audio getAudioByName(String name)
	{
		return audioTracks.get(name);
	}

	/**
	 * Gets the audio tracks by type.
	 *
	 * @param type the type
	 * @return the audio tracks by type
	 */
	public static List<String> getAudioTracksByType(String type)
	{
		return audioTrackTypes.get(type.toLowerCase());
	}

	/**
	 * Sets the amplitude by name.
	 *
	 * @param audio the audio
	 * @param value the value
	 */
	public static void setAmplitudeByName(String audio, float value)
	{
		Audio aud = audioTracks.get(audio);
		if (aud != null)
			aud.setAmplitude(value);
	}

	/**
	 * Sets the amplitude by type.
	 *
	 * @param type the type
	 * @param value the value
	 */
	public static void setAmplitudeByType(String type, float value)
	{
		List<String> audioList = audioTrackTypes.get(type.toLowerCase());

		for (String x : audioList)
			setAmplitudeByName(x, value);
	}

	/**
	 * Start.
	 */
	public void start()
	{
		clip.start();
	}

	/**
	 * Stop.
	 */
	public void stop()
	{
		clip.stop();
	}

	/**
	 * Sets the amplitude.
	 *
	 * @param amp the new amplitude
	 */
	public void setAmplitude(float amp)
	{
		masterControl.setValue((float) (20 * Math.log10(amp)));
	}

	/**
	 * Loop.
	 *
	 * @param count the count
	 */
	public void loop(int count)
	{
		if (count < 0)
			clip.loop(Clip.LOOP_CONTINUOUSLY);
		else
			clip.loop(count);
	}

	/**
	 * Reset.
	 */
	public void reset()
	{
		stop();
		clip.setFramePosition(0);
	}

	/**
	 * Stop all tracks by type.
	 *
	 * @param type the type
	 */
	public static void stopAllTracksByType(String type)
	{
		List<String> music = audioTrackTypes.get(type);

		if (music != null)
		{
			for (String x : music)
			{
				Audio aud = audioTracks.get(x);
				if (aud != null)
					aud.stop();
			}
		}
	}

	/**
	 * Stop all tracks.
	 */
	public static void stopAllTracks()
	{
		Set<String> keySet = audioTracks.keySet();
		Iterator<String> itr = keySet.iterator();

		while (itr.hasNext())
		{
			String n = itr.next();
			if (audioTracks.get(n) != null)
				audioTracks.get(n).stop();
		}
	}

	/**
	 * Stop all music.
	 */
	public static void stopAllMusic()
	{
		stopAllTracksByType("music");
	}

	/**
	 * Stop all sfx.
	 */
	public static void stopAllSFX()
	{
		stopAllTracksByType("sfx");
	}

}
