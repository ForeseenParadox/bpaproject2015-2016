package engine.file;

import java.io.InputStream;

/**
 * The Interface Readable.
 */
public interface Readable
{
	
	/**
	 * Open input stream.
	 *
	 * @return the input stream
	 */
	public InputStream openInputStream();

}
