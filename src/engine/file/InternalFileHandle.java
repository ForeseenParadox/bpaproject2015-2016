package engine.file;

import java.io.InputStream;

/**
 * The Class InternalFileHandle.
 */
public class InternalFileHandle extends FileHandle implements Readable
{

	/**
	 * Instantiates a new internal file handle.
	 *
	 * @param path the path
	 */
	public InternalFileHandle(String path)
	{
		super(path);
	}

	@Override
	public InputStream openInputStream()
	{
		return InternalFileHandle.class.getResourceAsStream(getPath());
	}

}
