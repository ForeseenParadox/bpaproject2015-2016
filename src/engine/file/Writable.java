package engine.file;

import java.io.OutputStream;

/**
 * The Interface Writable.
 */
public interface Writable
{
	
	/**
	 * Open output stream.
	 *
	 * @return the output stream
	 */
	public OutputStream openOutputStream();
}
