package engine.file;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * The Class ExternalFileHandle.
 */
public class ExternalFileHandle extends FileHandle implements Readable, Writable
{

	/**
	 * Instantiates a new external file handle.
	 *
	 * @param path the path
	 */
	public ExternalFileHandle(String path)
	{
		super(path);
	}

	@Override
	public InputStream openInputStream()
	{
		try
		{
			return new FileInputStream(getPath());
		} catch (FileNotFoundException e)
		{
			System.err.println("Unable to open input stream for external file handle at: " + getPath());
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public OutputStream openOutputStream()
	{
		try
		{
			return new FileOutputStream(getPath());
		} catch (FileNotFoundException e)
		{
			System.err.println("Unable to open output stream for external file handle at: " + getPath());
			e.printStackTrace();
			return null;
		}
	}

}