package engine.file;

/**
 * The Class FileHandle.
 */
public abstract class FileHandle implements Readable
{

	/** The path. */
	private String path;

	/**
	 * Instantiates a new file handle.
	 *
	 * @param path the path
	 */
	public FileHandle(String path)
	{
		this.path = path;
	}

	/**
	 * Gets the path.
	 *
	 * @return the path
	 */
	public String getPath()
	{
		return path;
	}

	/**
	 * Sets the path.
	 *
	 * @param path the new path
	 */
	public void setPath(String path)
	{
		this.path = path;
	}

}