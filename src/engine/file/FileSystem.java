package engine.file;

import engine.core.Subsystem;

/**
 * The Class FileSystem.
 */
public class FileSystem implements Subsystem
{
	
	/**
	 * Gets the internal file handle.
	 *
	 * @param path the path
	 * @return the internal file handle
	 */
	public FileHandle getInternalFileHandle(String path)
	{
		return new InternalFileHandle(path);
	}

	/**
	 * Gets the external file handle.
	 *
	 * @param path the path
	 * @return the external file handle
	 */
	public FileHandle getExternalFileHandle(String path)
	{
		return new ExternalFileHandle(path);
	}

	@Override
	public void init()
	{

	}

	@Override
	public void dispose()
	{

	}

}
