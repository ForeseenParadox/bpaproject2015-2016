package engine.core;

/**
 * The Enum ExitReason.
 */
public enum ExitReason
{

	/** The crash. */
	CRASH, /** The user exit. */
 USER_EXIT, /** The state close. */
 STATE_CLOSE;

}
