package engine.core;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import engine.audio.Audio;
import engine.core.task.TaskScheduler;
import engine.crash.CrashWindow;
import engine.event.listener.RenderListener;
import engine.event.listener.UpdateListener;
import engine.event.listener.WindowListener;
import engine.input.Input;
import engine.resource.ResourceSystem;
import engine.script.ScriptSystem;
import engine.util.log.Logger;

/**
 * The Class Engine.
 */
public class Engine implements UpdateListener, RenderListener
{

	/** The instance. */
	private static Engine instance;

	/** The simulation. */
	// core mechanic
	private Simulation simulation;
	
	/** The window. */
	private Window window;
	
	/** The application. */
	private Application application;

	/** The input. */
	// input engine
	private Input input;

	/** The task scheduler. */
	// task scheduling
	private TaskScheduler taskScheduler;

	/** The default window listener. */
	// default listeners
	private WindowListener defaultWindowListener;

	/** The resource system. */
	// resource subsystem
	private ResourceSystem resourceSystem;

	/** The script system. */
	// scripting subsystem
	private ScriptSystem scriptSystem;

	/** The log. */
	// system logger
	private Logger log;

	/**
	 * Instantiates a new engine.
	 */
	public Engine()
	{
		if (instance != null)
			throw new IllegalStateException("Can not create more than one instance of engine.");
		instance = this;
	}

	/**
	 * Instantiates a new engine.
	 *
	 * @param config the config
	 */
	public Engine(EngineConfig config)
	{
		if (instance != null)
			throw new IllegalStateException("Can not create more than one instance of engine.");
		instance = this;
		loadEngine(config);
	}

	/**
	 * Gets the single instance of Engine.
	 *
	 * @return single instance of Engine
	 */
	public static Engine getInstance()
	{
		return instance;
	}

	/**
	 * Gets the simulation.
	 *
	 * @return the simulation
	 */
	public Simulation getSimulation()
	{
		return simulation;
	}

	/**
	 * Gets the window.
	 *
	 * @return the window
	 */
	public Window getWindow()
	{
		return window;
	}

	/**
	 * Gets the input.
	 *
	 * @return the input
	 */
	public Input getInput()
	{
		return input;
	}

	/**
	 * Gets the task scheduler.
	 *
	 * @return the task scheduler
	 */
	public TaskScheduler getTaskScheduler()
	{
		return taskScheduler;
	}

	/**
	 * Gets the resource system.
	 *
	 * @return the resource system
	 */
	public ResourceSystem getResourceSystem()
	{
		return resourceSystem;
	}

	/**
	 * Gets the script system.
	 *
	 * @return the script system
	 */
	public ScriptSystem getScriptSystem()
	{
		return scriptSystem;
	}

	/**
	 * Gets the log.
	 *
	 * @return the log
	 */
	public Logger getLog()
	{
		return log;
	}

	/**
	 * Load engine.
	 *
	 * @param config the config
	 */
	public void loadEngine(EngineConfig config)
	{
		try
		{
			// init logger
			initLogger();

			log.normal("Starting engine");

			if (config == null)
				throw new NullPointerException("Engine configuration can not be null.");

			WindowConfig wConfig = config.getWindowConfig();
			SimulationConfig sConfig = config.getSimulationConfig();

			// instantiate subsystems
			this.application = config.getApplication();
			simulation = new Simulation(new SimulationConfig(sConfig.getMaxUps(), sConfig.getMaxFps()), this, this);
			window = new Window(new WindowConfig(wConfig.getTitle(), wConfig.getWidth(), wConfig.getHeight(), wConfig.isResizable(), wConfig.isVsyncEnabled()));
			input = new Input(window);
			taskScheduler = new TaskScheduler();
			scriptSystem = new ScriptSystem();

			// load window subsystem
			window.init();
			log.normal("Window initialized");

			defaultWindowListener = new WindowListener()
			{
				@Override
				public void windowResized(long window, int width, int height)
				{
					application.resized(width, height);
				}
			};
			window.setWindowListener(defaultWindowListener);

			log.normal("Initializing input...");
			// load input subsystem
			input.init();
			log.normal("Input initialized");

			log.normal("Initializing task scheduler...");
			// load task scheduler subsytem
			taskScheduler.init();

			log.normal("Task scheduler initialized");

			log.normal("Initializing resource system...");
			// initialize default resource handlers
			resourceSystem = new ResourceSystem();
			resourceSystem.init();

			log.normal("Resource system initialized");

			log.normal("Intiailizing script system...");
			// init script system
			scriptSystem.init();
			log.normal("Script system initialized");

			log.normal("Loading application...");
			application.load();
			log.normal("Application loaded");

			// async load
			// load resources
			resourceSystem.loadAllResources();

			log.normal("Initializing application...");
			// init game
			application.init();
			log.normal("Application initialized");

			// create initial window resize event
			window.invokeResizeEvent();
		} catch (Throwable t)
		{
			crash(t);
		}
	}

	/**
	 * Inits the logger.
	 */
	private void initLogger()
	{
		File logOutput = new File("log.txt");

		if (!logOutput.exists())
		{
			try
			{
				logOutput.createNewFile();
			} catch (IOException e)
			{
				crash(e);
			}
		}

		try
		{
			log = new Logger(new OutputStream[]
			{
					System.out,
					new FileOutputStream(logOutput) });
		} catch (FileNotFoundException e)
		{
			crash(e);
		}
	}

	/**
	 * Dispose engine.
	 *
	 * @param reason the reason
	 */
	public void disposeEngine(ExitReason reason)
	{
		Audio.stopAllTracks();

		application.dispose(reason);
		resourceSystem.dispose();
		taskScheduler.dispose();
		input.dispose();
		window.dispose();
	}

	/**
	 * Start engine.
	 */
	public void startEngine()
	{
		simulation.startSimulation();
	}

	/**
	 * Stop engine.
	 *
	 * @param reason the reason
	 */
	public void stopEngine(ExitReason reason)
	{
		if (reason == ExitReason.CRASH)
		{
			Audio.stopAllMusic();
			simulation.stopSimulation();
		} else
		{
			simulation.stopSimulation();
		}
	}

	/**
	 * Crash.
	 *
	 * @param t the t
	 */
	public void crash(Throwable t)
	{
		stopEngine(ExitReason.CRASH);
		String content = "Engine caught throwable:\n" + t.toString() + "\n";
		for (StackTraceElement el : t.getStackTrace())
			content += "\t" + el.toString() + "\n";
		log.severe(content);
		new CrashWindow("Crash", content, 800, 600);
	}

	@Override
	public void update(float delta)
	{
		input.update(delta);
		taskScheduler.update(delta);
		application.update(delta);
	}

	@Override
	public void render()
	{
		if (window.isCloseRequested())
			stopEngine(ExitReason.USER_EXIT);

		application.render();
		window.render();
	}

}
