package engine.core;

/**
 * The Interface Subsystem.
 */
public interface Subsystem
{

	/**
	 * Inits the.
	 */
	public void init();

	/**
	 * Dispose.
	 */
	public void dispose();

}
