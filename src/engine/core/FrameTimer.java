package engine.core;

/**
 * The Class FrameTimer.
 */
public class FrameTimer
{

	/** The fps. */
	private int fps;
	
	/** The ups. */
	private int ups;
	
	/** The delta. */
	private float delta;

	/**
	 * Instantiates a new frame timer.
	 */
	public FrameTimer()
	{
		this(0, 0, 0);
	}

	/**
	 * Instantiates a new frame timer.
	 *
	 * @param fps the fps
	 * @param ups the ups
	 * @param delta the delta
	 */
	public FrameTimer(int fps, int ups, float delta)
	{
		this.fps = fps;
		this.ups = ups;
		this.delta = delta;
	}

	/**
	 * Gets the fps.
	 *
	 * @return the fps
	 */
	public int getFps()
	{
		return fps;
	}

	/**
	 * Gets the ups.
	 *
	 * @return the ups
	 */
	public int getUps()
	{
		return ups;
	}

	/**
	 * Gets the delta.
	 *
	 * @return the delta
	 */
	public float getDelta()
	{
		return delta;
	}

	/**
	 * Sets the fps.
	 *
	 * @param fps the new fps
	 */
	public void setFps(int fps)
	{
		this.fps = fps;
	}

	/**
	 * Sets the ups.
	 *
	 * @param ups the new ups
	 */
	public void setUps(int ups)
	{
		this.ups = ups;
	}

	/**
	 * Sets the delta.
	 *
	 * @param delta the new delta
	 */
	public void setDelta(float delta)
	{
		this.delta = delta;
	}

}
