package engine.core;

import java.io.IOException;
import java.util.Properties;

import engine.file.ExternalFileHandle;

/**
 * The Class EngineConfig.
 */
public class EngineConfig
{

	/** The Constant WINDOW_TITLE. */
	public static final String WINDOW_TITLE = "window-title";
	
	/** The Constant WINDOW_WIDTH. */
	public static final String WINDOW_WIDTH = "window-width";
	
	/** The Constant WINDOW_HEIGHT. */
	public static final String WINDOW_HEIGHT = "window-height";
	
	/** The Constant WINDOW_RESIZABLE. */
	public static final String WINDOW_RESIZABLE = "window-resizable";
	
	/** The Constant WINDOW_VSYNC_ENABLED. */
	public static final String WINDOW_VSYNC_ENABLED = "window-vsync-enabled";
	
	/** The Constant SIMULATION_MAX_UPS. */
	public static final String SIMULATION_MAX_UPS = "simulation-max-ups";
	
	/** The Constant SIMULATION_MAX_FPS. */
	public static final String SIMULATION_MAX_FPS = "simulation-max-fps";

	/** The application. */
	private Application application;
	
	/** The window config. */
	private WindowConfig windowConfig;
	
	/** The simulation config. */
	private SimulationConfig simulationConfig;

	/**
	 * Instantiates a new engine config.
	 */
	public EngineConfig()
	{
		this(null, null, null);
	}

	/**
	 * Instantiates a new engine config.
	 *
	 * @param application the application
	 * @param windowInfo the window info
	 * @param simulationConfig the simulation config
	 */
	public EngineConfig(Application application, WindowConfig windowInfo, SimulationConfig simulationConfig)
	{
		this.application = application;
		this.windowConfig = windowInfo;
		this.simulationConfig = simulationConfig;
	}

	/**
	 * Instantiates a new engine config.
	 *
	 * @param application the application
	 * @param pathConfig the path config
	 */
	public EngineConfig(Application application, ExternalFileHandle pathConfig)
	{
		this.application = application;
		windowConfig = new WindowConfig();
		simulationConfig = new SimulationConfig();

		try
		{
			Properties props = new Properties();
			props.load(pathConfig.openInputStream());

			// load or set default properties from engine configuration file
			if (props.getProperty(WINDOW_TITLE) == null)
				windowConfig.setTitle("Game");
			else
				windowConfig.setTitle(props.getProperty(WINDOW_TITLE));

			if (props.getProperty(WINDOW_WIDTH) == null)
				windowConfig.setWidth(16 * 60);
			else
				windowConfig.setWidth(Integer.parseInt(props.getProperty(WINDOW_WIDTH)));

			if (props.getProperty(WINDOW_HEIGHT) == null)
				windowConfig.setHeight(9 * 60);
			else
				windowConfig.setHeight(Integer.parseInt(props.getProperty(WINDOW_HEIGHT)));

			if (props.getProperty(WINDOW_RESIZABLE) == null)
				windowConfig.setResizable(true);
			else
				windowConfig.setResizable(Boolean.parseBoolean(props.getProperty(WINDOW_RESIZABLE)));

			if (props.getProperty(WINDOW_VSYNC_ENABLED) == null)
				windowConfig.setVsyncEnabled(false);
			else
				windowConfig.setVsyncEnabled(Boolean.parseBoolean(props.getProperty(WINDOW_VSYNC_ENABLED)));

			if (props.getProperty(SIMULATION_MAX_UPS) == null)
				simulationConfig.setMaxUps(60);
			else
				simulationConfig.setMaxUps(Integer.parseInt(props.getProperty(SIMULATION_MAX_UPS)));

			if (props.getProperty(SIMULATION_MAX_FPS) == null)
				simulationConfig.setMaxFps(60);
			else
				simulationConfig.setMaxFps(Integer.parseInt(props.getProperty(SIMULATION_MAX_FPS)));

		} catch (IOException e)
		{
			e.printStackTrace();
		}

	}

	/**
	 * Gets the application.
	 *
	 * @return the application
	 */
	public Application getApplication()
	{
		return application;
	}

	/**
	 * Gets the window config.
	 *
	 * @return the window config
	 */
	public WindowConfig getWindowConfig()
	{
		return windowConfig;
	}

	/**
	 * Gets the simulation config.
	 *
	 * @return the simulation config
	 */
	public SimulationConfig getSimulationConfig()
	{
		return simulationConfig;
	}

	/**
	 * Sets the application.
	 *
	 * @param application the new application
	 */
	public void setApplication(Application application)
	{
		this.application = application;
	}

	/**
	 * Sets the window config.
	 *
	 * @param windowConfig the new window config
	 */
	public void setWindowConfig(WindowConfig windowConfig)
	{
		this.windowConfig = windowConfig;
	}

	/**
	 * Sets the simulation config.
	 *
	 * @param simulationConfig the new simulation config
	 */
	public void setSimulationConfig(SimulationConfig simulationConfig)
	{
		this.simulationConfig = simulationConfig;
	}

}
