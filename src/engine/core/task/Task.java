package engine.core.task;

/**
 * The Class Task.
 */
public class Task
{

	/** The last schedule time. */
	private long lastScheduleTime;
	
	/** The delay. */
	private long delay;
	
	/** The cancelled. */
	private boolean cancelled;
	
	/** The repeating. */
	private boolean repeating;
	
	/** The runnable. */
	private Runnable runnable;

	/**
	 * Instantiates a new task.
	 *
	 * @param delay the delay
	 * @param runnable the runnable
	 */
	public Task(long delay, Runnable runnable)
	{
		this(delay, runnable, false);
	}

	/**
	 * Instantiates a new task.
	 *
	 * @param delay the delay
	 * @param runnable the runnable
	 * @param repeating the repeating
	 */
	public Task(long delay, Runnable runnable, boolean repeating)
	{
		resetScheduleTime();
		this.delay = delay;
		this.runnable = runnable;
		this.repeating = repeating;
	}

	/**
	 * Gets the last schedule time.
	 *
	 * @return the last schedule time
	 */
	public long getLastScheduleTime()
	{
		return lastScheduleTime;
	}

	/**
	 * Gets the delay.
	 *
	 * @return the delay
	 */
	public long getDelay()
	{
		return delay;
	}

	/**
	 * Checks if is cancelled.
	 *
	 * @return true, if is cancelled
	 */
	public boolean isCancelled()
	{
		return cancelled;
	}

	/**
	 * Gets the runnable.
	 *
	 * @return the runnable
	 */
	public Runnable getRunnable()
	{
		return runnable;
	}

	/**
	 * Checks if is repeating.
	 *
	 * @return true, if is repeating
	 */
	public boolean isRepeating()
	{
		return repeating;
	}

	/**
	 * Checks if is ready.
	 *
	 * @return true, if is ready
	 */
	public boolean isReady()
	{
		return System.currentTimeMillis() - lastScheduleTime >= delay;
	}

	/**
	 * Sets the delay.
	 *
	 * @param delay the new delay
	 */
	public void setDelay(long delay)
	{
		this.delay = delay;
	}

	/**
	 * Sets the runnable.
	 *
	 * @param runnable the new runnable
	 */
	public void setRunnable(Runnable runnable)
	{
		this.runnable = runnable;
	}

	/**
	 * Reset schedule time.
	 */
	public void resetScheduleTime()
	{
		lastScheduleTime = System.currentTimeMillis();
	}

	/**
	 * Cancel.
	 */
	public void cancel()
	{
		cancelled = true;
	}

}