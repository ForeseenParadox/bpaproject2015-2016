package engine.core.task;

import java.util.ArrayList;
import java.util.List;

import engine.core.Subsystem;
import engine.event.listener.UpdateListener;

/**
 * The Class TaskScheduler.
 */
public class TaskScheduler implements Subsystem, UpdateListener
{

	/** The tasks. */
	private List<Task> tasks;

	/**
	 * Schedule sync task.
	 *
	 * @param t the t
	 */
	public void scheduleSyncTask(Task t)
	{
		tasks.add(t);
	}

	/**
	 * Instantiates a new task scheduler.
	 */
	public TaskScheduler()
	{
	}

	@Override
	public void init()
	{
		tasks = new ArrayList<Task>();
	}

	@Override
	public void dispose()
	{

	}

	@Override
	public void update(float delta)
	{
		int i = 0;
		while (i < tasks.size())
		{
			Task t = tasks.get(i);

			if (t.isReady())
			{
				t.getRunnable().run();

				if (!t.isRepeating())
					t.cancel();
				else
					t.resetScheduleTime();
			}

			if (t.isCancelled())
				tasks.remove(i);
			else
				i++;
		}
	}

}
