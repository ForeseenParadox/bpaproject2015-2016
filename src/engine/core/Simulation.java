package engine.core;

import engine.event.listener.RenderListener;
import engine.event.listener.UpdateListener;

/**
 * The Class Simulation.
 */
public class Simulation
{

	/** The config. */
	private SimulationConfig config;
	
	/** The frame timer. */
	private FrameTimer frameTimer;
	
	/** The update callback. */
	private UpdateListener updateCallback;
	
	/** The render callback. */
	private RenderListener renderCallback;
	
	/** The running. */
	private boolean running;

	/** The now. */
	// simulation fields
	private double now;
	
	/** The last. */
	private double last;
	
	/** The timer. */
	private float timer;
	
	/** The frame delta. */
	private float frameDelta;
	
	/** The event timers. */
	private float[] eventTimers;
	
	/** The frames. */
	private int frames;
	
	/** The updates. */
	private int updates;

	/** The average inaccuracy. */
	// thread inaccuracy caclulation fields
	private double averageInaccuracy;
	
	/** The sample count. */
	private int sampleCount;

	/**
	 * Instantiates a new simulation.
	 */
	public Simulation()
	{
	}

	/**
	 * Instantiates a new simulation.
	 *
	 * @param config the config
	 * @param updateCallback the update callback
	 * @param renderCallback the render callback
	 */
	public Simulation(SimulationConfig config, UpdateListener updateCallback, RenderListener renderCallback)
	{
		this.config = config;
		this.updateCallback = updateCallback;
		this.renderCallback = renderCallback;
		this.frameTimer = new FrameTimer();
	}

	/**
	 * Gets the config.
	 *
	 * @return the config
	 */
	public SimulationConfig getConfig()
	{
		return config;
	}

	/**
	 * Gets the frame timer.
	 *
	 * @return the frame timer
	 */
	public FrameTimer getFrameTimer()
	{
		return frameTimer;
	}

	/**
	 * Gets the update callback.
	 *
	 * @return the update callback
	 */
	public UpdateListener getUpdateCallback()
	{
		return updateCallback;
	}

	/**
	 * Gets the render callback.
	 *
	 * @return the render callback
	 */
	public RenderListener getRenderCallback()
	{
		return renderCallback;
	}

	/**
	 * Checks if is running.
	 *
	 * @return true, if is running
	 */
	public boolean isRunning()
	{
		return running;
	}

	/**
	 * Sets the config.
	 *
	 * @param config the new config
	 */
	public void setConfig(SimulationConfig config)
	{
		this.config = config;
	}

	/**
	 * Sets the update callback.
	 *
	 * @param updateCallback the new update callback
	 */
	public void setUpdateCallback(UpdateListener updateCallback)
	{
		this.updateCallback = updateCallback;
	}

	/**
	 * Sets the render callback.
	 *
	 * @param renderCallback the new render callback
	 */
	public void setRenderCallback(RenderListener renderCallback)
	{
		this.renderCallback = renderCallback;
	}

	/**
	 * Start simulation.
	 */
	public void startSimulation()
	{
		running = true;
		run();
	}

	/**
	 * Stop simulation.
	 */
	public void stopSimulation()
	{
		running = false;
	}

	/**
	 * Run.
	 */
	private void run()
	{
		try
		{
			last = System.nanoTime() / 1e6D;
			now = System.nanoTime() / 1e6D;
			timer = (float) (now - last);
			frameDelta = System.nanoTime() / 1e9f;
			eventTimers = new float[3];
			while (running)
			{
				now = System.nanoTime() / 1e6D;
				timer = (float) (now - last);
				last = now;

				for (int i = 0; i < eventTimers.length; i++)
					eventTimers[i] += timer;

				if (config.getMaxUps() >= 1)
					while (eventTimers[0] >= 1e3f / config.getMaxUps())
						update();
				else
					update();

				if (config.getMaxFps() < 1 || (config.getMaxFps() >= 1 && eventTimers[1] >= 1e3f / config.getMaxFps()))
					render();
				else
					attemptSleep();

				if (eventTimers[2] >= 1e3f)
				{
					frameTimer.setFps(frames);
					frameTimer.setUps(updates);
					frames = 0;
					updates = 0;
					eventTimers[2] -= 1e3f;
				}
			}
		} catch (Throwable t)
		{
			Engine.getInstance().crash(t);
		}
	}

	/**
	 * Update.
	 */
	private void update()
	{
		frameTimer.setDelta(System.nanoTime() / 1e9f - frameDelta);
		frameDelta = System.nanoTime() / 1e9f;
		updateCallback.update(frameTimer.getDelta());
		eventTimers[0] -= 1e3f / config.getMaxUps();
		updates++;
	}

	/**
	 * Render.
	 */
	private void render()
	{
		renderCallback.render();
		eventTimers[1] -= 1e3f / config.getMaxFps();
		frames++;
	}

	/**
	 * Attempt sleep.
	 */
	private void attemptSleep()
	{
		long sleep = (long) ((1e3f / config.getMaxFps() - eventTimers[1]) - averageInaccuracy);

		if (sleep > 0)
		{
			sampleCount++;
			try
			{
				long begin = System.currentTimeMillis();
				Thread.sleep(sleep);
				float sleepTime = System.currentTimeMillis() - begin;
				float error = sleepTime - sleep;
				averageInaccuracy = (averageInaccuracy + error) / (sampleCount++);
			} catch (InterruptedException e)
			{
				e.printStackTrace();
			}
		}
	}
}
