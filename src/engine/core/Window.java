package engine.core;

import static org.lwjgl.glfw.Callbacks.errorCallbackPrint;
import static org.lwjgl.glfw.Callbacks.glfwSetCallback;
import static org.lwjgl.glfw.GLFW.GLFW_RESIZABLE;
import static org.lwjgl.glfw.GLFW.GLFW_VISIBLE;
import static org.lwjgl.glfw.GLFW.glfwCreateWindow;
import static org.lwjgl.glfw.GLFW.glfwDefaultWindowHints;
import static org.lwjgl.glfw.GLFW.glfwDestroyWindow;
import static org.lwjgl.glfw.GLFW.glfwGetPrimaryMonitor;
import static org.lwjgl.glfw.GLFW.glfwGetVideoMode;
import static org.lwjgl.glfw.GLFW.glfwInit;
import static org.lwjgl.glfw.GLFW.glfwMakeContextCurrent;
import static org.lwjgl.glfw.GLFW.glfwPollEvents;
import static org.lwjgl.glfw.GLFW.glfwSetErrorCallback;
import static org.lwjgl.glfw.GLFW.glfwSetWindowPos;
import static org.lwjgl.glfw.GLFW.glfwSetWindowSize;
import static org.lwjgl.glfw.GLFW.glfwSetWindowTitle;
import static org.lwjgl.glfw.GLFW.glfwShowWindow;
import static org.lwjgl.glfw.GLFW.glfwSwapBuffers;
import static org.lwjgl.glfw.GLFW.glfwSwapInterval;
import static org.lwjgl.glfw.GLFW.glfwWindowHint;
import static org.lwjgl.glfw.GLFW.glfwWindowShouldClose;
import static org.lwjgl.opengl.EXTFramebufferObject.GL_FRAMEBUFFER_EXT;
import static org.lwjgl.opengl.EXTFramebufferObject.glBindFramebufferEXT;
import static org.lwjgl.opengl.GL11.GL_FALSE;
import static org.lwjgl.opengl.GL11.GL_TRUE;
import static org.lwjgl.system.MemoryUtil.NULL;

import java.nio.ByteBuffer;

import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWWindowSizeCallback;
import org.lwjgl.glfw.GLFWvidmode;
import org.lwjgl.opengl.GLContext;

import engine.event.listener.RenderListener;
import engine.event.listener.WindowListener;

/**
 * The Class Window.
 */
public class Window implements Subsystem, RenderListener
{

	/** The error callback. */
	private GLFWErrorCallback errorCallback;
	
	/** The glfw resize listener. */
	private GLFWWindowSizeCallback glfwResizeListener;
	
	/** The window handle. */
	private long windowHandle;
	
	/** The config. */
	private WindowConfig config;
	
	/** The window listener. */
	private WindowListener windowListener;

	/**
	 * Instantiates a new window.
	 *
	 * @param defaultInfo the default info
	 */
	public Window(WindowConfig defaultInfo)
	{
		this.config = defaultInfo;
	}

	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	public void setTitle(String title)
	{
		config.setTitle(title);
		glfwSetWindowTitle(windowHandle, title);
	}

	/**
	 * Sets the width.
	 *
	 * @param width the new width
	 */
	public void setWidth(int width)
	{
		glfwSetWindowSize(windowHandle, width, getHeight());
	}

	/**
	 * Sets the height.
	 *
	 * @param height the new height
	 */
	public void setHeight(int height)
	{
		glfwSetWindowSize(windowHandle, getWidth(), height);
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle()
	{
		return config.getTitle();
	}

	/**
	 * Gets the width.
	 *
	 * @return the width
	 */
	public int getWidth()
	{
		return config.getWidth();
	}

	/**
	 * Gets the height.
	 *
	 * @return the height
	 */
	public int getHeight()
	{
		return config.getHeight();
	}

	/**
	 * Gets the aspect ratio.
	 *
	 * @return the aspect ratio
	 */
	public float getAspectRatio()
	{
		return (float) getWidth() / getHeight();
	}

	/**
	 * Gets the window handle.
	 *
	 * @return the window handle
	 */
	public long getWindowHandle()
	{
		return windowHandle;
	}

	/**
	 * Gets the config.
	 *
	 * @return the config
	 */
	public WindowConfig getConfig()
	{
		return config;
	}

	/**
	 * Gets the window listener.
	 *
	 * @return the window listener
	 */
	public WindowListener getWindowListener()
	{
		return windowListener;
	}

	/**
	 * Checks if is close requested.
	 *
	 * @return true, if is close requested
	 */
	public boolean isCloseRequested()
	{
		return glfwWindowShouldClose(windowHandle) == GL_TRUE;
	}

	/**
	 * Sets the window listener.
	 *
	 * @param windowListener the new window listener
	 */
	public void setWindowListener(WindowListener windowListener)
	{
		this.windowListener = windowListener;
	}

	/**
	 * Invoke resize event.
	 */
	public void invokeResizeEvent()
	{
		if (windowListener != null)
			windowListener.windowResized(windowHandle, config.getWidth(), config.getHeight());
	}

	/**
	 * Bind fbo.
	 */
	public void bindFBO()
	{
		glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
	}

	@Override
	public void init()
	{
		glfwSetErrorCallback(errorCallback = errorCallbackPrint(System.err));

		if (glfwInit() != GL_TRUE)
			throw new IllegalStateException("Failed to properly initialize GLFW.");

		// set default window hints
		glfwDefaultWindowHints();

		// the window isn't shown until explicitly told to do so, i.e.
		// glfwShowWindow
		glfwWindowHint(GLFW_VISIBLE, GL_FALSE);

		if (config.isResizable())
			glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);
		else
			glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

		windowHandle = glfwCreateWindow(config.getWidth(), config.getHeight(), config.getTitle(), NULL, NULL);

		// resize callback
		glfwSetCallback(windowHandle, glfwResizeListener = new GLFWWindowSizeCallback()
		{
			@Override
			public void invoke(long window, int width, int height)
			{
				config.setWidth(width);
				config.setHeight(height);
				invokeResizeEvent();
			}
		});

		if (windowHandle == NULL)
			throw new IllegalArgumentException("Failed to successfully create a window handle.");

		ByteBuffer videoMode = glfwGetVideoMode(glfwGetPrimaryMonitor());

		// position the window
		int screenWidth = GLFWvidmode.width(videoMode), screenHeight = GLFWvidmode.height(videoMode);
		glfwSetWindowPos(windowHandle, (screenWidth - config.getWidth()) / 2, (screenHeight - config.getHeight()) / 2);

		// make the window the current opengl context
		glfwMakeContextCurrent(windowHandle);

		// vsync settings can't be set during window creation, so are handled
		// here
		if (config.isVsyncEnabled())
			glfwSwapInterval(1);
		else
			glfwSwapInterval(0);

		// show the window
		glfwShowWindow(windowHandle);

		GLContext.createFromCurrent();

	}

	@Override
	public void render()
	{
		// poll events and swap buffers
		glfwSwapBuffers(windowHandle);
		glfwPollEvents();
	}

	@Override
	public void dispose()
	{
		glfwDestroyWindow(windowHandle);
		errorCallback.release();
		glfwResizeListener.release();
	}

}
