package engine.core;

/**
 * The Class SimulationConfig.
 */
public class SimulationConfig
{

	/** The Constant MIN_FPS_CAP. */
	public static final int MIN_FPS_CAP = 30;
	
	/** The Constant MAX_FPS_CAP. */
	public static final int MAX_FPS_CAP = 120;

	/** The max ups. */
	private int maxUps;
	
	/** The max fps. */
	private int maxFps;

	/**
	 * Instantiates a new simulation config.
	 */
	public SimulationConfig()
	{
		this(0, 0);
	}

	/**
	 * Instantiates a new simulation config.
	 *
	 * @param maxUps the max ups
	 * @param maxFps the max fps
	 */
	public SimulationConfig(int maxUps, int maxFps)
	{
		this.maxUps = maxUps;
		this.maxFps = maxFps;
	}

	/**
	 * Gets the max ups.
	 *
	 * @return the max ups
	 */
	public int getMaxUps()
	{
		return maxUps;
	}

	/**
	 * Gets the max fps.
	 *
	 * @return the max fps
	 */
	public int getMaxFps()
	{
		return maxFps;
	}

	/**
	 * Sets the max ups.
	 *
	 * @param maxUps the new max ups
	 */
	public void setMaxUps(int maxUps)
	{
		this.maxUps = maxUps;
	}

	/**
	 * Sets the max fps.
	 *
	 * @param maxFps the new max fps
	 */
	public void setMaxFps(int maxFps)
	{
		this.maxFps = maxFps;
	}

}
