package engine.core;

import engine.event.listener.RenderListener;
import engine.event.listener.UpdateListener;

/**
 * The Interface Application.
 */
public interface Application extends UpdateListener, RenderListener
{

	/**
	 * Load.
	 */
	public void load();

	/**
	 * Inits the.
	 */
	public void init();

	/**
	 * Resized.
	 *
	 * @param width the width
	 * @param height the height
	 */
	public void resized(int width, int height);

	/**
	 * Dispose.
	 *
	 * @param reason the reason
	 */
	public void dispose(ExitReason reason);

}
