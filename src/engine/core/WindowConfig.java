package engine.core;

/**
 * The Class WindowConfig.
 */
public class WindowConfig
{

	/** The title. */
	private String title;
	
	/** The width. */
	private int width;
	
	/** The height. */
	private int height;
	
	/** The resizable. */
	private boolean resizable;
	
	/** The vsync enabled. */
	private boolean vsyncEnabled;

	/**
	 * Instantiates a new window config.
	 */
	public WindowConfig()
	{
		this("Game", 800, 600, false, false);
	}

	/**
	 * Instantiates a new window config.
	 *
	 * @param title the title
	 * @param width the width
	 * @param height the height
	 * @param resizable the resizable
	 * @param vsyncEnabled the vsync enabled
	 */
	public WindowConfig(String title, int width, int height, boolean resizable, boolean vsyncEnabled)
	{
		this.title = title;
		this.width = width;
		this.height = height;
		this.resizable = resizable;
		this.vsyncEnabled = vsyncEnabled;
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle()
	{
		return title;
	}

	/**
	 * Gets the width.
	 *
	 * @return the width
	 */
	public int getWidth()
	{
		return width;
	}

	/**
	 * Gets the height.
	 *
	 * @return the height
	 */
	public int getHeight()
	{
		return height;
	}

	/**
	 * Checks if is resizable.
	 *
	 * @return true, if is resizable
	 */
	public boolean isResizable()
	{
		return resizable;
	}

	/**
	 * Checks if is vsync enabled.
	 *
	 * @return true, if is vsync enabled
	 */
	public boolean isVsyncEnabled()
	{
		return vsyncEnabled;
	}

	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	public void setTitle(String title)
	{
		this.title = title;
	}

	/**
	 * Sets the width.
	 *
	 * @param width the new width
	 */
	public void setWidth(int width)
	{
		this.width = width;
	}

	/**
	 * Sets the height.
	 *
	 * @param height the new height
	 */
	public void setHeight(int height)
	{
		this.height = height;
	}

	/**
	 * Sets the resizable.
	 *
	 * @param resizable the new resizable
	 */
	public void setResizable(boolean resizable)
	{
		this.resizable = resizable;
	}

	/**
	 * Sets the vsync enabled.
	 *
	 * @param vsyncEnabled the new vsync enabled
	 */
	public void setVsyncEnabled(boolean vsyncEnabled)
	{
		this.vsyncEnabled = vsyncEnabled;
	}

}
