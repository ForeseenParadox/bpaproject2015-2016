package engine.tiled;

import java.util.List;

import engine.util.xml.XMLElement;

/**
 * The Class TiledLayerObjectLayer.
 */
public class TiledLayerObjectLayer extends TiledLayer
{

	/** The width. */
	private int width;
	
	/** The height. */
	private int height;
	// private List<Shape> shapes;

	/**
	 * Instantiates a new tiled layer object layer.
	 *
	 * @param properties the properties
	 * @param rootElement the root element
	 */
	public TiledLayerObjectLayer(TiledMapProperties properties, XMLElement rootElement)
	{
		super(properties, rootElement);

		if (rootElement.getAttrib("width") != null)
			width = Integer.parseInt(rootElement.getAttribValue("width"));

		if (rootElement.getAttrib("height") != null)
			height = Integer.parseInt(rootElement.getAttribValue("height"));

		// shapes = new ArrayList<Shape>();

		List<XMLElement> objectElements = rootElement.getChildrenByName("object");
		for (XMLElement objectElement : objectElements)
		{
			float x = Float.parseFloat(objectElement.getAttribValue("x"));
			float y = Float.parseFloat(objectElement.getAttribValue("y"));
			int mapHeight = properties.getHeight() * properties.getTileHeight();

			if (objectElement.getChildren().size() == 0)
			{
				int width = Integer.parseInt(objectElement.getAttribValue("width"));
				int height = Integer.parseInt(objectElement.getAttribValue("height"));
				// PolygonShape shape = new PolygonShape();
				// Vec2[] verts =
				// {
				// new Vec2(x, y),
				// new Vec2(x, y + height),
				// new Vec2(x + width, y + height),
				// new Vec2(x + width, y) };

				// for (int i = 0; i < verts.length; i++)
				// {
				// verts[i].y = mapHeight - verts[i].y;
				// verts[i] = verts[i].mul(1.0f / GameWorld.PPM);
				// System.out.println(verts[i].y);
				// }
				// shape.set(verts, verts.length);
				// shapes.add(shape);
			} else
			{
				XMLElement childElement = objectElement.getChildren().get(0);

				if (childElement.getName().equals("ellipse"))
				{
					// int width =
					// Integer.parseInt(objectElement.getAttribValue("width"));
					// int height =
					// Integer.parseInt(objectElement.getAttribValue("height"));
				} else if (childElement.getName().equals("polygon"))
				{
					// String[] pointsAsString =
					// childElement.getAttribValue("points").split(" ");
					// Vec2[] points = new Vec2[pointsAsString.length];
					//
					// for (int i = 0; i < points.length; i++)
					// {
					// String[] coordinateTokens = pointsAsString[i].split(",");
					// points[i] = new
					// Vec2((Float.parseFloat(coordinateTokens[0]) + x) /
					// GameWorld.PPM, (mapHeight -
					// (Float.parseFloat(coordinateTokens[1]) + y)) /
					// GameWorld.PPM);
					// }
					//
					// PolygonShape shape = new PolygonShape();
					// shape.set(points, points.length);
					// shapes.add(shape);
				}
			}
		}

	}

	/**
	 * Gets the width.
	 *
	 * @return the width
	 */
	public int getWidth()
	{
		return width;
	}

	/**
	 * Gets the height.
	 *
	 * @return the height
	 */
	public int getHeight()
	{
		return height;
	}

	// public List<Shape> getShapes()
	// {
	// return shapes;
	// }
	//
	// public void render(ShapeBatch shapeBatch)
	// {
	// for (Shape shape : shapes)
	// {
	// if (shape.m_type == ShapeType.POLYGON)
	// {
	// PolygonShape pshape = (PolygonShape) shape;
	// float[] xPoints = new float[pshape.getVertexCount()], yPoints = new
	// float[pshape.getVertexCount()];
	// for (int i = 0; i < pshape.getVertexCount(); i++)
	// {
	// xPoints[i] = (pshape.getVertices()[i].x) * GameWorld.PPM;
	// yPoints[i] = (pshape.getVertices()[i].y) * GameWorld.PPM;
	// }
	//
	// shapeBatch.renderPolygon(xPoints, yPoints, pshape.getVertexCount(), 0, 0,
	// 0);
	// }
	// }
	// }

}
