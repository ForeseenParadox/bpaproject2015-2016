package engine.tiled;

import java.util.List;

import engine.graphics.batch.SpriteBatch;
import engine.graphics.texture.TextureRegion;
import engine.util.xml.XMLElement;

/**
 * The Class TiledLayerTileLayer.
 */
public class TiledLayerTileLayer extends TiledLayer
{

	/** The width. */
	private int width;
	
	/** The height. */
	private int height;
	
	/** The tiles. */
	private TiledTile[] tiles;

	/**
	 * Instantiates a new tiled layer tile layer.
	 *
	 * @param properties the properties
	 * @param tilesets the tilesets
	 * @param rootElement the root element
	 */
	public TiledLayerTileLayer(TiledMapProperties properties, List<TiledTileSet> tilesets, XMLElement rootElement)
	{
		super(properties, rootElement);
		width = Integer.parseInt(rootElement.getAttribValue("width"));
		height = Integer.parseInt(rootElement.getAttribValue("height"));
		tiles = new TiledTile[width * height];

		List<XMLElement> tileData = rootElement.getChildByName("data").getChildrenByName("tile");
		int tileIndex = 0;
		for (int x = 0; x < width; x++)
		{
			for (int y = 0; y < height; y++)
			{
				XMLElement el = tileData.get(y + (width - x - 1) * height);
				int gid = Integer.parseInt(el.getAttribValue("gid"));

				if (gid > 0)
				{
					for (int i = 0; i < tilesets.size(); i++)
					{
						if (i == tilesets.size() - 1)
							tiles[tileIndex] = tilesets.get(i).getTile(gid);
						else
						{
							int thisGid = tilesets.get(i).getFirstGid(), nextGid = tilesets.get(i + 1).getFirstGid();
							if (gid >= thisGid && gid <= nextGid)
								tiles[tileIndex] = tilesets.get(i).getTile(gid);
						}
					}
				}

				tileIndex++;
			}
		}
	}

	/**
	 * Gets the width.
	 *
	 * @return the width
	 */
	public int getWidth()
	{
		return width;
	}

	/**
	 * Gets the height.
	 *
	 * @return the height
	 */
	public int getHeight()
	{
		return height;
	}

	/**
	 * Gets the tile.
	 *
	 * @param x the x
	 * @param y the y
	 * @return the tile
	 */
	public TiledTile getTile(int x, int y)
	{
		return tiles[x + y * width];
	}

	/**
	 * Gets the tiles.
	 *
	 * @return the tiles
	 */
	public TiledTile[] getTiles()
	{
		return tiles;
	}

	/**
	 * Render.
	 *
	 * @param spriteBatch the sprite batch
	 * @param tx the tx
	 * @param ty the ty
	 */
	public void render(SpriteBatch spriteBatch, int tx, int ty)
	{
		for (int x = 0; x < width; x++)
		{
			for (int y = 0; y < height; y++)
			{
				TextureRegion t = getTile(x, y).getTexture();
				spriteBatch.renderTexture(t, x * t.getWidth() + tx, y * t.getHeight() + ty, t.getWidth(), t.getHeight(), 0, 0, 0, 1, 1);
			}
		}
	}

}
