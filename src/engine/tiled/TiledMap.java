package engine.tiled;

import java.util.ArrayList;
import java.util.List;

import engine.file.FileHandle;
import engine.file.InternalFileHandle;
import engine.graphics.batch.ShapeBatch;
import engine.graphics.batch.SpriteBatch;
import engine.graphics.texture.Texture;
import engine.resource.TextFileResource;
import engine.resource.TextureResource;
import engine.util.xml.XMLDocument;
import engine.util.xml.XMLElement;

/**
 * The Class TiledMap.
 */
public class TiledMap
{

	/** The properties. */
	private TiledMapProperties properties;
	
	/** The tilesets. */
	private List<TiledTileSet> tilesets;
	
	/** The layers. */
	private List<TiledLayer> layers;

	/**
	 * Instantiates a new tiled map.
	 *
	 * @param handle the handle
	 */
	public TiledMap(FileHandle handle)
	{
		TextFileResource tfRes = new TextFileResource(handle);
		tfRes.load();
		XMLDocument xml = new XMLDocument(tfRes);
		XMLElement root = xml.getRootElement();

		properties = new TiledMapProperties(root);
		tilesets = new ArrayList<TiledTileSet>();
		layers = new ArrayList<TiledLayer>();

		List<XMLElement> ts = root.getChildrenByName("tileset");
		for (XMLElement child : ts)
		{
			String name = child.getAttribValue("name");
			int firstGid = Integer.parseInt(child.getAttribValue("firstgid"));
			int tileWidth = Integer.parseInt(child.getAttribValue("tilewidth"));
			int tileHeight = Integer.parseInt(child.getAttribValue("tileheight"));

			List<XMLElement> sources = child.getChildrenByName("image");

			// TODO: implement multi image tileset loading
			if (sources.size() > 1)
				throw new IllegalArgumentException("Tiled loader does not currently support multi image tilesets.");

			XMLElement source = sources.get(0);
			String imageLocation = source.getAttribValue("source");
			TextureResource res = new TextureResource(new InternalFileHandle("/" + imageLocation));
			res.load();
			tilesets.add(new TiledTileSet(name, new Texture(res), firstGid, tileWidth, tileHeight));
		}

		List<XMLElement> ls = root.getChildrenByName("layer");
		for (XMLElement l : ls)
		{
			TiledLayerTileLayer tlayer = new TiledLayerTileLayer(properties, tilesets, l);
			layers.add(tlayer);
		}

		List<XMLElement> os = root.getChildrenByName("objectgroup");
		for (XMLElement l : os)
		{
			TiledLayerObjectLayer tlayer = new TiledLayerObjectLayer(properties, l);
			layers.add(tlayer);
		}
	}

	/**
	 * Gets the properties.
	 *
	 * @return the properties
	 */
	public TiledMapProperties getProperties()
	{
		return properties;
	}

	/**
	 * Gets the tilesets.
	 *
	 * @return the tilesets
	 */
	public List<TiledTileSet> getTilesets()
	{
		return tilesets;
	}

	/**
	 * Gets the layers.
	 *
	 * @return the layers
	 */
	public List<TiledLayer> getLayers()
	{
		return layers;
	}

	/**
	 * Render.
	 *
	 * @param spriteBatch the sprite batch
	 * @param shapeBatch the shape batch
	 * @param tx the tx
	 * @param ty the ty
	 */
	public void render(SpriteBatch spriteBatch, ShapeBatch shapeBatch, int tx, int ty)
	{
		for (TiledLayer tileLayer : layers)
		{
			if (tileLayer instanceof TiledLayerTileLayer)
				((TiledLayerTileLayer) tileLayer).render(spriteBatch, tx, ty);
		}
	}

}