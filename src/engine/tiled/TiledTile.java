package engine.tiled;

import engine.graphics.texture.TextureRegion;

/**
 * The Class TiledTile.
 */
public class TiledTile
{

	/** The texture. */
	private TextureRegion texture;

	/**
	 * Instantiates a new tiled tile.
	 *
	 * @param texture the texture
	 */
	public TiledTile(TextureRegion texture)
	{
		this.texture = texture;
	}
	
	/**
	 * Gets the texture.
	 *
	 * @return the texture
	 */
	public TextureRegion getTexture()
	{
		return texture;
	}
	
	/**
	 * Sets the texture.
	 *
	 * @param texture the new texture
	 */
	public void setTexture(TextureRegion texture)
	{
		this.texture = texture;
	}

}