package game;

import static org.lwjgl.opengl.GL11.GL_COLOR_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.glClear;
import static org.lwjgl.opengl.GL11.glViewport;

import java.io.File;
import java.io.IOException;

import engine.audio.Audio;
import engine.core.Application;
import engine.core.Engine;
import engine.core.ExitReason;
import engine.file.ExternalFileHandle;
import engine.graphics.batch.LightBatch2D;
import engine.graphics.batch.ShapeBatch;
import engine.graphics.batch.SpriteBatch;
import engine.graphics.font.TrueTypeFont;
import engine.resource.TextFileResource;
import engine.state.GameStateManager;
import engine.util.xml.XMLDocument;
import game.entity.Entity;
import game.entity.def.MobDef;
import game.entity.def.ProjectileDef;
import game.states.MenuState;
import game.states.OptionsState;
import game.states.GameOverState;
import game.states.single.SinglePlayerState;

/**
 * The Class Game.
 */
public class Game implements Application
{

	/** The Constant MENU_STATE. */
	public static final String MENU_STATE = "MENU_STATE";
	
	/** The Constant SINGLE_PLAYER_STATE. */
	public static final String SINGLE_PLAYER_STATE = "SINGLE_PLAYER_STATE";
	
	/** The Constant OPTIONS_STATE. */
	public static final String OPTIONS_STATE = "OPTIONS_STATE";
	
	/** The Constant GAME_OVER_STATE. */
	public static final String GAME_OVER_STATE = "GAME_OVER_STATE";

	/** The instance. */
	private static Game instance;

	/** The state manager. */
	private GameStateManager stateManager;

	/** The sprite batch. */
	private SpriteBatch spriteBatch;
	
	/** The shape batch. */
	private ShapeBatch shapeBatch;
	
	/** The light batch. */
	private LightBatch2D lightBatch;
	
	/** The trebuchet20. */
	private TrueTypeFont trebuchet20;

	/** The options. */
	private OptionsFile options;

	/**
	 * Instantiates a new game.
	 */
	public Game()
	{
		instance = this;
	}

	/**
	 * Gets the single instance of Game.
	 *
	 * @return single instance of Game
	 */
	public static Game getInstance()
	{
		return instance;
	}

	/**
	 * Gets the state manager.
	 *
	 * @return the state manager
	 */
	public GameStateManager getStateManager()
	{
		return stateManager;
	}

	/**
	 * Gets the sprite batch.
	 *
	 * @return the sprite batch
	 */
	public SpriteBatch getSpriteBatch()
	{
		return spriteBatch;
	}

	/**
	 * Gets the shape batch.
	 *
	 * @return the shape batch
	 */
	public ShapeBatch getShapeBatch()
	{
		return shapeBatch;
	}

	/**
	 * Gets the light batch.
	 *
	 * @return the light batch
	 */
	public LightBatch2D getLightBatch()
	{
		return lightBatch;
	}

	/**
	 * Gets the options.
	 *
	 * @return the options
	 */
	public OptionsFile getOptions()
	{
		return options;
	}

	@Override
	public void init()
	{

		trebuchet20 = new TrueTypeFont("Courier New", 20, false);

		stateManager = new GameStateManager(MENU_STATE);
		stateManager.insertGameState(new MenuState(MENU_STATE));
		stateManager.insertGameState(new SinglePlayerState(SINGLE_PLAYER_STATE));
		stateManager.insertGameState(new OptionsState(OPTIONS_STATE));
		stateManager.insertGameState(new GameOverState(GAME_OVER_STATE));
		stateManager.pushGameState(MENU_STATE);

		spriteBatch = new SpriteBatch();
		shapeBatch = new ShapeBatch();
		lightBatch = new LightBatch2D();

		loadOptions();

	}

	/**
	 * Load options.
	 */
	private void loadOptions()
	{
		File optionsFile = new File("options.xml");
		if (!optionsFile.exists())
		{
			options = new OptionsFile(null);
			try
			{
				optionsFile.createNewFile();
			} catch (IOException e)
			{
				e.printStackTrace();
			}
		} else
		{
			TextFileResource res = new TextFileResource(new ExternalFileHandle("options.xml"));
			res.load();
			options = new OptionsFile(res);
		}

		// set defaults
		Engine.getInstance().getSimulation().getFrameTimer().setFps(options.getMaxFps());
		Audio.setAmplitudeByType("sfx", Game.getInstance().getOptions().getMasterVolume() * Game.getInstance().getOptions().getSfxVolume());
		Audio.setAmplitudeByType("music", Game.getInstance().getOptions().getMasterVolume() * Game.getInstance().getOptions().getMusicVolume());

	}

	@Override
	public void load()
	{
		Engine.getInstance().getResourceSystem().loadResourceFile(new ExternalFileHandle("./res_ext/resources/Texture.resource"));
		Engine.getInstance().getResourceSystem().loadResourceFile(new ExternalFileHandle("./res_ext/resources/Animation.resource"));
		Engine.getInstance().getResourceSystem().loadResourceFile(new ExternalFileHandle("./res_ext/resources/Audio.resource"));
		Engine.getInstance().getResourceSystem().loadResourceFile(new ExternalFileHandle("./res_ext/resources/TextFile.resource"));

		// register entity definitions
		Entity.registerEntityDef(new MobDef(new XMLDocument((TextFileResource) Engine.getInstance().getResourceSystem().getResource("SlimeDefFile"))));
		Entity.registerEntityDef(new MobDef(new XMLDocument((TextFileResource) Engine.getInstance().getResourceSystem().getResource("HoveringBoxDefFile"))));
		Entity.registerEntityDef(new MobDef(new XMLDocument((TextFileResource) Engine.getInstance().getResourceSystem().getResource("MainPlayerDefFile"))));

		// projectiles
		Entity.registerEntityDef(new ProjectileDef(new XMLDocument((TextFileResource) Engine.getInstance().getResourceSystem().getResource("BasicProjectileDefFile"))));
		Entity.registerEntityDef(new ProjectileDef(new XMLDocument((TextFileResource) Engine.getInstance().getResourceSystem().getResource("DestructionProjectileDefFile"))));
		Entity.registerEntityDef(new ProjectileDef(new XMLDocument((TextFileResource) Engine.getInstance().getResourceSystem().getResource("HitmarkerDefFile"))));

	}

	@Override
	public void update(float delta)
	{
		stateManager.updateCurrent(delta);
	}

	@Override
	public void render()
	{
		glClear(GL_COLOR_BUFFER_BIT);

		stateManager.renderCurrent();
	}

	@Override
	public void resized(int width, int height)
	{
		glViewport(0, 0, width, height);
		spriteBatch.getProjectionMatrix().setOrthogonalProjection(0, width, 0, height, -1, 1);
		shapeBatch.getProjectionMatrix().setOrthogonalProjection(0, width, 0, height, -1, 1);
		stateManager.resizeCurrent(width, height);
	}

	@Override
	public void dispose(ExitReason reason)
	{

	}

	/**
	 * Gets the trebuchet20.
	 *
	 * @return the trebuchet20
	 */
	public TrueTypeFont getTrebuchet20()
	{
		return trebuchet20;
	}

}
