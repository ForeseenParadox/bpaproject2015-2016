package game;

import engine.core.Engine;
import engine.graphics.Color;
import engine.graphics.batch.SpriteBatch;
import engine.graphics.font.TrueTypeFont;

/**
 * The Class DebugInfo.
 */
public class DebugInfo
{

	/** The debug info. */
	private String[] debugInfo;

	/**
	 * Instantiates a new debug info.
	 *
	 * @param infoCount the info count
	 */
	public DebugInfo(int infoCount)
	{
		debugInfo = new String[infoCount];
	}

	/**
	 * Gets the debug info.
	 *
	 * @return the debug info
	 */
	public String[] getDebugInfo()
	{
		return debugInfo;
	}

	/**
	 * Render.
	 *
	 * @param spriteBatch the sprite batch
	 * @param font the font
	 */
	public void render(SpriteBatch spriteBatch, TrueTypeFont font)
	{
		int y = Engine.getInstance().getWindow().getHeight() - font.getCharacterHeight();

		for (String x : debugInfo)
		{
			if (x != null)
			{
				font.render(spriteBatch, x, Color.WHITE, 10, y);
			}
			y -= font.getCharacterHeight();
		}

	}

}