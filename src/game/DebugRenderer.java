package game;

import java.awt.Font;

import engine.graphics.batch.SpriteBatch;

/**
 * The Class DebugRenderer.
 */
public class DebugRenderer
{

	/** The tokens. */
	private String[] tokens;

	/**
	 * Instantiates a new debug renderer.
	 *
	 * @param toks the toks
	 */
	public DebugRenderer(String[] toks)
	{
		this.tokens = toks;
	}

	/**
	 * Gets the tokens.
	 *
	 * @return the tokens
	 */
	public String[] getTokens()
	{
		return tokens;
	}

	/**
	 * Update tokens.
	 *
	 * @param tokens the tokens
	 */
	public void updateTokens(String[] tokens)
	{
		this.tokens = tokens;
	}

	/**
	 * Update.
	 */
	public void update()
	{

	}

	/**
	 * Render.
	 *
	 * @param font the font
	 * @param spriteBatch the sprite batch
	 */
	public void render(Font font, SpriteBatch spriteBatch)
	{

	}

}
