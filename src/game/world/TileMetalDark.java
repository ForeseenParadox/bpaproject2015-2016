package game.world;

import engine.graphics.texture.TextureRegion;
import game.Resources;

/**
 * The Class TileMetalDark.
 */
public class TileMetalDark extends Tile
{

	/**
	 * Instantiates a new tile metal dark.
	 */
	public TileMetalDark()
	{
		super(TextureRegion.getTextureRegion(Resources.TEXTURE_METAL_DARK), true);
	}

}
