package game.world;

import engine.animation.TextureAnimation;
import game.Resources;

/**
 * The Class TileSewage.
 */
public class TileSewage extends AnimatedTile
{

	/**
	 * The Enum SewageType.
	 */
	public enum SewageType
	{
		
		/** The reg. */
		REG,
		
		/** The top. */
		TOP,
		
		/** The pipe. */
		PIPE
	}

	/**
	 * Instantiates a new tile sewage.
	 *
	 * @param type the type
	 */
	public TileSewage(SewageType type)
	{
		super(TextureAnimation.getTextureAnimation(Resources.ANIM_SEWAGE), false);
		if (type == SewageType.TOP)
			setAnim(TextureAnimation.getTextureAnimation(Resources.ANIM_SEWAGE_TOP));
		if (type == SewageType.PIPE)
			setAnim(TextureAnimation.getTextureAnimation(Resources.ANIM_SEWAGE_PIPE));
	}

}
