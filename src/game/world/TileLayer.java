package game.world;

import engine.core.Engine;
import engine.graphics.batch.SpriteBatch;
import engine.util.math.Vector2f;

/**
 * The Class TileLayer.
 */
public class TileLayer
{

	/** The name. */
	private String name;
	
	/** The width. */
	private int width;
	
	/** The height. */
	private int height;
	
	/** The tiles. */
	private Tile[] tiles;
	
	/** The collision layer. */
	private boolean collisionLayer;

	/**
	 * Instantiates a new tile layer.
	 *
	 * @param name the name
	 * @param width the width
	 * @param height the height
	 * @param collisionLayer the collision layer
	 */
	public TileLayer(String name, int width, int height, boolean collisionLayer)
	{
		this.name = name;
		this.width = width;
		this.height = height;
		this.tiles = new Tile[width * height];
		this.collisionLayer = collisionLayer;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * Gets the width.
	 *
	 * @return the width
	 */
	public int getWidth()
	{
		return width;
	}

	/**
	 * Gets the height.
	 *
	 * @return the height
	 */
	public int getHeight()
	{
		return height;
	}

	/**
	 * Gets the tiles.
	 *
	 * @return the tiles
	 */
	public Tile[] getTiles()
	{
		return tiles;
	}

	/**
	 * Gets the tile.
	 *
	 * @param x the x
	 * @param y the y
	 * @return the tile
	 */
	public Tile getTile(int x, int y)
	{
		return tiles[x + y * width];
	}

	/**
	 * Checks if is collision layer.
	 *
	 * @return true, if is collision layer
	 */
	public boolean isCollisionLayer()
	{
		return collisionLayer;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name)
	{
		this.name = name;
	}

	/**
	 * Sets the tile.
	 *
	 * @param x the x
	 * @param y the y
	 * @param t the t
	 */
	public void setTile(int x, int y, Tile t)
	{
		tiles[x + y * width] = t;
	}

	/**
	 * Sets the collision layer.
	 *
	 * @param collisionLayer the new collision layer
	 */
	public void setCollisionLayer(boolean collisionLayer)
	{
		this.collisionLayer = collisionLayer;
	}

	/**
	 * Render.
	 *
	 * @param spriteBatch the sprite batch
	 * @param translation the translation
	 */
	public void render(SpriteBatch spriteBatch, Vector2f translation)
	{
		int tx = (int) translation.getX(), ty = (int) translation.getY();

		// calculate the bounds to start rendering tiles at
		int startX = -tx / (Tile.SIZE), startY = -ty / (Tile.SIZE);
		int endX = startX + (int) Math.ceil(Engine.getInstance().getWindow().getWidth() / (Tile.SIZE)) + 2, endY = startY + (int) Math.ceil(Engine.getInstance().getWindow().getHeight() / (Tile.SIZE)) + 2;

		for (int x = startX; x < endX; x++)
		{
			for (int y = startY; y < endY; y++)
			{
				// check if tile actually exists
				if (x >= 0 && x < width && y >= 0 && y < height && tiles[x + y * width] != null)
					spriteBatch.renderTexture(getTile(x, y).getTexture(), x * Tile.SIZE, y * Tile.SIZE, Tile.SIZE, Tile.SIZE, 0, 0, 0, 1, 1);
			}
		}
	}
}
