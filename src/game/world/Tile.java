package game.world;

import engine.graphics.texture.TextureRegion;

/**
 * The Class Tile.
 */
public class Tile
{

	/** The Constant SIZE. */
	public static final int SIZE = 32;

	/** The texture. */
	private TextureRegion texture;
	
	/** The collidable. */
	private boolean collidable;

	/**
	 * Instantiates a new tile.
	 *
	 * @param texture the texture
	 * @param collidable the collidable
	 */
	public Tile(TextureRegion texture, boolean collidable)
	{
		this.texture = texture;
		this.collidable = collidable;
	}

	/**
	 * Gets the texture.
	 *
	 * @return the texture
	 */
	public TextureRegion getTexture()
	{
		return texture;
	}

	/**
	 * Checks if is collidable.
	 *
	 * @return true, if is collidable
	 */
	public boolean isCollidable()
	{
		return collidable;
	}

	/**
	 * Sets the collidable.
	 *
	 * @param collidable the new collidable
	 */
	public void setCollidable(boolean collidable)
	{
		this.collidable = collidable;
	}

	/**
	 * Sets the texture.
	 *
	 * @param texture the new texture
	 */
	public void setTexture(TextureRegion texture)
	{
		this.texture = texture;
	}

}
