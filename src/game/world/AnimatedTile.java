package game.world;

import engine.animation.TextureAnimation;
import engine.event.listener.UpdateListener;

/**
 * The Class AnimatedTile.
 */
public abstract class AnimatedTile extends Tile implements UpdateListener
{

	/** The anim. */
	private TextureAnimation anim;

	/**
	 * Instantiates a new animated tile.
	 *
	 * @param anim the anim
	 * @param collidable the collidable
	 */
	public AnimatedTile(TextureAnimation anim, boolean collidable)
	{
		super(anim.getFrames().get(0), collidable);
		this.anim = anim;
	}

	/**
	 * Gets the anim.
	 *
	 * @return the anim
	 */
	public TextureAnimation getAnim()
	{
		return anim;
	}

	/**
	 * Sets the anim.
	 *
	 * @param anim the new anim
	 */
	public void setAnim(TextureAnimation anim)
	{
		this.anim = anim;
		setTexture(anim.getCurrentFrame());
	}

	@Override
	public void update(float delta)
	{
		anim.update();
		setTexture(anim.getCurrentFrame());
	}

}
