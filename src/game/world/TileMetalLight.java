package game.world;

import engine.graphics.texture.TextureRegion;
import game.Resources;

/**
 * The Class TileMetalLight.
 */
public class TileMetalLight extends Tile
{

	/**
	 * Instantiates a new tile metal light.
	 */
	public TileMetalLight()
	{
		super(TextureRegion.getTextureRegion(Resources.TEXTURE_METAL_LIGHT), false);
	}

}
