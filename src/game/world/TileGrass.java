package game.world;

import engine.graphics.texture.TextureRegion;
import game.Resources;

/**
 * The Class TileGrass.
 */
public class TileGrass extends Tile
{

	/**
	 * Instantiates a new tile grass.
	 */
	public TileGrass()
	{
		super(TextureRegion.getTextureRegion(Resources.TEXTURE_GRASS), false);
	}

}
