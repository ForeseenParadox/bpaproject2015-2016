package game.world.generator;

import game.world.World;

/**
 * The Class WorldGenerator.
 */
public abstract class WorldGenerator
{
	
	/**
	 * Generate.
	 *
	 * @param name the name
	 * @return the world
	 */
	public abstract World generate(String name);
	
}