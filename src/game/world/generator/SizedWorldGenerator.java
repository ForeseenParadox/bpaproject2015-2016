package game.world.generator;

import game.world.World;

/**
 * The Class SizedWorldGenerator.
 */
public abstract class SizedWorldGenerator
{

	/**
	 * Generate.
	 *
	 * @param name the name
	 * @param width the width
	 * @param height the height
	 * @return the world
	 */
	public abstract World generate(String name, int width, int height);

}
