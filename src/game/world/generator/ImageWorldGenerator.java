package game.world.generator;

import engine.graphics.texture.TextureData;
import game.world.TileGlowing;
import game.world.TileMetalDark;
import game.world.TileMetalLight;
import game.world.TileSewage;
import game.world.TileSewage.SewageType;
import game.world.World;

/**
 * The Class ImageWorldGenerator.
 */
public class ImageWorldGenerator extends WorldGenerator
{

	/** The world data. */
	private TextureData worldData;

	/**
	 * Instantiates a new image world generator.
	 *
	 * @param data the data
	 */
	public ImageWorldGenerator(TextureData data)
	{
		this.worldData = data;
	}

	/**
	 * Gets the world data.
	 *
	 * @return the world data
	 */
	public TextureData getWorldData()
	{
		return worldData;
	}

	/**
	 * Sets the world data.
	 *
	 * @param worldData the new world data
	 */
	public void setWorldData(TextureData worldData)
	{
		this.worldData = worldData;
	}

	public World generate(String name)
	{
		World result = new World(name, worldData.getWidth(), worldData.getHeight());

		for (int x = 0; x < result.getWidth(); x++)
		{
			for (int y = 0; y < result.getHeight(); y++)
			{
				int pixel = worldData.getData()[(result.getHeight() - y - 1) * result.getWidth() + x];

				if (pixel == 0xFF808080)
				{
					result.getLayer().setTile(x, y, new TileMetalLight());
				} else if (pixel == 0xFF008000)
				{
					result.getLayer().setTile(x, y, new TileGlowing());
				} else if (pixel == 0xFFFFFF00)
				{
					result.getLayer().setTile(x, y, new TileSewage(SewageType.REG));
				} else if (pixel == 0xFF000000)
				{
					result.getLayer().setTile(x, y, new TileMetalDark());
				} else if (pixel == 0xFF404040)
				{
					if (Math.random() > 0.5)
						result.getLayer().setTile(x, y, new TileSewage(SewageType.TOP));
					else
						result.getLayer().setTile(x, y, new TileSewage(SewageType.PIPE));
				}

			}
		}

		return result;
	}

}
