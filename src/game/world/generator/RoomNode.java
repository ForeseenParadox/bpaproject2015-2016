package game.world.generator;

import java.awt.Rectangle;
import java.awt.geom.Rectangle2D;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import engine.util.MathUtils;
import engine.util.math.Vector2f;

/**
 * The Class RoomNode.
 */
public class RoomNode
{

	/** The h. */
	public int x, y, w, h;
	
	/** The left. */
	private RoomNode left;
	
	/** The right. */
	private RoomNode right;
	
	/** The room. */
	private Rectangle2D room;
	
	/** The halls. */
	private List<Rectangle2D> halls;
	
	/** The min leaf size. */
	private int minLeafSize;
	
	/** The hall size. */
	private int hallSize;
	
	/** The min room size. */
	private int minRoomSize;

	/**
	 * Instantiates a new room node.
	 *
	 * @param x the x
	 * @param y the y
	 * @param w the w
	 * @param h the h
	 * @param minLeafSize the min leaf size
	 * @param hallSize the hall size
	 * @param minRoomSize the min room size
	 */
	public RoomNode(int x, int y, int w, int h, int minLeafSize, int hallSize, int minRoomSize)
	{
		this.x = x;
		this.y = y;
		this.w = w;
		this.h = h;
		this.minLeafSize = minLeafSize;
		this.hallSize = hallSize;
		this.minRoomSize = minRoomSize;
	}

	/**
	 * Gets the left.
	 *
	 * @return the left
	 */
	public RoomNode getLeft()
	{
		return left;
	}

	/**
	 * Gets the right.
	 *
	 * @return the right
	 */
	public RoomNode getRight()
	{
		return right;
	}

	/**
	 * Gets the room.
	 *
	 * @return the room
	 */
	public Rectangle2D getRoom()
	{
		return room;
	}

	/**
	 * Gets the halls.
	 *
	 * @return the halls
	 */
	public List<Rectangle2D> getHalls()
	{
		return halls;
	}

	/**
	 * Gets the min leaf size.
	 *
	 * @return the min leaf size
	 */
	public int getMinLeafSize()
	{
		return minLeafSize;
	}

	/**
	 * Gets the hall size.
	 *
	 * @return the hall size
	 */
	public int getHallSize()
	{
		return hallSize;
	}

	/**
	 * Gets the min room size.
	 *
	 * @return the min room size
	 */
	public int getMinRoomSize()
	{
		return minRoomSize;
	}

	/**
	 * Sets the left.
	 *
	 * @param left the new left
	 */
	public void setLeft(RoomNode left)
	{
		this.left = left;
	}

	/**
	 * Sets the right.
	 *
	 * @param right the new right
	 */
	public void setRight(RoomNode right)
	{
		this.right = right;
	}

	/**
	 * Sets the room.
	 *
	 * @param room the new room
	 */
	public void setRoom(Rectangle room)
	{
		this.room = room;
	}

	/**
	 * Sets the min leaf size.
	 *
	 * @param minLeafSize the new min leaf size
	 */
	public void setMinLeafSize(int minLeafSize)
	{
		this.minLeafSize = minLeafSize;
	}

	/**
	 * Sets the hall size.
	 *
	 * @param hallSize the new hall size
	 */
	public void setHallSize(int hallSize)
	{
		this.hallSize = hallSize;
	}

	/**
	 * Sets the min room size.
	 *
	 * @param minRoomSize the new min room size
	 */
	public void setMinRoomSize(int minRoomSize)
	{
		this.minRoomSize = minRoomSize;
	}

	/**
	 * Split.
	 *
	 * @return true, if successful
	 */
	public boolean split()
	{
		// begin splitting the leaf into two children
		if (left != null || right != null)
			return false; // we're already split! Abort!

		// determine direction of split
		// if the width is >25% larger than height, we split vertically
		// if the height is >25% larger than the width, we split horizontally
		// otherwise we split randomly
		boolean splitH = Math.random() > 0.5;
		if (w / h >= 1.25)
			splitH = false;
		else if (h / w >= 1.25)
			splitH = true;

		int max = (splitH ? h : w) - minLeafSize; // determine the maximum
													// height or width
		if (max <= minLeafSize)
			return false; // the area is too small to split any more...

		int split = (int) MathUtils.randomFloat(minLeafSize, max);// determine
		// where
		// we're
		// going
		// to
		// split

		// create our left and right children based on the direction of the
		// split
		if (splitH)
		{
			left = new RoomNode(x, y, w, split, minLeafSize, hallSize, minRoomSize);
			right = new RoomNode(x, y + split, w, h - split, minLeafSize, hallSize, minRoomSize);
		} else
		{
			left = new RoomNode(x, y, split, h, minLeafSize, hallSize, minRoomSize);
			right = new RoomNode(x + split, y, w - split, h, minLeafSize, hallSize, minRoomSize);
		}
		return true; // split successful!
	}

	/**
	 * Creates the rooms.
	 */
	public void createRooms()
	{
		if (left != null || right != null)
		{
			// this leaf has been split, so go into the children leafs
			if (left != null)
				left.createRooms();
			if (right != null)
				right.createRooms();

			if (left != null && right != null)
			{
				createHall(left.findRoom(), right.findRoom());
			}
		} else
		{
			Vector2f roomSize = new Vector2f(MathUtils.randomFloat(minRoomSize, w - 2), MathUtils.randomFloat(minRoomSize, h - 2));
			Vector2f roomPos = new Vector2f(MathUtils.randomFloat(1, w - roomSize.getX() - 1), MathUtils.randomFloat(1, h - roomSize.getY() - 1));
			room = new Rectangle2D.Float(x + roomPos.getX(), y + roomPos.getY(), roomSize.getX(), roomSize.getY());
		}
	}

	/**
	 * Find room.
	 *
	 * @return the rectangle2 d
	 */
	public Rectangle2D findRoom()
	{
		if (room != null)
			return room;
		else
		{
			Rectangle2D lRoom = null, rRoom = null;

			if (left != null)
				lRoom = left.findRoom();
			if (right != null)
				rRoom = right.findRoom();

			if (lRoom == null && rRoom == null)
				return null;
			else if (rRoom == null)
				return lRoom;
			else if (lRoom == null)
				return rRoom;
			else if (Math.random() > .5)
				return lRoom;
			else
				return rRoom;
		}
	}

	/**
	 * Creates the hall.
	 *
	 * @param l the l
	 * @param r the r
	 */
	public void createHall(Rectangle2D l, Rectangle2D r)
	{
		halls = new LinkedList<Rectangle2D>();

		Vector2f point1 = new Vector2f(MathUtils.randomFloat((float) (l.getX() + 1), (float) (l.getX() + l.getWidth() - 2)), MathUtils.randomFloat((float) (l.getY() + 1), (float) (l.getY() + l.getHeight() - 2)));
		Vector2f point2 = new Vector2f(MathUtils.randomFloat((float) (r.getX() + 1), (float) (r.getX() + r.getWidth() - 2)), MathUtils.randomFloat((float) (r.getY() + 1), (float) (r.getY() + r.getHeight() - 2)));

		float w = point2.getX() - point1.getX(), h = point2.getY() - point1.getY();

		if (w < 0)
		{
			if (h < 0)
			{
				if (Math.random() < 0.5)
				{
					halls.add(new Rectangle2D.Float(point2.getX(), point1.getY(), Math.abs(w), hallSize));
					halls.add(new Rectangle2D.Float(point2.getX(), point2.getY(), hallSize, Math.abs(h)));
				} else
				{
					halls.add(new Rectangle2D.Float(point2.getX(), point2.getY(), Math.abs(w), hallSize));
					halls.add(new Rectangle2D.Float(point1.getX(), point2.getY(), hallSize, Math.abs(h)));
				}
			} else if (h > 0)
			{
				if (Math.random() < 0.5)
				{
					halls.add(new Rectangle2D.Float(point2.getX(), point1.getY(), Math.abs(w), hallSize));
					halls.add(new Rectangle2D.Float(point2.getX(), point1.getY(), hallSize, Math.abs(h)));
				} else
				{
					halls.add(new Rectangle2D.Float(point2.getX(), point2.getY(), Math.abs(w), hallSize));
					halls.add(new Rectangle2D.Float(point1.getX(), point1.getY(), hallSize, Math.abs(h)));
				}
			} else // if (h == 0)
			{
				halls.add(new Rectangle2D.Float(point2.getX(), point2.getY(), Math.abs(w), 1));
			}
		} else if (w > 0)
		{
			if (h < 0)
			{
				if (Math.random() < 0.5)
				{
					halls.add(new Rectangle2D.Float(point1.getX(), point2.getY(), Math.abs(w), hallSize));
					halls.add(new Rectangle2D.Float(point1.getX(), point2.getY(), hallSize, Math.abs(h)));
				} else
				{
					halls.add(new Rectangle2D.Float(point1.getX(), point1.getY(), Math.abs(w), hallSize));
					halls.add(new Rectangle2D.Float(point2.getX(), point2.getY(), hallSize, Math.abs(h)));
				}
			} else if (h > 0)
			{
				if (Math.random() < 0.5)
				{
					halls.add(new Rectangle2D.Float(point1.getX(), point1.getY(), Math.abs(w), hallSize));
					halls.add(new Rectangle2D.Float(point2.getX(), point1.getY(), hallSize, Math.abs(h)));
				} else
				{
					halls.add(new Rectangle2D.Float(point1.getX(), point2.getY(), Math.abs(w), hallSize));
					halls.add(new Rectangle2D.Float(point1.getX(), point1.getY(), hallSize, Math.abs(h)));
				}
			} else // if (h == 0)
			{
				halls.add(new Rectangle2D.Float(point1.getX(), point1.getY(), Math.abs(w), hallSize));
			}
		} else // if (w == 0)
		{
			if (h < 0)
			{
				halls.add(new Rectangle2D.Float(point2.getX(), point2.getY(), hallSize, Math.abs(h)));
			} else if (h > 0)
			{
				halls.add(new Rectangle2D.Float(point1.getX(), point1.getY(), hallSize, Math.abs(h)));
			}
		}

	}

}