package game.world.generator;

import game.world.World;

/**
 * The Class LightedWorldGenerator.
 */
public class LightedWorldGenerator extends DefaultWorldGenerator
{

	@Override
	public World generate(String name, int width, int height)
	{
		World wor = super.generate(name, width, height);

		// float chance = 0.001f;
		// for (int x = 0; x < width; x++)
		// {
		// for (int y = 0; y < height; y++)
		// {
		// if (Math.random() < chance && !(wor.getLayers()[0].getTile(x, y)
		// instanceof TileMetalLight))
		// {
		// wor.getEntities().add(new LightEntity(x * Tile.SIZE + Tile.SIZE / 2,
		// y * Tile.SIZE + Tile.SIZE / 2));
		// }
		// }
		// }

		return wor;
	}

}
