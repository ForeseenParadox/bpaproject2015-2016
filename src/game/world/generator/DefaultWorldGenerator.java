package game.world.generator;

import java.awt.geom.Rectangle2D;
import java.util.Stack;

import game.world.TileGlowing;
import game.world.TileLayer;
import game.world.TileMetalDark;
import game.world.TileMetalLight;
import game.world.World;

/**
 * The Class DefaultWorldGenerator.
 */
public class DefaultWorldGenerator extends SizedWorldGenerator
{

	/** The parent. */
	private RoomNode parent;
	
	/** The max leaf size. */
	private int maxLeafSize;
	
	/** The min leaf size. */
	private int minLeafSize;
	
	/** The hall size. */
	private int hallSize;
	
	/** The min room size. */
	private int minRoomSize;

	/**
	 * Instantiates a new default world generator.
	 */
	public DefaultWorldGenerator()
	{
		this(15, 10, 2, 5);
	}

	/**
	 * Instantiates a new default world generator.
	 *
	 * @param maxLeafSize the max leaf size
	 * @param minLeafSize the min leaf size
	 * @param hallSize the hall size
	 * @param minRoomSize the min room size
	 */
	public DefaultWorldGenerator(int maxLeafSize, int minLeafSize, int hallSize, int minRoomSize)
	{
		this.maxLeafSize = maxLeafSize;
		this.minLeafSize = minLeafSize;
		this.hallSize = hallSize;
		this.minRoomSize = minRoomSize;
	}

	/**
	 * Gets the parent.
	 *
	 * @return the parent
	 */
	public RoomNode getParent()
	{
		return parent;
	}

	/**
	 * Gets the max leaf size.
	 *
	 * @return the max leaf size
	 */
	public int getMaxLeafSize()
	{
		return maxLeafSize;
	}

	/**
	 * Gets the min leaf size.
	 *
	 * @return the min leaf size
	 */
	public int getMinLeafSize()
	{
		return minLeafSize;
	}

	/**
	 * Gets the hall size.
	 *
	 * @return the hall size
	 */
	public int getHallSize()
	{
		return hallSize;
	}

	/**
	 * Gets the min room size.
	 *
	 * @return the min room size
	 */
	public int getMinRoomSize()
	{
		return minRoomSize;
	}

	/**
	 * Sets the max leaf size.
	 *
	 * @param maxLeafSize the new max leaf size
	 */
	public void setMaxLeafSize(int maxLeafSize)
	{
		this.maxLeafSize = maxLeafSize;
	}

	/**
	 * Sets the min leaf size.
	 *
	 * @param minLeafSize the new min leaf size
	 */
	public void setMinLeafSize(int minLeafSize)
	{
		this.minLeafSize = minLeafSize;
	}

	/**
	 * Sets the hall size.
	 *
	 * @param hallSize the new hall size
	 */
	public void setHallSize(int hallSize)
	{
		this.hallSize = hallSize;
	}

	/**
	 * Sets the min room size.
	 *
	 * @param minRoomSize the new min room size
	 */
	public void setMinRoomSize(int minRoomSize)
	{
		this.minRoomSize = minRoomSize;
	}

	@Override
	public World generate(String name, int width, int height)
	{
		World result = new World(name, width, height);

		for (int x = 0; x < width; x++)
			for (int y = 0; y < height; y++)
				result.getLayer().setTile(x, y, new TileMetalDark());

		parent = new RoomNode(0, 0, width - 1, height - 1, minLeafSize, hallSize, minRoomSize);
		bsp(parent);

		parent.createRooms();

		fillRooms(result, parent);
		fillBossRoom(result, parent);

		return result;
	}

	/**
	 * Bsp.
	 *
	 * @param parent the parent
	 */
	public void bsp(RoomNode parent)
	{
		Stack<RoomNode> leafs = new Stack<RoomNode>();
		leafs.push(parent);

		boolean didSplit = true;
		while (didSplit)
		{
			didSplit = false;
			while (!leafs.isEmpty())
			{
				RoomNode leaf = leafs.pop();
				if (leaf.getLeft() == null && leaf.getRight() == null)
				{
					if (leaf.w > maxLeafSize || leaf.h > maxLeafSize || Math.random() > 0.25)
					{
						if (leaf.split())
						{
							leafs.push(leaf.getLeft());
							leafs.push(leaf.getRight());
							didSplit = true;
						}
					}
				}
			}
		}
	}

	/**
	 * Fill rooms.
	 *
	 * @param world the world
	 * @param parent the parent
	 */
	public void fillRooms(World world, RoomNode parent)
	{
		if (parent.getHalls() != null)
			for (Rectangle2D hall : parent.getHalls())
			{
				for (int x = (int) hall.getX(); x <= (int) hall.getX() + (int) hall.getWidth(); x++)
				{
					for (int y = (int) hall.getY(); y <= (int) hall.getY() + (int) hall.getHeight(); y++)
						world.getLayer().setTile(x, y, new TileMetalLight());
				}
			}
		if (parent.getLeft() == null && parent.getRight() == null)
		{
			for (int x = (int) parent.getRoom().getX(); x <= (int) parent.getRoom().getX() + (int) parent.getRoom().getWidth(); x++)
			{
				for (int y = (int) parent.getRoom().getY(); y <= (int) parent.getRoom().getY() + (int) parent.getRoom().getHeight(); y++)
					world.getLayer().setTile(x, y, new TileMetalLight());
			}

		} else
		{
			fillRooms(world, parent.getLeft());
			fillRooms(world, parent.getRight());
		}
	}

	/**
	 * Fill boss room.
	 *
	 * @param world the world
	 * @param parent the parent
	 */
	public void fillBossRoom(World world, RoomNode parent)
	{
		while (parent.getRight() != null)
			parent = parent.getRight();
		for (int x = (int) parent.getRoom().getX(); x <= (int) parent.getRoom().getX() + (int) parent.getRoom().getWidth(); x++)
		{
			for (int y = (int) parent.getRoom().getY(); y <= (int) parent.getRoom().getY() + (int) parent.getRoom().getHeight(); y++)
				world.getLayer().setTile(x, y, new TileGlowing());
		}

	}

}