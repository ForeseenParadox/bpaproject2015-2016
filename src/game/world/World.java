package game.world;

import java.util.ArrayList;
import java.util.List;

import engine.core.Engine;
import engine.event.listener.RenderListener;
import engine.graphics.OrthographicCamera;
import engine.graphics.batch.ShapeBatch;
import engine.graphics.batch.SpriteBatch;
import engine.graphics.texture.Texture;
import engine.graphics.texture.TextureData;
import engine.physics.BoundingBox;
import engine.util.math.Matrix4f;
import engine.util.math.Vector2f;
import game.Game;
import game.entity.Entity;
import game.entity.PointEntity;
import game.entity.SizedEntity;
import game.entity.def.MobDef;
import game.entity.mobs.HoveringBox;
import game.entity.mobs.MobileEntity;
import game.entity.mobs.Player;
import game.entity.mobs.Slime;

/**
 * The Class World.
 */
public class World
{

	/** The Constant MAX_TILE_LAYERS. */
	public static final int MAX_TILE_LAYERS = 8;

	/** The name. */
	private String name;
	
	/** The width. */
	private int width;
	
	/** The height. */
	private int height;
	
	/** The layer. */
	private TileLayer layer;
	
	/** The entities. */
	private List<Entity> entities;

	/** The player. */
	private Player player;

	/** The camera. */
	private OrthographicCamera camera;

	/** The translation. */
	private Vector2f translation;
	
	/** The scale. */
	private Vector2f scale;

	/**
	 * Instantiates a new world.
	 *
	 * @param name the name
	 * @param width the width
	 * @param height the height
	 */
	public World(String name, int width, int height)
	{
		this.name = name;
		this.width = width;
		this.height = height;

		layer = new TileLayer("Background", width, height, false);
		entities = new ArrayList<Entity>();

		camera = new OrthographicCamera(Engine.getInstance().getWindow().getWidth(), Engine.getInstance().getWindow().getHeight());
		translation = new Vector2f();
		scale = new Vector2f(1, 1);
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * Gets the width.
	 *
	 * @return the width
	 */
	public int getWidth()
	{
		return width;
	}

	/**
	 * Gets the height.
	 *
	 * @return the height
	 */
	public int getHeight()
	{
		return height;
	}

	/**
	 * Gets the layer.
	 *
	 * @return the layer
	 */
	public TileLayer getLayer()
	{
		return layer;
	}

	/**
	 * Gets the entities.
	 *
	 * @return the entities
	 */
	public List<Entity> getEntities()
	{
		return entities;
	}

	/**
	 * Gets the player.
	 *
	 * @return the player
	 */
	public Player getPlayer()
	{
		return player;
	}

	/**
	 * Gets the translation.
	 *
	 * @return the translation
	 */
	public Vector2f getTranslation()
	{
		return translation;
	}

	/**
	 * Gets the scale.
	 *
	 * @return the scale
	 */
	public Vector2f getScale()
	{
		return scale;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name)
	{
		this.name = name;
	}

	/**
	 * Sets the player.
	 *
	 * @param player the new player
	 */
	public void setPlayer(Player player)
	{
		this.player = player;
	}

	/**
	 * Sets the translation.
	 *
	 * @param translation the new translation
	 */
	public void setTranslation(Vector2f translation)
	{
		this.translation = translation;
	}

	/**
	 * Sets the scale.
	 *
	 * @param scale the new scale
	 */
	public void setScale(Vector2f scale)
	{
		this.scale = scale;
	}

	/**
	 * Spawn entity.
	 *
	 * @param e the e
	 */
	public void spawnEntity(Entity e)
	{
		entities.add(e);
		e.invokeSpawnEvent();
	}

	/**
	 * Center camera on entity.
	 *
	 * @param e the e
	 */
	public void centerCameraOnEntity(Entity e)
	{
		if (e instanceof SizedEntity)
		{
			SizedEntity se = (SizedEntity) e;
			translation.setX(-se.getBounds().getPosition().getX() - se.getScaledWidth() / 2 + Engine.getInstance().getWindow().getWidth() / 2);
			translation.setY(-se.getBounds().getPosition().getY() - se.getScaledHeight() / 2 + Engine.getInstance().getWindow().getHeight() / 2);
		} else if (e instanceof PointEntity)
		{
			PointEntity pe = (PointEntity) e;
			translation.setX(-pe.getX() + Engine.getInstance().getWindow().getWidth() / 2);
			translation.setY(-pe.getY() + Engine.getInstance().getWindow().getHeight() / 2);

		}
	}

	/**
	 * Update.
	 *
	 * @param delta the delta
	 */
	public void update(float delta)
	{
		for (int i = 0; i < entities.size(); i++)
			entities.get(i).update(delta);
		Game.getInstance().getLightBatch().getBoxOccluders().clear();
		for (int x = 0; x < getWidth(); x++)
		{
			for (int y = 0; y < getHeight(); y++)
			{
				if (layer.getTile(x, y) != null)
				{
					if (layer.getTile(x, y) instanceof AnimatedTile)
						((AnimatedTile) layer.getTile(x, y)).update(delta);
					if (layer.getTile(x, y).isCollidable())
						Game.getInstance().getLightBatch().getBoxOccluders().add(new BoundingBox(new Vector2f(x * Tile.SIZE, y * Tile.SIZE), Tile.SIZE, Tile.SIZE));
				}
			}
		}
	}

	/**
	 * Checks if is point on collidable tile.
	 *
	 * @param x the x
	 * @param y the y
	 * @return true, if is point on collidable tile
	 */
	public boolean isPointOnCollidableTile(float x, float y)
	{
		boolean pointOnTile = false;

		if (x < 0 || y < 0 || x >= (width * Tile.SIZE) || y >= (height * Tile.SIZE))
			return true;

		Tile t = layer.getTile((int) (x / Tile.SIZE), (int) (y / Tile.SIZE));
		if (t == null || t.isCollidable())
			pointOnTile = true;

		return pointOnTile;
	}

	/**
	 * Render.
	 *
	 * @param spriteBatch the sprite batch
	 * @param shapeBatch the shape batch
	 */
	public void render(SpriteBatch spriteBatch, ShapeBatch shapeBatch)
	{

		Matrix4f viewBefore = spriteBatch.getViewMatrix();
		Matrix4f projectionBefore = spriteBatch.getProjectionMatrix();

		camera.setTranslation(translation);
		camera.setRotation(0);
		camera.setScale(scale);

		spriteBatch.setViewMatrix(camera.getView());
		spriteBatch.setProjectionMatrix(camera.getProjection());
		shapeBatch.setViewMatrix(camera.getView());
		shapeBatch.setProjectionMatrix(camera.getProjection());

		layer.render(spriteBatch, translation);

		for (Entity e : entities)
		{
			if (e instanceof RenderListener)
				((RenderListener) e).render();
			else if (e instanceof SizedEntity)
				((SizedEntity) e).render(spriteBatch, shapeBatch);
		}

		Game.getInstance().getLightBatch().renderAllLights(shapeBatch, getTranslation());

		for (Entity e : getEntities())
			if (e instanceof MobileEntity)
				((MobileEntity) e).renderGUI(shapeBatch);

		spriteBatch.flush();
		shapeBatch.flush();

		spriteBatch.setViewMatrix(viewBefore);
		spriteBatch.setProjectionMatrix(projectionBefore);
		shapeBatch.setViewMatrix(viewBefore);
		spriteBatch.setProjectionMatrix(projectionBefore);

	}

	/**
	 * Generate mini map.
	 *
	 * @return the texture
	 */
	public Texture generateMiniMap()
	{
		int[] data = new int[width * height];
		for (int x = 0; x < width; x++)
		{
			for (int y = 0; y < height; y++)
			{
				{
					Tile t = layer.getTile(x, height - y - 1);

					if (t.isCollidable())
					{
						data[x + y * width] = 0x00000000;
					} else
					{
						data[x + y * width] = 0xFF999999;
					}
				}
			}
		}
		return new Texture(new TextureData(width, height, data));
	}

	/**
	 * Spawn mobs.
	 *
	 * @param count the count
	 * @param minDist the min dist
	 */
	public void spawnMobs(int count, int minDist)
	{
		for (int i = 0; i < count; i++)
		{
			int randMob = (int) (Math.random() * 2);

			Vector2f loc = generateValidTileLocation(2, 2).multiply(Tile.SIZE);
			// int randX = (int) (Math.random() * width);
			// int randY = (int) (Math.random() * height);
			// while (layer.getTile(randX, randY) == null ||
			// layer.getTile(randX, randY).isCollidable() || player.dist(randX,
			// randY) < minDist)
			// {
			// randX = (int) (Math.random() * width);
			// randY = (int) (Math.random() * height);
			// }

			MobileEntity mob = null;
			if (randMob == 0)
			{
				mob = new Slime((MobDef) Entity.getEntityDef(Entity.SLIME), this, loc);
			} else if (randMob == 1)
			{
				mob = new HoveringBox((MobDef) Entity.getEntityDef(Entity.HOVERING_BOX), this, loc);
			}
			spawnEntity(mob);
		}
	}

	/**
	 * Checks if is area collidable.
	 *
	 * @param x the x
	 * @param y the y
	 * @param width the width
	 * @param height the height
	 * @return true, if is area collidable
	 */
	public boolean isAreaCollidable(int x, int y, int width, int height)
	{
		boolean collidable = false;
		for (int xx = x; xx <= x + width; xx++)
		{
			for (int yy = y; yy <= y + height; yy++)
			{
				if (isPointOnCollidableTile(xx, yy))
					collidable = true;
			}
		}
		return collidable;
	}

	/**
	 * Generate valid tile location.
	 *
	 * @param tileWidth the tile width
	 * @param tileHeight the tile height
	 * @return the vector2f
	 */
	public Vector2f generateValidTileLocation(int tileWidth, int tileHeight)
	{
		int randX = (int) (Math.random() * width);
		int randY = (int) (Math.random() * height);
		while (isAreaCollidable(randX * Tile.SIZE, randY * Tile.SIZE, tileWidth * Tile.SIZE, tileHeight * Tile.SIZE))
		{
			randX = (int) (Math.random() * width);
			randY = (int) (Math.random() * height);
		}

		return new Vector2f(randX, randY);
	}

}