package game.world;

import engine.animation.TextureAnimation;
import game.Resources;

/**
 * The Class TileGlowing.
 */
public class TileGlowing extends AnimatedTile
{

	/**
	 * Instantiates a new tile glowing.
	 */
	public TileGlowing()
	{
		super(TextureAnimation.getTextureAnimation(Resources.ANIM_GLOWING), false);
	}

}
