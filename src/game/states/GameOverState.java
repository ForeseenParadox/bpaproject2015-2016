package game.states;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

import engine.core.Engine;
import engine.core.ExitReason;
import engine.event.listener.ComponentAdapter;
import engine.graphics.Color;
import engine.graphics.batch.ShapeBatch;
import engine.graphics.batch.SpriteBatch;
import engine.graphics.font.TrueTypeFont;
import engine.graphics.gui.Component;
import engine.graphics.gui.FadingBasicButton;
import engine.state.GameState;
import game.Game;

/**
 * The Class GameOverState.
 */
public class GameOverState extends GameState
{

	/** The back to menu button. */
	private FadingBasicButton backToMenuButton;
	
	/** The times. */
	private List<String> times;
	
	/** The total time. */
	private String totalTime;
	
	/** The won. */
	private boolean won;

	/**
	 * Instantiates a new game over state.
	 *
	 * @param id the id
	 */
	public GameOverState(String id)
	{
		super(id);
	}

	/**
	 * Gets the times.
	 *
	 * @return the times
	 */
	public List<String> getTimes()
	{
		return times;
	}

	/**
	 * Gets the total time.
	 *
	 * @return the total time
	 */
	public String getTotalTime()
	{
		return totalTime;
	}

	/**
	 * Checks if is won.
	 *
	 * @return true, if is won
	 */
	public boolean isWon()
	{
		return won;
	}

	/**
	 * Sets the times.
	 *
	 * @param times the new times
	 */
	public void setTimes(List<String> times)
	{
		this.times = times;
	}

	/**
	 * Sets the total time.
	 *
	 * @param totalTime the new total time
	 */
	public void setTotalTime(String totalTime)
	{
		this.totalTime = totalTime;
	}

	/**
	 * Sets the won.
	 *
	 * @param won the new won
	 */
	public void setWon(boolean won)
	{
		this.won = won;
	}

	@Override
	public void update(float delta)
	{
		backToMenuButton.update(delta);
	}

	@Override
	public void resize(int w, int h)
	{

	}

	@Override
	public void render()
	{
		TrueTypeFont font = Game.getInstance().getTrebuchet20();
		ShapeBatch shapeBatch = Game.getInstance().getShapeBatch();
		SpriteBatch spriteBatch = Game.getInstance().getSpriteBatch();

		shapeBatch.begin();
		spriteBatch.begin();

		String totalTimeString = "Total Time: " + totalTime;
		String successString = "Success: " + won;

		for (int i = 0; i < times.size(); i++)
		{
			String timeString = "Level " + i + " time: " + times.get(i);
			font.render(spriteBatch, timeString, Color.WHITE, (Engine.getInstance().getWindow().getWidth() - font.getStringWidth(timeString)) / 2, 300 - i * 20);
		}
		font.render(spriteBatch, totalTimeString, Color.WHITE, (Engine.getInstance().getWindow().getWidth() - font.getStringWidth(totalTimeString)) / 2, 300 - times.size() * 20);

		backToMenuButton.render(spriteBatch, shapeBatch);

		spriteBatch.end();
		shapeBatch.end();
	}

	/**
	 * Gets the time score.
	 *
	 * @param time the time
	 * @return the time score
	 */
	public int getTimeScore(String time)
	{
		String[] tokens = time.split(":");
		return Integer.parseInt(tokens[0]) * 3600 + Integer.parseInt(tokens[1]) * 60 + Integer.parseInt(tokens[2]);
	}

	@Override
	public void init()
	{
		try
		{
			File highscores = new File("da.ta");
			if (!highscores.exists())
				highscores.createNewFile();

			Map<Integer, String> timeScores = new TreeMap<Integer, String>();

			Scanner scan = new Scanner(highscores);
			while (scan.hasNextLine())
			{
				String text = scan.nextLine();
				timeScores.put(getTimeScore(text), text);
			}

			timeScores.put(getTimeScore(totalTime), totalTime);

			// reset scanner
			scan.close();
			BufferedWriter writer = new BufferedWriter(new FileWriter(highscores));

			Iterator<Integer> itr = timeScores.keySet().iterator();
			for (int i = 0; i < 10 && itr.hasNext(); i++)
			{
				int j = itr.next();
				writer.write(timeScores.get(j) + "\n");
			}

			writer.close();

		} catch (IOException e1)
		{
			e1.printStackTrace();
		}

		backToMenuButton = new FadingBasicButton(null, 10, 10, 200, 20, Color.BLUE, Color.WHITE, "Back To Menu", Game.getInstance().getTrebuchet20());
		backToMenuButton.addComponentListener(new ComponentAdapter()
		{
			@Override
			public void componentClicked(Component source, int button)
			{
				super.componentClicked(source, button);
				Game.getInstance().getStateManager().pushGameState(Game.MENU_STATE);
			}
		});
	}

	@Override
	public void dispose(ExitReason reason)
	{

	}

}
