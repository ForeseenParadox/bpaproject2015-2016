package game.states;

import static org.lwjgl.opengl.GL11.GL_BLEND;
import static org.lwjgl.opengl.GL11.GL_ONE_MINUS_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.glBlendFunc;
import static org.lwjgl.opengl.GL11.glEnable;

import engine.audio.Audio;
import engine.core.Engine;
import engine.core.ExitReason;
import engine.core.SimulationConfig;
import engine.event.listener.ComponentAdapter;
import engine.graphics.Color;
import engine.graphics.batch.ShapeBatch;
import engine.graphics.batch.SpriteBatch;
import engine.graphics.font.TrueTypeFont;
import engine.graphics.gui.BasicSlider;
import engine.graphics.gui.Component;
import engine.graphics.gui.HorizontalCentering;
import engine.graphics.gui.ImageSlider;
import engine.graphics.gui.ImageTextButton;
import engine.graphics.gui.VerticalCentering;
import engine.graphics.texture.Texture;
import engine.graphics.texture.TextureRegion;
import engine.state.GameState;
import game.Game;

/**
 * The Class OptionsState.
 */
public class OptionsState extends GameState
{

	/** The fps cap slider. */
	// Performance sliders
	private ImageSlider fpsCapSlider;

	/** The master volume slider. */
	// Music/sound sliders
	private ImageSlider masterVolumeSlider;
	
	/** The sfx volume slider. */
	private ImageSlider sfxVolumeSlider;
	
	/** The music volume slider. */
	private ImageSlider musicVolumeSlider;

	/** The done button. */
	private ImageTextButton doneButton;

	/** The background. */
	private Texture background;

	/**
	 * Instantiates a new options state.
	 *
	 * @param id the id
	 */
	public OptionsState(String id)
	{
		super(id);
	}

	/**
	 * Gets the int from norm float.
	 *
	 * @param min the min
	 * @param max the max
	 * @param norm the norm
	 * @return the int from norm float
	 */
	public int getIntFromNormFloat(int min, int max, float norm)
	{
		return min + Math.round(((max - min) * norm));
	}

	@Override
	public void update(float delta)
	{
		doneButton.update(delta);
		fpsCapSlider.update(delta);
		masterVolumeSlider.update(delta);
		sfxVolumeSlider.update(delta);
		musicVolumeSlider.update(delta);

		Game.getInstance().getOptions().setMaxFps(getIntFromNormFloat(SimulationConfig.MIN_FPS_CAP, SimulationConfig.MAX_FPS_CAP, fpsCapSlider.getNormalizedSliderPosition()));
		Game.getInstance().getOptions().setMasterVolume(masterVolumeSlider.getNormalizedSliderPosition());
		Game.getInstance().getOptions().setSfxVolume(sfxVolumeSlider.getNormalizedSliderPosition());
		Game.getInstance().getOptions().setMusicVolume(musicVolumeSlider.getNormalizedSliderPosition());

		Engine.getInstance().getSimulation().getConfig().setMaxFps(getIntFromNormFloat(SimulationConfig.MIN_FPS_CAP, SimulationConfig.MAX_FPS_CAP, fpsCapSlider.getNormalizedSliderPosition()));
		Audio.setAmplitudeByType("sfx", Game.getInstance().getOptions().getMasterVolume() * Game.getInstance().getOptions().getSfxVolume());
		Audio.setAmplitudeByType("music", Game.getInstance().getOptions().getMasterVolume() * Game.getInstance().getOptions().getMusicVolume());
	}

	@Override
	public void resize(int w, int h)
	{

	}

	@Override
	public void render()
	{

		ShapeBatch shapeBatch = Game.getInstance().getShapeBatch();
		SpriteBatch spriteBatch = Game.getInstance().getSpriteBatch();

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		spriteBatch.begin();
		shapeBatch.begin();

		spriteBatch.renderTexture(background, 0, 0, Engine.getInstance().getWindow().getWidth(), Engine.getInstance().getWindow().getHeight(), 0, 0, 1, 1, 0, 0, 0, 1, 1);

		doneButton.render(spriteBatch, shapeBatch);

		TrueTypeFont font = Game.getInstance().getTrebuchet20();

		// render fps cap slider
		font.render(spriteBatch, "FPS cap:        " + Engine.getInstance().getSimulation().getConfig().getMaxFps(), Color.WHITE, 120, 500);


		fpsCapSlider.render(spriteBatch, shapeBatch);

		// render volume sliders
		font.render(spriteBatch, "Master Volume:  " + String.format("%.2f", Game.getInstance().getOptions().getMasterVolume()), Color.WHITE, 120, 500 - (BasicSlider.SLIDER_PIECE_HEIGHT + 5));
		font.render(spriteBatch, "SFX Volume:     " + String.format("%.2f", Game.getInstance().getOptions().getSfxVolume()), Color.WHITE, 120, 500 - (BasicSlider.SLIDER_PIECE_HEIGHT + 5) * 2);
		font.render(spriteBatch, "Music Volume:   " + String.format("%.2f", Game.getInstance().getOptions().getMusicVolume()), Color.WHITE, 120, 500 - (BasicSlider.SLIDER_PIECE_HEIGHT + 5) * 3);

		masterVolumeSlider.render(spriteBatch, shapeBatch);
		sfxVolumeSlider.render(spriteBatch, shapeBatch);
		musicVolumeSlider.render(spriteBatch, shapeBatch);

		spriteBatch.end();
		shapeBatch.end();
	}

	@Override
	public void init()
	{
		doneButton = new ImageTextButton(null, 120, 62, TextureRegion.getTextureRegion("Button1"), "Done", Game.getInstance().getTrebuchet20(), HorizontalCentering.MIDDLE, VerticalCentering.MIDDLE);
		doneButton.addComponentListener(new ComponentAdapter()
		{
			@Override
			public void componentClicked(Component source, int button)
			{
				super.componentClicked(source, button);
				if (button == 0)
					Game.getInstance().getStateManager().popGameState();
			}
		});

		TextureRegion sliderPieceTexture = TextureRegion.getTextureRegion("Slider_Piece");

		fpsCapSlider = new ImageSlider(200, 380, 500, Color.WHITE, sliderPieceTexture);
		fpsCapSlider.setNormalizedSliderPosition(((float) Game.getInstance().getOptions().getMaxFps() - SimulationConfig.MIN_FPS_CAP) / (SimulationConfig.MAX_FPS_CAP - SimulationConfig.MIN_FPS_CAP));
		masterVolumeSlider = new ImageSlider(200, 380, 500 - (BasicSlider.SLIDER_PIECE_HEIGHT + 5), Color.WHITE, sliderPieceTexture);
		masterVolumeSlider.setNormalizedSliderPosition(Game.getInstance().getOptions().getMasterVolume());
		sfxVolumeSlider = new ImageSlider(200, 380, 500 - (BasicSlider.SLIDER_PIECE_HEIGHT + 5) * 2, Color.WHITE, sliderPieceTexture);
		sfxVolumeSlider.setNormalizedSliderPosition(Game.getInstance().getOptions().getSfxVolume());
		musicVolumeSlider = new ImageSlider(200, 380, 500 - (BasicSlider.SLIDER_PIECE_HEIGHT + 5) * 3, Color.WHITE, sliderPieceTexture);
		musicVolumeSlider.setNormalizedSliderPosition(Game.getInstance().getOptions().getMusicVolume());

		background = Texture.getTexture("Options-Background");
	}

	@Override
	public void dispose(ExitReason reason)
	{
		Game.getInstance().getOptions().save();
	}

}
