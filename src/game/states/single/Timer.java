package game.states.single;

/**
 * The Class Timer.
 */
public class Timer
{
	
	/** The hours. */
	private int hours;
	
	/** The minutes. */
	private int minutes;
	
	/** The seconds. */
	private int seconds;

	/**
	 * Instantiates a new timer.
	 */
	public Timer()
	{
	}

	/**
	 * Instantiates a new timer.
	 *
	 * @param hours the hours
	 * @param minutes the minutes
	 * @param seconds the seconds
	 */
	public Timer(int hours, int minutes, int seconds)
	{
		this.hours = hours;

		this.minutes = minutes;
		this.seconds = seconds;
	}

	/**
	 * Gets the minutes.
	 *
	 * @return the minutes
	 */
	public int getMinutes()
	{
		return minutes;
	}

	/**
	 * Gets the seconds.
	 *
	 * @return the seconds
	 */
	public int getSeconds()
	{
		return seconds;
	}

	/**
	 * Gets the hours.
	 *
	 * @return the hours
	 */
	public int getHours()
	{
		return hours;
	}

	/**
	 * Sets the hours.
	 *
	 * @param hours the new hours
	 */
	public void setHours(int hours)
	{
		this.hours = hours;
	}

	/**
	 * Sets the minutes.
	 *
	 * @param minutes the new minutes
	 */
	public void setMinutes(int minutes)
	{
		this.minutes = minutes;
	}

	/**
	 * Sets the seconds.
	 *
	 * @param seconds the new seconds
	 */
	public void setSeconds(int seconds)
	{
		this.seconds = seconds;
	}

	/**
	 * Change.
	 *
	 * @param h the h
	 * @param m the m
	 * @param s the s
	 */
	public void change(int h, int m, int s)
	{
		int lowerMinutes = s / 60;
		int lowerHours = m / 60;

		seconds += s % 60;

		if (seconds < 0)
		{
			minutes--;
			seconds = 59;
		} else if (seconds >= 60)
		{
			minutes++;
			seconds = 0;
		}

		minutes += m % 60 + lowerMinutes;

		if (minutes < 0)
		{
			minutes--;
			minutes = 59;
		} else if (minutes >= 60)
		{
			minutes++;
			minutes = 0;
		}

		hours+= h + lowerHours;
	}

	@Override
	public String toString()
	{
		return String.format("%02d:%02d:%02d", hours, minutes, seconds);
	}

}
