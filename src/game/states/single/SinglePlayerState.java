package game.states.single;

import static org.lwjgl.glfw.GLFW.GLFW_KEY_ENTER;
import static org.lwjgl.opengl.GL11.GL_BLEND;
import static org.lwjgl.opengl.GL11.GL_ONE_MINUS_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.glBlendFunc;
import static org.lwjgl.opengl.GL11.glEnable;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import engine.audio.Audio;
import engine.core.Engine;
import engine.core.ExitReason;
import engine.core.task.Task;
import engine.event.listener.ComponentAdapter;
import engine.file.InternalFileHandle;
import engine.graphics.Color;
import engine.graphics.batch.ShapeBatch;
import engine.graphics.batch.SpriteBatch;
import engine.graphics.gui.BasicTextField;
import engine.graphics.gui.Component;
import engine.graphics.gui.ImageTextButton;
import engine.graphics.gui.TextArea;
import engine.graphics.texture.Texture;
import engine.graphics.texture.TextureData;
import engine.graphics.texture.TextureRegion;
import engine.physics.BoundingBox;
import engine.resource.TextureResource;
import engine.state.GameState;
import engine.util.math.Vector2f;
import game.DebugInfo;
import game.Game;
import game.entity.Entity;
import game.entity.SizedEntity;
import game.entity.def.MobDef;
import game.entity.mobs.Player;
import game.states.GameOverState;
import game.world.Tile;
import game.world.TileGlowing;
import game.world.World;
import game.world.generator.DefaultWorldGenerator;
import game.world.generator.ImageWorldGenerator;
import game.world.generator.RoomNode;

/**
 * The Class SinglePlayerState.
 */
public class SinglePlayerState extends GameState
{

	/** The Constant WORLDS_UNTIL_BOSS. */
	public static final int WORLDS_UNTIL_BOSS = 1;

	/** The times. */
	private List<String> times;

	/** The current world. */
	private World currentWorld;

	/** The boss world. */
	private World bossWorld;

	/** The mini map. */
	private Texture miniMap;

	/** The history. */
	private TextArea history;
	
	/** The command box. */
	private BasicTextField commandBox;

	/** The dbg info. */
	private DebugInfo dbgInfo;

	/** The timer. */
	private Timer timer;

	/** The options button. */
	private ImageTextButton optionsButton;

	/** The current world index. */
	private int currentWorldIndex;
	
	/** The world gen. */
	private DefaultWorldGenerator worldGen;

	/**
	 * Instantiates a new single player state.
	 *
	 * @param id the id
	 */
	public SinglePlayerState(String id)
	{
		super(id);

	}

	/**
	 * Execute command.
	 *
	 * @param command the command
	 */
	public void executeCommand(String command)
	{
		String[] args = command.split(" ");

		if (args.length >= 2)
		{
			if (args[0].equalsIgnoreCase("/ghost"))
				currentWorld.getPlayer().setGhost(Boolean.parseBoolean(args[1]));
		} else if (args.length >= 2)
		{
			if (args[0].equalsIgnoreCase("/speed"))
				currentWorld.getPlayer().setSpeed(Integer.parseInt(args[1]));
		}
	}

	/**
	 * Gets the player spawn.
	 *
	 * @return the player spawn
	 */
	public RoomNode getPlayerSpawn()
	{
		RoomNode playerSpawn = worldGen.getParent();
		while (playerSpawn.getLeft() != null)
			playerSpawn = playerSpawn.getLeft();
		return playerSpawn;
	}

	/**
	 * New world.
	 */
	public void newWorld()
	{
		times.add(timer.toString());
		Player player = new Player((MobDef) Entity.getEntityDef(Entity.PLAYER), currentWorld, new Vector2f());

		if (currentWorldIndex >= WORLDS_UNTIL_BOSS)
		{
			currentWorld = bossWorld;
			GameOverState gameOver = (GameOverState) Game.getInstance().getStateManager().getGameState(Game.GAME_OVER_STATE);
			gameOver.setTotalTime(timer.toString());
			gameOver.setTimes(times);
			Game.getInstance().getStateManager().pushGameState(Game.GAME_OVER_STATE);
		} else
		{
			currentWorldIndex++;
			worldGen.setHallSize(3);

			currentWorld = worldGen.generate("World-" + (currentWorldIndex), 50 + currentWorldIndex * 20, 50 + currentWorldIndex * 20);
			currentWorld.spawnMobs(20, 50);
		}
		miniMap = currentWorld.generateMiniMap();

		player.setWorld(currentWorld);
		currentWorld.setPlayer(player);
		currentWorld.spawnEntity(player);

		if (currentWorld == bossWorld)
		{
			player.getBounds().setPosition(new Vector2f((float) 20 * Tile.SIZE, (float) 20 * Tile.SIZE));
		} else
		{
			RoomNode playerSpawn = getPlayerSpawn();
			player.getBounds().setPosition(new Vector2f((float) playerSpawn.getRoom().getCenterX() * Tile.SIZE, (float) playerSpawn.getRoom().getCenterY() * Tile.SIZE));
		}
	}

	@Override
	public void update(float delta)
	{
		currentWorld.update(delta);

		if (Engine.getInstance().getInput().getKeyboard().isKeyJustPressed(GLFW_KEY_ENTER) && !commandBox.isFocused())
		{
			commandBox.setFocused(!commandBox.isFocused());
			commandBox.setHidden(!commandBox.isHidden());
			currentWorld.getPlayer().setControlDisabled(!currentWorld.getPlayer().isControlDisabled());
		}

		if (Engine.getInstance().getInput().getKeyboard().isKeyJustPressed(GLFW_KEY_ENTER) && commandBox.getText() != "")
		{
			String text = commandBox.getText();
			commandBox.setText("");
			history.setText(history.getText() + text + "\n");
			executeCommand(text);
			commandBox.setFocused(!commandBox.isFocused());
			commandBox.setHidden(!commandBox.isHidden());
			currentWorld.getPlayer().setControlDisabled(!currentWorld.getPlayer().isControlDisabled());
		}

		if (!(currentWorld == bossWorld))
		{
			Player player = currentWorld.getPlayer();
			if (currentWorld.getLayer().getTile((int) (player.getX() + player.getScaledWidth() / 2) / Tile.SIZE, (int) (player.getY() + player.getScaledHeight() / 2) / Tile.SIZE) instanceof TileGlowing)
				newWorld();
		}

		dbgInfo.getDebugInfo()[0] = "FPS: " + Engine.getInstance().getSimulation().getFrameTimer().getFps();
		dbgInfo.getDebugInfo()[1] = "UPS: " + Engine.getInstance().getSimulation().getFrameTimer().getUps();
		dbgInfo.getDebugInfo()[2] = "Entities: " + currentWorld.getEntities().size();
		dbgInfo.getDebugInfo()[3] = String.format("X:%.2f, Y:%.2f", currentWorld.getPlayer().getX() / Tile.SIZE, currentWorld.getPlayer().getY() / Tile.SIZE);

		optionsButton.update(delta);
	}

	@Override
	public void resize(int w, int h)
	{
		optionsButton.setRelativeX((Engine.getInstance().getWindow().getWidth() - optionsButton.getWidth()) / 2);
		optionsButton.setRelativeY(Engine.getInstance().getWindow().getHeight() - optionsButton.getHeight());

	}

	@Override
	public void render()
	{
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		ShapeBatch shapeBatch = Game.getInstance().getShapeBatch();
		SpriteBatch spriteBatch = Game.getInstance().getSpriteBatch();

		spriteBatch.getAmbientLight().setIntensity(0.5f);

		spriteBatch.begin();
		shapeBatch.begin();

		currentWorld.render(spriteBatch, shapeBatch);

		spriteBatch.end();
		shapeBatch.end();

		float mapScale = 1f;

		spriteBatch.getAmbientLight().setIntensity(1);

		spriteBatch.begin();
		shapeBatch.begin();

		optionsButton.render(spriteBatch, shapeBatch);

		spriteBatch.renderTexture(miniMap, Engine.getInstance().getWindow().getWidth() - miniMap.getWidth(), Engine.getInstance().getWindow().getHeight() - miniMap.getHeight(), 0, 0, 1, 1, 0, 0, 0, 1, 1);
		spriteBatch.renderTexture(TextureRegion.getTextureRegion("Minimap_Border"), Engine.getInstance().getWindow().getWidth() - miniMap.getWidth(), Engine.getInstance().getWindow().getHeight() - miniMap.getHeight(), miniMap.getWidth(), miniMap.getHeight(), 0, 0, 0, 1, 1);

		// render entities onto the minimap
		for (int i = 0; i < currentWorld.getEntities().size(); i++)
		{
			Entity e = currentWorld.getEntities().get(i);
			if (e instanceof SizedEntity)
			{
				BoundingBox b = ((SizedEntity) e).getBounds();
				shapeBatch.setDrawColor(Color.BLUE);
				float markerSize = 3;
				shapeBatch.renderFilledRect(Engine.getInstance().getWindow().getWidth() - miniMap.getWidth() + b.getPosition().getX() * mapScale / Tile.SIZE - markerSize / 2, Engine.getInstance().getWindow().getHeight() - miniMap.getHeight() + b.getPosition().getY() * mapScale / Tile.SIZE - markerSize / 2, markerSize, markerSize, 0, 0, 0, 1, 1);
			}
		}

		history.render(spriteBatch, shapeBatch);
		commandBox.render(spriteBatch, shapeBatch);

		String timeString = "Time: " + timer.toString();
		Game.getInstance().getTrebuchet20().render(spriteBatch, timeString, Color.WHITE, Engine.getInstance().getWindow().getWidth() - Game.getInstance().getTrebuchet20().getStringWidth(timeString) - 5, 10);
		String worldIndexString = "World: " + currentWorldIndex;
		Game.getInstance().getTrebuchet20().render(spriteBatch, worldIndexString, Color.WHITE, Engine.getInstance().getWindow().getWidth() - Game.getInstance().getTrebuchet20().getStringWidth(timeString) - 5, 30);
		String leveString = "Level: " + currentWorld.getPlayer().getLevel();
		Game.getInstance().getTrebuchet20().render(spriteBatch, leveString, Color.WHITE, Engine.getInstance().getWindow().getWidth() - Game.getInstance().getTrebuchet20().getStringWidth(timeString) - 5, 50);

		dbgInfo.render(spriteBatch, Game.getInstance().getTrebuchet20());

		spriteBatch.end();
		shapeBatch.end();
	}

	@Override
	public void init()
	{
		if (!isInitialized())
		{
			worldGen = new DefaultWorldGenerator();

			currentWorld = worldGen.generate("Test", 50, 50);

			TextureResource bossImage = new TextureResource(new InternalFileHandle("/Textures/LEV_boss.png"));
			bossImage.load();
			bossWorld = new ImageWorldGenerator(new TextureData(bossImage)).generate("Boss World");

			times = new ArrayList<String>();
			timer = new Timer(0, 0, 0);

			newWorld();

			history = new TextArea(null, 10, 50, 200, 400, Game.getInstance().getTrebuchet20());
			commandBox = new BasicTextField(null, 10, 10, 200, 30, Game.getInstance().getTrebuchet20(), Color.GRAY, new Color(0, 0, 0, 0), Color.WHITE);
			commandBox.setFocused(false);
			commandBox.setHidden(true);

			dbgInfo = new DebugInfo(4);

			currentWorldIndex = 1;

			Engine.getInstance().getTaskScheduler().scheduleSyncTask(new Task(1000, new Runnable()
			{
				@Override
				public void run()
				{
					timer.change(0, 0, 1);
				}
			}, true));

			Audio.stopAllMusic();
			Audio.getAudioByName("Theme2").loop(-1);
			Audio.getAudioByName("Theme2").start();

			TextureRegion optionsTexture = TextureRegion.getTextureRegion("Button1");
			optionsButton = new ImageTextButton(null, (int) ((Engine.getInstance().getWindow().getWidth() - optionsTexture.getWidth())) / 2, (int) (Engine.getInstance().getWindow().getHeight() - optionsTexture.getHeight()), optionsTexture, "Options", Game.getInstance().getTrebuchet20());

			optionsButton.addComponentListener(new ComponentAdapter()
			{
				@Override
				public void componentClicked(Component source, int button)
				{
					super.componentPressed(source, button);
					Game.getInstance().getStateManager().pushGameState(Game.OPTIONS_STATE);
				}
			});
		}
	}

	@Override
	public void dispose(ExitReason reason)
	{

	}

}
