package game.states;

import engine.audio.Audio;
import engine.core.Engine;
import engine.core.ExitReason;
import engine.event.listener.ComponentAdapter;
import engine.graphics.Color;
import engine.graphics.OrthographicCamera;
import engine.graphics.batch.ShapeBatch;
import engine.graphics.batch.SpriteBatch;
import engine.graphics.font.TrueTypeFont;
import engine.graphics.gui.BasicButton;
import engine.graphics.gui.Component;
import engine.graphics.gui.FadingBasicButton;
import engine.state.GameState;
import engine.util.math.Vector2f;
import game.Game;
import game.entity.LightEntity;
import game.world.Tile;
import game.world.World;
import game.world.generator.DefaultWorldGenerator;

/**
 * The Class MenuState.
 */
public class MenuState extends GameState
{

	/** The new game button button. */
	private FadingBasicButton newGameButtonButton;
	
	/** The multi player button. */
	private FadingBasicButton multiPlayerButton;
	
	/** The tutorial button. */
	private FadingBasicButton tutorialButton;
	
	/** The options button. */
	private FadingBasicButton optionsButton;
	
	/** The quit button. */
	private FadingBasicButton quitButton;

	/** The background world. */
	private World backgroundWorld;
	
	/** The light ent. */
	private LightEntity lightEnt;

	/** The cam. */
	private OrthographicCamera cam;

	/**
	 * Instantiates a new menu state.
	 *
	 * @param id the id
	 */
	public MenuState(String id)
	{
		super(id);
	}

	@Override
	public void update(float delta)
	{
		newGameButtonButton.update(delta);
		multiPlayerButton.update(delta);
		optionsButton.update(delta);
		tutorialButton.update(delta);
		quitButton.update(delta);

		backgroundWorld.update(delta);
	}

	@Override
	public void resize(int w, int h)
	{

	}

	@Override
	public void render()
	{
		ShapeBatch shapeBatch = Game.getInstance().getShapeBatch();
		SpriteBatch spriteBatch = Game.getInstance().getSpriteBatch();

		spriteBatch.begin();
		shapeBatch.begin();
		spriteBatch.getAmbientLight().setIntensity(0.4f);

		// backgroundWorld.getTranslation().setX(-lightEnt.getX() +
		// Engine.getInstance().getWindow().getWidth() / 2);
		// backgroundWorld.getTranslation().setY(-lightEnt.getY() +
		// Engine.getInstance().getWindow().getHeight() / 2);

		// cam.setTranslation(worldTranslation);
		// Matrix4f view = cam.getView();
		// Matrix4f projection = cam.getProjection();

		// spriteBatch.setViewMatrix(view);
		// spriteBatch.setProjectionMatrix(projection);

		// shapeBatch.setViewMatrix(view);
		// shapeBatch.setProjectionMatrix(projection);

		backgroundWorld.render(spriteBatch, shapeBatch);

		spriteBatch.end();
		shapeBatch.end();

		// RENDER GUI

		shapeBatch.begin();
		spriteBatch.begin();

		// view.setIdentity();
		// spriteBatch.setViewMatrix(view);

		spriteBatch.getAmbientLight().setIntensity(1f);

		newGameButtonButton.render(spriteBatch, shapeBatch);
		multiPlayerButton.render(spriteBatch, shapeBatch);
		optionsButton.render(spriteBatch, shapeBatch);
		tutorialButton.render(spriteBatch, shapeBatch);
		quitButton.render(spriteBatch, shapeBatch);

		shapeBatch.end();
		spriteBatch.end();

	}

	@Override
	public void init()
	{
		// TODO: make gui components editable through xml file

		if (!isInitialized())
		{

			// the default vertical padding of basic buttons
			int vp = BasicButton.DEFAULT_PADDING;
			// the vertical margin between menu buttons
			int vm = 10;
			// the height of a menu button
			int bh = 30;

			TrueTypeFont font = Game.getInstance().getTrebuchet20();
			quitButton = new FadingBasicButton(null, 10, 10, 300, bh, Color.BLUE, Color.WHITE, "Quit", font);
			multiPlayerButton = new FadingBasicButton(null, 10, 10 + 1 * (bh + vp * 2 + vm), 300, bh, Color.BLUE, Color.WHITE, "High Scores", font);
			optionsButton = new FadingBasicButton(null, 10, 10 + 2 * (bh + vp * 2 + vm), 300, bh, Color.BLUE, Color.WHITE, "Options", font);
			tutorialButton = new FadingBasicButton(null, 10, 10 + 3 * (bh + vp * 2 + vm), 300, bh, Color.BLUE, Color.WHITE, "Tutorial", font);
			newGameButtonButton = new FadingBasicButton(null, 10, 10 + 4 * (bh + vp * 2 + vm), 300, bh, Color.BLUE, Color.WHITE, "New Game", font);

			// single player event listener
			newGameButtonButton.addComponentListener(new ComponentAdapter()
			{
				@Override
				public void componentClicked(Component source, int button)
				{
					super.componentClicked(source, button);
					if (button == 0)
						Game.getInstance().getStateManager().pushGameState(Game.SINGLE_PLAYER_STATE);
				}
			});

			tutorialButton.addComponentListener(new ComponentAdapter()
			{
				@Override
				public void componentClicked(Component source, int button)
				{
					super.componentClicked(source, button);
				}
			});

			// quite button event listener
			quitButton.addComponentListener(new ComponentAdapter()
			{
				@Override
				public void componentClicked(Component source, int button)
				{
					super.componentPressed(source, button);

					if (button == 0)
						Engine.getInstance().stopEngine(ExitReason.USER_EXIT);
				}
			});

			// options button event listener
			optionsButton.addComponentListener(new ComponentAdapter()
			{
				@Override
				public void componentClicked(Component source, int button)
				{
					super.componentPressed(source, button);

					if (button == 0)
						Game.getInstance().getStateManager().pushGameState(Game.OPTIONS_STATE);
				}
			});

			DefaultWorldGenerator gen = new DefaultWorldGenerator(15, 10, 4, 8);

			backgroundWorld = gen.generate("Main-Menu", 100, 100);
			// worldScale = new Vector2f(1, 1);

			cam = new OrthographicCamera(Engine.getInstance().getWindow().getWidth(), Engine.getInstance().getWindow().getHeight());

			Vector2f worldLoc = backgroundWorld.generateValidTileLocation(1, 1);
			backgroundWorld.getEntities().add(lightEnt = new LightEntity(backgroundWorld, Color.GREEN, 0.4f, worldLoc.getX() * Tile.SIZE, worldLoc.getY() * Tile.SIZE));

			Audio.getAudioByName("Theme1").loop(-1);
			Audio.getAudioByName("Theme1").start();
		}
	}

	@Override
	public void dispose(ExitReason reason)
	{

	}

}
