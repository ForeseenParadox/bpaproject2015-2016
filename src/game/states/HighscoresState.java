package game.states;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

import engine.core.ExitReason;
import engine.state.GameState;

/**
 * The Class HighscoresState.
 */
public class HighscoresState extends GameState
{

	/** The times. */
	private List<String> times;

	/**
	 * Instantiates a new highscores state.
	 *
	 * @param id the id
	 */
	public HighscoresState(String id)
	{
		super(id);
	}

	@Override
	public void update(float delta)
	{

	}

	@Override
	public void resize(int w, int h)
	{

	}

	@Override
	public void render()
	{

	}

	/**
	 * Gets the time score.
	 *
	 * @param time the time
	 * @return the time score
	 */
	public int getTimeScore(String time)
	{
		String[] tokens = time.split(":");
		return Integer.parseInt(tokens[0]) * 3600 + Integer.parseInt(tokens[1]) * 60 + Integer.parseInt(tokens[2]);
	}

	@Override
	public void init()
	{

		File highscores = new File("da.ta");
		if (!highscores.exists())
			highscores.createNewFile();

		Map<Integer, String> timeScores = new TreeMap<Integer, String>();

		Scanner scan = new Scanner(highscores);
		while (scan.hasNextLine())
		{
			String text = scan.nextLine();
			timeScores.put(getTimeScore(text), text);
		}

		timeScores.put(getTimeScore(totalTime), totalTime);

	}

	@Override
	public void dispose(ExitReason reason)
	{

	}

}
