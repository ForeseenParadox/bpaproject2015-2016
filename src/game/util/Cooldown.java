package game.util;

/**
 * The Class Cooldown.
 */
public class Cooldown
{
	
	/** The length. */
	private long length;
	
	/** The last check. */
	private long lastCheck;

	/**
	 * Instantiates a new cooldown.
	 *
	 * @param length the length
	 */
	public Cooldown(long length)
	{
		lastCheck = System.currentTimeMillis();
		this.length = length;
	}

	/**
	 * Gets the last check.
	 *
	 * @return the last check
	 */
	public long getLastCheck()
	{
		return lastCheck;
	}

	/**
	 * Gets the length.
	 *
	 * @return the length
	 */
	public long getLength()
	{
		return length;
	}

	/**
	 * Sets the length.
	 *
	 * @param length the new length
	 */
	public void setLength(long length)
	{
		this.length = length;
	}

	/**
	 * Checks if is ready.
	 *
	 * @return true, if is ready
	 */
	public boolean isReady()
	{
		if((System.currentTimeMillis()-lastCheck)>=length)
		{
			lastCheck = System.currentTimeMillis();
			return true;
		} else 
		{
			return false;
		}
	}

}
