package game;

import engine.core.Engine;
import engine.core.EngineConfig;
import engine.file.ExternalFileHandle;

/**
 * The Class Main.
 */
public class Main
{

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args)
	{
		Engine eng = new Engine(new EngineConfig(new Game(), new ExternalFileHandle("./res_ext/econf.properties")));
		eng.startEngine();
	}

}
