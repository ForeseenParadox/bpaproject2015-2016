package game;

import engine.file.ExternalFileHandle;
import engine.resource.TextFileResource;
import engine.util.xml.XMLDocument;
import engine.util.xml.XMLElement;

/**
 * The Class OptionsFile.
 */
public class OptionsFile
{

	/** The resource. */
	private TextFileResource resource;
	
	/** The xml doc. */
	private XMLDocument xmlDoc;
	
	/** The max fps. */
	private int maxFps;
	
	/** The master volume. */
	private float masterVolume;
	
	/** The sfx volume. */
	private float sfxVolume;
	
	/** The music volume. */
	private float musicVolume;

	/**
	 * Instantiates a new options file.
	 *
	 * @param res the res
	 */
	public OptionsFile(TextFileResource res)
	{

		if (res == null)
		{
			this.resource = new TextFileResource(new ExternalFileHandle("options.xml"));
			this.xmlDoc = new XMLDocument();
			maxFps = 60;
			masterVolume = 0.5f;
			sfxVolume = 0.5f;
			musicVolume = 0.5f;
			xmlDoc.setRootElement(new XMLElement("Options", null));
			xmlDoc.getRootElement().addChild(new XMLElement("MaxFPS", null));
			xmlDoc.getRootElement().addChild(new XMLElement("MasterVolume", null));
			xmlDoc.getRootElement().addChild(new XMLElement("SFXVolume", null));
			xmlDoc.getRootElement().addChild(new XMLElement("MusicVolume", null));
		} else
		{
			this.resource = res;
			this.xmlDoc = new XMLDocument(res);
			XMLElement root = xmlDoc.getRootElement();
			maxFps = Integer.parseInt(root.getChildByName("MaxFPS").getValue());
			masterVolume = Float.parseFloat(root.getChildByName("MasterVolume").getValue());
			sfxVolume = Float.parseFloat(root.getChildByName("SFXVolume").getValue());
			musicVolume = Float.parseFloat(root.getChildByName("MusicVolume").getValue());
		}
	}

	/**
	 * Gets the resource.
	 *
	 * @return the resource
	 */
	public TextFileResource getResource()
	{
		return resource;
	}

	/**
	 * Gets the xml doc.
	 *
	 * @return the xml doc
	 */
	public XMLDocument getXmlDoc()
	{
		return xmlDoc;
	}

	/**
	 * Gets the max fps.
	 *
	 * @return the max fps
	 */
	public int getMaxFps()
	{
		return maxFps;
	}

	/**
	 * Gets the master volume.
	 *
	 * @return the master volume
	 */
	public float getMasterVolume()
	{
		return masterVolume;
	}

	/**
	 * Gets the sfx volume.
	 *
	 * @return the sfx volume
	 */
	public float getSfxVolume()
	{
		return sfxVolume;
	}

	/**
	 * Gets the music volume.
	 *
	 * @return the music volume
	 */
	public float getMusicVolume()
	{
		return musicVolume;
	}

	/**
	 * Sets the max fps.
	 *
	 * @param maxFps the new max fps
	 */
	public void setMaxFps(int maxFps)
	{
		this.maxFps = maxFps;
	}

	/**
	 * Sets the master volume.
	 *
	 * @param masterVolume the new master volume
	 */
	public void setMasterVolume(float masterVolume)
	{
		this.masterVolume = masterVolume;
	}

	/**
	 * Sets the sfx volume.
	 *
	 * @param sfxVolume the new sfx volume
	 */
	public void setSfxVolume(float sfxVolume)
	{
		this.sfxVolume = sfxVolume;
	}

	/**
	 * Sets the music volume.
	 *
	 * @param musicVolume the new music volume
	 */
	public void setMusicVolume(float musicVolume)
	{
		this.musicVolume = musicVolume;
	}

	/**
	 * Save.
	 */
	public void save()
	{
		xmlDoc.getRootElement().getChildByName("MaxFPS").setValue(Integer.toString(maxFps));
		xmlDoc.getRootElement().getChildByName("MasterVolume").setValue(String.format("%.2f", masterVolume));
		xmlDoc.getRootElement().getChildByName("SFXVolume").setValue(String.format("%.2f", sfxVolume));
		xmlDoc.getRootElement().getChildByName("MusicVolume").setValue(String.format("%.2f", musicVolume));

		xmlDoc.save(((ExternalFileHandle) resource.getHandle()).openOutputStream());
	}

}