package game;

/**
 * The Class Resources.
 */
public class Resources
{

	/** The Constant TEXTURE_GRASS. */
	public static final String TEXTURE_GRASS = "Grass";
	
	/** The Constant TEXTURE_METAL_DARK. */
	public static final String TEXTURE_METAL_DARK = "Metal_Dark_Tile";
	
	/** The Constant TEXTURE_METAL_LIGHT. */
	public static final String TEXTURE_METAL_LIGHT = "Metal_Light_Tile";
	
	/** The Constant ANIM_GLOWING. */
	public static final String ANIM_GLOWING = "Glowing_Tile_Anim";
	
	/** The Constant ANIM_SEWAGE. */
	public static final String ANIM_SEWAGE = "Sewage_Tile_Anim";
	
	/** The Constant ANIM_SEWAGE_TOP. */
	public static final String ANIM_SEWAGE_TOP = "Sewage_Tile_Top_Anim";
	
	/** The Constant ANIM_SEWAGE_PIPE. */
	public static final String ANIM_SEWAGE_PIPE = "Sewage_Tile_Pipe_Anim";

}
