package game.entity.projectile;

import engine.animation.TextureAnimation;
import engine.graphics.texture.TextureRegion;
import engine.util.math.Vector2f;
import game.entity.Entity;
import game.entity.def.ProjectileDef;
import game.world.World;

/**
 * The Class HitmarkerParticle.
 */
public class HitmarkerParticle extends Projectile
{

	/** The played. */
	private boolean played = false;

	/**
	 * Instantiates a new hitmarker particle.
	 *
	 * @param world the world
	 * @param pos the pos
	 * @param firing the firing
	 * @param angle the angle
	 */
	public HitmarkerParticle(World world, Vector2f pos, Entity firing, float angle)
	{
		super((ProjectileDef) Entity.getEntityDef(Entity.HITMARKER_PROJECTILE), world, pos, firing, angle);
		setCurrentAnimation(new TextureAnimation(TextureRegion.getTextureRegion("Hitmarker")));
		setVelocity(new Vector2f());

		setScale(new Vector2f(0.3f, 0.3f));

		setLife(10);
	}

	@Override
	public void update(float delta)
	{
		super.update(delta);

		if (!played)
		{
			played = true;
		}
	}

}
