package game.entity.projectile;

import java.util.List;

import engine.util.math.Vector2f;
import game.entity.Entity;
import game.entity.SizedEntity;
import game.entity.def.ProjectileDef;
import game.entity.mobs.MobileEntity;
import game.world.World;

/**
 * The Class Projectile.
 */
public abstract class Projectile extends SizedEntity
{

	/** The velocity. */
	private Vector2f velocity;
	
	/** The max speed. */
	private float maxSpeed;
	
	/** The tile contact. */
	private boolean tileContact;
	
	/** The contacting. */
	private Entity contacting;
	
	/** The total life. */
	private int totalLife;
	
	/** The damage infliction. */
	private int damageInfliction;

	/** The ticks alive. */
	// lifetime
	private int ticksAlive;

	/** The tile ghost. */
	// special settings
	private boolean tileGhost;
	
	/** The entity ghost. */
	private boolean entityGhost;
	
	/** The expires. */
	private boolean expires;

	/** The firing. */
	// firing
	private Entity firing;

	/**
	 * Instantiates a new projectile.
	 *
	 * @param def the def
	 * @param world the world
	 * @param position the position
	 * @param firing the firing
	 * @param angle the angle
	 */
	public Projectile(ProjectileDef def, World world, Vector2f position, Entity firing, float angle)
	{
		super(def, world, position);

		this.maxSpeed = def.getSpeed();

		this.velocity = new Vector2f((float) Math.cos(angle) * def.getSpeed(), (float) Math.sin(angle) * def.getSpeed());

		this.totalLife = def.getTotalLife();
		this.damageInfliction = def.getDamageInfliction();

		this.firing = firing;
		this.expires = true;
	}

	/**
	 * Gets the velocity.
	 *
	 * @return the velocity
	 */
	public Vector2f getVelocity()
	{
		return velocity;
	}

	/**
	 * Gets the max speed.
	 *
	 * @return the max speed
	 */
	public float getMaxSpeed()
	{
		return maxSpeed;
	}

	/**
	 * Checks if is contacting tile.
	 *
	 * @return true, if is contacting tile
	 */
	public boolean isContactingTile()
	{
		return tileContact;
	}

	/**
	 * Gets the contacting.
	 *
	 * @return the contacting
	 */
	public Entity getContacting()
	{
		return contacting;
	}

	/**
	 * Checks if is contacting entity.
	 *
	 * @return true, if is contacting entity
	 */
	public boolean isContactingEntity()
	{
		return contacting != null;
	}

	/**
	 * Checks if is expires.
	 *
	 * @return true, if is expires
	 */
	public boolean isExpires()
	{
		return expires;
	}

	/**
	 * Gets the life.
	 *
	 * @return the life
	 */
	public int getLife()
	{
		return totalLife;
	}

	/**
	 * Gets the ticks alive.
	 *
	 * @return the ticks alive
	 */
	public int getTicksAlive()
	{
		return ticksAlive;
	}

	/**
	 * Gets the firing.
	 *
	 * @return the firing
	 */
	public Entity getFiring()
	{
		return firing;
	}

	/**
	 * Gets the damage infliction.
	 *
	 * @return the damage infliction
	 */
	public int getDamageInfliction()
	{
		return damageInfliction;
	}

	/**
	 * Sets the velocity.
	 *
	 * @param velocity the new velocity
	 */
	public void setVelocity(Vector2f velocity)
	{
		this.velocity = velocity;
	}

	/**
	 * Sets the entity ghost.
	 *
	 * @param entityGhost the new entity ghost
	 */
	public void setEntityGhost(boolean entityGhost)
	{
		this.entityGhost = entityGhost;
	}

	/**
	 * Sets the tile ghost.
	 *
	 * @param tileGhost the new tile ghost
	 */
	public void setTileGhost(boolean tileGhost)
	{
		this.tileGhost = tileGhost;
	}

	/**
	 * Sets the expires.
	 *
	 * @param expires the new expires
	 */
	public void setExpires(boolean expires)
	{
		this.expires = expires;
	}

	/**
	 * Sets the life.
	 *
	 * @param life the new life
	 */
	public void setLife(int life)
	{
		this.totalLife = life;
	}

	/**
	 * Sets the firing.
	 *
	 * @param firing the new firing
	 */
	public void setFiring(Entity firing)
	{
		this.firing = firing;
	}

	/**
	 * Sets the damage infliction.
	 *
	 * @param damageInfliction the new damage infliction
	 */
	public void setDamageInfliction(int damageInfliction)
	{
		this.damageInfliction = damageInfliction;
	}

	@Override
	public void update(float delta)
	{
		setPosition(getX() + velocity.getX(), getY() + velocity.getY());

		ticksAlive++;
		if (ticksAlive >= totalLife && expires)
			getWorld().getEntities().remove(this);

		if (!canMove(0, 0))
			tileContact = true;

		if (!tileGhost && tileContact)
			getWorld().getEntities().remove(this);

		List<Entity> ent = getWorld().getEntities();
		for (int i = 0; i < ent.size(); i++)
		{
			Entity e = ent.get(i);
			if (e instanceof MobileEntity && e != getFiring())
			{
				MobileEntity mob = (MobileEntity) e;
				if (mob.intersects(this))
				{
					contacting = mob;
					mob.changeHealth(-damageInfliction);
					getWorld().getEntities().remove(this);

					if (!entityGhost)
						getWorld().getEntities().remove(this);
				}
			}
		}

	}

}
