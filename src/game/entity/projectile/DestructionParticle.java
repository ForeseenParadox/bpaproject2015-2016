package game.entity.projectile;

import engine.util.math.Vector2f;
import game.entity.Entity;
import game.entity.def.ProjectileDef;
import game.world.World;

/**
 * The Class DestructionParticle.
 */
public class DestructionParticle extends Projectile
{

	/**
	 * Instantiates a new destruction particle.
	 *
	 * @param world the world
	 * @param pos the pos
	 * @param firing the firing
	 */
	public DestructionParticle(World world, Vector2f pos, Entity firing)
	{
		super((ProjectileDef) Entity.getEntityDef(Entity.DESCTUCTION_PROJECTILE), world, pos, firing, 0);

		float min = getMaxSpeed() * 0.6f, max = getMaxSpeed();
		setVelocity(new Vector2f((float) ((Math.random() * 2 - 1) * (max - min) + min), (float) ((Math.random() * 2 - 1) * (max - min) + min)));
		setEntityGhost(true);
		setTileGhost(true);
	}

	@Override
	public void update(float delta)
	{
		if (!canMove(getVelocity().getX(), 0))
			getVelocity().setX(-getVelocity().getX());
		if (!canMove(0, getVelocity().getY()))
			getVelocity().setY(-getVelocity().getY());

		super.update(delta);

	}

}
