package game.entity.projectile;

import engine.animation.TextureAnimation;
import engine.util.math.Vector2f;
import game.entity.Entity;
import game.entity.def.ProjectileDef;
import game.entity.mobs.MobileEntity;
import game.world.World;

/**
 * The Class BasicProjectile.
 */
public class BasicProjectile extends Projectile
{

	/** The Constant BASIC_PROJECTILE_ANIMATION_NAME. */
	public static final String BASIC_PROJECTILE_ANIMATION_NAME = "Basic-Projectile-Anim";

	/**
	 * Instantiates a new basic projectile.
	 *
	 * @param world the world
	 * @param pos the pos
	 * @param firing the firing
	 * @param angle the angle
	 */
	public BasicProjectile(World world, Vector2f pos, Entity firing, float angle)
	{
		super((ProjectileDef) Entity.getEntityDef(Entity.BASIC_PROJECTILE), world, pos, firing, angle);

		setCurrentAnimation(TextureAnimation.getTextureAnimation(BASIC_PROJECTILE_ANIMATION_NAME));
		setDamageInfliction(1);
	}

	@Override
	public void update(float delta)
	{
		super.update(delta);

		if (isContactingEntity())
		{
			getWorld().spawnEntity(new HitmarkerParticle(getWorld(), new Vector2f(getX() + getScaledWidth() / 2, getY() + getScaledHeight() / 2), getFiring(), 0));

			if (getFiring() == getWorld().getPlayer())
			{
				if (((MobileEntity) getContacting()).isDead())
					getWorld().getPlayer().setExp(getWorld().getPlayer().getExp() + 5);
			}
		}
		if (isContactingTile())
		{
			for (int i = 0; i < 5; i++)
				getWorld().spawnEntity((new DestructionParticle(getWorld(), new Vector2f(getX() + getScaledWidth() / 2, getY() + getScaledHeight() / 2), getFiring())));
		}
	}

}
