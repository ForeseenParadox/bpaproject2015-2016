package game.entity;

import game.entity.def.EntityDef;
import game.world.World;

/**
 * The Class PointEntity.
 */
public abstract class PointEntity extends Entity
{

	/** The x. */
	private float x;
	
	/** The y. */
	private float y;

	/**
	 * Instantiates a new point entity.
	 *
	 * @param world the world
	 * @param def the def
	 * @param x the x
	 * @param y the y
	 */
	public PointEntity(World world, EntityDef def, float x, float y)
	{
		super(world, def);
		this.x = x;
		this.y = y;
	}

	/**
	 * Gets the x.
	 *
	 * @return the x
	 */
	public float getX()
	{
		return x;
	}

	/**
	 * Gets the y.
	 *
	 * @return the y
	 */
	public float getY()
	{
		return y;
	}

	/**
	 * Sets the x.
	 *
	 * @param x the new x
	 */
	public void setX(float x)
	{
		this.x = x;
	}

	/**
	 * Sets the y.
	 *
	 * @param y the new y
	 */
	public void setY(float y)
	{
		this.y = y;
	}

}
