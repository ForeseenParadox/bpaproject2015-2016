package game.entity.mobs;

import engine.audio.Audio;
import engine.graphics.Color;
import engine.graphics.batch.ShapeBatch;
import engine.graphics.batch.SpriteBatch;
import engine.util.math.Vector2f;
import game.entity.SizedEntity;
import game.entity.def.MobDef;
import game.world.World;

/**
 * The Class MobileEntity.
 */
public class MobileEntity extends SizedEntity
{

	/** The max health. */
	private float maxHealth;
	
	/** The health. */
	private float health;

	/**
	 * Instantiates a new mobile entity.
	 *
	 * @param def the def
	 * @param world the world
	 * @param position the position
	 */
	public MobileEntity(MobDef def, World world, Vector2f position)
	{
		super(def, world, position);
		this.maxHealth = def.getMaxHealth();
		this.health = maxHealth;
		getBindings().put("health", health);
		getBindings().put("maxHealth", maxHealth);
	}

	/**
	 * Gets the health.
	 *
	 * @return the health
	 */
	public float getHealth()
	{
		return health;
	}

	/**
	 * Gets the max health.
	 *
	 * @return the max health
	 */
	public float getMaxHealth()
	{
		return maxHealth;
	}

	/**
	 * Sets the health.
	 *
	 * @param health the new health
	 */
	public void setHealth(float health)
	{
		if (health < this.health)
		{
			Audio.getAudioByName("MLG_Hitmarker").reset();
			Audio.getAudioByName("MLG_Hitmarker").start();
		}

		this.health = health;
		if (health > maxHealth)
		{
			this.health = maxHealth;
		}

		if (health <= 0)
		{
			getWorld().getEntities().remove(this);
			getWorld().getPlayer().playAirhorn();
		}

	}

	/**
	 * Sets the max health.
	 *
	 * @param maxHealth the new max health
	 */
	public void setMaxHealth(float maxHealth)
	{
		this.maxHealth = maxHealth;
	}

	/**
	 * Change health.
	 *
	 * @param health the health
	 */
	public void changeHealth(float health)
	{
		setHealth(getHealth() + health);
	}

	@Override
	public void update(float delta)
	{
		super.update(delta);
	}

	/**
	 * Checks if is dead.
	 *
	 * @return true, if is dead
	 */
	public boolean isDead()
	{
		return health <= 0;
	}

	/**
	 * Render gui.
	 *
	 * @param shapeBatch the shape batch
	 */
	public void renderGUI(ShapeBatch shapeBatch)
	{
		float defWidth = 100;
		float defHeight = 15;
		float gap = 10;
		renderHealthBar(shapeBatch, (getX() + getScaledWidth() / 2 - defWidth / 2), getScaledHeight() + defHeight + getY() + gap, defWidth, defHeight, Color.WHITE, Color.RED, Color.GREEN);
	}

	/**
	 * Render health bar.
	 *
	 * @param shapeBatch the shape batch
	 * @param x the x
	 * @param y the y
	 * @param width the width
	 * @param height the height
	 * @param border the border
	 * @param bg the bg
	 * @param fg the fg
	 */
	public void renderHealthBar(ShapeBatch shapeBatch, float x, float y, float width, float height, Color border, Color bg, Color fg)
	{
		renderBar(shapeBatch, health, maxHealth, x, y, width, height, border, bg, fg);
	}

	/**
	 * Render bar.
	 *
	 * @param shapeBatch the shape batch
	 * @param num the num
	 * @param denom the denom
	 * @param x the x
	 * @param y the y
	 * @param width the width
	 * @param height the height
	 * @param border the border
	 * @param bg the bg
	 * @param fg the fg
	 */
	public void renderBar(ShapeBatch shapeBatch, float num, float denom, float x, float y, float width, float height, Color border, Color bg, Color fg)
	{
		shapeBatch.setDrawColor(bg);
		shapeBatch.renderFilledRect(x, y, width, height, 0, 0, 0, 1, 1);
		shapeBatch.setDrawColor(fg);
		shapeBatch.renderFilledRect(x, y, Math.max(0, num / denom * width), height, 0, 0, 0, 1, 1);
		shapeBatch.setDrawColor(border);
		shapeBatch.renderRect(x, y, width, height, 0, 0, 0, 1, 1);
	}

	@Override
	public void render(SpriteBatch spriteBatch, ShapeBatch shapeBatch)
	{
		super.render(spriteBatch, shapeBatch);
	}

}
