package game.entity.mobs;

import static org.lwjgl.glfw.GLFW.GLFW_KEY_A;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_D;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_S;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_W;

import engine.animation.TextureAnimation;
import engine.audio.Audio;
import engine.core.Engine;
import engine.graphics.Color;
import engine.graphics.batch.ShapeBatch;
import engine.graphics.batch.SpriteBatch;
import engine.graphics.light.PointLight;
import engine.input.Mouse;
import engine.physics.QuadraticAttenuation;
import engine.util.math.Vector2f;
import game.Game;
import game.entity.Direction;
import game.entity.WalkingAnimation;
import game.entity.def.MobDef;
import game.entity.projectile.BasicProjectile;
import game.util.Cooldown;
import game.world.World;

/**
 * The Class Player.
 */
public class Player extends MobileEntity
{

	/** The light. */
	private PointLight light;
	
	/** The last shot. */
	private long lastShot = System.currentTimeMillis();
	
	/** The delay. */
	private long delay = 100;

	/** The control disabled. */
	private boolean controlDisabled;
	
	/** The heal cooldown. */
	private Cooldown healCooldown;
	
	/** The shot. */
	private boolean shot;

	/** The dir. */
	private Direction dir;
	
	/** The anim. */
	private WalkingAnimation anim;

	/** The ghost. */
	private boolean ghost;
	
	/** The speed. */
	private int speed;

	/** The exp. */
	private float exp;
	
	/** The exp needed. */
	private float expNeeded;
	
	/** The level. */
	private int level;

	/**
	 * Instantiates a new player.
	 *
	 * @param def the def
	 * @param world the world
	 * @param position the position
	 */
	public Player(MobDef def, World world, Vector2f position)
	{
		super(def, world, position);
		light = new PointLight(new Color(0, 0, 1, 1f), 1, new QuadraticAttenuation(0.0001f, 0, 4), new Vector2f(), 500);
		healCooldown = new Cooldown(1);
		controlDisabled = false;

		getScale().set(2, 2);

		dir = Direction.NORTH;
		anim = new WalkingAnimation(TextureAnimation.getTextureAnimation("Girl-Walking-Forward"), TextureAnimation.getTextureAnimation("Girl-Walking-Downward"), TextureAnimation.getTextureAnimation("Girl-Walking-Rightward"), TextureAnimation.getTextureAnimation("Girl-Walking-Leftward"));
		setCurrentAnimation(anim.getForwardAnimation());

		speed = 5;
		level = 1;
		exp = 0;
		expNeeded = level * 10;
	}

	/**
	 * Gets the exp.
	 *
	 * @return the exp
	 */
	public float getExp()
	{
		return exp;
	}

	/**
	 * Gets the exp needed.
	 *
	 * @return the exp needed
	 */
	public float getExpNeeded()
	{
		return expNeeded;
	}

	/**
	 * Gets the level.
	 *
	 * @return the level
	 */
	public int getLevel()
	{
		return level;
	}

	/**
	 * Sets the exp.
	 *
	 * @param exp the new exp
	 */
	public void setExp(float exp)
	{
		this.exp = exp;

		if (exp >= expNeeded)
		{
			level++;
			exp = 0;
			expNeeded = level * 10;
		}
	}

	/**
	 * Sets the exp needed.
	 *
	 * @param expNeeded the new exp needed
	 */
	public void setExpNeeded(float expNeeded)
	{
		this.expNeeded = expNeeded;
	}

	/**
	 * Sets the level.
	 *
	 * @param level the new level
	 */
	public void setLevel(int level)
	{
		this.level = level;
	}

	/**
	 * Checks if is ghost.
	 *
	 * @return true, if is ghost
	 */
	public boolean isGhost()
	{
		return ghost;
	}

	/**
	 * Gets the speed.
	 *
	 * @return the speed
	 */
	public int getSpeed()
	{
		return speed;
	}

	/**
	 * Sets the ghost.
	 *
	 * @param ghost the new ghost
	 */
	public void setGhost(boolean ghost)
	{
		this.ghost = ghost;
	}

	/**
	 * Sets the speed.
	 *
	 * @param speed the new speed
	 */
	public void setSpeed(int speed)
	{
		this.speed = speed;
	}

	/**
	 * Checks if is control disabled.
	 *
	 * @return true, if is control disabled
	 */
	public boolean isControlDisabled()
	{
		return controlDisabled;
	}

	/**
	 * Gets the dir.
	 *
	 * @return the dir
	 */
	public Direction getDir()
	{
		return dir;
	}

	/**
	 * Sets the control disabled.
	 *
	 * @param controlDisabled the new control disabled
	 */
	public void setControlDisabled(boolean controlDisabled)
	{
		this.controlDisabled = controlDisabled;
	}

	/**
	 * Gets the rumble.
	 *
	 * @return the rumble
	 */
	public Vector2f getRumble()
	{
		if (shot)
			return new Vector2f((float) (Math.random() * 2 - 1) * 5 + 4, (float) (Math.random() * 2 - 1) + 4);
		else
			return new Vector2f();
	}

	@Override
	public void update(float delta)
	{
		super.update(delta);
		if (getHealth() < getMaxHealth() && healCooldown.isReady())
		{
			changeHealth(0.003f);
		}
		if (!controlDisabled)
		{
			boolean move = false;

			if ((ghost || canMove(-speed, 0)) && Engine.getInstance().getInput().getKeyboard().isKeyPressed(GLFW_KEY_A))
			{
				getBounds().getPosition().setX(getBounds().getPosition().getX() - speed);
				move = true;
				dir = Direction.WEST;
				setCurrentAnimation(anim.getLeftwardAnimation());

			}
			if ((ghost || canMove(speed, 0)) && Engine.getInstance().getInput().getKeyboard().isKeyPressed(GLFW_KEY_D))
			{
				getBounds().getPosition().setX(getBounds().getPosition().getX() + speed);
				move = true;
				dir = Direction.EAST;

				setCurrentAnimation(anim.getRightwardAnimation());
			}
			if ((ghost || canMove(0, speed)) && Engine.getInstance().getInput().getKeyboard().isKeyPressed(GLFW_KEY_W))
			{
				getBounds().getPosition().setY(getBounds().getPosition().getY() + speed);
				move = true;
				dir = Direction.NORTH;
				setCurrentAnimation(anim.getForwardAnimation());
			}
			if ((ghost || canMove(0, -speed)) && Engine.getInstance().getInput().getKeyboard().isKeyPressed(GLFW_KEY_S))
			{
				getBounds().getPosition().setY(getBounds().getPosition().getY() - speed);
				move = true;
				dir = Direction.SOUTH;
				setCurrentAnimation(anim.getDownwardAnimation());
			}
			getWorld().centerCameraOnEntity(this);

			if (!move)
				getCurrentAnimation().setLockFrameIndex(0);
			else
				getCurrentAnimation().setLockFrameIndex(-1);

			light.setPosition(getBounds().getPosition().add(new Vector2f(getScaledWidth() / 2, getScaledHeight() / 2)));

			Mouse m = Engine.getInstance().getInput().getMouse();

			shot = false;
			if (m.isButtonPressed(0) && (System.currentTimeMillis() - lastShot) >= delay)
			{
				shot = true;

				Audio.getAudioByName("MLG_Quickscope").reset();
				Audio.getAudioByName("MLG_Quickscope").start();

				float x = m.getX(), y = m.getY();

				float dx = x - Engine.getInstance().getWindow().getWidth() / 2, dy = y - Engine.getInstance().getWindow().getHeight() / 2;
				float ang = (float) Math.atan2(dy, dx) + (float) Math.toRadians(((Math.random() * 2) - 1) * 2);
				getWorld().spawnEntity(new BasicProjectile(getWorld(), new Vector2f(getX() + getScaledWidth() / 2, getY() + getScaledHeight() / 2), this, ang));
				lastShot = System.currentTimeMillis();
			}
		}
	}

	/**
	 * Play airhorn.
	 */
	public void playAirhorn()
	{
		Audio.getAudioByName("MLG_Airhorn").reset();
		Audio.getAudioByName("MLG_Airhorn").start();
	}

	@Override
	public void render(SpriteBatch spriteBatch, ShapeBatch shapeBatch)
	{
		super.render(spriteBatch, shapeBatch);
		Game.getInstance().getLightBatch().pushLight(light);
	}

	@Override
	public void renderGUI(ShapeBatch shapeBatch)
	{
		super.renderGUI(shapeBatch);

		float defWidth = 100;
		float defHeight = 15;
		float gap = 10;

		renderBar(shapeBatch, exp, expNeeded, (getX() + getScaledWidth() / 2 - defWidth / 2), getScaledHeight() + defHeight * 2 + getY() + gap * 2, defWidth, defHeight, Color.WHITE, Color.BLUE, Color.CORNFLOWER_BLUE);
	}

}
