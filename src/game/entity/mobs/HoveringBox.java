package game.entity.mobs;

import engine.animation.TextureAnimation;
import engine.util.math.Vector2f;
import game.entity.def.MobDef;
import game.entity.projectile.BasicProjectile;
import game.world.World;

/**
 * The Class HoveringBox.
 */
public class HoveringBox extends MobileEntity
{
	
	/** The last attack. */
	private long lastAttack;
	
	/** The attack speed. */
	private long attackSpeed;

	/**
	 * Instantiates a new hovering box.
	 *
	 * @param def the def
	 * @param world the world
	 * @param position the position
	 */
	public HoveringBox(MobDef def, World world, Vector2f position)
	{
		super(def, world, position);

		setCurrentAnimation(TextureAnimation.getTextureAnimation("HoveringBox"));
		getScale().set(2, 2);
		lastAttack = System.currentTimeMillis();
		attackSpeed = 2000;
	}

	/**
	 * Attack.
	 */
	public void attack()
	{
		Player player = getWorld().getPlayer();
		// float dx = (player.getX() - getX() + getBounds().getWidth() / 2), dy
		// = (player.getY() - getY() + getBounds().getHeight() / 2);

		if (player.dist(getX(), getY()) <= 500)
		{
			for (float i = 0; i < 360; i += 360 / 16f)
			{
				float radians = (float) Math.toRadians(i);
				getWorld().spawnEntity(new BasicProjectile(getWorld(), new Vector2f(getX() + getScaledWidth() / 2, getY() + getScaledHeight() / 2), this, radians));
			}
		}
	}

	/**
	 * Gets the attack speed.
	 *
	 * @return the attack speed
	 */
	public long getAttackSpeed()
	{
		return attackSpeed;
	}

	/**
	 * Sets the attack speed.
	 *
	 * @param attackSpeed the new attack speed
	 */
	public void setAttackSpeed(long attackSpeed)
	{
		this.attackSpeed = attackSpeed;
	}

	@Override
	public void update(float delta)
	{
		// TODO Auto-generated method stub
		super.update(delta);
		if ((System.currentTimeMillis() - lastAttack) >= attackSpeed)
		{
			attack();
			lastAttack = System.currentTimeMillis();
		}
	}

}
