package game.entity.mobs;

import engine.graphics.Color;
import engine.graphics.batch.ShapeBatch;
import engine.util.math.Vector2f;
import game.entity.def.MobDef;
import game.entity.pathfinding.PathNode;
import game.entity.projectile.BasicProjectile;
import game.world.Tile;
import game.world.World;

/**
 * The Class Slime.
 */
public class Slime extends MobileEntity
{

	/** The last attack. */
	private long lastAttack;
	
	/** The attack speed. */
	private long attackSpeed;

	/** The current. */
	private PathNode current;

	/**
	 * Instantiates a new slime.
	 *
	 * @param def the def
	 * @param world the world
	 * @param position the position
	 */
	public Slime(MobDef def, World world, Vector2f position)
	{
		super(def, world, position);

		lastAttack = System.currentTimeMillis();
		attackSpeed = 2000;
	}

	/**
	 * Gets the attack speed.
	 *
	 * @return the attack speed
	 */
	public long getAttackSpeed()
	{
		return attackSpeed;
	}

	/**
	 * Sets the attack speed.
	 *
	 * @param attackSpeed the new attack speed
	 */
	public void setAttackSpeed(long attackSpeed)
	{
		this.attackSpeed = attackSpeed;
	}

	@Override
	public void update(float delta)
	{
		super.update(delta);

		if ((System.currentTimeMillis() - lastAttack) >= attackSpeed)
		{
			attack();
			lastAttack = System.currentTimeMillis();
		}
		current = findPathCorners(getWorld().getPlayer());

	}

	/**
	 * Attack.
	 */
	public void attack()
	{
		Player player = getWorld().getPlayer();
		float dx = (player.getX() - getX() + getBounds().getWidth() / 2), dy = (player.getY() - getY() + getBounds().getHeight() / 2);
		float angle = (float) Math.atan2(dy, dx);

		getWorld().spawnEntity(new BasicProjectile(getWorld(), new Vector2f(getX(), getY()), this, angle));
	}

}
