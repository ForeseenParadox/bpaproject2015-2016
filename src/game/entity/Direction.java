package game.entity;

/**
 * The Enum Direction.
 */
public enum Direction
{

	/** The north. */
	NORTH,
	
	/** The south. */
	SOUTH,
	
	/** The east. */
	EAST,
	
	/** The west. */
	WEST

}