package game.entity;

import engine.animation.TextureAnimation;

/**
 * The Class WalkingAnimation.
 */
public class WalkingAnimation
{

	/** The forward animation. */
	private TextureAnimation forwardAnimation;
	
	/** The downward animation. */
	private TextureAnimation downwardAnimation;
	
	/** The rightward animation. */
	private TextureAnimation rightwardAnimation;
	
	/** The leftward animation. */
	private TextureAnimation leftwardAnimation;

	/**
	 * Instantiates a new walking animation.
	 */
	public WalkingAnimation()
	{
		this(null, null, null, null);
	}

	/**
	 * Instantiates a new walking animation.
	 *
	 * @param forwardAnimation the forward animation
	 * @param downwardAnimation the downward animation
	 * @param rightwardAnimation the rightward animation
	 * @param leftwardAnimation the leftward animation
	 */
	public WalkingAnimation(TextureAnimation forwardAnimation, TextureAnimation downwardAnimation, TextureAnimation rightwardAnimation, TextureAnimation leftwardAnimation)
	{
		this.forwardAnimation = forwardAnimation;
		this.downwardAnimation = downwardAnimation;
		this.rightwardAnimation = rightwardAnimation;
		this.leftwardAnimation = leftwardAnimation;
	}

	/**
	 * Gets the forward animation.
	 *
	 * @return the forward animation
	 */
	public TextureAnimation getForwardAnimation()
	{
		return forwardAnimation;
	}

	/**
	 * Sets the forward animation.
	 *
	 * @param forwardAnimation the new forward animation
	 */
	public void setForwardAnimation(TextureAnimation forwardAnimation)
	{
		this.forwardAnimation = forwardAnimation;
	}

	/**
	 * Gets the downward animation.
	 *
	 * @return the downward animation
	 */
	public TextureAnimation getDownwardAnimation()
	{
		return downwardAnimation;
	}

	/**
	 * Sets the downward animation.
	 *
	 * @param downwardAnimation the new downward animation
	 */
	public void setDownwardAnimation(TextureAnimation downwardAnimation)
	{
		this.downwardAnimation = downwardAnimation;
	}

	/**
	 * Gets the rightward animation.
	 *
	 * @return the rightward animation
	 */
	public TextureAnimation getRightwardAnimation()
	{
		return rightwardAnimation;
	}

	/**
	 * Sets the rightward animation.
	 *
	 * @param rightwardAnimation the new rightward animation
	 */
	public void setRightwardAnimation(TextureAnimation rightwardAnimation)
	{
		this.rightwardAnimation = rightwardAnimation;
	}

	/**
	 * Gets the leftward animation.
	 *
	 * @return the leftward animation
	 */
	public TextureAnimation getLeftwardAnimation()
	{
		return leftwardAnimation;
	}

	/**
	 * Sets the leftward animation.
	 *
	 * @param leftwardAnimation the new leftward animation
	 */
	public void setLeftwardAnimation(TextureAnimation leftwardAnimation)
	{
		this.leftwardAnimation = leftwardAnimation;
	}

}