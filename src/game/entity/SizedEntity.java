package game.entity;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

import engine.animation.TextureAnimation;
import engine.graphics.Color;
import engine.graphics.batch.ShapeBatch;
import engine.graphics.batch.SpriteBatch;
import engine.graphics.texture.TextureRegion;
import engine.physics.BoundingBox;
import engine.util.math.Vector2f;
import game.entity.def.SizedEntityDef;
import game.entity.pathfinding.PathNode;
import game.world.Tile;
import game.world.World;

/**
 * The Class SizedEntity.
 */
public abstract class SizedEntity extends Entity
{

	/** The bounds. */
	private BoundingBox bounds;
	
	/** The current animation. */
	private TextureAnimation currentAnimation;
	
	/** The origin. */
	private Vector2f origin;
	
	/** The scale. */
	private Vector2f scale;
	
	/** The rotation. */
	private float rotation;
	
	/** The tint. */
	private Color tint;

	/**
	 * Instantiates a new sized entity.
	 *
	 * @param def the def
	 * @param world the world
	 * @param position the position
	 */
	public SizedEntity(SizedEntityDef def, World world, Vector2f position)
	{
		super(world, def);

		bounds = new BoundingBox(position, def.getWidth(), def.getHeight());

		origin = new Vector2f(bounds.getWidth() / 2, bounds.getHeight() / 2);
		scale = new Vector2f(1, 1);
		rotation = 0;

		// TODO: make an animation set in this class
		currentAnimation = def.getAnimationSet().entrySet().iterator().next().getValue();

		tint = new Color(Color.WHITE);
	}

	/**
	 * Gets the bounds.
	 *
	 * @return the bounds
	 */
	public BoundingBox getBounds()
	{
		return bounds;
	}

	/**
	 * Gets the current animation.
	 *
	 * @return the current animation
	 */
	public TextureAnimation getCurrentAnimation()
	{
		return currentAnimation;
	}

	/**
	 * Gets the origin.
	 *
	 * @return the origin
	 */
	public Vector2f getOrigin()
	{
		return origin;
	}

	/**
	 * Gets the scale.
	 *
	 * @return the scale
	 */
	public Vector2f getScale()
	{
		return scale;
	}

	/**
	 * Gets the rotation.
	 *
	 * @return the rotation
	 */
	public float getRotation()
	{
		return rotation;
	}

	/**
	 * Gets the tint.
	 *
	 * @return the tint
	 */
	public Color getTint()
	{
		return tint;
	}

	/**
	 * Gets the position.
	 *
	 * @return the position
	 */
	public Vector2f getPosition()
	{
		return bounds.getPosition();
	}

	/**
	 * Gets the x.
	 *
	 * @return the x
	 */
	public float getX()
	{
		return getPosition().getX();
	}

	/**
	 * Gets the y.
	 *
	 * @return the y
	 */
	public float getY()
	{
		return getPosition().getY();
	}

	/**
	 * Gets the scaled width.
	 *
	 * @return the scaled width
	 */
	public float getScaledWidth()
	{
		return getBounds().getWidth() * getScale().getX();
	}

	/**
	 * Gets the scaled height.
	 *
	 * @return the scaled height
	 */
	public float getScaledHeight()
	{
		return getBounds().getHeight() * getScale().getY();
	}

	/**
	 * Intersects.
	 *
	 * @param other the other
	 * @return true, if successful
	 */
	public boolean intersects(SizedEntity other)
	{
		if (getX() + getScaledWidth() < other.getX() || getX() > other.getX() + other.getScaledWidth() || getY() + getScaledHeight() < other.getY() || getY() > other.getY() + other.getScaledHeight())
			return false;
		else
			return true;
	}

	/**
	 * Dist.
	 *
	 * @param x the x
	 * @param y the y
	 * @return the float
	 */
	public float dist(float x, float y)
	{
		float dx = (getX() - x);
		float dy = (getY() - y);

		return (float) (Math.sqrt(dx * dx + dy * dy));
	}

	/**
	 * Find path corners.
	 *
	 * @param other the other
	 * @return the path node
	 */
	public PathNode findPathCorners(SizedEntity other)
	{
		for (float xx = getX(); xx <= getX() + getScaledWidth(); xx += getScaledWidth())
		{
			for (float yy = getY(); yy <= getY() + getScaledHeight(); yy += getScaledHeight())
			{
				PathNode n = findPath(other);
				if (n != null)
					return n;
			}
		}
		return null;
	}

	/**
	 * Find path.
	 *
	 * @param other the other
	 * @return the path node
	 */
	public PathNode findPath(SizedEntity other)
	{
		return findPath((int) (other.getX() / Tile.SIZE), (int) (other.getY() / Tile.SIZE));
	}

	/**
	 * Find path.
	 *
	 * @param x the x
	 * @param y the y
	 * @return the path node
	 */
	public PathNode findPath(int x, int y)
	{
		Queue<PathNode> searchStack = new LinkedList<PathNode>();
		Set<PathNode> visited = new HashSet<PathNode>();

		PathNode start = new PathNode(x, y, null);
		searchStack.offer(start);
		visited.add(start);

		while (!searchStack.isEmpty())
		{
			PathNode next = searchStack.poll();

			if (next.getX() == (int) ((getX() + getScaledWidth() / 2) / Tile.SIZE) && next.getY() == (int) ((getY() + getScaledHeight() / 2) / Tile.SIZE))
				return next;

			for (int i = -1; i <= 1; i++)
			{
				for (int j = -1; j <= 1; j++)
				{
					// make sure only either i or j is zero, but not both
					if ((i == 0 ^ j == 0))
					{
						PathNode p = new PathNode(next.getX() + i, next.getY() + j, next);

						if (!visited.contains(p) && !getWorld().isPointOnCollidableTile(p.getX() * Tile.SIZE, p.getY() * Tile.SIZE))
						{
							boolean neighbors = false;
							for (int ni = -1; ni <= 1; ni++)
							{
								for (int nj = -1; nj <= 1; nj++)
								{
									int xi = p.getX() + ni, yi = p.getY() + nj;
									if (getWorld().isPointOnCollidableTile(xi * Tile.SIZE, yi * Tile.SIZE))
										neighbors = true;
								}
							}
							if (!neighbors)
							{
								searchStack.offer(p);
								visited.add(p);
							}
						}
					}
				}
			}
		}

		return null;
	}

	/**
	 * Sets the x.
	 *
	 * @param x the new x
	 */
	public void setX(float x)
	{
		getPosition().setX(x);
	}

	/**
	 * Sets the y.
	 *
	 * @param y the new y
	 */
	public void setY(float y)
	{
		getPosition().setY(y);
	}

	/**
	 * Sets the position.
	 *
	 * @param x the x
	 * @param y the y
	 */
	public void setPosition(float x, float y)
	{
		setX(x);
		setY(y);
	}

	/**
	 * Sets the bounds.
	 *
	 * @param bounds the new bounds
	 */
	public void setBounds(BoundingBox bounds)
	{
		this.bounds = bounds;
	}

	/**
	 * Sets the current animation.
	 *
	 * @param currentAnimation the new current animation
	 */
	public void setCurrentAnimation(TextureAnimation currentAnimation)
	{
		this.currentAnimation = currentAnimation;
	}

	/**
	 * Sets the origin.
	 *
	 * @param origin the new origin
	 */
	public void setOrigin(Vector2f origin)
	{
		this.origin = origin;
	}

	/**
	 * Sets the scale.
	 *
	 * @param scale the new scale
	 */
	public void setScale(Vector2f scale)
	{
		this.scale = scale;
	}

	/**
	 * Sets the rotation.
	 *
	 * @param rotation the new rotation
	 */
	public void setRotation(float rotation)
	{
		this.rotation = rotation;
	}

	/**
	 * Sets the tint.
	 *
	 * @param tint the new tint
	 */
	public void setTint(Color tint)
	{
		this.tint = tint;
	}

	/**
	 * Can move.
	 *
	 * @param dx the dx
	 * @param dy the dy
	 * @return true, if successful
	 */
	public boolean canMove(float dx, float dy)
	{
		boolean canMove = true;
		float w = getScaledWidth(), h = getScaledHeight();
		for (float x = getX(); x <= getX() + w; x += (w / 10))
		{
			for (float y = getY(); y <= getY() + h; y += (h / 10))
			{
				float xx = x + dx;
				float yy = y + dy;
				if (getWorld().isPointOnCollidableTile(xx, yy))
					canMove = false;
			}
		}

		return canMove;
	}

	@Override
	public void update(float delta)
	{
		super.update(delta);
		getCurrentAnimation().update();
	}

	/**
	 * Render.
	 *
	 * @param spriteBatch the sprite batch
	 * @param shapeBatch the shape batch
	 */
	public void render(SpriteBatch spriteBatch, ShapeBatch shapeBatch)
	{
		if (!spriteBatch.isDrawing())
			spriteBatch.begin();

		TextureRegion currentFrame = getCurrentAnimation().getCurrentFrame();

		Color prevTint = new Color(spriteBatch.getTintColor());
		spriteBatch.setTintColor(tint);
		spriteBatch.renderTexture(currentFrame, getX(), getY(), currentFrame.getWidth(), currentFrame.getHeight(), origin.getX(), origin.getY(), rotation, scale.getX(), scale.getY());
		spriteBatch.setTintColor(prevTint);
	}

}
