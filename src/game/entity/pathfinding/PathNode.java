package game.entity.pathfinding;

/**
 * The Class PathNode.
 */
public class PathNode
{

	/** The x. */
	private int x;
	
	/** The y. */
	private int y;
	
	/** The next. */
	private PathNode next;

	/**
	 * Instantiates a new path node.
	 *
	 * @param x the x
	 * @param y the y
	 * @param next the next
	 */
	public PathNode(int x, int y, PathNode next)
	{
		this.x = x;
		this.y = y;
		this.next = next;
	}

	/**
	 * Gets the x.
	 *
	 * @return the x
	 */
	public int getX()
	{
		return x;
	}

	/**
	 * Gets the y.
	 *
	 * @return the y
	 */
	public int getY()
	{
		return y;
	}

	/**
	 * Gets the next.
	 *
	 * @return the next
	 */
	public PathNode getNext()
	{
		return next;
	}

	/**
	 * Sets the x.
	 *
	 * @param x the new x
	 */
	public void setX(int x)
	{
		this.x = x;
	}

	/**
	 * Sets the y.
	 *
	 * @param y the new y
	 */
	public void setY(int y)
	{
		this.y = y;
	}

	/**
	 * Sets the next.
	 *
	 * @param next the new next
	 */
	public void setNext(PathNode next)
	{
		this.next = next;
	}

	@Override
	public int hashCode()
	{
		return 997 * ((int) x) ^ 991 * ((int) y);
	}

	@Override
	public boolean equals(Object obj)
	{
		if (!(obj instanceof PathNode))
			return false;
		else
		{
			PathNode node = (PathNode) obj;
			return hashCode() == node.hashCode();
		}
	}

	@Override
	public String toString()
	{
		return "[" + x + ", " + y + "]";
	}

}
