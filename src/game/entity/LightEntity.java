package game.entity;

import engine.event.listener.RenderListener;
import engine.graphics.Color;
import engine.graphics.light.PointLight;
import engine.physics.QuadraticAttenuation;
import engine.util.math.Vector2f;
import game.Game;
import game.world.World;

/**
 * The Class LightEntity.
 */
public class LightEntity extends PointEntity implements RenderListener
{

	/** The vel. */
	private Vector2f vel;
	
	/** The light. */
	private PointLight light;

	/**
	 * Instantiates a new light entity.
	 *
	 * @param world the world
	 * @param lightColor the light color
	 * @param lightIntensity the light intensity
	 * @param x the x
	 * @param y the y
	 */
	public LightEntity(World world, Color lightColor, float lightIntensity, float x, float y)
	{
		super(world, null, x, y);
		vel = new Vector2f(4, 4);

		light = new PointLight(lightColor, lightIntensity, new QuadraticAttenuation(0.00004f, 0, 1), new Vector2f(x, y), 500);
	}

	/**
	 * Gets the vel.
	 *
	 * @return the vel
	 */
	public Vector2f getVel()
	{
		return vel;
	}

	/**
	 * Gets the light.
	 *
	 * @return the light
	 */
	public PointLight getLight()
	{
		return light;
	}

	/**
	 * Sets the vel.
	 *
	 * @param vel the new vel
	 */
	public void setVel(Vector2f vel)
	{
		this.vel = vel;
	}

	@Override
	public void update(float delta)
	{
		if (getWorld().isPointOnCollidableTile(getX() + 2 * vel.getX(), getY()))
			vel.setX(-vel.getX());
		if (getWorld().isPointOnCollidableTile(getX(), getY() + 2 * vel.getY()))
			vel.setY(-vel.getY());

		setX(getX() + vel.getX());
		setY(getY() + vel.getY());

		light.getPosition().set(getX(), getY());
		
		getWorld().centerCameraOnEntity(this);
	}

	@Override
	public void render()
	{
		Game.getInstance().getLightBatch().pushLight(light);
	}

}
