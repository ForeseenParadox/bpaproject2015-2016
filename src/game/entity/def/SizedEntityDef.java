package game.entity.def;

import java.util.HashMap;
import java.util.Map;

import engine.animation.TextureAnimation;
import engine.util.xml.XMLDocument;
import engine.util.xml.XMLElement;

/**
 * The Class SizedEntityDef.
 */
public class SizedEntityDef extends EntityDef
{

	/** The width. */
	private int width;
	
	/** The height. */
	private int height;
	
	/** The animation set. */
	private Map<String, TextureAnimation> animationSet;

	/**
	 * Instantiates a new sized entity def.
	 *
	 * @param doc the doc
	 */
	public SizedEntityDef(XMLDocument doc)
	{
		super(doc);

		width = Integer.parseInt(doc.getRootElement().getChildByName("Width").getValue());
		height = Integer.parseInt(doc.getRootElement().getChildByName("Height").getValue());
		animationSet = new HashMap<String, TextureAnimation>();
		for (XMLElement anim : doc.getRootElement().getChildrenByName("Animation"))
			animationSet.put(anim.getValue(), TextureAnimation.getTextureAnimation(anim.getValue()));
	}

	/**
	 * Gets the width.
	 *
	 * @return the width
	 */
	public int getWidth()
	{
		return width;
	}

	/**
	 * Gets the height.
	 *
	 * @return the height
	 */
	public int getHeight()
	{
		return height;
	}

	/**
	 * Gets the animation set.
	 *
	 * @return the animation set
	 */
	public Map<String, TextureAnimation> getAnimationSet()
	{
		return animationSet;
	}

}
