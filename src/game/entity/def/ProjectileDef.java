package game.entity.def;

import engine.util.xml.XMLDocument;

/**
 * The Class ProjectileDef.
 */
public class ProjectileDef extends SizedEntityDef
{

	/** The speed. */
	private float speed;
	
	/** The total life. */
	private int totalLife;
	
	/** The damage infliction. */
	private int damageInfliction;

	/**
	 * Instantiates a new projectile def.
	 *
	 * @param def the def
	 */
	public ProjectileDef(XMLDocument def)
	{
		super(def);

		speed = Float.parseFloat(def.getRootElement().getChildByName("Speed").getValue());
		totalLife = Integer.parseInt(def.getRootElement().getChildByName("TotalLife").getValue());
		damageInfliction = Integer.parseInt(def.getRootElement().getChildByName("Damage").getValue());
	}

	/**
	 * Gets the speed.
	 *
	 * @return the speed
	 */
	public float getSpeed()
	{
		return speed;
	}

	/**
	 * Gets the total life.
	 *
	 * @return the total life
	 */
	public int getTotalLife()
	{
		return totalLife;
	}

	/**
	 * Gets the damage infliction.
	 *
	 * @return the damage infliction
	 */
	public int getDamageInfliction()
	{
		return damageInfliction;
	}

}
