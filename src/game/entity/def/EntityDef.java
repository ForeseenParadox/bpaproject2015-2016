package game.entity.def;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.script.Bindings;
import javax.script.Compilable;
import javax.script.CompiledScript;
import javax.script.ScriptException;

import engine.core.Engine;
import engine.file.ExternalFileHandle;
import engine.util.xml.XMLDocument;
import engine.util.xml.XMLElement;

/**
 * The Class EntityDef.
 */
public class EntityDef
{

	/** The update event. */
	public static String UPDATE_EVENT = "update";
	
	/** The spawn event. */
	public static String SPAWN_EVENT = "spawn";

	/** The type. */
	private String type;
	
	/** The name. */
	private String name;
	
	/** The events. */
	private Map<String, CompiledScript> events;

	/**
	 * Instantiates a new entity def.
	 *
	 * @param def the def
	 */
	public EntityDef(XMLDocument def)
	{
		events = new HashMap<String, CompiledScript>();
		if (def != null)
			loadXMLDef(def);
	}

	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public String getType()
	{
		return type;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * Gets the events.
	 *
	 * @return the events
	 */
	public Map<String, CompiledScript> getEvents()
	{
		return events;
	}

	/**
	 * Load xml def.
	 *
	 * @param def the def
	 */
	public void loadXMLDef(XMLDocument def)
	{
		XMLElement root = def.getRootElement();
		type = root.getAttribValue("type");
		name = root.getChildByName("Name").getValue();

		List<XMLElement> eventElements = root.getChildrenByName("Event");
		for (XMLElement el : eventElements)
		{
			try
			{
				String id = el.getAttribValue("id");
				String src = el.getAttribValue("src");

				InputStream input = null;
				if (src == null)
				{
					input = new ByteArrayInputStream(el.getValue().getBytes());
				} else
				{
					input = new ExternalFileHandle(src).openInputStream();
				}
				CompiledScript script = ((Compilable) Engine.getInstance().getScriptSystem().getJavaScriptEngine()).compile(new InputStreamReader(input));
				events.put(id, script);
			} catch (ScriptException e)
			{
				Engine.getInstance().crash(e);
			}
		}
	}

	/**
	 * Checks for event.
	 *
	 * @param eventId the event id
	 * @return true, if successful
	 */
	public boolean hasEvent(String eventId)
	{
		if (events.containsKey(eventId))
			return true;
		else
			return false;
	}

	/**
	 * Execute event.
	 *
	 * @param id the id
	 * @param bindings the bindings
	 * @return the object
	 */
	public Object executeEvent(String id, Bindings bindings)
	{
		try
		{
			return events.get(id).eval(bindings);
		} catch (ScriptException e)
		{
			Engine.getInstance().crash(e);
			return null;
		}
	}

}