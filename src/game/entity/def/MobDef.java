package game.entity.def;

import engine.util.xml.XMLDocument;

/**
 * The Class MobDef.
 */
public class MobDef extends SizedEntityDef
{

	/** The max health. */
	private int maxHealth;

	/**
	 * Instantiates a new mob def.
	 *
	 * @param doc the doc
	 */
	public MobDef(XMLDocument doc)
	{
		super(doc);

		maxHealth = Integer.parseInt(doc.getRootElement().getChildByName("MaxHealth").getValue());
	}

	/**
	 * Gets the max health.
	 *
	 * @return the max health
	 */
	public int getMaxHealth()
	{
		return maxHealth;
	}

}
