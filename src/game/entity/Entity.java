package game.entity;

import static org.lwjgl.glfw.GLFW.GLFW_KEY_A;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_D;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_S;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_W;

import java.util.HashMap;
import java.util.Map;

import javax.script.Bindings;
import javax.script.ScriptContext;
import javax.script.SimpleScriptContext;

import engine.core.Engine;
import engine.event.listener.UpdateListener;
import game.entity.def.EntityDef;
import game.world.World;

/**
 * The Class Entity.
 */
public abstract class Entity implements UpdateListener
{

	/** The Constant SLIME. */
	// MOBS
	public static final String SLIME = "Slime";
	
	/** The Constant HOVERING_BOX. */
	public static final String HOVERING_BOX = "HoveringBox";
	
	/** The Constant PLAYER. */
	public static final String PLAYER = "MainPlayer";

	/** The Constant BASIC_PROJECTILE. */
	// PROJECTILES
	public static final String BASIC_PROJECTILE = "BasicProjectile";
	
	/** The Constant DESCTUCTION_PROJECTILE. */
	public static final String DESCTUCTION_PROJECTILE = "DestructionProjectile";
	
	/** The Constant HITMARKER_PROJECTILE. */
	public static final String HITMARKER_PROJECTILE = "HitmarkerProjectile";

	/** The defs. */
	private static Map<String, EntityDef> defs;

	/** The world. */
	private World world;
	
	/** The def. */
	private EntityDef def;
	
	/** The bindings. */
	private Bindings bindings;

	static
	{
		defs = new HashMap<String, EntityDef>();
	}

	/**
	 * Instantiates a new entity.
	 *
	 * @param world the world
	 * @param def the def
	 */
	public Entity(World world, EntityDef def)
	{
		this.world = world;

		this.def = def;

		ScriptContext context = new SimpleScriptContext();
		bindings = context.getBindings(ScriptContext.ENGINE_SCOPE);

		bindings.put("world", world);
		bindings.put("engine", Engine.getInstance());
		bindings.put("current", this);
		bindings.put("GLFW_KEY_A", GLFW_KEY_A);
		bindings.put("GLFW_KEY_W", GLFW_KEY_W);
		bindings.put("GLFW_KEY_S", GLFW_KEY_S);
		bindings.put("GLFW_KEY_D", GLFW_KEY_D);

	}

	/**
	 * Register entity def.
	 *
	 * @param def the def
	 */
	public static void registerEntityDef(EntityDef def)
	{
		defs.put(def.getName(), def);
	}

	/**
	 * Gets the entity def.
	 *
	 * @param type the type
	 * @return the entity def
	 */
	public static EntityDef getEntityDef(String type)
	{
		return defs.get(type);
	}

	/**
	 * Invoke spawn event.
	 */
	public void invokeSpawnEvent()
	{
		if (def != null && def.hasEvent(EntityDef.SPAWN_EVENT))
			def.executeEvent(EntityDef.SPAWN_EVENT, bindings);
	}

	/**
	 * Gets the world.
	 *
	 * @return the world
	 */
	public World getWorld()
	{
		return world;
	}

	/**
	 * Gets the def.
	 *
	 * @return the def
	 */
	public EntityDef getDef()
	{
		return def;
	}

	/**
	 * Sets the world.
	 *
	 * @param world the new world
	 */
	public void setWorld(World world)
	{
		this.world = world;
	}

	/**
	 * Gets the bindings.
	 *
	 * @return the bindings
	 */
	public Bindings getBindings()
	{
		return bindings;
	}

	/**
	 * Sets the bindings.
	 *
	 * @param bindings the new bindings
	 */
	public void setBindings(Bindings bindings)
	{
		this.bindings = bindings;
	}

	@Override
	public void update(float delta)
	{
		if (def.hasEvent(EntityDef.UPDATE_EVENT))
			def.executeEvent(EntityDef.UPDATE_EVENT, getBindings());
	}

}
