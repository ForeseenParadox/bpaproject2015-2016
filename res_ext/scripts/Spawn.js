var loc = world.generateValidTileLocation();
current.setX(loc.getX() * 32);
current.setY(loc.getY() * 32);

current.getTint().set(0xFF000000 | (Math.random() * 0xFFFFFF));
current.getScale().set(2, 2);