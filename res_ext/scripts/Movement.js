var speed = 5.0;
if (current.canMove(-speed, 0) && engine.getInput().getKeyboard().isKeyPressed(GLFW_KEY_A))
{
	current.getBounds().getPosition().setX(current.getBounds().getPosition().getX() - speed);
}
if (current.canMove(speed, 0) && engine.getInput().getKeyboard().isKeyPressed(GLFW_KEY_D))
{
	current.getBounds().getPosition().setX(current.getBounds().getPosition().getX() + speed);
}
if (current.canMove(0, speed) && engine.getInput().getKeyboard().isKeyPressed(GLFW_KEY_W))
{
	current.getBounds().getPosition().setY(current.getBounds().getPosition().getY() + speed);
}
if ( current.canMove(0, -speed) && engine.getInput().getKeyboard().isKeyPressed(GLFW_KEY_S))
{
	current.getBounds().getPosition().setY(current.getBounds().getPosition().getY() - speed);
}

function centerCamera(world, entity)
{
	var translation = world.getTranslation();
	translation.setX(-entity.getBounds().getPosition().getX() - entity.getScaledWidth() / 2 + engine.getWindow().getWidth() / 2);
	translation.setY(-entity.getBounds().getPosition().getY() - entity.getScaledHeight() / 2 + engine.getWindow().getHeight() / 2);
}

centerCamera(world, current);